//
//  NavigationBar+Shadow.swift
//  Qurick
//
//  Created by Ruchika on 21/04/17.
//  Copyright © 2017 Ruchika. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationBar
{
    func setShadow(view: UIView)
    {
        let shadowView =  UIView(frame: (self.frame)) //UIView(frame : CGRect(x: 0, y: 20, width: SCREEN_WIDTH, height: 44))
                                
        shadowView.backgroundColor = UIColor.white
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowColor = UIColor.lightGray.cgColor
        
        shadowView.layer.shadowOffset = CGSize(width:0, height:3)
        shadowView.layer.shadowOpacity = 0.2
        shadowView.layer.shadowRadius = 3.0
        
        view.addSubview(shadowView)
    }
}
