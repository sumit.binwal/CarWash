//
//  UILabel+Spacing.swift
//  CarWash
//
//  Created by Neha Chaudhary on 20/07/17.
//  Copyright © 2017 Neha Chaudhary. All rights reserved.
//

import Foundation
import UIKit

extension UITextField
{
    func addCharactersSpacing(spacing:CGFloat, text:String) {
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSKernAttributeName, value: spacing, range: NSMakeRange(0, text.count))
        self.attributedText = attributedString
    }
}
