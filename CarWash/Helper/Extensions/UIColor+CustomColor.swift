//
//  UIColor+CustomColor.swift
//  Qurick
//
//  Created by Ruchika on 14/04/17.
//  Copyright © 2017 Ruchika. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
   class func uicolorFromRGB(_ red:CGFloat,_ green:CGFloat,_ blue:CGFloat) -> UIColor {
        let color = UIColor.init(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1.0)
        return color
        
    }
}
