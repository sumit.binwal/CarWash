//
//  AlertViewController.swift
//  Qurick
//
//  Created by Ruchika on 17/04/17.
//  Copyright © 2017 Ruchika. All rights reserved.
//

import Foundation
import UIKit

struct AlertViewController {
    
    static func showAlertWith(title: String, message: String, dismissBloack:@escaping () -> Void) {
        
        var alertController: UIAlertController
        
        alertController = UIAlertController.init(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        var alertAction: UIAlertAction
        
        alertAction = UIAlertAction.init(title: NSLocalizedString("OK_TEXT", comment: ""), style: UIAlertActionStyle.default, handler: { (alertAction) in
            
            dismissBloack()

            alertController.dismiss(animated: true, completion: {
                print("dismiss")
            })
            
        })
        
        alertController.addAction(alertAction)
        
        APPDELEGATE.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    static func showAlertWith(title: String, message: String, buttonsArray: [String], dismissBlock:@escaping (_ index: Int) -> Void )  {
        
        var alertController: UIAlertController
        
        alertController = UIAlertController.init(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)

        for i in 0..<buttonsArray.count {
            
            var alertAction: UIAlertAction
            
            alertAction = UIAlertAction.init(title: buttonsArray[i], style: UIAlertActionStyle.default, handler: { (alertAction) in
                
                dismissBlock(i)
                
                alertController.dismiss(animated: true, completion: {
                    print("dismiss")
                })
                
            })
            
            alertController.addAction(alertAction)

        }
        
        APPDELEGATE.window?.rootViewController?.present(alertController, animated: true, completion: nil)

    }
    
}
