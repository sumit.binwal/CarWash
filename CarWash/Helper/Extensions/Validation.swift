
//
//  Validation.swift
//  DemoSwiftLogin
//
//  Created by Ruchika on 12/04/17.
//  Copyright © 2017 Ruchika. All rights reserved.
//

import Foundation

class Validation {
    
    class func validateEmail(email: String?) -> Bool {
        
        let emailRegex: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        let emailTest: NSPredicate =  NSPredicate(format: "SELF MATCHES %@", emailRegex)
        
        let isValid: Bool = emailTest.evaluate(with: email)
        
        return isValid
    }
    
    
    class func validateURl(url: String?) -> Bool
    {
         let urlRegex: String = "(http://|https://)?((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        
        let urlTest: NSPredicate = NSPredicate.init(format: "SELF MATCHES %@", urlRegex)
        
        let isValid: Bool = urlTest.evaluate(with:url)
        
        return isValid
        
    }
    
    class func validatePassword(password: String?) -> Bool {
        
        let passwordRegex: String = "^.{6,}$"
        
        let passwordTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
        
        let isValid: Bool = passwordTest.evaluate(with: password)
        return isValid
    }
    
    class func validatePhoneNo(phoneNo: String?) -> Bool {
        
//        let phoneRegex: String = "^[0-9]{5,15}$"
        let phoneRegex: String = "^[0-9]*$"

        let phoneTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        
        let isValid: Bool = phoneTest.evaluate(with: phoneNo)
        print(isValid)
        return isValid

    }
    
    class func isValueNotEmpty(aString: String?) -> Bool {
        
        var aString = aString
        let whitespace: CharacterSet =  CharacterSet.whitespacesAndNewlines
        
        aString = aString?.trimmingCharacters(in: whitespace)
        
        let isEmpty: Bool? = aString?.isEmpty
        
        guard  let _ = isEmpty, (aString?.count)! > 0 else {
            return false
        }
        
        return true
        
    }
    
    
}
