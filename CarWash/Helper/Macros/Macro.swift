//
//  Macro.swift
//  Qurick
//
//  Created by Ruchika on 14/04/17.
//  Copyright © 2017 Ruchika. All rights reserved.
//

import Foundation
import UIKit

let SCREEN_WIDTH    = UIScreen.main.bounds.size.width
let SCREEN_HEIGTH   = UIScreen.main.bounds.size.height
let USERDEFAULT     = UserDefaults.standard
let IS_IPHONE_6P    = SCREEN_HEIGTH == 736.0

var lattitude = Float();
var longtitude = Float();

var userLattitude = Float();
var userLongtitude = Float();

var addressUser = String();

var isOpenAboutPage    :    Bool = false

var isGuestUser     = Bool()
var device_token    = NSString();
var language        = NSString();

let RESIGN_KEYBOARD =  UIView.animate(withDuration: 0.3, animations: {() -> Void in
    UIApplication.shared.keyWindow?.endEditing(true)
})


let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate


let FONT_BLACK           =       "ProximaNova-Black"
let FONT_REGULAR         =       "ProximaNova-Regular"
let FONT_BOLD            =       "ProximaNova-Bold"
let FONT_LIGHT           =       "ProximaNova-Light"
let FONT_SEMIBOLD        =       "ProximaNova-Semibold"

let FONT_NAVIGATION_BAR  =       "Circular-Std-Medium"

let NO_INTERNET_CONNECTION = "No Internet Connection"
let SERVER_ERROR = "No Server Connection"


/**
 *  KeyBoardType is defining input field keyboard type
 */
enum KeyboardType : Int {
    case keyboardTypeDefault = 0,
    keyboardTypeASCIICapable,
    keyboardTypeNumbersAndPunctuation,
    keyboardTypeURL,
    keyboardTypeNumberPad,
    keyboardTypePhonePad,
    keyboardTypeNamePhonePad,
    keyboardTypeEmailAddress,
    keyboardTypeDecimalPad,
    keyboardTypeTwitter,
    keyboardTypeWebSearch,
    keyboardTypeAlphabet
}

enum WebViewType:Int {
    case terms = 0,
    privacy
}



let EMPTY_EMAIL = "Please enter email"
let EMPTY_PASSWORD = "Please enter password"
let PASSWORD_LENGTH = "Password must be between 6 to 30 charcaters."

let EMPTY_OLD_PASSWORD = "Please enter old password"
let EMPTY_NEW_PASSWORD = "Please enter new password"
let EMPTY_CONFIRMPASSWORD = "Please enter confirm password"
let MATCH_CONFIRMPASSWORD = "New password and Confirm password did not matched."

let EMPTY_NAME = "Please enter name"
let INVALID_EMAIL = "Please enter valid email address"
let EMPTY_VERIFICATION = "Please enter verification code."
let EMPTY_PHONE = "Please enter mobile number."


let EMPTY_LANGUAGE = "Please select language."
let EMPTY_SUBJECT = "Please enter subject."
let EMPTY_MESSAGE = "Please enter message."


/////////////////////////////


let EMPTY_TITLE_EN  = "Please enter title in english."
let EMPTY_TITLE_AR  = "Please enter title in arabic."
let EMPTY_NUMBER    = "Please enter number of cars."
let EMPTY_LOGO      = "Please select service logo."
let EMPTY_COVER     = "Please select cover photo."
let EMPTY_HOURS     = "Please manage operating hours."

let EMPTY_CARS          = "Please  select car type."
let EMPTY_DESC_EN       = "Please enter description in english."
let EMPTY_DESC_AR       = "Please enter description in arabic."
let EMPTY_DURATION      = "Please enter duration."
let EMPTY_PRICE         = "Please enter price."
let EMPTY_DOC           = "Please upload atleast one document."


////////////////////////////////

let ADD_SERVICE  = "Please add sub service first."
let ADD_SUB_SERVICE  = "Please add additional sub services."

let DIFFERENT_PRICE         = "Both prices are not same."
let DIFFERENT_DURATION         = "Both duration are not same."
