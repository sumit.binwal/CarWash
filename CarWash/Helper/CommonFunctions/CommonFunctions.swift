//
//  CommonFunctions.swift
//  DemoSwiftLogin
//


import Foundation
import SystemConfiguration
import  UIKit
import LocalAuthentication
import Firebase

class CommonFunctions : NSObject
{
    class func setUserDefault(key : String, value : Any) {
        
        let userDefault : UserDefaults = UserDefaults.standard
        userDefault.removeObject(forKey: key)
        
        userDefault.set(value, forKey: key)
        
        userDefault.synchronize()
    }

    class func getUserDefaultValue(key : String) -> Any? {
        
        let userDefault : UserDefaults = UserDefaults.standard
        
        guard let value = userDefault.value(forKey: key) else{
            return nil
        }
        
        return value
    }
    
    class func removeUserDefaultValue(key:String) {
        
        let userDefault : UserDefaults = UserDefaults.standard
        
        userDefault.removeObject(forKey: key)
    }

    class func callNumber(phoneNumber:String)
    {
        print("phoneNumber = \(phoneNumber)")
        
        //let formatedNumber = phoneNumber.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
        
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)")
        {
            let application:UIApplication = UIApplication.shared
            
            if (application.canOpenURL(phoneCallURL))
            {
                if #available(iOS 10.0, *)
                {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                }
                else
                {
                    // Fallback on earlier versions
                    print("other versions")
                    application.openURL(phoneCallURL)
                }
            }
            else
            {
                print("can not open url")
            }
        }
        else
        {
            print("not valid url")
        }
    }
  
    /// Selector used to get array of model properties
    ///
    /// - parameter modelClass: model whose properties to be returned in array
    ///
    /// - returns: returns array of properties
    static func getModelPropertiesToArray(_ modelClass : AnyClass) -> NSArray {
        
        var count = UInt32()
        let properties : UnsafeMutablePointer = class_copyPropertyList(modelClass, &count)
        var propertyNames = [String]()
        let intCount = Int(count)
        for i in 0 ..< intCount {
            let property : objc_property_t = properties[i]!
            guard let propertyName = NSString(utf8String: property_getName(property)) as String? else {
                debugPrint("Couldn't unwrap property name for \(property)")
                break
            }
            propertyNames.append(propertyName)
        }
        free(properties)
//        print(propertyNames)
        return propertyNames as NSArray;
    }

    
    
   class func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
  
    class func getCallingCodeString() -> String
    {
        //let countryCodeDictionary : NSDictionary = ["AF":"93", "AL":"355", "DZ":"213","AS":"1", "AD":"376", "AO":"244", "AI":"1", "AG":"1", "AR":"54", "AM":"374", "AW":"297", "AU":"61", "AT":"43", "AZ":"994", "BS":"1", "BH":"973", "BD":"880", "BB":"1", "BY":"375", "BE":"32", "BZ":"501", "BJ":"229", "BM":"1", "BT":"975", "BA":"387", "BW":"267", "BR":"55", "IO":"246", "BG":"359", "BF":"226", "BI":"257", "KH":"855", "CM":"237", "CA":"1", "CV":"238", "KY":"345", "CF":"236", "TD":"235", "CL":"56", "CN":"86", "CX":"61", "CO":"57", "KM":"269", "CG":"242", "CK":"682", "CR":"506", "HR":"385", "CU":"53", "CY":"537", "CZ":"420", "DK":"45", "DJ":"253", "DM":"1", "DO":"1", "EC":"593", "EG":"20", "SV":"503", "GQ":"240", "ER":"291", "EE":"372", "ET":"251", "FO":"298", "FJ":"679", "FI":"358", "FR":"33", "GF":"594", "PF":"689", "GA":"241", "GM":"220", "GE":"995", "DE":"49", "GH":"233", "GI":"350", "GR":"30", "GL":"299", "GD":"1", "GP":"590", "GU":"1", "GT":"502", "GN":"224", "GW":"245", "GY":"595", "HT":"509", "HN":"504", "HU":"36", "IS":"354", "IN":"91", "ID":"62", "IQ":"964", "IE":"353", "IL":"972", "IT":"39", "JM":"1", "JP":"81", "JO":"962", "KZ":"77", "KE":"254", "KI":"686", "KW":"965", "KG":"996", "LV":"371", "LB":"961","LS":"266","LR":"231","LI":"423","LT":"370","LU":"352","MG":"261","MW":"265","MY":"60", "MV":"960", "ML":"223", "MT":"356", "MH":"692", "MQ":"596", "MR":"222", "MU":"230", "YT":"262", "MX":"52", "MC":"377", "MN":"976", "ME":"382", "MS":"1", "MA":"212", "MM":"95", "NA":"264", "NR":"674", "NP":"977", "NL":"31", "AN":"599", "NC":"687", "NZ":"64", "NI":"505", "NE":"227", "NG":"234", "NU":"683", "NF":"672", "MP":"1", "NO":"47", "OM":"968", "PK":"92", "PW":"680", "PA":"507", "PG":"675", "PY":"595", "PE":"51", "PH":"63", "PL":"48", "PT":"351", "PR":"1", "QA":"974", "RO":"40", "RW":"250", "WS":"685", "SM":"378", "SA":"966", "SN":"221", "RS":"381", "SC":"248", "SL":"232", "SG":"65", "SK":"421", "SI":"386", "SB":"677", "ZA":"27", "GS":"500", "ES":"34", "LK":"94", "SD":"249", "SR":"597", "SZ":"268", "SE":"46", "CH":"41", "TJ":"992", "TH":"66", "TG":"228", "TK":"690", "TO":"676", "TT":"1", "TN":"216", "TR":"90", "TM":"993", "TC":"1", "TV":"688", "UG":"256", "UA":"380", "AE":"971", "GB":"44", "US":"1", "UY":"598", "UZ":"998", "VU":"678", "WF":"681", "YE":"967", "ZM":"260", "ZW":"263", "BO":"591", "BN":"673", "CC":"61", "CD":"243", "CI":"225", "FK":"500", "GG":"44", "VA":"379", "HK":"852", "IR":"98", "IM":"44", "JE":"44", "KP":"850", "KR":"82", "LA":"856", "LY":"218", "MO":"853", "MK":"389", "FM":"691", "MD":"373", "MZ":"258", "PS":"970", "PN":"872", "RE":"262", "RU":"7", "BL":"590", "SH":"290", "KN":"1", "LC":"1", "MF":"590", "PM":"508", "VC":"1", "ST":"239", "SO":"252", "SJ":"47", "SY":"963", "TW":"886", "TZ":"255", "TL":"670", "VE":"58", "VN":"84", "VG":"284", "VI":"340"]
        //
        //let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String
        //
        //return countryCodeDictionary[countryCode ?? ""] as! String
        
        return "966"
    }
    
    class func composeMessage(type: MessageType, content: Any, device_token : String, fcm_user_id : String)  {
        
        let message = Message.init(type: type, content: content, owner: .sender, timestamp: Int(Date().timeIntervalSince1970), isRead: false)
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dictUser = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        
        let fcm_id = dictUser.object(forKey: "fcm_user_id") as! String
        
        let uid =  fcm_user_id
        let fcmID = dictUser["login_token"] as! String
        let senderName = dictUser["name"] as! String
        
        let senderInfoDict = ["fcm_user_id" : fcm_id, "device_id" : fcmID, "name": senderName] as NSDictionary
        
        //let myDID = fcmID
        
        Message.send(message: message, toID: uid, completion: {(state) in
            
            //Send Push Notification if user is Offline
            if state == true
            {
                User.checkUserStatus(firebaseID: uid, completion: { (status) in
                    
                    if status == "offline"
                    {
                        let pushDict = ["title" : "Test Messagae", "message" : message.content, "userData":senderInfoDict] as NSDictionary
                        
                        print("pushDict = \(pushDict)")
                        
                        User.getDeviceToken(forUserID: fcm_user_id, completion: { (device_token_string) in
                            
                            print("device_token_string = \(device_token_string)")
                            
                            if device_token_string != ""
                            {
                                APPDELEGATE.sendPush(toDevice: device_token_string, withNotifyData: pushDict as! [AnyHashable : Any], withTitle: senderName)
                            }
                        })
                    }
                })
            }
        })
    }
    
    class func timeAgoSinceDate(_ date:Date, currentDate:Date, numericDates:Bool) -> String {
        
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
    }
    
    class func timeStringAgoSinceDate(_ date:Date, currentDate:Date, numericDates:Bool) -> String {
        
        //print("current date = \(currentDate)")
        
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        //print("components.day = \(components.day ?? 0)")
        //print("components.day = \(components.year ?? 0)")
        
        if (components.year! >= 2) {
            return "Other"
        } else if (components.year! >= 1){
            if (numericDates){
                return "Other"
            } else {
                return "Other"
            }
        } else if (components.month! >= 2) {
            return "Other"
        } else if (components.month! >= 1){
            if (numericDates){
                return "Other"
            } else {
                return "Other"
            }
        } else if (components.weekOfYear! >= 2) {
            return "Other"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "Other"
            } else {
                return "Other"
            }
        } else if (components.day! >= 2) {
            return "Other"
        } else if (components.day! >= 1){
            if (numericDates){
                return "Yesterday"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "Today"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "Today"
            } else {
                return "Today"
            }
        } else if (components.minute! >= 2) {
            return "Today"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "Today"
            } else {
                return "Today"
            }
        } else if (components.second! >= 3) {
            return "Today"
        } else {
            return "Today"
        }
    }
    
    class func imageBorderColor() -> UIColor {
        return UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1.0)
    }
    
    class func getDocumentsDirectoryPath() -> URL {
        return try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    }
    
    class func getDocumentsFolder(withName folderName: String) -> URL? {
        let documentsPath = CommonFunctions.getDocumentsDirectoryPath()
        //print(documentsPath)
        
        let newDirectoryPath = documentsPath.appendingPathComponent(folderName) //documentsPath.appending("/"+folderName)
        
        let fileManager = FileManager.default
        
        if !fileManager.fileExists(atPath: newDirectoryPath.path) {
            do {
                try fileManager.createDirectory(atPath: newDirectoryPath.path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                return nil
            }
        }
        return newDirectoryPath
    }
    
    class func addFileToFolder(_ folder: String, fileName: String, fileData: Data) -> Bool {
        guard let folderDirectory = CommonFunctions.getDocumentsFolder(withName: folder) else {
            return false
        }
        
        let fileManager = FileManager.default
        
        let newFilePath = folderDirectory.appendingPathComponent(fileName) //folderDirectory.appending("/"+fileName)
        
        let isSaved = fileManager.createFile(atPath: newFilePath.path, contents: fileData, attributes: nil)
        
        return isSaved
    }
    
    class func getFileURLFromFolder(_ folder: String, fileName: String) -> URL? {
        guard let folderDirectory = CommonFunctions.getDocumentsFolder(withName: folder) else {
            return nil
        }
        
        let fileManager = FileManager.default
        
        let newFilePath = folderDirectory.appendingPathComponent(fileName)
        if fileManager.fileExists(atPath: newFilePath.path) {
            return newFilePath
        }
        return nil
    }
    
    //MARK:- get timeStamp from current data
    class func getCurrentTimeStamp () -> String
    {
        // Get the Unix timestamp
        let timestamp = Date().timeIntervalSince1970
        //print(timestamp)
        
        return String(Int(timestamp))
    }
    
    class func callStatusCodeLogout() {
        
        USERDEFAULT.removeObject(forKey: "USER_DATA")
        USERDEFAULT.synchronize()
        
        let vc = SignInVC() //change this to your class name
        
        let navController = UINavigationController(rootViewController: vc)
        
        APPDELEGATE.window?.rootViewController = navController
    }
    
    //MARK: User ID ( GET )
    class func getUserIdData() -> String?
    {
        guard let data = USERDEFAULT.object(forKey: "USER_DATA") as? Data else
        {
            return ""
        }
        
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        let strSid = userDict["id"] as! String
        
        return strSid
    }
    
    //MARK:- Get Comma Seprated String From Array
    
    class func getCommaSepratedStringFromArrayDict(completeArray:[String]) -> String {
        
        var nameArr: [String] = []
        
        for name in completeArray
        {
            nameArr.append(name)
        }
        
        let commaSeparatedNameString = nameArr.joined(separator: "###")
        
        return commaSeparatedNameString
    }
    
    //MARK:- Get Comma Seprated String From Array
    
    class func getCommaSepratedStringFromArray(completeArray:[String]) -> String {
        
        var nameArr: [String] = []
        
        for name in completeArray
        {
            nameArr.append(name)
        }
        
        let commaSeparatedNameString = nameArr.joined(separator: ",")
        
        return commaSeparatedNameString
    }
    
    class func getLocalizedString (localizedName name : String) -> String
    {
        return NSLocalizedString(name, comment: "")
    }
    
    class func saveNotificatioOnOff (value isOnOrOff : Bool)
    {
        USERDEFAULT.set(isOnOrOff, forKey: "notification_on_off")
        USERDEFAULT.synchronize()
    }
    
    class func getNotificatioOnOff() -> Bool
    {
        return USERDEFAULT.value(forKey: "notification_on_off") as! Bool
    }
    
    //MARK:- Alert View
    class func showAlertWithTitle(title:String? = "", message:String? = "", onViewController:UIViewController?, withButtonArray buttonArray:[String]? = [], dismissHandler:((_ buttonIndex:Int)->())?) -> Void
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        var ignoreButtonArray = false
        
        if buttonArray == nil
        {
            ignoreButtonArray = true
        }
        
        if !ignoreButtonArray
        {
            for item in buttonArray!
            {
                let action = UIAlertAction(title: item, style: .default, handler: { (action) in
                    
                    alertController.dismiss(animated: true, completion: nil)
                    
                    guard (dismissHandler != nil) else
                    {
                        return
                    }
                    
                    dismissHandler!(buttonArray!.index(of: item)!)
                })
                
                alertController.addAction(action)
            }
        }
        
        let action = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: .cancel, handler: { (action) in
            
            guard (dismissHandler != nil) else
            {
                return
            }
            
            dismissHandler!(LONG_MAX)
        })
        
        alertController.addAction(action)
        
        onViewController?.present(alertController, animated: true, completion: nil)
    }
    
    class func setTextFieldBorderColor(_ textfield : UITextField)
    {
        textfield.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0).cgColor
        textfield.layer.borderWidth = 1.0
        textfield.layer.cornerRadius = 5.0
        textfield.layer.masksToBounds = true
    }
    
    class func setTextViewBorderColor(_ textview : UITextView)
    {
        textview.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0).cgColor
        textview.layer.borderWidth = 1.0
        textview.layer.cornerRadius = 5.0
        textview.layer.masksToBounds = true
    }
    
    class func setUIButtonBorderColor(_ button : UIButton)
    {
        button.layer.borderColor = UIColor.clear.cgColor
        button.layer.borderWidth = 1.0
        button.layer.cornerRadius = 5.0
        button.layer.masksToBounds = true
    }
}
