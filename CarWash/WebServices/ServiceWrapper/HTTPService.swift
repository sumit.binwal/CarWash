import UIKit
import Alamofire
import Foundation
import MBProgressHUD

// For Client Side
//let BASE_URL  = "http://202.157.76.19:1337/"

// For staging
//let BASE_URL = "http://202.157.76.19:1339/"

// For Development
let BASE_URL = "http://202.157.76.19:1338/"

// website
//let BASE_URL  = "http://192.168.0.233:1337/"
        
enum kHttpMethodType : NSInteger {
    case kHttpMethodTypeGet = 0,    // GET
    kHttpMethodTypePost     = 1,    // POST
    kHttpMethodTypeDelete   = 2,    // DELETE
    kHttpMethodTypePut      = 3     // PUT
}

class HTTPService: NSObject {
    
    /**
     *   Alamofire Client Manager
     */
    let alamoFireManager : SessionManager?
    
    /**
     *  Base URL for all http communication
     */
    var httpBaseURL : NSString
    
    //MARK: - Superclass methods
    override init() {
        httpBaseURL = BASE_URL as NSString
        alamoFireManager = Alamofire.SessionManager.default
    }
    
    //MARK: - Instance Methods
    /**
     Call this to create a request with any HTTP method
     
     - parameter httpMethodType: HTTP method type post,get etc
     - parameter headers:        HTTP header key-value pair (no need to Content-Type,Accept)
     - parameter serviceName:    name of service which need to call
     - parameter paramDic:       parameters in key-value pair
     - parameter success:        success callback handler
     - parameter failure:        failure callback handler
     */
    func startRequestWithHttpMethod(_ httpMethodType : kHttpMethodType, headers : NSMutableDictionary?, serviceName : NSString, paramDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary?) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void))
    {
        let urlString = BASE_URL + (serviceName as String)
        let url : URL = URL.init(string: urlString)!
        var request = URLRequest(url: url)
        let header : NSMutableDictionary = [:]
        
        switch (httpMethodType) {
        case .kHttpMethodTypeGet:
            request.httpMethod = "GET"
            break
        case .kHttpMethodTypePost:
            request.httpMethod = "POST"
            break
        case .kHttpMethodTypePut:
            request.httpMethod = "PUT"
            break
        case .kHttpMethodTypeDelete:
            request.httpMethod = "DELETE"
            break
        }
        
        header.setValue("1.0", forKey: "app-version")
        header.setValue("ios", forKey: "os")
        header.setValue(language, forKey: "language")
        
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let allHeaders : NSArray = header.allKeys as NSArray
        for key in allHeaders
        {
            request.setValue(header.object(forKey: key)! as? String, forHTTPHeaderField: key as! String)
        }
     
        print("Full HTTP URL -> \(urlString)")
        print("HTTP Request Method -> \(String(describing: request.httpMethod))")
        print("Full HTTP Headers -> \(String(describing: header))")
        print("Full HTTP Parameters -> \(String(describing: paramDic))")

        if paramDic != nil{
            request.httpBody = try! JSONSerialization.data(withJSONObject: paramDic!, options: [])
        }
        
        print(request.httpBody ?? "")
        
        alamoFireManager!.request(request as URLRequestConvertible).responseJSON {
            response in
            switch response.result {
            case .failure(let error):
                print("\(urlString) failure response -> \n NSHTTPURLResponse ->\(String(describing: response.response)) \n \(error)")
                
                if response.response?.statusCode == 440
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                   
                    return
                }
                
                if response.response?.statusCode == 401
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    DispatchQueue.main.async
                        {
                            let alertController = UIAlertController(title: "", message: NSLocalizedString("You are logged in in other device.", comment: ""), preferredStyle: .alert)
                            
                            let action2 = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: .default) { (action:UIAlertAction) in
                                //self.dismiss(animated: true, completion: nil)
                                
                                USERDEFAULT.removeObject(forKey: "USER_DATA")
                                USERDEFAULT.synchronize()
                                
                                let vc = SignInVC() //change this to your class name
                                
                                let navController = UINavigationController(rootViewController: vc)
                                
                                APPDELEGATE.window?.rootViewController = navController
                            }
                            
                            alertController.addAction(action2)
                            
                            APPDELEGATE.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                    }
                    
                    return
                }
                
                failure(response.response, error as NSError?)
            case .success :
                print("\(urlString) success response -> \n NSHTTPURLResponse ->\(String(describing: response.response)) \n Data -> \(String(describing: response.result.value as? NSDictionary))")
                
                if response.response?.statusCode == 440
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    return
                }
                
                if response.response?.statusCode == 401
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    DispatchQueue.main.async
                        {
                            let alertController = UIAlertController(title: "", message: NSLocalizedString("You are logged in in other device.", comment: ""), preferredStyle: .alert)
                            
                            let action2 = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: .default) { (action:UIAlertAction) in
                                //self.dismiss(animated: true, completion: nil)
                                
                                USERDEFAULT.removeObject(forKey: "USER_DATA")
                                USERDEFAULT.synchronize()
                                
                                let vc = SignInVC() //change this to your class name
                                
                                let navController = UINavigationController(rootViewController: vc)
                                
                                APPDELEGATE.window?.rootViewController = navController
                            }
                            
                            alertController.addAction(action2)
                            
                            APPDELEGATE.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                    }
                    
                    return
                }
                
                success(response.response, response.result.value as? NSDictionary)
            }
        }
    }
    
    func startRequestWithHttpMethodWithAuthorization(_ httpMethodType : kHttpMethodType, headers : NSMutableDictionary?, serviceName : NSString, paramDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary?) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void))
    {
        let urlString = BASE_URL + (serviceName as String)
        print("url = ", urlString)
        
        let url : URL = URL.init(string: urlString)!
        var request = URLRequest(url: url)
        let header : NSMutableDictionary = [:]
        
        switch (httpMethodType) {
            
        case .kHttpMethodTypeGet:
            request.httpMethod = "GET"
            break
        
        case .kHttpMethodTypePost:
            request.httpMethod = "POST"
            break
        
        case .kHttpMethodTypePut:
            request.httpMethod = "PUT"
            break
        
        case .kHttpMethodTypeDelete:
            request.httpMethod = "DELETE"
            break
        }
        
        header.setValue("1.0", forKey: "app-version")
        header.setValue("ios", forKey: "os")
        header.setValue(language, forKey: "language")
        
        if((USERDEFAULT.object(forKey: "USER_DATA") != nil))
        {
            let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
            let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
            header.setValue(dict.object(forKey: "login_token"), forKey: "Authorization")
        }
        
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let allHeaders : NSArray = header.allKeys as NSArray
        for key in allHeaders
        {
            request.setValue(header.object(forKey: key)! as? String, forHTTPHeaderField: key as! String)
        }
        
        print("Full HTTP URL -> \(urlString)")
        print("HTTP Request Method -> \(String(describing: request.httpMethod))")
        print("Full HTTP Headers -> \(String(describing: header))")
        print("Full HTTP Parameters -> \(String(describing: paramDic))")
        
        if paramDic != nil{
            request.httpBody = try! JSONSerialization.data(withJSONObject: paramDic!, options: [])
        }
        
        print(request.httpBody ?? "")
        
        alamoFireManager!.request(request as URLRequestConvertible).responseJSON {
            response in
            switch response.result {
            case .failure(let error):
                
                print("\(urlString) failure response -> \n NSHTTPURLResponse ->\(String(describing: response.response)) \n \(error)")
                
                if response.response?.statusCode == 440
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    return
                }
                
                if response.response?.statusCode == 401
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    DispatchQueue.main.async
                        {
                            let alertController = UIAlertController(title: "", message: NSLocalizedString("You are logged in in other device.", comment: ""), preferredStyle: .alert)
                            
                            let action2 = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: .default) { (action:UIAlertAction) in
                                //self.dismiss(animated: true, completion: nil)
                                
                                USERDEFAULT.removeObject(forKey: "USER_DATA")
                                USERDEFAULT.synchronize()
                                
                                let vc = SignInVC() //change this to your class name
                                
                                let navController = UINavigationController(rootViewController: vc)
                                
                                APPDELEGATE.window?.rootViewController = navController
                            }
                            
                            alertController.addAction(action2)
                            
                            APPDELEGATE.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                    }
                    
                    return
                }
                
                failure(response.response, error as NSError?)
            case .success :
                print("\(urlString) success response -> \n NSHTTPURLResponse ->\(String(describing: response.response)) \n Data -> \(String(describing: response.result.value as? NSDictionary))")
                
                if response.response?.statusCode == 440
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    return
                }
                
                if response.response?.statusCode == 401
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    DispatchQueue.main.async
                        {
                            let alertController = UIAlertController(title: "", message: NSLocalizedString("You are logged in in other device.", comment: ""), preferredStyle: .alert)
                            
                            let action2 = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: .default) { (action:UIAlertAction) in
                                //self.dismiss(animated: true, completion: nil)
                                
                                USERDEFAULT.removeObject(forKey: "USER_DATA")
                                USERDEFAULT.synchronize()
                                
                                let vc = SignInVC() //change this to your class name
                                
                                let navController = UINavigationController(rootViewController: vc)
                                
                                APPDELEGATE.window?.rootViewController = navController
                            }
                            
                            alertController.addAction(action2)
                            
                            APPDELEGATE.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                    }
                    
                    return
                }
                success(response.response, response.result.value as? NSDictionary)
            }
        }
    }
    
    /**
     Call this to create a request with any HTTP method
     
     - parameter httpMethodType: HTTP method type post,get etc
     - parameter headers:        HTTP header key-value pair (no need to Content-Type,Accept)
     - parameter serviceName:    name of service which need to call
     - parameter paramDic:       parameters in key-value pair
     - parameter files:          array of NSData objects for file content
     - parameter success:        success callback handler
     - parameter failure:        failure callback handler
     */
    func uploadFileRequestWithParameters(_ paramDic : NSMutableDictionary?, files: NSMutableArray?, isImage: Bool?, headers : NSMutableDictionary?, serviceName : NSString, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary?) -> Void), failure: @escaping ((_ error : Error? ) -> Void)) {
        
        let urlString = BASE_URL + (serviceName as String)
        let url : URL = URL.init(string: urlString)!
        
        let header : NSMutableDictionary = [:]
        
        header.setValue("1.0", forKey: "app-version")
        header.setValue("ios", forKey: "os")
        header.setValue(language, forKey: "language")
        header["Content-Type"] = "multipart/form-data"
        
        // Begin upload
        
        alamoFireManager?.upload(multipartFormData:
            { (multipartFormData) in
            
                multipartFormData.append((paramDic?.object(forKey: "name") as! String).data(using: String.Encoding.utf8)!, withName: "name")
                multipartFormData.append((paramDic?.object(forKey: "email") as! String).data(using: String.Encoding.utf8)!, withName: "email")
                multipartFormData.append((paramDic?.object(forKey: "password") as! String).data(using: String.Encoding.utf8)!, withName: "password")
                multipartFormData.append((paramDic?.object(forKey: "fcm_user_id") as! String).data(using: String.Encoding.utf8)!, withName: "fcm_user_id")
                multipartFormData.append((paramDic?.object(forKey: "device_token") as! String).data(using: String.Encoding.utf8)!, withName: "device_token")
                multipartFormData.append((paramDic?.object(forKey: "country_code") as! String).data(using: String.Encoding.utf8)!, withName: "country_code")
                multipartFormData.append((paramDic?.object(forKey: "mobile") as! String).data(using: String.Encoding.utf8)!, withName: "mobile")
                multipartFormData.append((paramDic?.object(forKey: "iso_code") as! String).data(using: String.Encoding.utf8)!, withName: "iso_code")
                multipartFormData.append((paramDic?.object(forKey: "avatar") as! String).data(using: String.Encoding.utf8)!, withName: "avatar")

                if(isImage!)
                {
                    // import image to request
                    for (file)  in files!
                    {
                        var mimeTypeStr = ""
                        let fileDic : NSDictionary = file as! NSDictionary
                    
                        if (fileDic.object(forKey: "fileType") as? String == "image" )
                        {
                            mimeTypeStr = "image/jpeg";
                        }
                        else if (fileDic.object(forKey: "fileType") as? String == "video" )
                        {
                            mimeTypeStr = "video/mp4";
                        }
                        else if (fileDic.object(forKey: "fileType") as? String == "gif" )
                        {
                            mimeTypeStr = "image/gif";
                        }
                        
                        multipartFormData.append(fileDic.object(forKey: "avatar") as! Data, withName: fileDic.object(forKey: "name") as! String, fileName: fileDic.object(forKey: "fileName") as! String, mimeType: mimeTypeStr)
                    }
                }
            
            print("multipartFormData = ", multipartFormData)
            
        },
        usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: url, method: .post, headers: header as? [String : String], encodingCompletion: {
            encodingResult in
                                    
            switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON(completionHandler: { response in
                        debugPrint(response)
                        success(response.response, response.result.value as? NSDictionary)
                    })
                                        
                case .failure(let error):
                    print(error)
                    failure(error)
            }
        })
    }
    
    // MARK: Logged In user Api's
    
    func startLoggedInUserRequestWithHttpMethod(_ httpMethodType : kHttpMethodType, headers : NSMutableDictionary?, serviceName : NSString, paramDic : NSMutableDictionary?, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary?) -> Void), failure: @escaping (( _ response : HTTPURLResponse?, _ error : NSError? ) -> Void)) {
        
        let urlString = BASE_URL + (serviceName as String)
        print("urlString =", urlString)
         
        let url : URL = URL.init(string: urlString)!
        var request = URLRequest(url: url)
        
        let header : NSMutableDictionary = [:]
        
        switch (httpMethodType) {
        case .kHttpMethodTypeGet:
            request.httpMethod = "GET"
            break
        case .kHttpMethodTypePost:
            request.httpMethod = "POST"
            break
        case .kHttpMethodTypePut:
            request.httpMethod = "PUT"
            break
        case .kHttpMethodTypeDelete:
            request.httpMethod = "DELETE"
            break
        }
        
        header.setValue("1.0", forKey: "app-version")
        header.setValue("ios", forKey: "os")
        header.setValue(language, forKey: "language")
        
        // DEV
        if((USERDEFAULT.object(forKey: "USER_DATA") != nil)){
            let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
            let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
            header.setValue(dict.object(forKey: "login_token"), forKey: "Authorization")
        }
        
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let allHeaders : NSArray = header.allKeys as NSArray
        for key in allHeaders
        {
            request.setValue(header.object(forKey: key)! as? String, forHTTPHeaderField: key as! String)
        }
        
        print("Full HTTP URL -> \(urlString)")
        print("HTTP Request Method -> \(String(describing: request.httpMethod))")
        print("Full HTTP Headers -> \(String(describing: header))")
        print("Full HTTP Parameters -> \(String(describing: paramDic))")
        
        if paramDic != nil {
            request.httpBody = try! JSONSerialization.data(withJSONObject: paramDic!, options: [])
        }
        
        alamoFireManager!.request(request as URLRequestConvertible).responseJSON
            {
            response in
            switch response.result {
            case .failure(let error):
                
                print("\(urlString) failure response -> \n NSHTTPURLResponse ->\(String(describing: response.response)) \n \(error)")
                
                if response.response?.statusCode == 440
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    return
                }
                
                if response.response?.statusCode == 401
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    DispatchQueue.main.async
                        {
                            let alertController = UIAlertController(title: "", message: NSLocalizedString("You are logged in in other device.", comment: ""), preferredStyle: .alert)
                            
                            let action2 = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: .default) { (action:UIAlertAction) in
                                //self.dismiss(animated: true, completion: nil)
                                
                                USERDEFAULT.removeObject(forKey: "USER_DATA")
                                USERDEFAULT.synchronize()
                                
                                let vc = SignInVC() //change this to your class name
                                
                                let navController = UINavigationController(rootViewController: vc)
                                
                                APPDELEGATE.window?.rootViewController = navController
                            }
                            
                            alertController.addAction(action2)
                            
                            APPDELEGATE.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                    }
                    
                    return
                }
                
                failure(response.response, error as NSError?)
                
            case .success :
                
                print("\(urlString) success response -> \n NSHTTPURLResponse ->\(String(describing: response.response)) \n Data -> \(String(describing: response.result.value as? NSDictionary))")
                
                if response.response?.statusCode == 440
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    return
                }
                
                if response.response?.statusCode == 401
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    DispatchQueue.main.async
                        {
                            let alertController = UIAlertController(title: "", message: NSLocalizedString("You are logged in in other device.", comment: ""), preferredStyle: .alert)
                            
                            let action2 = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: .default) { (action:UIAlertAction) in
                                //self.dismiss(animated: true, completion: nil)
                                
                                USERDEFAULT.removeObject(forKey: "USER_DATA")
                                USERDEFAULT.synchronize()
                                
                                let vc = SignInVC() //change this to your class name
                                
                                let navController = UINavigationController(rootViewController: vc)
                                
                                APPDELEGATE.window?.rootViewController = navController
                            }
                            
                            alertController.addAction(action2)
                            
                            APPDELEGATE.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                    }
                    
                    return
                }
                
                success(response.response, response.result.value as? NSDictionary)
            }
        }
    }

    func uploadLoggedInFileRequestWithParameters(_ paramDic : NSMutableDictionary?, files: NSMutableArray?, isImage: Bool?, headers : NSMutableDictionary?, serviceName : NSString, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary?) -> Void), failure: @escaping ((_ error : Error? ) -> Void))
    {
        let urlString = BASE_URL + (serviceName as String)
        let url : URL = URL.init(string: urlString)!
        // var headerDic = [String : String]()
        let header : NSMutableDictionary = [:]
        
        header.setValue("1.0", forKey: "app-version")
        header.setValue("ios", forKey: "os")
        header.setValue(language, forKey: "language")
        header["Content-Type"] = "multipart/form-data"
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        header.setValue(dict.object(forKey: "login_token"), forKey: "Authorization")
        
        // Begin upload
        
        alamoFireManager?.upload(multipartFormData:
            { (multipartFormData) in
                
                if(paramDic != nil)
                {
                    for (key, value) in paramDic!
                    {
                        multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                    }
                }
                
                
                if(isImage!)
                {
                    // import image to request
                    for (file)  in files!
                    {
                        var mimeTypeStr = ""
                        let fileDic : NSDictionary = file as! NSDictionary
                        if (fileDic.object(forKey: "fileType") as? String == "image" )
                        {
                            mimeTypeStr = "image/jpeg";
                        }
                        else if (fileDic.object(forKey: "fileType") as? String == "video" ) {
                            mimeTypeStr = "video/mp4";
                        }
                        else if (fileDic.object(forKey: "fileType") as? String == "gif" ) {
                            mimeTypeStr = "image/gif";
                        }
                        multipartFormData.append(fileDic.object(forKey: "avatar") as! Data, withName: fileDic.object(forKey: "name") as! String, fileName: fileDic.object(forKey: "fileName") as! String, mimeType: mimeTypeStr)
                    }
                }
                
                print("multipartFormData = ", multipartFormData )
        },
        usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: url, method: .post, headers: header as? [String : String], encodingCompletion: {
            encodingResult in
                                    
                switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON(completionHandler: { response in
                            debugPrint(response)
                            success(response.response, response.result.value as? NSDictionary)
                        })
                                        
                    case .failure(let error):
                        print(error)
                        failure(error)
                }
        })
    }
    
    func uploadCoverLoggedInFileRequestWithParameters(_ paramDic : NSMutableDictionary?, files: NSMutableArray?, isImage: Bool?, headers : NSMutableDictionary?, serviceName : NSString, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary?) -> Void), failure: @escaping ((_ error : Error? ) -> Void))
    {
        let urlString = BASE_URL + (serviceName as String)
        let url : URL = URL.init(string: urlString)!
        // var headerDic = [String : String]()
        let header : NSMutableDictionary = [:]
        
        header.setValue("1.0", forKey: "app-version")
        header.setValue("ios", forKey: "os")
        header.setValue(language, forKey: "language")
        header["Content-Type"] = "multipart/form-data"
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        header.setValue(dict.object(forKey: "login_token"), forKey: "Authorization")
        
        // Begin upload
        
        alamoFireManager?.upload(multipartFormData:
            { (multipartFormData) in
                
                if(paramDic != nil)
                {
                    for (key, value) in paramDic!
                    {
                        multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                    }
                }
                
                
                if(isImage!)
                {
                    // import image to request
                    for (file)  in files!
                    {
                        var mimeTypeStr = ""
                        let fileDic : NSDictionary = file as! NSDictionary
                        if (fileDic.object(forKey: "fileType") as? String == "image" )
                        {
                            mimeTypeStr = "image/jpeg";
                        }
                        else if (fileDic.object(forKey: "fileType") as? String == "video" ) {
                            mimeTypeStr = "video/mp4";
                        }
                        else if (fileDic.object(forKey: "fileType") as? String == "gif" ) {
                            mimeTypeStr = "image/gif";
                        }
                        multipartFormData.append(fileDic.object(forKey: "cover_image") as! Data, withName: fileDic.object(forKey: "name") as! String, fileName: fileDic.object(forKey: "fileName") as! String, mimeType: mimeTypeStr)
                    }
                }
                
                print("multipartFormData = ", multipartFormData )
        },
                                 usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: url, method: .post, headers: header as? [String : String], encodingCompletion: {
                                    encodingResult in
                                    
                                    switch encodingResult {
                                    case .success(let upload, _, _):
                                        upload.responseJSON(completionHandler: { response in
                                            debugPrint(response)
                                            success(response.response, response.result.value as? NSDictionary)
                                        })
                                        
                                    case .failure(let error):
                                        print(error)
                                        failure(error)
                                    }
        })
    }
    
    //MARK: upload documents
    
    func uploadDocumentsRequestWithParameters(_ paramDic : NSMutableDictionary?, files: NSMutableArray?, isImage: Bool?, headers : NSMutableDictionary?, serviceName : NSString, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary?) -> Void), failure: @escaping ((_ error : Error? ) -> Void))
    {
        let urlString = BASE_URL + (serviceName as String)
        let url : URL = URL.init(string: urlString)!
        // var headerDic = [String : String]()
        let header : NSMutableDictionary = [:]
        
        header.setValue("1.0", forKey: "app-version")
        header.setValue("ios", forKey: "os")
        header.setValue(language, forKey: "language")
        header["Content-Type"] = "multipart/form-data"
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        header.setValue(dict.object(forKey: "login_token"), forKey: "Authorization")
        
        // Begin upload
        
        alamoFireManager?.upload(multipartFormData:
            { (multipartFormData) in
                
                if(paramDic != nil)
                {
                    for (key, value) in paramDic!
                    {
                        multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                    }
                }
        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: url, method: .post, headers: header as? [String : String], encodingCompletion: {
            encodingResult in
                                    
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { response in
                    debugPrint(response)
                    success(response.response, response.result.value as? NSDictionary)
                })
                                        
            case .failure(let error):
                print(error)
                failure(error)
            }
        })
    }
    
    // Upload Service
    
    func addServiceRequestWithParameters(_ paramDic : NSMutableDictionary?, files: NSMutableArray?, isImage: Bool?, headers : NSMutableDictionary?, serviceName : NSString, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary?) -> Void), failure: @escaping ((_ error : Error? ) -> Void))
    {
        let urlString = BASE_URL + (serviceName as String)
        let url : URL = URL.init(string: urlString)!
        // var headerDic = [String : String]()
        let header : NSMutableDictionary = [:]
        
        header.setValue("1.0", forKey: "app-version")
        header.setValue("ios", forKey: "os")
        header.setValue(language, forKey: "language")
        header["Content-Type"] = "multipart/form-data"
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        header.setValue(dict.object(forKey: "login_token"), forKey: "Authorization")
        
        // Begin upload
        
        alamoFireManager?.upload(multipartFormData:
            { (multipartFormData) in
                
                if(paramDic != nil)
                {
                    for (key, value) in paramDic!
                    {
                        multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                    }
                }
                
                
                if(isImage!)
                {
                    // import image to request
                    for (file)  in files!
                    {
                        var mimeTypeStr = ""
                        let fileDic : NSDictionary = file as! NSDictionary
                        if (fileDic.object(forKey: "fileType") as? String == "image" )
                        {
                            mimeTypeStr = "image/jpeg";
                        }
                        else if (fileDic.object(forKey: "fileType") as? String == "video" ) {
                            mimeTypeStr = "video/mp4";
                        }
                        else if (fileDic.object(forKey: "fileType") as? String == "gif" ) {
                            mimeTypeStr = "image/gif";
                        }
                        multipartFormData.append(fileDic.object(forKey: "service_image") as! Data, withName: fileDic.object(forKey: "name") as! String, fileName: fileDic.object(forKey: "fileName") as! String, mimeType: mimeTypeStr)
                    }
                }
                
                print("multipartFormData = ", multipartFormData )
        },
                                 usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: url, method: .post, headers: header as? [String : String], encodingCompletion: {
                                    encodingResult in
                                    
                                    switch encodingResult {
                                    case .success(let upload, _, _):
                                        upload.responseJSON(completionHandler: { response in
                                            debugPrint(response)
                                            success(response.response, response.result.value as? NSDictionary)
                                        })
                                        
                                    case .failure(let error):
                                        print(error)
                                        failure(error)
                                    }
        })
    }
    
    // delete Service
    
    func deleteServiceRequestWithParameters(_ paramDic : NSMutableDictionary?, files: NSMutableArray?, isImage: Bool?, headers : NSMutableDictionary?, serviceName : NSString, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary?) -> Void), failure: @escaping ((_ error : Error? ) -> Void))
    {
        let urlString = BASE_URL + (serviceName as String)
        let url : URL = URL.init(string: urlString)!
        // var headerDic = [String : String]()
        let header : NSMutableDictionary = [:]
        
        header.setValue("1.0", forKey: "app-version")
        header.setValue("ios", forKey: "os")
        header.setValue(language, forKey: "language")
        header["Content-Type"] = "multipart/form-data"
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        header.setValue(dict.object(forKey: "login_token"), forKey: "Authorization")
        
        // Begin upload
        
        alamoFireManager?.upload(multipartFormData:
            { (multipartFormData) in
                
                if(paramDic != nil)
                {
                    for (key, value) in paramDic!
                    {
                        multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                    }
                }
                
                
                if(isImage!)
                {
                    // import image to request
                    for (file)  in files!
                    {
                        var mimeTypeStr = ""
                        let fileDic : NSDictionary = file as! NSDictionary
                        if (fileDic.object(forKey: "fileType") as? String == "image" )
                        {
                            mimeTypeStr = "image/jpeg";
                        }
                        else if (fileDic.object(forKey: "fileType") as? String == "video" ) {
                            mimeTypeStr = "video/mp4";
                        }
                        else if (fileDic.object(forKey: "fileType") as? String == "gif" ) {
                            mimeTypeStr = "image/gif";
                        }
                        multipartFormData.append(fileDic.object(forKey: "service_image") as! Data, withName: fileDic.object(forKey: "name") as! String, fileName: fileDic.object(forKey: "fileName") as! String, mimeType: mimeTypeStr)
                    }
                }
                
                print("multipartFormData = ", multipartFormData )
        },
                                 usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: url, method: .post, headers: header as? [String : String], encodingCompletion: {
                                    encodingResult in
                                    
                                    switch encodingResult {
                                    case .success(let upload, _, _):
                                        upload.responseJSON(completionHandler: { response in
                                            debugPrint(response)
                                            success(response.response, response.result.value as? NSDictionary)
                                        })
                                        
                                    case .failure(let error):
                                        print(error)
                                        failure(error)
                                    }
        })
    }
    
    // Upload edit Service
    
    func editServiceRequestWithParameters(_ paramDic : NSMutableDictionary?, files: NSMutableArray?, isImage: Bool?, headers : NSMutableDictionary?, serviceName : NSString, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary?) -> Void), failure: @escaping ((_ error : Error? ) -> Void))
    {
        let urlString = BASE_URL + (serviceName as String)
        let url : URL = URL.init(string: urlString)!
        
        let header : NSMutableDictionary = [:]
        
        header.setValue("1.0", forKey: "app-version")
        header.setValue("ios", forKey: "os")
        header.setValue(language, forKey: "language")
        header["Content-Type"] = "multipart/form-data"
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        header.setValue(dict.object(forKey: "login_token"), forKey: "Authorization")
        
        // Begin upload
        
        alamoFireManager?.upload(multipartFormData:
            { (multipartFormData) in
                
                if(paramDic != nil)
                {
                    //print("paramdic = \(String(describing: paramDic))")
                    
                    for (key, value) in paramDic!
                    {
                        multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                    }
                }
                
                if(isImage!)
                {
                    // import image to request
                    for (file)  in files!
                    {
                        var mimeTypeStr = ""
                        let fileDic : NSDictionary = file as! NSDictionary
                        if (fileDic.object(forKey: "fileType") as? String == "image" )
                        {
                            mimeTypeStr = "image/jpeg";
                        }
                        else if (fileDic.object(forKey: "fileType") as? String == "video" ) {
                            mimeTypeStr = "video/mp4";
                        }
                        else if (fileDic.object(forKey: "fileType") as? String == "gif" ) {
                            mimeTypeStr = "image/gif";
                        }
                        multipartFormData.append(fileDic.object(forKey: "service_image") as! Data, withName: fileDic.object(forKey: "name") as! String, fileName: fileDic.object(forKey: "fileName") as! String, mimeType: mimeTypeStr)
                    }
                }
                
                print("multipartFormData = ", multipartFormData )
        },
                                 usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: url, method: .post, headers: header as? [String : String], encodingCompletion: {
                                    encodingResult in
                                    
                                    switch encodingResult {
                                    case .success(let upload, _, _):
                                        upload.responseJSON(completionHandler: { response in
                                            debugPrint(response)
                                            success(response.response, response.result.value as? NSDictionary)
                                        })
                                        
                                    case .failure(let error):
                                        print(error)
                                        failure(error)
                                    }
        })
    }
    
    //MARK : create profile
    
    func uploadCoverWithParameters(_ paramDic : NSMutableDictionary?, files: NSMutableArray?, isImage: Bool?, headers : NSMutableDictionary?, serviceName : NSString, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary?) -> Void), failure: @escaping ((_ error : Error? ) -> Void))
    {
        let urlString = BASE_URL + (serviceName as String)
        let url : URL = URL.init(string: urlString)!
        // var headerDic = [String : String]()
        let header : NSMutableDictionary = [:]
        
        header.setValue("1.0", forKey: "app-version")
        header.setValue("ios", forKey: "os")
        header.setValue(language, forKey: "language")
        header["Content-Type"] = "multipart/form-data"
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        header.setValue(dict.object(forKey: "login_token"), forKey: "Authorization")
        
        // Begin upload
        
        alamoFireManager?.upload(multipartFormData:
            { (multipartFormData) in
                
                if(paramDic != nil)
                {
                    for (key, value) in paramDic!
                    {
                        multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                    }
                }
                
                if(isImage!)
                {
                    // import image to request
                    for (file)  in files!
                    {
                        var mimeTypeStr = ""
                        let fileDic : NSDictionary = file as! NSDictionary
                        if (fileDic.object(forKey: "fileType") as? String == "image" )
                        {
                            mimeTypeStr = "image/jpeg";
                        }
                        else if (fileDic.object(forKey: "fileType") as? String == "video" ) {
                            mimeTypeStr = "video/mp4";
                        }
                        else if (fileDic.object(forKey: "fileType") as? String == "gif" ) {
                            mimeTypeStr = "image/gif";
                        }
                        
                        if((fileDic.object(forKey: "type") as! String) == "1")
                        {
                            multipartFormData.append(fileDic.object(forKey: "cover_image") as! Data, withName: fileDic.object(forKey: "name") as! String, fileName: fileDic.object(forKey: "fileName") as! String, mimeType: mimeTypeStr)
                            
                            print("cover_image = ", fileDic.object(forKey: "cover_image") as! Data )
                            print("name = ", fileDic.object(forKey: "name") as! String )
                            print("fileName = ", fileDic.object(forKey: "fileName") as! String )
                        }
                        else
                        {
                            multipartFormData.append(fileDic.object(forKey: "avatar") as! Data, withName: fileDic.object(forKey: "name") as! String, fileName: fileDic.object(forKey: "fileName") as! String, mimeType: mimeTypeStr)
                            
                            print("avatar = ", fileDic.object(forKey: "avatar") as! Data )
                            print("name = ", fileDic.object(forKey: "name") as! String )
                            print("fileName = ", fileDic.object(forKey: "fileName") as! String )
                        }
                    }
                }
                
                print("multipartFormData = ", multipartFormData )
        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: url, method: .post, headers: header as? [String : String], encodingCompletion: {
            encodingResult in
                                    
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { response in
                    debugPrint(response)
                    success(response.response, response.result.value as? NSDictionary)
                })
                                        
            case .failure(let error):
                print(error)
                failure(error)
            }
        })
    }
    
    func uploadAvatarImageRequestWithParameters(_ paramDic : NSMutableDictionary?, serviceName : NSString, success: @escaping (( _ response : HTTPURLResponse?, _ data : NSDictionary?) -> Void), failure: @escaping ((_ error : Error? ) -> Void))
    {
        let urlString = BASE_URL + (serviceName as String)
        let url : URL = URL.init(string: urlString)!
        // var headerDic = [String : String]()
        let header : NSMutableDictionary = [:]
        
        header.setValue("1.0", forKey: "app-version")
        header.setValue("ios", forKey: "os")
        header.setValue(language, forKey: "language")
        header["Content-Type"] = "multipart/form-data"
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        header.setValue(dict.object(forKey: "login_token"), forKey: "Authorization")
        
        // Begin upload
        
        alamoFireManager?.upload(multipartFormData:
            { (multipartFormData) in
                
                multipartFormData.append((paramDic?.object(forKey: "avatar") as! String).data(using: String.Encoding.utf8)!, withName: "avatar")
                print("multipartFormData = ", multipartFormData)
                
        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: url, method: .post, headers: header as? [String : String], encodingCompletion:
            { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON(completionHandler: { response in
                        debugPrint(response)
                        success(response.response, response.result.value as? NSDictionary)
                    })
                    
                case .failure(let error):
                    print(error)
                    failure(error)
                }
        })
    }
}
