//
//  AppDelegate.swift
//  CarWash
//
//  Created by Neha Chaudhary on 04/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

//sumitkonstant@gmail.com

//https://github.com/s3lvin/DXCustomCallout-ObjC

//http://iosbucket.blogspot.in/2016/06/uidocumentpickerviewcontroller-and.html
//https://github.com/aaronksaunders/ios8documemtpicker

//https://stackoverflow.com/questions/32041420/cropping-image-with-swift-and-put-it-on-center-position

import UIKit
import IQKeyboardManagerSwift

import CoreLocation

import GooglePlaces
import GoogleMaps

import Alamofire

import UserNotifications

import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import FirebaseAuth
import FirebaseStorage
import FirebaseCrash
import FirebaseDatabase

import StoreKit

import AWSS3
import AWSCognito

let AmazonS3PoolID = "us-west-2:aa52e724-84fe-4009-ae89-162a200be89a"
let AmazonS3BucketNameDocuments = "car-wash/documents/"
let AmazonS3BucketNameAvatar = "car-wash/avatar/"
let AmazonS3BucketNameCover = "car-wash/cover/"
let AmazonS3BucketNameService = "car-wash/service/"

var appDelegate = UIApplication.shared.delegate

var scaleFactorX = UIScreen.main.bounds.size.width/375

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate, UNUserNotificationCenterDelegate, MessagingDelegate
{
    @IBOutlet var window: UIWindow?
    @IBOutlet var customView : UIView?
    
    var tabBarController : UITabBarController? = nil
    let locationManager = CLLocationManager()

    var backgroundTaskIdentifier = UIBackgroundTaskIdentifier()
    var myTimer: Timer?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        GMSPlacesClient.provideAPIKey("AIzaSyA4qrOp3IyWCEytrpsBX8excxJhH-vIcGQ")
        GMSServices.provideAPIKey("AIzaSyAh87Bhv7nvo-A-z44iuTelhBMGlJefjMk")
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().previousNextDisplayMode = .alwaysShow
        
        //location manager
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled()
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        if (USERDEFAULT.float(forKey: "lattitude") != 0)
        {
            lattitude = USERDEFAULT.float(forKey: "lattitude")
            longtitude = USERDEFAULT.float(forKey: "longtitude")
            
            userLattitude = USERDEFAULT.float(forKey: "lattitude")
            userLongtitude = USERDEFAULT.float(forKey: "longtitude")
        }
        
        //   navigationbar title custom font
        UINavigationBar.appearance().titleTextAttributes =
        [
            NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.6)!,
        ]
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        UINavigationBar.appearance().tintColor = UIColor.uicolorFromRGB(2.0, 166.0, 242.0)
        
        //add amazon s3 to app
        
        AmazonServiceManager.defaultServiceManager().setDefaultServiceConfiguration (forRegion: .USWest2, poolID: AmazonS3PoolID)
        
        let varVC = VerificationVC()
        let navC = UINavigationController()
        navC.viewControllers = [varVC]
        window?.rootViewController = navC
        
        if(!(USERDEFAULT.object(forKey: "USER_DATA") != nil))
        {
            if (UserDefaults.standard.bool(forKey: "GUEST_USER") == true)
            {
                APPDELEGATE.addTabBar()
            }
            else if(UserDefaults.standard.bool(forKey: "isFirstTimeLaunch") == false)
            {
                let rootView  = ChangeLanguageVC()
                let navC = UINavigationController()
                
                if let window = self.window
                {
                    navC.viewControllers = [rootView]
                    window.rootViewController = navC
                }
            }
            else
            {
                let rootView = SignInVC()
                let navC = UINavigationController()

                if let window = self.window
                {
                    navC.viewControllers = [rootView]
                    window.rootViewController = navC
                }
            }
        }
        else
        {
            let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
            let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary

            let str = dict.object(forKey: "user_role") as! NSNumber

            if(str == 27)
            {
                APPDELEGATE.addTabBarForServiceProvider()
            }
            else
            {
                APPDELEGATE.addTabBar()
            }
        }
        
        LanguageManager.setupCurrentLanguage()
        
        window?.makeKeyAndVisible()
        window?.makeKey()
        
        customView?.layer.shadowColor = UIColor.uicolorFromRGB(179, 187, 200).cgColor
        customView?.layer.shadowOpacity = 1
        customView?.layer.shadowOffset = CGSize.zero
        customView?.layer.shadowRadius = 5
        
        if #available(iOS 10.0, *)
        {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .alert, .sound]) { (granted, error) in
                
                if granted
                {
                    application.registerForRemoteNotifications()
                }
            }
        }
        else
        {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        
        Messaging.messaging().delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotificaiton), name: .InstanceIDTokenRefresh, object: nil)
        
        application.applicationIconBadgeNumber = 0

        if LanguageManager.currentLanguageIndex() == 0
        {
            language = "en"
        }
        else
        {
            language = "ar"
        }
        
        return true
    }
    
    func requestReview()
    {
        if #available(iOS 10.3, *)
        {
            SKStoreReviewController.requestReview()
        }
        else
        {
            // Fallback on earlier versions
            
            //    NSString *reviewURL = [NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@",appiraterId];
            //    NSLog(@"App_iD %@",reviewURL);
            //
            //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:reviewURL]];
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication)
    {
        if isMultitaskingSupported() == false
        {
            return
        }
        
        myTimer = Timer.scheduledTimer(timeInterval: 600.0, target: self, selector: #selector(self.timerMethod), userInfo: nil, repeats: true)
        
        backgroundTaskIdentifier = application.beginBackgroundTask(expirationHandler: {(_: Void) -> Void in
            self.endBackgroundTask()
        })
    }

    func applicationWillEnterForeground(_ application: UIApplication)
    {
        if isMultitaskingSupported() == false {
            return
        }
        
        myTimer = Timer.scheduledTimer(timeInterval: 800.0, target: self, selector: #selector(self.timerMethod), userInfo: nil, repeats: true)
        
        backgroundTaskIdentifier = application.beginBackgroundTask(expirationHandler: {(_: Void) -> Void in
            self.endBackgroundTask()
        })
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK: locationManager Delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        lattitude = Float(locValue.latitude);
        longtitude = Float(locValue.longitude)
        
        if userLongtitude == 0.0
        {
            userLattitude = Float(locValue.latitude);
            userLongtitude = Float(locValue.longitude)
        }
        
        USERDEFAULT.set(lattitude, forKey: "lattitude")
        USERDEFAULT.set(longtitude, forKey: "longtitude")
    }
    
    func addTabBar()
    {
        tabBarController = nil
        tabBarController = UITabBarController()
        tabBarController?.tabBar.frame = CGRect(x: 50, y: SCREEN_HEIGTH - (SCREEN_WIDTH * 0.141), width: SCREEN_WIDTH, height: SCREEN_WIDTH * 0.143)
        tabBarController?.tabBar.backgroundColor = UIColor.white
        
        tabBarController?.viewControllers = [UINavigationController(rootViewController: HomeVC()), UINavigationController(rootViewController: HistoryVC()), UINavigationController(rootViewController: ConcersationsVC()), UINavigationController(rootViewController: SettingVC())]
        
        customView?.frame = CGRect(x: 0, y: SCREEN_HEIGTH - (SCREEN_WIDTH * 0.143), width: SCREEN_WIDTH, height: SCREEN_WIDTH * 0.143)
        tabBarController?.view?.addSubview(customView!)
        
        APPDELEGATE.window?.rootViewController = nil
        APPDELEGATE.window?.rootViewController = tabBarController
        
        (customView?.viewWithTag(1) as? UIButton)?.setImage(UIImage(named: "homeActive"), for: .normal)
        (customView?.viewWithTag(2) as? UIButton)?.setImage(UIImage(named: "clockMenu"), for: .normal)
        (customView?.viewWithTag(3) as? UIButton)?.setImage(UIImage(named: "message"), for: .normal)
        (customView?.viewWithTag(4) as? UIButton)?.setImage(UIImage(named: "defaultSetting"), for: .normal)
    }
    
    func addTabBarForServiceProvider()
    {
        tabBarController = nil
        tabBarController = UITabBarController()
        tabBarController?.tabBar.frame = CGRect(x: 50, y: SCREEN_HEIGTH - (SCREEN_WIDTH * 0.141), width: SCREEN_WIDTH, height: SCREEN_WIDTH * 0.143)
        tabBarController?.tabBar.backgroundColor = UIColor.white
        
        tabBarController?.viewControllers = [UINavigationController(rootViewController: ProviderHomeVC()), UINavigationController(rootViewController: SelectServicesVC()), UINavigationController(rootViewController: ConcersationsVC()), UINavigationController(rootViewController: SettingVC())]
        
        customView?.frame = CGRect(x: 0, y: SCREEN_HEIGTH - (SCREEN_WIDTH * 0.143), width: SCREEN_WIDTH, height: SCREEN_WIDTH * 0.143)
        tabBarController?.view?.addSubview(customView!)
        
        APPDELEGATE.window?.rootViewController = nil
        APPDELEGATE.window?.rootViewController = tabBarController
        
        (customView?.viewWithTag(1) as? UIButton)?.setImage(UIImage(named: "homeActive"), for: .normal)
        (customView?.viewWithTag(2) as? UIButton)?.setImage(UIImage(named: "clockMenu"), for: .normal)
        (customView?.viewWithTag(3) as? UIButton)?.setImage(UIImage(named: "message"), for: .normal)
        (customView?.viewWithTag(4) as? UIButton)?.setImage(UIImage(named: "defaultSetting"), for: .normal)
    }
    
    @IBAction func customTabBarPressed(sender : UIButton)
    {
        //selectTab(tabIndex: sender.tag)
        
        //DEV
        
        if sender.tag == 2 || sender.tag == 3 || sender.tag == 4
        {
            if(!(USERDEFAULT.object(forKey: "USER_DATA") != nil))
            {
                self.validateAlert()
            }
            else
            {
                selectTab(tabIndex: sender.tag)
            }
        }
        else
        {
            selectTab(tabIndex: sender.tag)
        }
    }

    //  The converted code is limited to 1 KB.
    //  Please Sign Up (Free!) to remove this limitation.
    
    //  Converted to Swift 4 by Swiftify v1.0.6554 - https://objectivec2swift.com/

    func validateAlert()
    {
        let alert = UIAlertController(title: "", message: NSLocalizedString("PLESE_SIGN_IN_FIRST", comment: ""), preferredStyle: .alert)
        
        let noButton = UIAlertAction(title: NSLocalizedString("SIGN_IN", comment: ""), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            
            let rootView = SignInVC()
            let navC = UINavigationController()
            
            if let window = self.window
            {
                navC.viewControllers = [rootView]
                window.rootViewController = navC
            }
        })
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("SKIP", comment: ""), style: .default, handler: {(_ action: UIAlertAction) -> Void in
        }))
        
        alert.addAction(noButton)
        
         APPDELEGATE.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func selectTab(tabIndex : Int)
    {
        let navController1: UINavigationController? = (tabBarController?.selectedViewController as? UINavigationController)
        var testController: UIViewController? = navController1?.presentedViewController
        
        if testController != nil
        {
            testController?.dismiss(animated: false) { _ in }
            testController = nil
        }
        
        var btn: UIButton? = (customView?.viewWithTag(tabIndex) as? UIButton)
        let newIndex = Int((btn?.tag)!)
        let currentIndex = Int((tabBarController?.selectedIndex)!) + 1
        
        var image: UIImage?
        switch currentIndex
        {
       
        case 1:
            image = UIImage(named: "home")
            break;
       
        case 2:
            image = UIImage(named: "clockMenu")
            break;
        
        case 3:
            image = UIImage(named: "message")
            break;
        
        case 4:
            image = UIImage(named: "defaultSetting")
            break;
        
        default:
            break;
        }
        
        btn = (customView?.viewWithTag(currentIndex) as? UIButton)
        btn?.setImage(image, for: .normal)
        switch newIndex
        {
        case 1:
            let navController: UINavigationController? = (tabBarController?.viewControllers?[0] as? UINavigationController)
            navController?.popToRootViewController(animated: true)
            
            (customView?.viewWithTag(1) as? UIButton)?.setImage(UIImage(named: "homeActive"), for: .normal)
        case 2:
            let navController: UINavigationController? = (tabBarController?.viewControllers?[1] as? UINavigationController)
            navController?.popToRootViewController(animated: true)
            
            (customView?.viewWithTag(2) as? UIButton)?.setImage(UIImage(named: "historyActive"), for: .normal)
        case 3:
            let navController: UINavigationController? = (tabBarController?.viewControllers?[2] as? UINavigationController)
            navController?.popToRootViewController(animated: true)
            
            (customView?.viewWithTag(3) as? UIButton)?.setImage(UIImage(named: "mesaageActive"), for: .normal)
        case 4:
            let navController: UINavigationController? = (tabBarController?.viewControllers?[3] as? UINavigationController)
            navController?.popToRootViewController(animated: true)
            
            (customView?.viewWithTag(4) as? UIButton)?.setImage(UIImage(named: "settingActive"), for: .normal)
        default:
            break;
        }
        
        tabBarController?.selectedIndex = newIndex - 1
    }
    
    //MARK : Background Process
    
    func isMultitaskingSupported() -> Bool
    {
        var result = false
    
        if UIDevice.current.responds(to: #selector(self.isMultitaskingSupported))
        {
            result = UIDevice.current.isMultitaskingSupported
        }
        
        return result
    }
    
    func timerMethod(_ paramSender: Timer)
    {
        let backgroundTimeRemaining: TimeInterval = UIApplication.shared.backgroundTimeRemaining
        if backgroundTimeRemaining == DBL_MAX
        {
            //NSLog(@"Background Time Remaining = Undetermined");
        }
        else
        {
            //NSLog(@"Background Time Remaining = %.02f Seconds",backgroundTimeRemaining);
        }
    }
    
    func endBackgroundTask() {
        let mainQueue = DispatchQueue.main
        weak var weakSelf: AppDelegate? = self
        mainQueue.async(execute: {(_: Void) -> Void in
            let strongSelf: AppDelegate? = weakSelf
            if strongSelf != nil {
                strongSelf?.myTimer?.invalidate()
                UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier)
                strongSelf?.backgroundTaskIdentifier = UIBackgroundTaskInvalid
            }
        })
    }

    func tokenRefreshNotificaiton(_ notification: Foundation.Notification)
    {
        if let refreshedToken = InstanceID.instanceID().token()
        {
            debugPrint("fcm refresh token: \(refreshedToken)")
            
            USERDEFAULT.set(refreshedToken, forKey: "FCM_Token")
            USERDEFAULT.synchronize()
        }
        
        connectToFcm()
    }
    
    func connectToFcm() {
        
        if InstanceID.instanceID().token() != nil
        {
            return
        }
        
        Messaging.messaging().disconnect()
        
        Messaging.messaging().connect { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    // Called when APNs has assigned the device a unique token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
        
        var refreshToken = deviceTokenString
        
        if let refreshedToken = InstanceID.instanceID().token()
        {
            debugPrint("fcm token: \(refreshedToken)")
            
            refreshToken = refreshedToken
            
            USERDEFAULT.set(refreshToken, forKey: "FCM_Token")
            USERDEFAULT.synchronize()
        }
        
        Messaging.messaging().apnsToken = deviceToken
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any])
    {
        print("Push notification received: \(data)")
        
        //Messaging.messaging().sendMessage(data, to: "Sender", withMessageID: "", timeToLive: 10)
    }
    
    func application(
        _ application: UIApplication,
        didReceiveRemoteNotification userInfo: [AnyHashable : Any],
        fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        print("notification received =========>   \(userInfo)")
        
        if let type = userInfo[AnyHashable("gcm.notification.notification_type")] as? String
        {
            if type == "chat"
            {
                let user_name = userInfo[AnyHashable("gcm.notification.sender_name")] as? String
                let user_id = userInfo[AnyHashable("gcm.notification.sender_id")] as? String
                
                guard let appD = appDelegate as? AppDelegate else {
                    return
                }
                
                if let chatVwCntroler = appD.window?.visibleViewController() as? MessagesVC
                {
                    chatVwCntroler.selected_user_id = user_id!
                    chatVwCntroler.selected_user_name = user_name!
                    
                    chatVwCntroler.fetchData()
                }
                else
                {
                    let topController = appD.window?.visibleViewController()
                    
                    let messges = MessagesVC()
                    
                    messges.selected_user_id = user_id!
                    messges.selected_user_name = user_name!
                    
                    topController?.navigationController?.pushViewController(messges, animated: true)
                }
            }
        }
        else if let type = userInfo[AnyHashable("notification_type")] as? String
        {
            if type == "updatebookingstatus"
            {
                guard let appD = appDelegate as? AppDelegate else {
                    return
                }
                
                if let chatVwCntroler = appD.window?.visibleViewController() as? HistoryDetailVC
                {
                    let booking_id = userInfo[AnyHashable("request_id")] as! String
                    chatVwCntroler.booking_id = booking_id
                    
                    chatVwCntroler.getBookingDetailApiCall()
                }
                else
                {
                    let topController = appD.window?.visibleViewController()
                    
                    let history = HistoryDetailVC()
                    
                    let booking_id = userInfo[AnyHashable("request_id")] as! String
                    history.booking_id = booking_id
                    topController?.navigationController?.pushViewController(history, animated: true)
                }
            }
            else if type == "rating"
            {
                guard let appD = appDelegate as? AppDelegate else {
                    return
                }
                
                if let chatVwCntroler = appD.window?.visibleViewController() as? RatingViewController
                {
                    chatVwCntroler.getReviewListApi()
                }
                else
                {
                    let topController = appD.window?.visibleViewController()
                    
                    let reviewlist = RatingViewController()
                    topController?.navigationController?.pushViewController(reviewlist, animated: true)
                }
            }
            else if type == "servicebooking"
            {
                (customView?.viewWithTag(1) as? UIButton)?.setImage(UIImage(named: "homeActive"), for: .normal)
                
                (customView?.viewWithTag(2) as? UIButton)?.setImage(UIImage(named: "clockMenu"), for: .normal)
                (customView?.viewWithTag(3) as? UIButton)?.setImage(UIImage(named: "message"), for: .normal)
                (customView?.viewWithTag(4) as? UIButton)?.setImage(UIImage(named: "defaultSetting"), for: .normal)
                
                guard let appD = appDelegate as? AppDelegate else {
                    return
                }
                
                if let chatVwCntroler = appD.window?.visibleViewController() as? ProviderHomeVC
                {
                    chatVwCntroler.serviceApiSearch(searchString: "")
                }
                else
                {
                    let topController = appD.window?.visibleViewController()
                    
                    let providerHome = ProviderHomeVC()
                    topController?.navigationController?.pushViewController(providerHome, animated: true)
                }
            }
        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        print("Firebase registration token for messaging : \(fcmToken)")
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func sendPush (toDevice : String, withNotifyData : [AnyHashable : Any], withTitle : String)
    {
        let sender = "\(withTitle) says:"
        
        // Set up the URL request
        let urlString = "https://fcm.googleapis.com/fcm/send"
        
        let url : URL = URL.init(string: urlString)!
        
        var request = URLRequest(url: url)
        
        let server_key = "AAAATul2lWk:APA91bEYMVG9-ga99BNq_vZ5yFlCbZ4oxHiu4VsjWRfJan_vdcxRAU2GdX6Y-2sA84i5BBxb9eU9XadxIDcRhFoFsAH0tLaSISD58G8Bc6zLzY0D-ZX-CNgTE58t1o6U5ElplgqzDbA4"
        
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        request.setValue("key=\(server_key)", forHTTPHeaderField: "Authorization")
        
        request.httpMethod = "POST"
        
        let pushDict = withNotifyData as NSDictionary
        let userDict = pushDict.object(forKey: "userData") as! NSDictionary
        
        let notificationDict : NSDictionary = [
            "title" : sender,
            "body" : pushDict.object(forKey: "message") as! String,
            "sound" : "default",
            "badge" : "1",
            "click_action" : "",
            "notification_type" : "chat",
            "sender_name" : userDict.object(forKey: "name") as! String,
            "sender_id" : userDict.object(forKey: "fcm_user_id") as! String
        ]
        
        let reqData : NSMutableDictionary = [
            "priority" : "high",
            "to" : toDevice,
            "notification" : notificationDict,
            "data" : notificationDict
        ]
        
        print("request data = \(reqData)")
        
        request.httpBody = try! JSONSerialization.data(withJSONObject: reqData, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        let alamoFireManager : SessionManager?
        
        alamoFireManager = Alamofire.SessionManager.default
            
        alamoFireManager!.request(request as URLRequestConvertible).responseJSON {
            response in
            
            switch response.result
            {
            case .failure(let error):
                print("\(urlString) failure response -> \n NSHTTPURLResponse ->\(String(describing: response.response)) \n \(error)")
                
            case .success :
                print("\(urlString) success response -> \n NSHTTPURLResponse ->\(String(describing: response.response)) \n Data -> \(String(describing: response.result.value as? NSDictionary))")
            }
        }
    }
    
    // This method will be called when app received push notifications in foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        print("will receive notification in active state")
        completionHandler([.alert, .badge, .sound])
    }
}

//MARK:-
extension UIWindow
{
    //MARK:-
    func visibleViewController() -> UIViewController?
    {
        if let rootViewController: UIViewController  = self.rootViewController
        {
            return UIWindow.getVisibleViewControllerFrom(vc: rootViewController)
        }
        
        return nil
    }
    
    //MARK:-
    class func getVisibleViewControllerFrom(vc:UIViewController) -> UIViewController
    {
        if vc.isKind(of: UINavigationController.self)
        {
            let navigationController = vc as! UINavigationController
            return UIWindow.getVisibleViewControllerFrom( vc: navigationController.visibleViewController!)
        }
        else if vc.isKind(of: UITabBarController.self)
        {
            let tabBarController = vc as! UITabBarController
            return UIWindow.getVisibleViewControllerFrom(vc: tabBarController.selectedViewController!)
        }
        else
        {
            if let presentedViewController = vc.presentedViewController
            {
                return UIWindow.getVisibleViewControllerFrom(vc: presentedViewController)
            }
            else
            {
                return vc
            }
        }
    }
}

