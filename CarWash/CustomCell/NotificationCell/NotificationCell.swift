//
//  NotificationCell.swift
//  CarWash
//
//  Created by Neha Chaudhary on 15/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell
{
    @IBOutlet var lblTitle      : UILabel?
    @IBOutlet var lblDate       : UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
