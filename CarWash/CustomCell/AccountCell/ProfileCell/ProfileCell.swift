//
//  ProfileCell.swift
//  CarWash
//
//  Created by Neha Chaudhary on 12/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewHeaderFooterView
{
    @IBOutlet var imgProfile    : UIImageView?
    @IBOutlet var coverImgProfile    : UIImageView?
    
    @IBOutlet weak var profileCollectionView: UICollectionView!
    @IBOutlet weak var profilePageControl: UIPageControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   
    
}
