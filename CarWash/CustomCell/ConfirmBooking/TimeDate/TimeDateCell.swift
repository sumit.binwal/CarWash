//
//  TimeDateCell.swift
//  CarWash
//
//  Created by Neha Chaudhary on 27/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class TimeDateCell: UITableViewCell
{
    @IBOutlet var lblTime   : UILabel?
    @IBOutlet var txtPoints : UITextField?
    
    @IBOutlet weak var lblTimeAndDate: UILabel!
    @IBOutlet weak var lblLoyaltyPoint: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
