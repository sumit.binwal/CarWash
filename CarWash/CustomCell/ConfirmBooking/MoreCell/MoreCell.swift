//
//  MoreCell.swift
//  CarWash
//
//  Created by Neha Chaudhary on 28/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class MoreCell: UITableViewCell
{
    @IBOutlet var btnMore : UIButton?
    
    @IBOutlet weak var lblReviewAndRating: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
