//
//  CollapsibleTableHeader.swift
//  ios-swift-collapsible-table-section
//
//  Created by Neha Chaudhary on 26/09/17.
//  Copyright © 2017 Yong Su. All rights reserved.
//

import UIKit

class CollapsibleTableHeader: UITableViewCell
{
    @IBOutlet var lblServiceName    : UILabel?
    @IBOutlet var lblDesc           : UILabel?
    @IBOutlet var bgView            : UIView?
    @IBOutlet weak var selectedImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
