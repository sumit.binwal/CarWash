//
//  DiscountCell.swift
//  CarWash
//
//  Created by Neha Chaudhary on 25/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class DiscountCell: UITableViewCell
{
    @IBOutlet var lblDiscount   : UILabel?
    @IBOutlet var bgView        : UIView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
