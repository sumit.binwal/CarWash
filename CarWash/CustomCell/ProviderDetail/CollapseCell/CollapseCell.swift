//
//  CollapseCell.swift
//  ios-swift-collapsible-table-section
//
//  Created by Neha Chaudhary on 26/09/17.
//  Copyright © 2017 Yong Su. All rights reserved.
//

import UIKit

class CollapseCell: UITableViewCell
{
    @IBOutlet var lbltitle : UILabel?
    @IBOutlet var lblSubtitle : UILabel?

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
