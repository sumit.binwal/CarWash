//
//  WashServiceCell.swift
//  CarWash
//
//  Created by Suman Payal on 11/12/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class WashServiceCell: UITableViewCell {

    @IBOutlet weak var lowerView: UIView!
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var lblTakenTime: UILabel!
    @IBOutlet weak var lblCharge: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
