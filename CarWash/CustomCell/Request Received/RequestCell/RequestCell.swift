//
//  RequestCell.swift
//  CarWash
//
//  Created by iOS on 25/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class RequestCell: UITableViewCell
{
    @IBOutlet var imgService    : UIImageView?
    @IBOutlet var lblName       : UILabel?
    @IBOutlet var lblPhone      : UILabel?
    @IBOutlet var lblKm         : UILabel?
    @IBOutlet weak var requestNumberLabel: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    
    @IBOutlet weak var unReadCountView: UIView!
    @IBOutlet weak var unReadCountLabel: UILabel!
    
    @IBOutlet weak var distanceConstraint: NSLayoutConstraint!
    @IBOutlet var btnCall       : UIButton?
    @IBOutlet var btnMail       : UIButton?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
