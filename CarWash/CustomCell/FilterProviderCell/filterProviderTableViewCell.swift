//
//  filterProviderTableViewCell.swift
//  CarWash
//
//  Created by Ratina on 4/23/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit

class filterProviderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var sortImage: UIImageView!
    @IBOutlet weak var sortLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
