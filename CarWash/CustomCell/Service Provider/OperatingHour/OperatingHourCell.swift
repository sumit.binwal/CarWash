//
//  OperatingHourCell.swift
//  CarWash
//
//  Created by iOS on 10/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import IQDropDownTextField

class OperatingHourCell: UITableViewCell
{
    @IBOutlet var txtOpening1 : UITextField?
    @IBOutlet var txtClosing1 : UITextField?
    @IBOutlet var txtOpening2 : UITextField?
    @IBOutlet var txtClosing2 : UITextField?
    
    @IBOutlet weak var lblOpeningTime1: UILabel!
    @IBOutlet weak var lblClosingTime1: UILabel!
    @IBOutlet weak var lblOpeningTime2: UILabel!
    @IBOutlet weak var lblClosingTime2: UILabel!
    @IBOutlet var bgView     : UIView?
    @IBOutlet var headerView : UIView?
    
    @IBOutlet var shift1     : UIView?
    @IBOutlet var shift2     : UIView?
    
    @IBOutlet var lblDay     : UILabel?
    @IBOutlet var btnAdd     : UIButton?
    @IBOutlet var btnCross     : UIButton?
    @IBOutlet var delSwitch     : UISwitch?
    
    
    @IBOutlet weak var shiftHeight: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        bgView?.layer.cornerRadius = 4.0
        bgView?.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
