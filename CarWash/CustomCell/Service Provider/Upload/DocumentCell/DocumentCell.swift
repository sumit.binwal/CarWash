//
//  DocumentCell.swift
//  CarWash
//
//  Created by iOS on 17/11/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class DocumentCell: UITableViewCell
{
    @IBOutlet var icon      : UIImageView?
    @IBOutlet var lblDoc    : UILabel?
    @IBOutlet var btnAdd    : UIButton?
    @IBOutlet var deleteBtn: UIButton!
    @IBOutlet var addView   : UIView?

    @IBOutlet weak var lblAddDocument: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
