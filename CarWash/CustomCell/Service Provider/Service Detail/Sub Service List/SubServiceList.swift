//
//  SubServiceList.swift
//  CarWash
//
//  Created by iOS on 24/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

protocol SubServiceListDelegate
{
    func subServiceListDelegateClicked(cell : SubServiceList)
}

class SubServiceList: UITableViewCell
{
    var delegate : SubServiceListDelegate?
    
    @IBOutlet var txtTitle_en    : UITextField?
    @IBOutlet var txtTitle_ar    : UITextField?
    
    @IBOutlet var txtDuration_en : UITextField?
    @IBOutlet var txtDuration_ar : UITextField?
    @IBOutlet var txtPrice_en    : UITextField?
    @IBOutlet var txtPrice_ar    : UITextField?
    
    @IBOutlet var txvDesc_en     : UITextView?
    @IBOutlet var txvDesc_ar     : UITextView?

    @IBOutlet weak var edit_btn: UIButton!
    
    var cellSection : Int  = 0
    var cellRow : Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func editBtnClickedServiceList(_ sender: Any)
    {
        self.delegate?.subServiceListDelegateClicked(cell: self)
    }
}
