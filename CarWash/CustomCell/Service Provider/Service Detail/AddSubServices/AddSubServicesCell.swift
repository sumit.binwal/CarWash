//
//  AddSubServices.swift
//  CarWash
//
//  Created by iOS on 30/11/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class AddSubServicesCell: UITableViewCell
{
    @IBOutlet var btnAddService : UIButton?
    @IBOutlet var btnAddAdditionalService : UIButton?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
