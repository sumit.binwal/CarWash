//
//  ProfileDetailCell.swift
//  CarWash
//
//  Created by iOS on 23/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class ProfileDetailCell: UITableViewCell
{
    @IBOutlet var imgService        : UIImageView?
    @IBOutlet var txtNameEng        : UITextField?
    @IBOutlet var txtNameArabic     : UITextField?
    
    @IBOutlet var txVDescEng        : UITextView?
    @IBOutlet var txVDescArabic     : UITextView?
    
    @IBOutlet var btnType           : UIButton?
    @IBOutlet var btnCamera         : UIButton?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.imgService?.layer.cornerRadius = ((self.imgService?.frame.size.width)!/375*SCREEN_WIDTH)/2
        //self.imgService?.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
        self.imgService?.layer.borderColor = UIColor.clear.cgColor
        self.imgService?.layer.borderWidth = 1.0
        self.imgService?.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
