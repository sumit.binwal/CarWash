//
//  SubServiceCell.swift
//  CarWash
//
//  Created by iOS on 17/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class SubServiceCell: UITableViewCell
{
    @IBOutlet var lblTitle  : UILabel?
    @IBOutlet var lblSubTitle  : UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
