//
//  ChangeLanguageVC.swift
//  CarWash
//
//  Created by Jitendra Singh on 10/01/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit

class ChangeLanguageVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        UIApplication.shared.isStatusBarHidden = true
        self.navigationController?.navigationBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onArebicButtonAction(_ sender: UIButton) {
        
        UserDefaults.standard.set(true, forKey: "Language_Class")
        UserDefaults.standard.set(true, forKey: "isFirstTimeLaunch")
        UserDefaults.standard.synchronize()
        
        LanguageManager.saveLanguage(by: 1)
        //language = "ar"
    }
    
    //MARK:- English Button Action
    @IBAction func onEnglishButtonAction(_ sender: UIButton)
    {
        UserDefaults.standard.set(true, forKey: "Language_Class")
        UserDefaults.standard.set(true, forKey: "isFirstTimeLaunch")
        UserDefaults.standard.synchronize()
        
        LanguageManager.saveLanguage(by: 0)
        //language = "en"
    }
}
