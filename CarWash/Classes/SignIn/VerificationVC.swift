//
//  VerificationVC.swift
//  CarWash
//
//  Created by Neha Chaudhary on 14/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD

class VerificationVC: UIViewController
{
    @IBOutlet weak var varificationTextLbl: UILabel!
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var accountVerifiedLbl: UILabel!
    @IBOutlet weak var congratulationLbl: UILabel!
    
    @IBOutlet weak var mobilNoLabel: UILabel!
    
    @IBOutlet var lblResend     : UILabel?
    @IBOutlet var lblTime       : UILabel?
    @IBOutlet var btnConfirm    : UIButton!
    
    @IBOutlet weak var changemobilebtn: UIButton!
    @IBOutlet var txtPwd1       : UITextField?
    @IBOutlet var txtPwd2       : UITextField?
    @IBOutlet var txtPwd3       : UITextField?
    @IBOutlet var txtPwd4       : UITextField?
    
    @IBOutlet var verificationPopUp     : UIView?

    var isSignUp: Bool = false
    var codeDict : NSMutableDictionary = [:]
    var codeDictMobileChange : NSMutableDictionary = [:]
    
    var isFromConfirmBooking = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        print("code dict = ", codeDict)
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.hidesBackButton = true
        
        //self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        
        if (LanguageManager.currentLanguageIndex() == 0)
        {
            txtPwd1?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            txtPwd2?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            txtPwd3?.contentHorizontalAlignment     = UIControlContentHorizontalAlignment.right
            txtPwd4?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
        }
        else
        {
            txtPwd1?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            txtPwd2?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            txtPwd3?.contentHorizontalAlignment     = UIControlContentHorizontalAlignment.left
            txtPwd4?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        }

        self.title = NSLocalizedString("VARIFICATION_TITLE", comment: "")
        
        self.varificationTextLbl.text = NSLocalizedString("ENTER_VARIFICATION_TITLE", comment: "")
        self.congratulationLbl.text = NSLocalizedString("CONGRATULATIONS_TEXT", comment: "")
        self.accountVerifiedLbl.text = NSLocalizedString("ACCOUNT_VERIFIED_MESSAGE", comment: "")
        
        self.btnConfirm?.setTitle(NSLocalizedString("CONFIRM_TEXT", comment: ""), for:UIControlState.normal)
        self.okBtn?.setTitle(NSLocalizedString("OK_TEXT", comment: ""), for:UIControlState.normal)

        let str1 = NSLocalizedString("DONT_RECEIVE_CODE", comment: "") as NSString
        let str2 = NSLocalizedString("RESEND_CODE", comment: "") as NSString
        
        let str12 = NSLocalizedString("Change Mobile Number", comment: "") as NSString
        
        let attributedString = NSMutableAttributedString(string: String(format: "%@ %@", str1, str2))

        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 15)!,
                NSForegroundColorAttributeName:UIColor.uicolorFromRGB(89, 89, 89)
            ], range: NSRange(location:0 , length: str1.length)
        )

        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 15)!,
                NSForegroundColorAttributeName:UIColor.uicolorFromRGB(2, 166, 242)
            ], range: NSRange(location: attributedString.length - str2.length, length: str2.length)
        )
        
        lblResend?.attributedText = attributedString
        
        let attributedString1 = NSMutableAttributedString(string: String(format: "%@", str12))
        
        attributedString1.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 15)!,
                NSForegroundColorAttributeName:UIColor.uicolorFromRGB(2, 166, 241)
            ], range: NSRange(location:0 , length: str12.length)
        )
        
        self.mobilNoLabel.attributedText = attributedString1
        
        if isSignUp
        {
            self.mobilNoLabel.isHidden = false
            self.changemobilebtn.isUserInteractionEnabled = true
        }
        else
        {
            self.mobilNoLabel.isHidden = true
            self.changemobilebtn.isUserInteractionEnabled = false
        }
        
        btnConfirm?.layer.cornerRadius = 5.0
        btnConfirm?.layer.masksToBounds = true
        
        verificationPopUp?.viewWithTag(1)?.layer.cornerRadius = 3.0
        verificationPopUp?.viewWithTag(1)?.layer.masksToBounds = true
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
 
    //MARK: @IBAction Methods
    
    @IBAction func resendCode()
    {
        RESIGN_KEYBOARD
        if(CommonFunctions.isInternetAvailable())
        {
            MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
            resendCodeApi()
        }
    }
    
    @IBAction func confirm()
    {
        RESIGN_KEYBOARD
        if(validateUserInput())
        {
            if(CommonFunctions.isInternetAvailable())
            {
                MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
                isSignUp ? verifyOtpApi() : verifyForgotOTPApi()
            }
        }
    }
    
    @IBAction func changeMobileNoButtonAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let signInVC = ForgotPasswordVC()
        
        signInVC.isSignUp = true
        signInVC.isFromConfirmBooking = isFromConfirmBooking
        signInVC.codeDictMobileChange = self.codeDictMobileChange
        
        self.navigationController?.pushViewController(signInVC, animated: true)
    }
    
    @IBAction func verificationOk()
    {
        verificationPopUp?.removeFromSuperview()
        
        if isFromConfirmBooking
        {
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
        else
        {
            APPDELEGATE.addTabBar()
        }
    }
    
    @IBAction func closeVerificationView()
    {
        verificationPopUp?.removeFromSuperview()
    }
    
    // MARK: Validation
    
    func validateUserInput() -> Bool
    {
        var isError : Bool = true
        
        if(txtPwd1?.text?.isEmpty)!
        {
            isError = false
            
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("EMPTY_VERIFICATION", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
                self.txtPwd1?.becomeFirstResponder()
            })
        }
        else if(txtPwd2?.text?.isEmpty)!
        {
            isError = false
            
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("EMPTY_VERIFICATION", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
                self.txtPwd2?.becomeFirstResponder()
            })
        }
        else if(txtPwd3?.text?.isEmpty)!
        {
            isError = false
            
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("EMPTY_VERIFICATION", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
                self.txtPwd3?.becomeFirstResponder()
            })
        }
        else if(txtPwd4?.text?.isEmpty)!
        {
            isError = false
            
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("EMPTY_VERIFICATION", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
                self.txtPwd4?.becomeFirstResponder()
            })
        }
        
        return isError
    }
    
    // MARK: API Call
    
    func verifyOtpApi()
    {
        let service = CustomerService()
        
        var otp : NSString = (txtPwd1?.text! as NSString?)!
        otp = otp.appending((txtPwd2?.text! as NSString?)! as String) as NSString
        otp = otp.appending((txtPwd3?.text! as NSString?)! as String) as NSString
        otp = otp.appending((txtPwd4?.text! as NSString?)! as String) as NSString
        
        let token : NSString = USERDEFAULT.object(forKey: "temp_token") as! NSString
        
        let dic : NSMutableDictionary = [
            
            "temp_token"      : token,
            "otp"             : otp
        ]
        
        service.verifyOtpAPIRequestWithParameters(dic, success:
        { (response, data) in
         
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    USERDEFAULT.set(NSKeyedArchiver.archivedData(withRootObject: data?.object(forKey: "data") as! NSDictionary), forKey: "USER_DATA")
                    USERDEFAULT.synchronize()
                    
                    self.verificationPopUp?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
                    
                    if self.isSignUp
                    {
                        APPDELEGATE.window?.addSubview(self.verificationPopUp!)
                    }
                }
                else
                {
                    CommonFunctions.showAlertWithTitle(title: "", message: data?.object(forKey: "msg") as? String, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                        self.txtPwd1?.text = ""
                        self.txtPwd2?.text = ""
                        self.txtPwd3?.text = ""
                        self.txtPwd4?.text = ""
                    })
                }
            }
            else
            {
                CommonFunctions.showAlertWithTitle(title: "", message: data?.object(forKey: "msg") as? String, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                    
                    self.txtPwd1?.text = ""
                    self.txtPwd2?.text = ""
                    self.txtPwd3?.text = ""
                    self.txtPwd4?.text = ""
                })
            }
        })
        { (response, error) in
         
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    func verifyForgotOTPApi()
    {
        let service = CustomerService()
        var otp : NSString = (txtPwd1?.text! as NSString?)!
        otp = otp.appending((txtPwd2?.text! as NSString?)! as String) as NSString
        otp = otp.appending((txtPwd3?.text! as NSString?)! as String) as NSString
        otp = otp.appending((txtPwd4?.text! as NSString?)! as String) as NSString
        
        let token : NSString = USERDEFAULT.object(forKey: "temp_token") as! NSString
        
        let dic : NSMutableDictionary = [
            
            "temp_token"      : token,
            "otp"             : otp
        ]
        
        service.verifyForgotOTPRequestWithParameters(dic, success:
            { (response, data) in
                
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                
                if(response?.statusCode == 200)
                {
                    let str : NSString = data?.object(forKey: "status") as! NSString
                    
                    if (str.isEqual(to: "success"))
                    {
                        let obj = SetPasswordVC()
                       
                        obj.isFromConfirmBooking = self.isFromConfirmBooking
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                    else
                    {
                        CommonFunctions.showAlertWithTitle(title: "", message: data?.object(forKey: "msg") as? String, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                            
                            self.txtPwd1?.text = ""
                            self.txtPwd2?.text = ""
                            self.txtPwd3?.text = ""
                            self.txtPwd4?.text = ""
                        })
                    }
                }
                else
                {
                    CommonFunctions.showAlertWithTitle(title: "", message: data?.object(forKey: "msg") as? String, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                        self.txtPwd1?.text = ""
                        self.txtPwd2?.text = ""
                        self.txtPwd3?.text = ""
                        self.txtPwd4?.text = ""
                    })
                }
        })
        { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    func resendCodeApi()
    {
        let service = CustomerService()
        
        codeDict.setObject(true, forKey: "resend" as NSCopying)
        print("resend dict = ", codeDict)
        
        if(self.isSignUp)
        {
            service.sendVerificationAPIRequestWithParameters(codeDict, success:
                { (response, data) in
                    
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    if(response?.statusCode == 200)
                    {
                        let str : NSString = data?.object(forKey: "status") as! NSString
                        
                        if (str.isEqual(to: "success"))
                        {
                            
                            
                            CommonFunctions.showAlertWithTitle(title: "", message: data?.object(forKey: "msg") as? String, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                                
                            })
                        }
                        else
                        {
                            self.txtPwd1?.text = ""
                            self.txtPwd2?.text = ""
                            self.txtPwd3?.text = ""
                            self.txtPwd4?.text = ""
                        }
                    }
                    else
                    {
                        MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                        CommonFunctions.showAlertWithTitle(title: "", message: data?.object(forKey: "msg") as? String, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                            
                            self.txtPwd1?.text = ""
                            self.txtPwd2?.text = ""
                            self.txtPwd3?.text = ""
                            self.txtPwd4?.text = ""
                        })
                    }
            })
            { (response, error) in
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            }
        }
        else
        {
            service.forgotPwdAPIRequestWithParameters(codeDict, success:
                { (response, data) in
                    
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    if(response?.statusCode == 200)
                    {
                        let str : NSString = data?.object(forKey: "status") as! NSString
                        
                        if (str.isEqual(to: "success"))
                        {
                            USERDEFAULT.set(data!["temp_token"] as! NSString, forKey: "temp_token")
                            
                            CommonFunctions.showAlertWithTitle(title: "", message: data?.object(forKey: "msg") as? String, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                                
                            })
                        }
                        else
                        {
                            self.txtPwd1?.text = ""
                            self.txtPwd2?.text = ""
                            self.txtPwd3?.text = ""
                            self.txtPwd4?.text = ""
                        }
                    }
                    else
                    {
                        MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                        
                        CommonFunctions.showAlertWithTitle(title: "", message: data?.object(forKey: "msg") as? String, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                            
                            self.txtPwd1?.text = ""
                            self.txtPwd2?.text = ""
                            self.txtPwd3?.text = ""
                            self.txtPwd4?.text = ""
                        })
                    }
            })
            { (response, error) in
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            }
        }
    }
}

extension VerificationVC : UITextFieldDelegate
{
    // MARK:    UITextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        // textField.text = ""
    }
    
    @IBAction func textFieldDidChange(_ theTextField: UITextField)
    {
        if !(theTextField.text?.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty)!
        {
            if theTextField == txtPwd1 && txtPwd2?.text?.count == 0
            {
                txtPwd2?.becomeFirstResponder()
            }
            else if theTextField == txtPwd2 && txtPwd3?.text?.count == 0
            {
                txtPwd3?.becomeFirstResponder()
            }
            else if theTextField == txtPwd3 && txtPwd4?.text?.count == 0
            {
                txtPwd4?.becomeFirstResponder()
            }
            else if theTextField == txtPwd4
            {
                txtPwd4?.resignFirstResponder()
            }
            else
            {
                theTextField.resignFirstResponder()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        //if !string.canBeConverted(to: String.Encoding.ascii){
        //    return false
        //}
        
        if textField.textInputMode?.primaryLanguage == "emoji" || !((textField.textInputMode?.primaryLanguage) != nil)
        {
            return false
        }
        
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92)
        {
            if (textField == txtPwd4)
            {
                txtPwd4?.text = ""
                txtPwd3?.becomeFirstResponder()
            }
            else if (textField == txtPwd3)
            {
                txtPwd3?.text = ""
                txtPwd2?.becomeFirstResponder()
            }
            else if (textField == txtPwd2)
            {
                txtPwd2?.text = ""
                txtPwd1?.becomeFirstResponder()
            }
            else
            {
                txtPwd1?.text = ""
            }
            
            return false
        }
        
        return true
    }
}
