//
//  CoveImageProviderCollectionViewCell.swift
//  CarWash
//
//  Created by Ratina on 4/23/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit

class CoveImageProviderCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var coverImageViewScrollView: UIScrollView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

extension CoveImageProviderCollectionViewCell : UIScrollViewDelegate
{
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return coverImageView
    }
}
