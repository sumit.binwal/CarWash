//
//  HomeVC.swift
//  CarWash
//
//  Created by Neha Chaudhary on 08/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MapKit
import GooglePlaces
import GooglePlacePicker
import MBProgressHUD

class HomeVC: UIViewController
{
    @IBOutlet var mapView            : MKMapView?
    @IBOutlet var searchBar          : UISearchBar?
    @IBOutlet var serviceView        : UIView?
    @IBOutlet var notificationView   : UIView?
    
    @IBOutlet weak var notificationBgImage: UIImageView!
    @IBOutlet weak var labelNoNotification: UILabel!
    
    @IBOutlet var popUp              : UIView?
    @IBOutlet var lblClick           : UILabel?
    
    @IBOutlet var tblService         : UITableView?
    @IBOutlet var tblNotification    : UITableView?
    
    @IBOutlet var datePciker         : UIDatePicker?
    @IBOutlet var timePicker         : UIPickerView?
    @IBOutlet var btnNext            : UIButton?
    
    //popUp
    @IBOutlet weak var lblLocationAlert: UILabel!
    @IBOutlet weak var lblMsgAlert: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    
    var notificatioDataAry : NSMutableArray = []
    
    @IBOutlet weak var heightConstarint: NSLayoutConstraint!
    
    @IBOutlet weak var bottomCosntraint: NSLayoutConstraint!
    
    var bell        : UIBarButtonItem?
    var dict        : NSMutableDictionary = [:]
    
    var address     : NSString = ""
    var duration    : NSString = ""
    var selectedLat : CLLocationDegrees?
    var selectedLon : CLLocationDegrees?
    
    var serviceAry : NSMutableArray = [NSLocalizedString("SELECT_CAR_TYPE", comment: ""), NSLocalizedString("SELECT_DATE", comment: ""), NSLocalizedString("SELECT_TIME", comment: "")]
    
    var timeSlotArr = [String]()

    let formatter = DateFormatter()
    var notificationAry : NSMutableArray = []
    var annotation = PinAnnotation()
    
    var isAddressChnage: Bool = true;
   
    override func viewDidLoad()
    {
        super.viewDidLoad()

        mapView?.delegate = self
        mapView?.showsUserLocation = true
        
        print("current location latitude = ", mapView?.userLocation.coordinate.latitude)
        print("current location longitude = ", mapView?.userLocation.coordinate.longitude)
        
        configureSearchController()
        
        serviceView?.layer.cornerRadius = 4.0
        serviceView?.layer.masksToBounds = true
        
        btnNext?.isHidden = true
        
        heightConstarint.constant = 194
        bottomCosntraint.constant = 0
        
        btnNext?.layer.cornerRadius = 3.0
        btnNext?.layer.masksToBounds = true
       
        tblNotification?.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
        mapView?.addGestureRecognizer(tapGesture)
        
        popUp?.viewWithTag(2)?.layer.cornerRadius = 10.0
        popUp?.viewWithTag(2)?.layer.masksToBounds = true
        
        let str1 = NSLocalizedString("LOCATION_ALERT_TITLE", comment: "") as NSString
        let str2 = NSLocalizedString("LOCATION_ALERT_TITLE1", comment: "") as NSString
        
        let attributedString = NSMutableAttributedString(string: String(format: "%@ %@", str1, str2))
        
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 17.5)!,
                NSForegroundColorAttributeName: UIColor.uicolorFromRGB(134, 134, 134)
            ], range: NSRange(location: 0, length: str1.length)
        )
        
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 17.5)!,
                NSForegroundColorAttributeName: UIColor.uicolorFromRGB(2, 166, 242)
            ], range: NSRange(location: attributedString.length - str2.length, length: str2.length)
        )
        
        lblClick?.attributedText = attributedString
       
        tblService?.register(UINib(nibName: "HomeCell", bundle: nil), forCellReuseIdentifier: "HomeCell")
        
        tblNotification?.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        
        //  datePciker?.maximumDate = Date()
        datePciker?.minimumDate = Date()
        formatter.dateFormat = "dd MMM yyyy"
        
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector (doneAction), name: NSNotification.Name(rawValue: "doneAction"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector (nextAction), name: NSNotification.Name(rawValue: "nextAction"), object: nil)
        
        lblLocationAlert.text = NSLocalizedString("LOCATION_ALERT", comment: "")
        lblMsgAlert.text = NSLocalizedString("LOCATION_ALERT_MSG", comment: "")
        btnOk.setTitle(NSLocalizedString("OK_TEXT", comment: ""), for: UIControlState.normal)
        btnNext?.setTitle(NSLocalizedString("NEXT_TEXT", comment: ""), for: UIControlState.normal)
        
        if language == "en"
        {
            notificationBgImage.image = UIImage.init(named: "popoverBg")
        }
        else
        {
            notificationBgImage.image = UIImage.init(named: "popoverBg_ar")
        }
    }
    func currentLocationButtonAction(_ sender: UIButton) {
        mapView?.setCenter((mapView?.userLocation.location?.coordinate)!, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        searchBar?.setValue(NSLocalizedString("CANCEL_TEXT", comment: ""), forKey:"_cancelButtonText")
        searchBar?.placeholder = NSLocalizedString("SEARCH_TEXT", comment: "")
        
        let textFieldInsideUISearchBar = searchBar?.value(forKey: "searchField") as? UITextField
        
        if (LanguageManager.currentLanguageIndex() == 0)
        {
            textFieldInsideUISearchBar?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            textFieldInsideUISearchBar?.textAlignment = NSTextAlignment.left
        }
        else
        {
            textFieldInsideUISearchBar?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            textFieldInsideUISearchBar?.textAlignment = NSTextAlignment.right
        }
        
        labelNoNotification?.text = NSLocalizedString("NO_NOTIFICATION", comment: "")
        
        print("isAddressChnage = \(isAddressChnage)")
        print("lattitude = \(userLattitude)")
        print("longtitude = \(userLongtitude)")
        
        if isAddressChnage
        {
            self.searchBar?.text = NSLocalizedString("SELECT_LOCATION", comment: "")
            
            let center = CLLocationCoordinate2D(latitude: CLLocationDegrees(userLattitude), longitude: CLLocationDegrees(userLongtitude))
            
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            
            mapView?.setRegion(region, animated: true)
            
            MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
            checkServiceProviderApi()
            getAddressFromCoordinates(center: center)
        }
        
        setUpNavigationBar()
        
        NotificationCenter.default.addObserver(self, selector: #selector (resetFilters), name: NSNotification.Name(rawValue: "resetFilters"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpPickerView()
    {
        let cal = Calendar (identifier: .gregorian)
        
        let dateFormatter = DateFormatter()
        
        timeSlotArr = []
        
        let newDate1 = datePciker?.date
        //print("date picker = \(String(describing: newDate1))")
        
        var newDate = newDate1!
        //print("selected date = \(newDate)")
        
        let dateNow = Date()
        //print("date now = \(String(describing: dateNow))")
        
        var datesame = 0
        
        let order = cal.compare(dateNow, to: newDate, toGranularity: .day)
        
        switch order
        {
            case .orderedAscending:
                print("orderedAscending")
                datesame = 0
            case .orderedSame:
                print("orderedSame")
                datesame = 1
            case .orderedDescending:
                print("orderedDescending")
                datesame = 0
        }
        
        dateFormatter.dateFormat = "hh:mm a"
        
        let minutes = cal.component(.minute, from: newDate)
        //print("minutes = \(minutes)")
        
        var components = cal.dateComponents([.year, .month, .day, .hour, .minute], from: newDate)
        
        if datesame == 0
        {
            components.hour = 0
            components.minute = 0
        }
        else
        {
            if minutes >= 30
            {
                components.minute = 30
                components.hour = components.hour! + 1
            }
            else
            {
                if minutes >= 10
                {
                    components.minute = 0
                    components.hour = components.hour! + 1
                }
                else
                {
                    components.minute = 30
                }
            }
        }
        
        newDate = cal.date(from: components)!
        
        for _ in 0 ... 47
        {
            let date = cal.date(byAdding: .minute, value: 0, to: newDate)
            newDate = cal.date(byAdding: .minute, value: 30, to: date!)!
            
            let stringDate = "\( dateFormatter.string(from: date!)) - \(dateFormatter.string(from: newDate))"
            
            if stringDate == "11:30 PM - 12:00 AM"
            {
                timeSlotArr.append(stringDate)
                break
            }
            else
            {
                timeSlotArr.append(stringDate)
            }
        }
        
        //print("timeSlotArr = \(timeSlotArr)")
        
        timePicker?.dataSource = self;
        timePicker?.delegate = self;
        
        duration = timeSlotArr[0] as NSString
        //print("duration  = \(duration)")
        
        timePicker?.reloadAllComponents()
    }
    
    func configureSearchController()
    {
        searchBar?.delegate = self
    }
    
    func doneAction()
    {
        let cell = tblService?.cellForRow(at: IndexPath(row: 2, section: 0)) as! HomeCell
        
        if(cell.txtDate?.isFirstResponder)!
        {
            formatter.dateFormat = "dd MMM yyyy"
            cell.txtDate?.text = formatter.string(from: (datePciker?.date)!)
            
            self.dict.setValue(formatter.string(from: (datePciker?.date)!), forKey: "date")
            
            //print("date picker date = \(String(describing: datePciker?.date))")
        }
        else
        {
            let cell = tblService?.cellForRow(at: IndexPath(row: 3, section: 0)) as! HomeCell
            
            if(cell.txtDate?.isFirstResponder)!
            {
                cell.txtDate?.text = duration as String
                self.dict.setValue(duration, forKey: "time")
            }
        }
        
       if(self.dict.allKeys.count == 3)
       {
            btnNext?.isHidden = false
            heightConstarint.constant = 247
            bottomCosntraint.constant = 53
       }
    }
    
    func nextAction()
    {
        let cell = tblService?.cellForRow(at: IndexPath(row: 2, section: 0)) as! HomeCell
        formatter.dateFormat = "dd MMM yyyy"
        cell.txtDate?.text = formatter.string(from: (datePciker?.date)!)
        
        self.dict.setValue(formatter.string(from: (datePciker?.date)!), forKey: "date")
    }
    
    func setUpNavigationBar()
    {
        self.title = NSLocalizedString("BOOK_YOUR_SERVICE", comment: "")

        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        //self.title = "Select Services"
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        bell = UIBarButtonItem(image:  UIImage(named: "bell"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(bellButtonPressed))
        
        if (!UserDefaults.standard.bool(forKey: "GUEST_USER") == true)
        {
            self.navigationItem.rightBarButtonItem = bell
        }
        else
        {
            if((USERDEFAULT.object(forKey: "USER_DATA") != nil))
            {
                self.navigationItem.rightBarButtonItem = bell
            }
        }
    }
    
    func bellButtonPressed()
    {
        showPopOver()
    }

    func showPopOver()
    {
        notificationView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
        bell?.isEnabled = false
        
        self.labelNoNotification.isHidden = true
        self.tblNotification?.isHidden = true
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        notificationApi()
        
        APPDELEGATE.window?.addSubview(notificationView!)
    }
    
    // MARK: -  @IBAction Methods
    
    @IBAction func closeNotificationView()
    {
        tblService?.isHidden = false
        bell?.isEnabled = true
        notificationView?.removeFromSuperview()
    }
    
    @IBAction func changeDate()
    {
        let cell = tblService?.cellForRow(at: IndexPath(row: 2, section: 0)) as! HomeCell
        formatter.dateFormat = "dd MMM yyyy"
        cell.txtDate?.text = formatter.string(from: (datePciker?.date)!)
        
        self.dict.setValue(formatter.string(from: (datePciker?.date)!), forKey: "date")
    }
    
    @IBAction func next()
    {
        print("lat = \(lattitude)")
        print("long = \(longtitude)")
            
        print("userLattitude = \(userLattitude)")
        print("userLongtitude = \(userLongtitude)")
            
        let serviceObj = ServiceProviderVC()
        serviceObj.serviceDict = self.dict
        self.navigationController?.pushViewController(serviceObj, animated: true)
    }
    
    @IBAction func okPopUp()
    {
        popUp?.removeFromSuperview()
    }
    
    @IBAction func closePopUp()
    {
        popUp?.removeFromSuperview()
    }
    
    @IBAction func click()
    {
        popUp?.removeFromSuperview()
        
        let contact = ContactUs()
        contact.isParesent = "1"
        self.present(contact, animated: true, completion: nil)
    }
    
    //MARK: Other Methods
    
    func checkData()
    {
        if(self.dict.allKeys.count == 3)
        {
            btnNext?.isHidden = false
            heightConstarint.constant = 247
            bottomCosntraint.constant = 53
        }
    }
    
    func resetFilters()
    {
        dict.removeAllObjects()
        
        duration = ""
        
        datePciker?.minimumDate = Date()
        formatter.dateFormat = "dd MMM yyyy"
        
        datePciker?.reloadInputViews()
        
        timePicker?.reloadAllComponents()
        btnNext?.isHidden = true
        
        heightConstarint.constant = 194
        bottomCosntraint.constant = 0
        
        tblService?.reloadData()
    }
    
    func validateAlert()
    {
        let alert = UIAlertController(title: "", message: NSLocalizedString("PLESE_SIGN_IN_FIRST", comment: ""), preferredStyle: .alert)
        
        let yesButton = UIAlertAction(title: NSLocalizedString("SIGN_IN", comment: ""), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            
            print(self.navigationController ?? "")
            
            let vc = WelComeVC() //change this to your class name
            
            let navController = UINavigationController(rootViewController: vc)
            self.present(navController, animated:true, completion: nil)
        })
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("SKIP", comment: ""), style: .default, handler: {(_ action: UIAlertAction) -> Void in
        }))
        
        alert.addAction(yesButton)
        
        APPDELEGATE.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func notificationApi()
    {
        let service = CustomerService()
        
        service.notificationApiRequestWithParameters(nil, success: { (response, data) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            print("notificationApiRequestWithParameters = ", data ?? "")
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    self.notificatioDataAry = (data?.object(forKey: "data")as! NSArray).mutableCopy() as! NSMutableArray
                    
                    DispatchQueue.main.async
                        {
                            if(self.notificatioDataAry.count == 0)
                            {
                                self.tblNotification?.isHidden = true
                                self.labelNoNotification.isHidden = false
                            }
                            else
                            {
                                self.labelNoNotification.isHidden = true
                                self.tblNotification?.isHidden = false
                                self.tblService?.isHidden = true
                                self.tblNotification?.reloadData()
                            }
                    }
                }
            }
            else
            {
                AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                    
                })
            }
            
        })
        { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    func checkServiceProviderApi()
    {
        let service1 = CustomerService()
            
        let dic : NSMutableDictionary = [
            "latitude" : userLattitude,
            "longitude" : userLongtitude
        ]
        
        print("dic = \(dic)")
            
        service1.checkServiceProviderDataRequestWithParameters(dic, nil, success: { (response, data) in
                
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                
            //print("check service provide data = ", data ?? "")
                
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                    
                if (str.isEqual(to: "success"))
                {
                    let isServiceProviderAvailable = data?.object(forKey: "exist") as! Bool
                    print("isServiceProviderAvailable = \(isServiceProviderAvailable)")
                        
                    DispatchQueue.main.async
                    {
                        if isServiceProviderAvailable == false
                        {
                            USERDEFAULT.set(true, forKey: "isKeyPopUp")
                                
                            self.popUp?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
                            APPDELEGATE.window?.addSubview(self.popUp!)
                        }
                    }
                }
            }
        })
        { (response, error) in
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
}

extension HomeVC : GMSAutocompleteViewControllerDelegate
{
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace)
    {
        searchBar?.text = place.formattedAddress
        address = place.formattedAddress! as NSString
        
        let indexPath = IndexPath(item: 0, section: 0)
        tblService?.reloadRows(at: [indexPath], with: .none)
        
        self.dismiss(animated: true, completion: nil)
        
        lattitude = Float(CLLocationDegrees(place.coordinate.latitude))
        longtitude = Float(CLLocationDegrees(place.coordinate.longitude))
        
        userLattitude = Float(CLLocationDegrees(place.coordinate.latitude))
        userLongtitude = Float(CLLocationDegrees(place.coordinate.longitude))
        
        isAddressChnage = true
        
        if let annotations = self.mapView?.annotations
        {
            for annotation in annotations
            {
                self.mapView?.removeAnnotation(annotation)
            }
        }
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        checkServiceProviderApi()
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error)
    {
        print( "error", error)
        self.dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension HomeVC : UIGestureRecognizerDelegate
{
    // MARK: UIGestureRecognizer Delegates
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool
    {
        return true
    }
    
    func handleLongPress(_ gestureRecognizer: UIGestureRecognizer)
    {
        let touchPoint = gestureRecognizer.location(in: mapView)
        let touchMapCoordinate = mapView?.convert(touchPoint, toCoordinateFrom: mapView)
        
        getAddressFromCoordinates(center: touchMapCoordinate!)
    }
    
    func getAddressFromCoordinates(center : CLLocationCoordinate2D)
    {
        let location = CLLocation(latitude: CLLocationDegrees(center.latitude), longitude: CLLocationDegrees(center.longitude))
        
        lattitude = Float(center.latitude)
        longtitude = Float(center.longitude)
        
        userLattitude = Float(center.latitude)
        userLongtitude = Float(center.longitude)
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            
            if error != nil
            {
                return
            }
            
            if ((placemarks?.count)! > 0 && center.latitude != 0)
            {
                self.address = ""
                
                let pm = placemarks?[0]
                
                var subLocality : NSString            = ""
                var locality : NSString               = ""
                var administrativeArea : NSString     = ""
                var thoroughfare : NSString           = ""
                
                if( pm?.thoroughfare != nil)
                {
                    thoroughfare = (pm?.thoroughfare!)! as NSString
                    self.address = thoroughfare.appending(", ") as NSString
                }
                
                if(pm?.subLocality != nil)
                {
                    subLocality = (pm?.subLocality!)! as NSString
                    self.address = self.address.appending(subLocality as String) as (String) as NSString
                    self.address = self.address.appending(", ") as NSString
                }
                
                if(pm?.locality != nil)
                {
                    locality = (pm?.locality!)! as NSString
                    self.address = self.address.appending(locality as String) as NSString
                    self.address = self.address.appending(", ") as NSString
                }
                
                if(pm?.administrativeArea != nil )
                {
                    administrativeArea = (pm?.administrativeArea!)! as NSString
                    self.address = self.address.appending(administrativeArea as String) as NSString
                }
                
                addressUser = self.address as String
                
                let indexPath = IndexPath(item: 0, section: 0)
                self.tblService?.reloadRows(at: [indexPath], with: .none)
                self.searchBar?.text = self.address as String
                
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                
                /////////////
                self.mapView?.removeAnnotation(self.annotation)
                self.annotation = PinAnnotation()
                self.annotation.coordinate = center
                self.annotation.userName = self.address as String
                self.mapView?.addAnnotation(self.annotation)
            }
            else
            {
                print("Problem with the data received from geocoder")
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                self.searchBar?.text = NSLocalizedString("SELECT_YOUR_LOCATION", comment: "")
            }
        })
    }
}

extension HomeVC : UITableViewDelegate, UITableViewDataSource
{
    // MARK: - tableview delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  tableView == tblService ? 4 :  notificatioDataAry.count //notificationAry.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(tableView == tblService)
        {
            return indexPath.row == 0 ? 45 : 50
        }
        else if tableView == tblNotification
        {
            return SCREEN_WIDTH * (88/375)
        }
        
        return SCREEN_WIDTH * 0.234
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell") as! HomeCell?
        
        if(tableView == tblService)
        {
            if cell == nil
            {
                cell = HomeCell(style: UITableViewCellStyle.default, reuseIdentifier: "HomeCell")
            }
            
            cell?.imgArrow?.isHidden = true
            
            if(indexPath.row > 0)
            {
                cell?.imgArrow?.isHidden = false
                cell?.lblAddress?.isHidden = true
                cell?.txtDate?.text = serviceAry[indexPath.row-1] as? String
            }
            else
            {
                cell?.imgArrow?.isHidden = true
                
                if address == ""
                {
                    cell?.lblAddress?.text = NSLocalizedString("SELECT_LOCATION", comment: "")
                }
                else
                {
                    cell?.lblAddress?.text = address as String as String
                }
                
                cell?.lblAddress?.isHidden = false
                cell?.lblTitle?.isHidden = true
                cell?.txtDate?.isHidden = true
                cell?.btnIcon?.isHidden = true
            }
            
            if(indexPath.row == 1)
            {
                cell?.txtDate?.isUserInteractionEnabled = false
                
                if (LanguageManager.currentLanguageIndex() == 0)
                {
                    cell?.imgArrow?.setImage(UIImage(named:"tableArrow"), for: .normal)
                }
                else
                {
                    cell?.imgArrow?.setImage(UIImage(named:"tableArrowRight"), for: .normal)
                }
                
                cell?.lblTitle?.text = NSLocalizedString("VEHICLE_TYPE", comment: "")
                cell?.btnIcon?.setImage(UIImage (named: "vehicle"), for: .normal)
            }
            else if(indexPath.row == 2)
            {
                cell?.txtDate?.isUserInteractionEnabled = true
                cell?.txtDate?.inputView = datePciker
                cell?.txtDate?.delegate = self
                cell?.imgArrow?.setImage(UIImage(named:"closeDown"), for: .normal)
                cell?.lblTitle?.text = NSLocalizedString("DATE_TEXT", comment: "")
                cell?.btnIcon?.setImage(UIImage (named: "calender"), for: .normal)
            }
            else if(indexPath.row == 3)
            {
                cell?.imgArrow?.setImage(UIImage(named:"closeDown"), for: .normal)
                cell?.txtDate?.inputView = timePicker
                cell?.lblTitle?.text = NSLocalizedString("TIME_TEXT", comment: "")
                cell?.txtDate?.delegate = self
                cell?.btnIcon?.setImage(UIImage (named: "clock"), for: .normal)
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell!
        }
        else
        {
            var cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell?
            
            if cell == nil
            {
                cell = NotificationCell(style: UITableViewCellStyle.default, reuseIdentifier: "NotificationCell")
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            let dic:NSDictionary = notificatioDataAry.object(at: indexPath.row) as! NSDictionary
            
            cell?.lblTitle?.text = dic.value(forKey: "message") as? String
            
            let datestring = dic.value(forKey: "createdAt") as? String
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let date = dateFormatter.date(from: datestring!)
            dateFormatter.dateFormat = "dd MMM yyyy, hh:mm a"
            let dateString = dateFormatter.string(from: date!)
            
            cell?.lblDate?.text = dateString
            
            return cell!
        }
    }
    
    func  tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == tblService
        {
            if (indexPath.row == 0)
            {
                let acController = GMSAutocompleteViewController()
                acController.delegate = self
                present(acController, animated: true, completion: nil)
            }
            else if (indexPath.row == 1)
            {
                isAddressChnage = false
                
                self.view.endEditing(true)
                
                let obj = TypeVC()
                obj.okPressed =
                    { (str : NSString) -> Void in
                        
                        self.dict.setValue(str, forKey: "type")
                        
                        let cell = self.tblService?.cellForRow(at: IndexPath(row: 1, section: 0)) as! HomeCell
                        cell.txtDate?.text = str as String
                        self.checkData()
                }
                
                self.navigationController?.pushViewController(obj, animated: true)
            }
                
            else if (indexPath.row == 2)
            {
                let cell = self.tblService?.cellForRow(at: IndexPath(row: 2, section: 0)) as! HomeCell
                cell.txtDate?.becomeFirstResponder()
            }
            else if (indexPath.row == 3)
            {
                let cell = self.tblService?.cellForRow(at: IndexPath(row: 3, section: 0)) as! HomeCell
                cell.txtDate?.becomeFirstResponder()
            }
        }
    }
}

extension HomeVC : UIPickerViewDelegate, UIPickerViewDataSource
{
    // MARK: -  PickerView Delegates
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return timeSlotArr.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        //return timeSlotArr[row] as? String
        return timeSlotArr[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        //print("row = \(row)")
        
        duration = timeSlotArr[row] as NSString
        //duration = timeAry[row] as! NSString
        //print("duration = \(duration)")
        
        let cell = tblService?.cellForRow(at: IndexPath(row: 3, section: 0)) as! HomeCell
        cell.txtDate?.text = duration as String
    }
}

extension HomeVC : UISearchBarDelegate
{
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool
    {
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        
        present(acController, animated: true, completion: nil)
        
        return false
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        checkServiceProviderApi()
    }
}

extension HomeVC : UITextFieldDelegate
{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        let cellDate = tblService?.cellForRow(at: IndexPath(row: 2, section: 0)) as! HomeCell
        
        let cellTime = tblService?.cellForRow(at: IndexPath(row: 3, section: 0)) as! HomeCell
        
        if textField == cellTime.txtDate
        {
            if cellDate.txtDate?.text == "" || cellDate.txtDate?.text == NSLocalizedString("SELECT_DATE", comment: "")
            {
                AlertViewController.showAlertWith(title: NSLocalizedString("CARWASH", comment: ""), message: NSLocalizedString("PLEASE_SELECT_DATE", comment: ""), dismissBloack:
                    {
                        
                })
                
                return false
            }
            else
            {
                setUpPickerView()
                return true
            }
        }
        
        return true
    }
}

extension HomeVC : MKMapViewDelegate
{
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?
    {
        if annotation.isKind(of: MKUserLocation.self.self)
        {
            return nil
        }
        
        var annotationIdentifier = ""
        var annotationView: MKAnnotationView?
        
        if(annotation.isKind(of: PinAnnotation.self))
        {
            annotationIdentifier = "Pin"
            
            if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
            {
                annotationView = dequeuedAnnotationView
                annotationView?.annotation = annotation
            }
            else
            {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            }
            
            if let annotationView = annotationView
            {
                annotationView.canShowCallout = false
                annotationView.image = UIImage(named: "location")
            }
            
            annotationView?.annotation = annotation
            return annotationView
        }
        
        return nil
    }
    
    func getAddress(handler: @escaping (String) -> Void)
    {
        var address: String = ""
        let geoCoder = CLGeocoder()
        let location = CLLocation (latitude: selectedLat!, longitude: selectedLon!)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark?
            placeMark = placemarks?[0]
            
            // Address dictionary
            //print(placeMark.addressDictionary ?? "")
            
            // Location name
            if let locationName = placeMark?.addressDictionary?["Name"] as? String
            {
                address += locationName + ", "
            }
            // Street address
            else if let street = placeMark?.addressDictionary?["Thoroughfare"] as? String
            {
                address += street + ", "
            }
            // City
            else if let city = placeMark?.addressDictionary?["City"] as? String
            {
                address += city + ", "
            }
            // Zip code
            else if let zip = placeMark?.addressDictionary?["ZIP"] as? String
            {
                address += zip + ", "
            }
            // Country
            else if let country = placeMark?.addressDictionary?["Country"] as? String
            {
                address += country
            }
            
            // Passing address back
            handler(address)
        })
    }
}
