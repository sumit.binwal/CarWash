//
//  ConfirmBookingVC.swift
//  CarWash
//
//  Created by Neha Chaudhary on 27/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol confirmBookingDelegate
{
    func confirmFunctionClicked()
}

class ConfirmBookingVC: UIViewController
{
    var delegate : confirmBookingDelegate?
    
    @IBOutlet var tblConfirm    :  UITableView?
    @IBOutlet var confirmView   :  UIView?
    @IBOutlet var phoneNoView   :  UIView?
    
    @IBOutlet weak var lblConfirmMsg: UILabel!
    @IBOutlet weak var lblTotalCost: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnOkey: UIButton!
    @IBOutlet weak var lblSharePhoneNumber: UILabel!
    @IBOutlet weak var lblShareDescription: UILabel!
    @IBOutlet weak var btnNotShare: UIButton!
    
    @IBOutlet weak var btnShare: UIButton!
    
    var timeDict     : NSDictionary?
    var totalCost : String?
    
    var isSharePhoneNumber : Bool = false
    
    @IBOutlet weak var totalCostLabel: UILabel!
    
    var providerData = ProviderListModel()
    var serviceDictAdditional : [String : Any]?
    var selectedAdditionalArr : [[String : Any]]?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tblConfirm?.register(UINib(nibName: "ServiceCell", bundle: nil), forCellReuseIdentifier: "ServiceCell")
        tblConfirm?.register(UINib(nibName: "TimeDateCell", bundle: nil), forCellReuseIdentifier: "TimeDateCell")
        tblConfirm?.register(UINib(nibName: "CarServices", bundle: nil), forCellReuseIdentifier: "CarServices")
        tblConfirm?.register(UINib(nibName: "ReviewCell", bundle: nil), forCellReuseIdentifier: "ReviewCell")
        tblConfirm?.register(UINib(nibName: "MoreCell", bundle: nil), forCellReuseIdentifier: "MoreCell")
        
        confirmView?.viewWithTag(1)?.layer.cornerRadius = 10.0
        confirmView?.viewWithTag(1)?.layer.masksToBounds = true
        
        phoneNoView?.viewWithTag(1)?.layer.cornerRadius = 10.0
        phoneNoView?.viewWithTag(1)?.layer.masksToBounds = true
        
        lblTotalCost.text = String (format : "\(NSLocalizedString("TOTAL_COST", comment: "")) :")
        lblConfirmMsg.text = NSLocalizedString("CONFIRM_MSG", comment: "")
        lblSharePhoneNumber.text = NSLocalizedString("SHARE_PHONE_NUMBER", comment: "")
        lblShareDescription.text = NSLocalizedString("SHARE_DESCRIPTION_MSG", comment: "")
        btnConfirm.setTitle(NSLocalizedString("CONFIRM_TEXT", comment: ""), for: UIControlState.normal)
        btnOkey.setTitle(NSLocalizedString("OKEY", comment: ""), for: UIControlState.normal)
        btnShare.setTitle(NSLocalizedString("SHARE_TEXT", comment: ""), for: UIControlState.normal)
        btnNotShare.setTitle(NSLocalizedString("NOT_SHARE", comment: ""), for: UIControlState.normal)
        
        totalCostLabel.text = totalCost
        
        if language == "en"
        {
            totalCostLabel.textAlignment = .right
        }
        else
        {
            totalCostLabel.textAlignment = .left
        }
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        setUpNavBar()
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpNavBar()
    {
        if language == "en"
        {
            self.title = providerData.provider_name_en
        }
        else
        {
            self.title = providerData.provider_name_ar
        }
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }

    // MARK: IBAction Methods
    
    @IBAction func okButtonPressed()
    {
        confirmView?.removeFromSuperview()
        
        // post a notification
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "resetFilters"), object: nil, userInfo: nil)
        
        self.navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func confirm()
    {
        if((USERDEFAULT.object(forKey: "USER_DATA") != nil))
        {
            phoneNoView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
            APPDELEGATE.window?.addSubview(phoneNoView!)
        }
        else
        {
            validateAlert()
        }
    }
    
    func validateAlert()
    {
        let alert = UIAlertController(title: "", message: NSLocalizedString("PLESE_SIGN_IN_FIRST", comment: ""), preferredStyle: .alert)
        
        let yesButton = UIAlertAction(title: NSLocalizedString("SIGN_IN", comment: ""), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            
            print(self.navigationController ?? "")
            
            let signInVC = SignInVC()
            signInVC.isFromConfirmBooking = true
            
            let navController = UINavigationController(rootViewController: signInVC)
            self.present(navController, animated:true, completion: nil)
        })
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("SKIP", comment: ""), style: .default, handler: {(_ action: UIAlertAction) -> Void in
        }))
        
        alert.addAction(yesButton)
        
        APPDELEGATE.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func closeBookingPopup()
    {
        confirmView?.removeFromSuperview()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func cancel()
    {
        phoneNoView?.removeFromSuperview()
        
        isSharePhoneNumber = false
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        addBookingApi()
    }
    
    @IBAction func okay()
    {
        phoneNoView?.removeFromSuperview()
        
        isSharePhoneNumber = true
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        addBookingApi()
    }
    
    func addBookingApi()
    {
        let service = CustomerService()
        
        var serviceJSON : NSString = ""
        
        do
        {
            let ary : NSArray = selectedAdditionalArr! as NSArray
            if(ary.count > 0)
            {
                let jsonData = try JSONSerialization.data(withJSONObject: ary , options: JSONSerialization.WritingOptions.prettyPrinted)
                serviceJSON = String(data: jsonData, encoding: String.Encoding.utf8)! as NSString
            }
        }
        catch
        {
            print(error)
        }
        
        var dateStr = timeDict?.value(forKey: "date")as! String
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        var date = formatter.date(from: dateStr)
        formatter.dateFormat = "yyyy-MM-dd"
        dateStr = formatter.string(from: date!)
        
        let str2 = timeDict?.value(forKey: "time") as! String
        let timearr = str2.components(separatedBy: " - ") as NSArray
        
        var starttime = timearr[0]
        
        formatter.dateFormat = "hh:mm a"
        date = formatter.date(from: starttime as! String)
        formatter.dateFormat = "HH:mm"
        starttime = formatter.string(from: date!)
        
        var endtime = timearr[1]
        
        formatter.dateFormat = "hh:mm a"
        date = formatter.date(from: endtime as! String)
        formatter.dateFormat = "HH:mm"
        endtime = formatter.string(from: date!)
        
        let dic : NSMutableDictionary = [
            
            "provider_id"    :    serviceDictAdditional!["user_id"] as! String,
            "service_id"     :    serviceDictAdditional!["_id"] as! String,
            "date"           :    dateStr,
            "starttime"      :    starttime,
            "endtime"        :    endtime,
            "show_phone"     :    isSharePhoneNumber,
            "booked_service" :    serviceJSON,
            "user_address"   :    addressUser,
            "user_latitude"  :    userLattitude,
            "user_longitude" :    userLongtitude
        ]
        
        print("dic = \(dic)")
        
        service.addBookingRequestWithParameters(dic, success: { (response, data) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            //print("addBookingApiData = ", data ?? "")
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    DispatchQueue.main.async
                        {
                            self.confirmView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
                            APPDELEGATE.window?.addSubview(self.confirmView!)
                    }
                }
            }
            else
            {
                AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                    
                })
            }
            
        })
        { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
}

extension ConfirmBookingVC : UITableViewDataSource, UITableViewDelegate
{
    // MARK: - tableview delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // return 6
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return  indexPath.row == 0 ? (326/375) * SCREEN_WIDTH : indexPath.row == 1 ? 0.330 * SCREEN_WIDTH : indexPath.row == 2 ? SCREEN_WIDTH * 0.319 : SCREEN_WIDTH *  0.08
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell") as! ServiceCell?
            
            cell?.btnCard?.isHidden         = true
            cell?.btnCash?.isHidden         = true
            cell?.btnDicount?.isHidden      = true
            cell?.topConstraint.constant    = 18
            
            cell?.btnCall?.addTarget(self, action: #selector(callProvider), for: .touchUpInside)
            cell?.btnMail?.addTarget(self, action: #selector(mailProvider), for: .touchUpInside)
            
            cell?.ureadCountView.isHidden = true
            
            cell?.ureadCountView.layer.cornerRadius = (cell?.ureadCountView.frame.size.height)!/2
            cell?.ureadCountView.layer.borderColor = UIColor.clear.cgColor
            cell?.ureadCountView.layer.borderWidth = 1.0
            cell?.ureadCountView.layer.masksToBounds = true
            
            if((USERDEFAULT.object(forKey: "USER_DATA") != nil))
            {
                cell?.btnMail?.isHidden = false
                
                Conversation.showProviderUnReadCount(from_fcm_user_id: providerData.fcm_user_id, completion: { (unreadcount) in
                    
                    if unreadcount == "0"
                    {
                        cell?.ureadCountLabel.text = ""
                        cell?.ureadCountView.isHidden = true
                    }
                    else
                    {
                        cell?.ureadCountView.isHidden = false
                        cell?.ureadCountLabel.text = unreadcount
                    }
                })
            }
            else
            {
                cell?.btnMail?.isHidden = true
            }
            
            cell?.btnImage?.addTarget(self, action: #selector(coverImageClicked), for: .touchUpInside)
            
            cell?.imgLogo?.layer.cornerRadius  = ((0.14) * SCREEN_WIDTH)/2
            cell?.imgLogo?.layer.masksToBounds = true
            
            cell?.imgLogo?.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
            cell?.imgLogo?.layer.borderWidth = 1.0
            
            if language == "en"
            {
                cell?.lblName?.text = providerData.provider_name_en
            }
            else
            {
                cell?.lblName?.text = providerData.provider_name_en
            }
            
            cell?.lblName?.text = cell?.lblName?.text?.uppercased()
            
            cell?.imgLogo?.setShowActivityIndicator(true)
            cell?.imgLogo?.setIndicatorStyle(.gray)
            
            cell?.imgService?.setShowActivityIndicator(true)
            cell?.imgService?.setIndicatorStyle(.gray)
            
            if providerData.cover_image.count > 0
            {
                let str1 = providerData.cover_image[0]
                cell?.imgService?.sd_setImage(with: URL.init(string: str1), placeholderImage: #imageLiteral(resourceName: "group29"))
            }
            else
            {
                cell?.imgService?.image = #imageLiteral(resourceName: "group29")
            }
            
            cell?.imgLogo?.sd_setImage(with: URL.init(string: providerData.provider_image), placeholderImage: #imageLiteral(resourceName: "group20"))
            
            let contactTxt = NSLocalizedString("CONTACT_TEXT", comment: "") as NSString
            let contactValue  = String(format : "%@ %@", providerData.country_code, providerData.mobile) as NSString
            
            let contact : String = String(format : "\(contactTxt): %@ %@", providerData.country_code, providerData.mobile)
            
            var attributedString = NSMutableAttributedString(string: contact)
            attributedString.addAttributes(
                [
                    NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                    NSForegroundColorAttributeName: UIColor(red: 130 / 255.0, green: 130 / 255.0, blue: 130 / 255.0, alpha: 1.0)
                ], range: NSRange(location: 0, length: contactTxt.length+1)
            )
            
            attributedString.addAttributes(
                [
                    NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                    NSForegroundColorAttributeName: UIColor(red: 83 / 255.0, green: 83 / 255.0, blue: 83 / 255.0, alpha: 1.0)
                ], range: NSRange(location: attributedString.length - contactValue.length, length: contactValue.length)
            )
            cell?.lblContact?.attributedText = attributedString
            
            let distDict = providerData.dist
            let distanceNum = distDict["calculated"] as! Double
            let distanceNumInMeter = (distanceNum/1000)
            let distanceStr : String = String (format : "%.2f", distanceNumInMeter)
            let distanceStr1 = "\(distanceStr)km"
            
            attributedString = NSMutableAttributedString(string: distanceStr1)
            attributedString.addAttributes(
                [
                    NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 15)!,
                    NSForegroundColorAttributeName: UIColor(red: 83 / 255.0, green: 83 / 255.0, blue: 83 / 255.0, alpha: 1.0)
                ], range: NSRange(location: 0, length: attributedString.length-2)
            )
            
            attributedString.addAttributes(
                [
                    NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 15)!,
                    NSForegroundColorAttributeName: UIColor(red: 120 / 255.0, green: 120 / 255.0, blue: 120 / 255.0, alpha: 1.0)
                ], range: NSRange(location: attributedString.length-2, length: 2)
            )
            
            cell?.lblKm?.attributedText = attributedString
            
            if language == "en"
            {
                cell?.lblKm?.textAlignment = .right
            }
            else
            {
                cell?.lblKm?.textAlignment = .left
            }
            
            let ratings = providerData.ratings
            cell?.ratingView.rating = ratings
            
            let reviews_count = providerData.review_count
            cell?.lblReview?.text = "\(reviews_count) \(NSLocalizedString("REVIEWS", comment: ""))"
            
            if((USERDEFAULT.object(forKey: "USER_DATA") != nil))
            {
                cell?.lblReview?.isUserInteractionEnabled = true
            }
            else
            {
                cell?.lblReview?.isUserInteractionEnabled = false
            }
            
            let tap = UITapGestureRecognizer.init(target: self, action: #selector(reviewLabelClicked))
            cell?.lblReview?.addGestureRecognizer(tap)
            
            cell?.newServiceProviderLabel.text = CommonFunctions.getLocalizedString(localizedName: "New")
            
            let booking_compleated = providerData.booking_compleated
            if booking_compleated > 5
            {
                cell?.ratingView.isHidden = false
                cell?.lblReview?.isHidden = false
                
                cell?.newServiceProviderLabel.isHidden = true
            }
            else
            {
                cell?.ratingView.isHidden = true
                cell?.lblReview?.isHidden = true
                
                cell?.newServiceProviderLabel.isHidden = false
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell!;
        }
        else if(indexPath.row == 1)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarServices") as! CarServices?
            
            let service_name = serviceDictAdditional!["service_name"] as! [String : Any]
            let service_desc = serviceDictAdditional!["description"] as! [String : Any]
            
            cell?.lblServices.text = String (format : "\(NSLocalizedString("SERVICES_TEXT", comment: "")):")
            
            if(language.isEqual(to: "en"))
            {
                let name : String = service_name["en"] as! String
                cell?.lblTitle?.text = name.capitalized
                
                cell?.lblService?.text = service_desc["en"] as? String
            }
            else
            {
                cell?.lblTitle?.text = service_name["ar"] as? String
                cell?.lblService?.text = service_desc["ar"] as? String
            }
            
            let duration : Int = serviceDictAdditional!["duration"] as! Int
            let price1 : Int = serviceDictAdditional!["price"] as! Int
                
            let durationTxt : NSString = String (format : "\(NSLocalizedString("DURATION_TEXT", comment: "")):") as NSString
            let durationValue : NSString = String (format : "%dMIN", duration) as NSString
            let priceTxt : NSString = String (format : "\(NSLocalizedString("PRICE_TEXT", comment: "")):") as NSString
                
            let countryCodeCA = providerData.iso_code
            let localeIdCA = NSLocale.localeIdentifier(fromComponents: [ NSLocale.Key.countryCode.rawValue : countryCodeCA])
            let localeCA = NSLocale(localeIdentifier: localeIdCA)
            _ = localeCA.object(forKey: NSLocale.Key.currencySymbol)
            let currencyCodeCA = localeCA.object(forKey: NSLocale.Key.currencyCode)
                
            let price = String(format : "%@%d", currencyCodeCA as! String, price1)
                
            let str : String = String (format : "\(durationTxt) \(durationValue), \(priceTxt) \(price)")
            //print("str = \(str)")
                
            let attributedString = NSMutableAttributedString(string: str)
                
            attributedString.addAttributes(
                [
                    NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                    NSForegroundColorAttributeName: UIColor.uicolorFromRGB(130, 130, 130)
                ],
                range: NSRange(location: 0, length: durationTxt.length)
            )
                
            attributedString.addAttributes(
                [
                    NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                    NSForegroundColorAttributeName: UIColor.uicolorFromRGB(83, 83, 83)
                ],
                range: NSRange(location: durationTxt.length + 1, length: durationValue.length)
            )
                
            attributedString.addAttributes(
                [
                    NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                    NSForegroundColorAttributeName: UIColor.uicolorFromRGB(130, 130, 130)
                ],
                range: NSRange(location: durationTxt.length + durationValue.length + 2, length: priceTxt.length)
            )
                
            attributedString.addAttributes(
                [
                    NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                    NSForegroundColorAttributeName: UIColor.uicolorFromRGB(83, 83, 83)
                ],
                range: NSRange(location: durationTxt.length + durationValue.length + priceTxt.length + 3, length:price.count)
            )
                
            cell?.lblDuration?.attributedText = attributedString
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell!
        }
        else if(indexPath.row == 2)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimeDateCell") as! TimeDateCell?
            
            cell?.lblTimeAndDate.text = String (format : "\(NSLocalizedString("TIME_&_DATE", comment: "")) :")
            cell?.lblLoyaltyPoint.text = NSLocalizedString("LOYALTY_POINTS", comment: "").uppercased()
            cell?.txtPoints?.layer.cornerRadius = 5
            cell?.txtPoints?.layer.masksToBounds = true
            
            var str1 = timeDict?.value(forKey: "date")as! String
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd MMM yyyy"
            let date = formatter.date(from: str1)
            formatter.dateFormat = "dd MMM"
            str1 = formatter.string(from: date! )
            
            let str2 = timeDict?.value(forKey: "time")as! String
            
            cell?.lblTime?.text = str1 + ", " + str2
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell!
        }
        else if(indexPath.row == 3)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreCell") as! MoreCell?
            
            cell?.lblReviewAndRating.text = String (format : "\(NSLocalizedString("REVIEWS_&_RATING", comment: "")) :")
            
            cell?.btnMore?.setTitle(NSLocalizedString("MORE", comment: ""), for: UIControlState.normal)
            
            cell?.btnMore?.addTarget(self, action: #selector(showMoreReviews), for: UIControlEvents.touchUpInside)
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell!
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell") as! ReviewCell?
            
            cell?.imgUser?.layer.cornerRadius = (0.133 * SCREEN_WIDTH)/2
            cell?.imgUser?.layer.masksToBounds = true
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell!
        }
    }
    
    func coverImageClicked ()
    {
        let providerCoverImageVC = ProviderCoverImageVC()
        providerCoverImageVC.coverImagesArr = providerData.cover_image
        self.navigationController?.pushViewController(providerCoverImageVC, animated: true)
    }
    
    func reviewLabelClicked(sender : UITapGestureRecognizer)
    {
        let reviewlist = ReviewsListVC()
        reviewlist.reviewProviderId = providerData.provider_id
        self.navigationController?.pushViewController(reviewlist, animated: true)
    }
    
    // MARK: - Other Methods
    
    func showMoreReviews()
    {
        let reviewVC = ReviewsListVC()
        self.navigationController?.pushViewController(reviewVC, animated: true)
    }
    
    func callProvider(sender : UIButton)
    {
        let contact : String = String(format : "%@%@", providerData.country_code, providerData.mobile)
        
        CommonFunctions.callNumber(phoneNumber: contact)
    }
    
    func mailProvider(sender : UIButton)
    {
        var provider_name = ""
        if language == "en"
        {
            provider_name = providerData.provider_name_en
        }
        else
        {
            provider_name = providerData.provider_name_ar
        }
        
        if providerData.fcm_user_id != ""
        {
            let messages = MessagesVC()
            
            messages.selected_user_name = provider_name
            messages.selected_user_id = providerData.fcm_user_id
            messages.device_token_string = providerData.device_token
            
            self.navigationController?.pushViewController(messages, animated: true)
        }
    }
}
