//
//  ZoomImageVC.swift
//  CarWash
//
//  Created by iOS on 30/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class ZoomImageVC: UIViewController, UIScrollViewDelegate, EFImageViewZoomDelegate
{
    @IBOutlet var imageViewZoom: EFImageViewZoom!
    var image    : UIImage?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //invalidName
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "invalidName"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(closeView))
        
        imageViewZoom.image = image
        imageViewZoom._delegate = self
        imageViewZoom.contentMode = .center
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
    }
    
    func closeView()
    {
        self.dismiss(animated: true, completion: nil)
    }
}
