//
//  CustomAnnotation.swift
//  CarWash
//
//  Created by iOS on 26/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MapKit

class CustomAnnotation: NSObject, MKAnnotation
{
    var title : String?
    var subtitle : String?
    var dict = [Any]()
    var coordinate = CLLocationCoordinate2D()
}
