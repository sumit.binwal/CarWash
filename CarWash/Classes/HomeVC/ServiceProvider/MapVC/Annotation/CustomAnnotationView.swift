//
//  CustomAnnotationView.swift
//  CarWash
//
//  Created by iOS on 26/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MapKit

class CustomAnnotationView: MKAnnotationView
{
    var title = ""
    var subtitle = ""
    var dict = [Any]()
    var coordinate = CLLocationCoordinate2D()
    var mapPin: UIButton!
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
