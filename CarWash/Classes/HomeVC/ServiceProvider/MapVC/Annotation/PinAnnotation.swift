//
//  PinAnnotation.swift
//  CarWash
//
//  Created by iOS on 03/11/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MapKit

class PinAnnotation: NSObject, MKAnnotation
{
    var tag1 : Int!
    var userName : String?
    var distance : String?
    var imgUrl   : NSURL?
    var rating   : Double?
    var booking_compleated : Int = 0
    
    var dict = [Any]()
    var coordinate = CLLocationCoordinate2D()
}
