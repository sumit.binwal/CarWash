
//
//  ProviderDetailVC.swift
//  CarWash
//
//  Created by Neha Chaudhary on 21/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD

class ProviderDetailVC: UIViewController
{
   @IBOutlet weak var lblTotalCost: UILabel!
   @IBOutlet weak var btnNext: UIButton!
   @IBOutlet var tblProvider : UITableView?
   @IBOutlet weak var lblPrice : UILabel!
   
   var selectedService : [String : Any]?
   var timeDict : NSDictionary?
   
   var providerData = ProviderListModel()
   
   var selectedIndex = 0
    
   override func viewDidLoad()
   {
      super.viewDidLoad()

      lblTotalCost.text = String (format : "\(NSLocalizedString("TOTAL_COST", comment: "")) :")
      btnNext.setTitle(NSLocalizedString("NEXT_TEXT", comment: ""), for: UIControlState.normal)
      
      if language == "en"
      {
         lblPrice.textAlignment = .right
      }
      else
      {
         lblPrice.textAlignment = .left
      }
      
      setUpView()
   }

   override func viewWillAppear(_ animated: Bool)
   {
      super.viewWillAppear(animated)
        
      setUpNavBar()
      
      APPDELEGATE.tabBarController?.tabBar.isHidden = true
      APPDELEGATE.customView?.isHidden = true
      
      tblProvider?.setContentOffset(CGPoint.zero, animated: true)
   }
    
   override func viewWillDisappear(_ animated: Bool)
   {
      super.viewWillDisappear(animated)
        
      APPDELEGATE.tabBarController?.tabBar.isHidden = false
      APPDELEGATE.customView?.isHidden = false
   }
    
   override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
   }
    
   func setUpNavBar()
   {
      if language == "en"
      {
         self.title = providerData.provider_name_en.capitalized
      }
      else
      {
         self.title = providerData.provider_name_ar
      }

      UIApplication.shared.isStatusBarHidden = false
      
      self.navigationController?.navigationBar.isHidden = false
      self.navigationController?.navigationBar.isTranslucent = false
      
      navigationController?.navigationBar.titleTextAttributes =
         [
            NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
      ]
      
      self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
      
      //remove navigation bar bottom line
      let navigationBar = self.navigationController?.navigationBar
      navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
      navigationBar?.shadowImage = UIImage()
      
      //add shadow on navigation bar
      self.navigationController?.navigationBar.layer.masksToBounds = false
      self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
      self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
      self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
      
      self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex() == 0 ? "back" : "backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func setUpView()
    {
        tblProvider?.register(UINib(nibName: "DiscountCell", bundle: nil), forCellReuseIdentifier: "DiscountCell")
        tblProvider?.register(UINib(nibName: "ServiceCell", bundle: nil), forCellReuseIdentifier: "ServiceCell")
        tblProvider?.register(UINib(nibName: "PriceCell", bundle: nil), forCellReuseIdentifier: "PriceCell")
        
        tblProvider?.register(UINib(nibName: "CollapseCell", bundle: nil), forCellReuseIdentifier: "CollapseCell")
        tblProvider?.register(UINib(nibName: "CollapsibleTableHeader", bundle: nil), forCellReuseIdentifier: "CollapsibleTableHeader")
    }
   
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: @IBAction Methods
   
    @IBAction func  confirmBooking()
    {
         if lblPrice.text == "0"
         {
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("ALERT_SELECT_SERVICE", comment: ""), dismissBloack:
               {
                  
            })
         }
         else
         {
            if let additional_services = selectedService!["additional_services"] as? [[String : Any]]
            {
               if additional_services.count > 0
               {
                  let confirmVC = AdditionalServices()
                  
                  confirmVC.serviceDictAdditional = selectedService
                  confirmVC.providerData = providerData
                  confirmVC.timeDict = timeDict
                  
                  self.navigationController?.pushViewController(confirmVC, animated: true)
               }
               else
               {
                  let obj = ConfirmBookingVC()
                  
                  obj.serviceDictAdditional = selectedService
                  obj.providerData = providerData
                  obj.timeDict = timeDict
                  obj.totalCost = lblPrice.text
                  
                  let selectedAdditionalServiceAry : [[String : Any]] = []
                  obj.selectedAdditionalArr = selectedAdditionalServiceAry
                  
                  self.navigationController?.pushViewController(obj, animated: true)
               }
            }
         }
    }
}

extension ProviderDetailVC : UITableViewDataSource, UITableViewDelegate
{
   // MARK: - tableview delegates
   
   func numberOfSections(in tableView: UITableView) -> Int
   {
      return 1
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
   {
      if providerData.services.count > 0
      {
         if providerData.booking_compleated > 5
         {
            return providerData.services.count + 2
         }
         else
         {
            return providerData.services.count + 1
         }
      }
      
      return 0
   }
   
   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
   {
      return 0
   }
   
   func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
   {
      return 0
   }
   
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
   {
      if providerData.booking_compleated > 5
      {
         if(indexPath.row <= 1)
         {
            return indexPath.row == 0 ? 326 * scaleFactorX : 110 * scaleFactorX
         }
         else
         {
            return 66 * scaleFactorX
         }
      }
      else
      {
         if(indexPath.row <= 0)
         {
            return 326 * scaleFactorX
         }
         else
         {
            return 66 * scaleFactorX
         }
      }
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
   {
      if providerData.booking_compleated > 5
      {
         if(indexPath.row == 0)
         {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell") as! ServiceCell?
            
            cell?.btnCall?.addTarget(self, action: #selector(callProvider), for: .touchUpInside)
            
            cell?.ureadCountView.isHidden = true
            
            cell?.ureadCountView.layer.cornerRadius = (cell?.ureadCountView.frame.size.height)!/2
            cell?.ureadCountView.layer.borderColor = UIColor.clear.cgColor
            cell?.ureadCountView.layer.borderWidth = 1.0
            cell?.ureadCountView.layer.masksToBounds = true
            
            if((USERDEFAULT.object(forKey: "USER_DATA") != nil))
            {
               cell?.btnMail?.isHidden = false
               
               Conversation.showProviderUnReadCount(from_fcm_user_id: providerData.fcm_user_id, completion: { (unreadcount) in
                  
                  if unreadcount == "0"
                  {
                     cell?.ureadCountLabel.text = ""
                     cell?.ureadCountView.isHidden = true
                  }
                  else
                  {
                     cell?.ureadCountView.isHidden = false
                     cell?.ureadCountLabel.text = unreadcount
                  }
               })
            }
            else
            {
               cell?.btnMail?.isHidden = true
            }
            
            cell?.btnMail?.addTarget(self, action: #selector(mailProvider), for: .touchUpInside)
            
            cell?.imgLogo?.layer.cornerRadius  = ((0.14) * SCREEN_WIDTH)/2
            cell?.imgLogo?.layer.masksToBounds = true
            
            cell?.imgLogo?.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
            cell?.imgLogo?.layer.borderWidth = 1.0
            
            if language == "en"
            {
               cell?.lblName?.text = providerData.provider_name_en
            }
            else
            {
               cell?.lblName?.text = providerData.provider_name_ar
            }
            
            cell?.lblName?.text = cell?.lblName?.text?.uppercased()
            
            cell?.imgLogo?.setShowActivityIndicator(true)
            cell?.imgLogo?.setIndicatorStyle(.gray)
            
            cell?.imgService?.setShowActivityIndicator(true)
            cell?.imgService?.setIndicatorStyle(.gray)
            
            if providerData.cover_image.count > 0
            {
               let str1 = providerData.cover_image[0]
               cell?.imgService?.sd_setImage(with: URL.init(string: str1), placeholderImage: #imageLiteral(resourceName: "group29"))
            }
            else
            {
               cell?.imgService?.image = #imageLiteral(resourceName: "group29")
            }
            
            cell?.imgLogo?.sd_setImage(with: URL.init(string: providerData.provider_image), placeholderImage: #imageLiteral(resourceName: "group20"))
            
            cell?.btnImage?.addTarget(self, action: #selector(coverImageClicked), for: .touchUpInside)
            
            let contactTxt = NSLocalizedString("CONTACT_TEXT", comment: "") as NSString
            let contactValue  = String(format : "%@ %@", providerData.country_code, providerData.mobile) as NSString
            
            let contact : String = String(format : "\(contactTxt): %@ %@", providerData.country_code, providerData.mobile)
            
            var attributedString = NSMutableAttributedString(string: contact)
            attributedString.addAttributes(
               [
                  NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                  NSForegroundColorAttributeName: UIColor(red: 130 / 255.0, green: 130 / 255.0, blue: 130 / 255.0, alpha: 1.0)
               ], range: NSRange(location: 0, length: contactTxt.length+1)
            )
            
            attributedString.addAttributes(
               [
                  NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                  NSForegroundColorAttributeName: UIColor(red: 83 / 255.0, green: 83 / 255.0, blue: 83 / 255.0, alpha: 1.0)
               ], range: NSRange(location: attributedString.length - contactValue.length , length: contactValue.length)
            )
            cell?.lblContact?.attributedText = attributedString
            
            let distDict = providerData.dist
            let distanceNum = distDict["calculated"] as! Double
            let distanceNumInMeter = (distanceNum/1000)
            let distanceStr : String = String (format : "%.2f", distanceNumInMeter)
            let distanceStr1 = "\(distanceStr)km"
            
            attributedString = NSMutableAttributedString(string: distanceStr1)
            attributedString.addAttributes(
               [
                  NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 15)!,
                  NSForegroundColorAttributeName: UIColor(red: 83 / 255.0, green: 83 / 255.0, blue: 83 / 255.0, alpha: 1.0)
               ], range: NSRange(location: 0, length: attributedString.length-2)
            )
            
            attributedString.addAttributes(
               [
                  NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 15)!,
                  NSForegroundColorAttributeName: UIColor(red: 120 / 255.0, green: 120 / 255.0, blue: 120 / 255.0, alpha: 1.0)
               ], range: NSRange(location: attributedString.length-2, length: 2)
            )
            cell?.lblKm?.attributedText = attributedString
            
            if language == "en"
            {
               cell?.lblKm?.textAlignment = .right
            }
            else
            {
               cell?.lblKm?.textAlignment = .left
            }
            
            let ratings = providerData.ratings
            cell?.ratingView.rating = ratings
            
            let reviews_count = providerData.review_count
            cell?.lblReview?.text = "\(reviews_count) \(NSLocalizedString("REVIEWS", comment: ""))"
            
            if((USERDEFAULT.object(forKey: "USER_DATA") != nil))
            {
               cell?.lblReview?.isUserInteractionEnabled = true
            }
            else
            {
               cell?.lblReview?.isUserInteractionEnabled = false
            }
            
            let tap = UITapGestureRecognizer.init(target: self, action: #selector(reviewLabelClicked))
            cell?.lblReview?.addGestureRecognizer(tap)
            
            cell?.newServiceProviderLabel.text = CommonFunctions.getLocalizedString(localizedName: "New")
            
            if providerData.booking_compleated > 5
            {
               cell?.ratingView.isHidden = false
               cell?.lblReview?.isHidden = false
               
               cell?.newServiceProviderLabel.isHidden = true
            }
            else
            {
               cell?.ratingView.isHidden = true
               cell?.lblReview?.isHidden = true
               
               cell?.newServiceProviderLabel.isHidden = false
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell!;
         }
         else if(indexPath.row == 1)
         {
            var cell = tableView.dequeueReusableCell(withIdentifier: "PriceCell") as! PriceCell?
            
            if cell == nil
            {
               cell = PriceCell(style: UITableViewCellStyle.default, reuseIdentifier: "PriceCell")
            }
            
            cell?.priceRatingView.rating = providerData.price_rating
            cell?.qualityRatingView.rating = providerData.quality_rating
            cell?.timeRatingView.rating = providerData.time_rating
            
            cell?.lblPrice.text = NSLocalizedString("PRICE_TEXT", comment: "").capitalized
            cell?.lblTime.text = NSLocalizedString("TIME_TEXT", comment: "").capitalized
            cell?.lblQuality.text = NSLocalizedString("QUALITY_TEXT", comment: "").capitalized
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell!;
         }
         else
         {
            var cell = tableView.dequeueReusableCell(withIdentifier: "CollapsibleTableHeader") as! CollapsibleTableHeader?
            
            if cell == nil
            {
               cell = CollapsibleTableHeader(style: UITableViewCellStyle.default, reuseIdentifier: "CollapsibleTableHeader")
            }
            
            let service_name = providerData.services[indexPath.row - 2]["service_name"] as! [String : Any]
            
            if(language.isEqual(to: "en"))
            {
               let name : String = service_name["en"] as! String
               cell?.lblServiceName?.text = name.capitalized
            }
            else
            {
               cell?.lblServiceName?.text = service_name["ar"] as? String
            }
            
            let duration : Int = providerData.services[indexPath.row - 2]["duration"] as! Int
            
            let price1 : Int = providerData.services[indexPath.row - 2]["price"] as! Int
            
            let durationTxt : NSString = String (format : "\(NSLocalizedString("DURATION_TEXT", comment: "")):") as NSString
            let durationValue : NSString = String (format : "%dMIN", duration) as NSString
            let priceTxt : NSString = String (format : "\(NSLocalizedString("PRICE_TEXT", comment: "")):") as NSString
            
            let countryCodeCA = providerData.iso_code
            let localeIdCA = NSLocale.localeIdentifier(fromComponents: [ NSLocale.Key.countryCode.rawValue : countryCodeCA])
            let localeCA = NSLocale(localeIdentifier: localeIdCA)
            _ = localeCA.object(forKey: NSLocale.Key.currencySymbol)
            let currencyCodeCA = localeCA.object(forKey: NSLocale.Key.currencyCode)
            
            let price = String(format : "%@%d", currencyCodeCA as! String, price1)
            
            let str : String = String (format : "\(durationTxt) \(durationValue), \(priceTxt) \(price)")
            //print("str = \(str)")
            
            let attributedString = NSMutableAttributedString(string: str)
            
            attributedString.addAttributes(
               [
                  NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                  NSForegroundColorAttributeName: UIColor.uicolorFromRGB(130, 130, 130)
               ],
               range: NSRange(location: 0, length: durationTxt.length)
            )
            
            attributedString.addAttributes(
               [
                  NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                  NSForegroundColorAttributeName: UIColor.uicolorFromRGB(83, 83, 83)
               ],
               range: NSRange(location: durationTxt.length+1, length: durationValue.length)
            )
            
            attributedString.addAttributes(
               [
                  NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                  NSForegroundColorAttributeName: UIColor.uicolorFromRGB(130, 130, 130)
               ],
               range: NSRange(location:durationTxt.length+durationValue.length+3, length: priceTxt.length)
            )
            
            attributedString.addAttributes(
               [
                  NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                  NSForegroundColorAttributeName: UIColor.uicolorFromRGB(83, 83, 83)
               ],
               range: NSRange(location:durationTxt.length+durationValue.length+priceTxt.length+4, length:price.count)
            )
            
            cell?.lblDesc?.attributedText = attributedString
            
            if selectedIndex == indexPath.row
            {
               cell?.selectedImageView.image = #imageLiteral(resourceName: "checked")
               cell?.bgView?.backgroundColor = UIColor.uicolorFromRGB(209, 240, 255)
               selectedService = providerData.services[indexPath.row - 2]
               lblPrice.text = price
            }
            else
            {
               cell?.selectedImageView.image = #imageLiteral(resourceName: "plus_gray")
               cell?.bgView?.backgroundColor = UIColor.uicolorFromRGB(248, 248, 248)
               lblPrice.text = "0"
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell!
         }
      }
      else
      {
         if(indexPath.row == 0)
         {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell") as! ServiceCell?
            
            cell?.btnCall?.addTarget(self, action: #selector(callProvider), for: .touchUpInside)
            
            cell?.ureadCountView.isHidden = true
            
            cell?.ureadCountView.layer.cornerRadius = (cell?.ureadCountView.frame.size.height)!/2
            cell?.ureadCountView.layer.borderColor = UIColor.clear.cgColor
            cell?.ureadCountView.layer.borderWidth = 1.0
            cell?.ureadCountView.layer.masksToBounds = true
            
            if((USERDEFAULT.object(forKey: "USER_DATA") != nil))
            {
               cell?.btnMail?.isHidden = false
               
               Conversation.showProviderUnReadCount(from_fcm_user_id: providerData.fcm_user_id, completion: { (unreadcount) in
                  
                  if unreadcount == "0"
                  {
                     cell?.ureadCountLabel.text = ""
                     cell?.ureadCountView.isHidden = true
                  }
                  else
                  {
                     cell?.ureadCountView.isHidden = false
                     cell?.ureadCountLabel.text = unreadcount
                  }
               })
            }
            else
            {
               cell?.btnMail?.isHidden = true
            }
            
            cell?.btnMail?.addTarget(self, action: #selector(mailProvider), for: .touchUpInside)
            
            cell?.imgLogo?.layer.cornerRadius  = ((0.14) * SCREEN_WIDTH)/2
            cell?.imgLogo?.layer.masksToBounds = true
            
            cell?.imgLogo?.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
            cell?.imgLogo?.layer.borderWidth = 1.0
            
            if language == "en"
            {
               cell?.lblName?.text = providerData.provider_name_en
            }
            else
            {
               cell?.lblName?.text = providerData.provider_name_ar
            }
            
            cell?.lblName?.text = cell?.lblName?.text?.uppercased()
            
            cell?.imgLogo?.setShowActivityIndicator(true)
            cell?.imgLogo?.setIndicatorStyle(.gray)
            
            cell?.imgService?.setShowActivityIndicator(true)
            cell?.imgService?.setIndicatorStyle(.gray)
            
            if providerData.cover_image.count > 0
            {
               let str1 = providerData.cover_image[0]
               cell?.imgService?.sd_setImage(with: URL.init(string: str1), placeholderImage: #imageLiteral(resourceName: "group29"))
            }
            else
            {
               cell?.imgService?.image = #imageLiteral(resourceName: "group29")
            }
            
            cell?.imgLogo?.sd_setImage(with: URL.init(string: providerData.provider_image), placeholderImage: #imageLiteral(resourceName: "group20"))
            
            cell?.btnImage?.addTarget(self, action: #selector(coverImageClicked), for: .touchUpInside)
            
            let contactTxt = NSLocalizedString("CONTACT_TEXT", comment: "") as NSString
            let contactValue  = String(format : "%@ %@", providerData.country_code, providerData.mobile) as NSString
            
            let contact : String = String(format : "\(contactTxt): %@ %@", providerData.country_code, providerData.mobile)
            
            var attributedString = NSMutableAttributedString(string: contact)
            attributedString.addAttributes(
               [
                  NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                  NSForegroundColorAttributeName: UIColor(red: 130 / 255.0, green: 130 / 255.0, blue: 130 / 255.0, alpha: 1.0)
               ], range: NSRange(location: 0, length: contactTxt.length+1)
            )
            
            attributedString.addAttributes(
               [
                  NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                  NSForegroundColorAttributeName: UIColor(red: 83 / 255.0, green: 83 / 255.0, blue: 83 / 255.0, alpha: 1.0)
               ], range: NSRange(location: attributedString.length - contactValue.length , length: contactValue.length)
            )
            cell?.lblContact?.attributedText = attributedString
            
            let distDict = providerData.dist
            let distanceNum = distDict["calculated"] as! Double
            let distanceNumInMeter = (distanceNum/1000)
            let distanceStr : String = String (format : "%.2f", distanceNumInMeter)
            let distanceStr1 = "\(distanceStr)km"
            
            attributedString = NSMutableAttributedString(string: distanceStr1)
            attributedString.addAttributes(
               [
                  NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 15)!,
                  NSForegroundColorAttributeName: UIColor(red: 83 / 255.0, green: 83 / 255.0, blue: 83 / 255.0, alpha: 1.0)
               ], range: NSRange(location: 0, length: attributedString.length-2)
            )
            
            attributedString.addAttributes(
               [
                  NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 15)!,
                  NSForegroundColorAttributeName: UIColor(red: 120 / 255.0, green: 120 / 255.0, blue: 120 / 255.0, alpha: 1.0)
               ], range: NSRange(location: attributedString.length-2, length: 2)
            )
            cell?.lblKm?.attributedText = attributedString
            
            if language == "en"
            {
               cell?.lblKm?.textAlignment = .right
            }
            else
            {
               cell?.lblKm?.textAlignment = .left
            }
            
            let ratings = providerData.ratings
            cell?.ratingView.rating = ratings
            
            let reviews_count = providerData.review_count
            cell?.lblReview?.text = "\(reviews_count) \(NSLocalizedString("REVIEWS", comment: ""))"
            
            if((USERDEFAULT.object(forKey: "USER_DATA") != nil))
            {
               cell?.lblReview?.isUserInteractionEnabled = true
            }
            else
            {
               cell?.lblReview?.isUserInteractionEnabled = false
            }
            
            let tap = UITapGestureRecognizer.init(target: self, action: #selector(reviewLabelClicked))
            cell?.lblReview?.addGestureRecognizer(tap)
            
            cell?.newServiceProviderLabel.text = CommonFunctions.getLocalizedString(localizedName: "New")
            
            if providerData.booking_compleated > 5
            {
               cell?.ratingView.isHidden = false
               cell?.lblReview?.isHidden = false
               
               cell?.newServiceProviderLabel.isHidden = true
            }
            else
            {
               cell?.ratingView.isHidden = true
               cell?.lblReview?.isHidden = true
               
               cell?.newServiceProviderLabel.isHidden = false
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell!;
         }
         else
         {
            var cell = tableView.dequeueReusableCell(withIdentifier: "CollapsibleTableHeader") as! CollapsibleTableHeader?
            
            if cell == nil
            {
               cell = CollapsibleTableHeader(style: UITableViewCellStyle.default, reuseIdentifier: "CollapsibleTableHeader")
            }
            
            let service_name = providerData.services[indexPath.row - 1]["service_name"] as! [String : Any]
            
            if(language.isEqual(to: "en"))
            {
               let name : String = service_name["en"] as! String
               cell?.lblServiceName?.text = name.capitalized
            }
            else
            {
               cell?.lblServiceName?.text = service_name["ar"] as? String
            }
            
            let duration : Int = providerData.services[indexPath.row - 1]["duration"] as! Int
            
            let price1 : Int = providerData.services[indexPath.row - 1]["price"] as! Int
            
            let durationTxt : NSString = String (format : "\(NSLocalizedString("DURATION_TEXT", comment: "")):") as NSString
            let durationValue : NSString = String (format : "%dMIN", duration) as NSString
            let priceTxt : NSString = String (format : "\(NSLocalizedString("PRICE_TEXT", comment: "")):") as NSString
            
            let countryCodeCA = providerData.iso_code
            let localeIdCA = NSLocale.localeIdentifier(fromComponents: [ NSLocale.Key.countryCode.rawValue : countryCodeCA])
            let localeCA = NSLocale(localeIdentifier: localeIdCA)
            _ = localeCA.object(forKey: NSLocale.Key.currencySymbol)
            let currencyCodeCA = localeCA.object(forKey: NSLocale.Key.currencyCode)
            
            let price = String(format : "%@%d", currencyCodeCA as! String, price1)
            
            let str : String = String (format : "\(durationTxt) \(durationValue), \(priceTxt) \(price)")
            //print("str = \(str)")
            
            let attributedString = NSMutableAttributedString(string: str)
            
            attributedString.addAttributes(
               [
                  NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                  NSForegroundColorAttributeName: UIColor.uicolorFromRGB(130, 130, 130)
               ],
               range: NSRange(location: 0, length: durationTxt.length)
            )
            
            attributedString.addAttributes(
               [
                  NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                  NSForegroundColorAttributeName: UIColor.uicolorFromRGB(83, 83, 83)
               ],
               range: NSRange(location: durationTxt.length+1, length: durationValue.length)
            )
            
            attributedString.addAttributes(
               [
                  NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                  NSForegroundColorAttributeName: UIColor.uicolorFromRGB(130, 130, 130)
               ],
               range: NSRange(location:durationTxt.length+durationValue.length+3, length: priceTxt.length)
            )
            
            attributedString.addAttributes(
               [
                  NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                  NSForegroundColorAttributeName: UIColor.uicolorFromRGB(83, 83, 83)
               ],
               range: NSRange(location:durationTxt.length+durationValue.length+priceTxt.length+4, length:price.count)
            )
            
            cell?.lblDesc?.attributedText = attributedString
            
            if selectedIndex == indexPath.row
            {
               cell?.selectedImageView.image = #imageLiteral(resourceName: "checked")
               cell?.bgView?.backgroundColor = UIColor.uicolorFromRGB(209, 240, 255)
               selectedService = providerData.services[indexPath.row - 1]
               lblPrice.text = price
            }
            else
            {
               cell?.selectedImageView.image = #imageLiteral(resourceName: "plus_gray")
               cell?.bgView?.backgroundColor = UIColor.uicolorFromRGB(248, 248, 248)
               lblPrice.text = "0"
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell!
         }
      }
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
   {
      if let cell = tableView.cellForRow(at: indexPath) as? CollapsibleTableHeader
      {
         if cell.selectedImageView.image == #imageLiteral(resourceName: "checked")
         {
            selectedIndex = 0
         }
         else
         {
            selectedIndex = indexPath.row
         }
         
         self.tblProvider?.reloadData()
      }
   }
   
   func coverImageClicked ()
   {
      let providerCoverImageVC = ProviderCoverImageVC()
      providerCoverImageVC.coverImagesArr = providerData.cover_image
      self.navigationController?.pushViewController(providerCoverImageVC, animated: true)
   }
   
   func reviewLabelClicked(sender : UITapGestureRecognizer)
   {
      let reviewlist = ReviewsListVC()
      reviewlist.reviewProviderId = providerData.provider_id
      self.navigationController?.pushViewController(reviewlist, animated: true)
   }
   
   // MARK: - Other Methods
   func callProvider(sender : UIButton)
   {
      let contact : String = String(format : "%@%@", providerData.country_code, providerData.mobile)
      CommonFunctions.callNumber(phoneNumber: contact)
   }
   
   func mailProvider(sender : UIButton)
   {
      var provider_name = ""
      if language == "en"
      {
         provider_name = providerData.provider_name_en
      }
      else
      {
         provider_name = providerData.provider_name_ar
      }
      
      if providerData.fcm_user_id != ""
      {
         let messages = MessagesVC()
         
         messages.selected_user_name = provider_name
         messages.selected_user_id = providerData.fcm_user_id
         messages.device_token_string = providerData.device_token
         
         self.navigationController?.pushViewController(messages, animated: true)
      }
   }
}
