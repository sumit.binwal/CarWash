//
//  ProviderSearchVC.swift
//  CarWash
//
//  Created by Ratina on 5/2/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD

class ProviderSearchVC: UIViewController
{
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var noResultLabel: UILabel!
    @IBOutlet weak var searchTableView: UITableView!
    
    @IBOutlet weak var navigationView: UIView!
    
    var providerListDataArr = [ProviderListModel]()
    
    var searchString = ""
    
    var serviceDict : NSDictionary?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        searchTextField.delegate = self
        searchTextField.isHidden = true
        
        searchTableView.isHidden = true
        noResultLabel.isHidden = true
        
        searchTableView?.register(UINib(nibName: "ProviderCell", bundle: nil), forCellReuseIdentifier: "ProviderCell")
        
        if (LanguageManager.currentLanguageIndex() == 0)
        {
            searchTextField?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            searchTextField?.textAlignment = NSTextAlignment.left
            
            backButton.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        }
        else
        {
            searchTextField?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            searchTextField?.textAlignment = NSTextAlignment.right
            
            backButton.setImage(#imageLiteral(resourceName: "backRight"), for: .normal)
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        //add shadow on navigation bar
        navigationView.layer.masksToBounds = false
        navigationView.layer.shadowColor = UIColor.darkGray.cgColor
        navigationView.layer.shadowOpacity = 0.2
        navigationView.layer.shadowRadius = 0.0
        navigationView.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        titleLabel.text = NSLocalizedString("Service Provider's List", comment: "")
        
        searchTextField.placeholder = NSLocalizedString("SEARCH_TEXT", comment: "")
        
        self.navigationController?.isNavigationBarHidden = true
        
        UIApplication.shared.isStatusBarHidden = false
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - On Search Button Action
    @IBAction func searchButtonAction(_ sender: Any)
    {
        searchTextField.isHidden = false
        
        searchBtn.isHidden = true
        titleLabel.isHidden = true
        
        searchTextField.placeholder = NSLocalizedString("SEARCH_TEXT", comment: "")
        
        searchTextField.becomeFirstResponder()
    }
    
    //MARK; - on Back Button Action
    @IBAction func backButtonAction(_ sender: Any)
    {
        if searchTextField.isHidden
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.view.endEditing(true)
           
            searchTextField.isHidden = true
            
            searchBtn.isHidden = false
            titleLabel.isHidden = false
            
            searchTextField.text = ""
            searchString = ""
        }
    }
    
    // MARK: API Call
    func serviceProvidersListingApi()
    {
        let formatter = DateFormatter()
        
        var str1 = serviceDict?.value(forKey: "date")as! String
        formatter.dateFormat = "dd MMM yyyy"
        var date = formatter.date(from: str1)
        formatter.dateFormat = "YYYY-MM-dd"
        str1 = formatter.string(from: date!)
        
        var str2 = serviceDict?.value(forKey: "time")as! String
        // split string into array
        var fullNameArr = str2.components(separatedBy: "-")
        str2 = fullNameArr[0]
        
        formatter.dateFormat = "hh:mm a"
        date = formatter.date(from: str2)
        formatter.dateFormat = "HH:mm"
        str2 = formatter.string(from: date!)
        
        var type : NSString = ""
        let str = serviceDict?.value(forKey: "type") as? String
        
        if(str?.isEqual(NSLocalizedString("SMALL_CARS", comment: "")))!
        {
            type = "small"
        }
        else if(str?.isEqual(NSLocalizedString("MEDIUM_CARS", comment: "")))!
        {
            type = "medium"
        }
        else if(str?.isEqual(NSLocalizedString("BIG_CARS", comment: "")))!
        {
            type = "big"
        }
        
        let dic : [String : Any] =
            [
                "latitude"          : userLattitude,
                "longitude"         : userLongtitude,
                "car_type"          : type,
                "date"              : str1,
                "time"              : str2,
                "provider_name"     : searchString
        ]
        
        print("dict = \(dic)")
        
        let url = BASE_URL + "home"
        
        AppWebHandler.sharedInstance().fetchData(fromURL: URL.init(string: url), httpMethod: .post, parameters: dic, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            guard self != nil else {return}
            
            if error != nil
            {
                AlertViewController.showAlertWith(title: "", message: (error?.localizedDescription)!, dismissBloack:
                    {
                        
                })
                
                return
            }
            
            if dictionary == nil
            {
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let servicesDtaArr1 = dictionary!["data"] as? [[String : Any]]
                    
                    guard servicesDtaArr1 != nil else
                    {
                        return
                    }
                    
                    self?.updateProviderListModelArray(usingArray: servicesDtaArr1!)
                }
                else
                {
                    AlertViewController.showAlertWith(title: "", message: (responseDictionary["msg"] as? String)!, dismissBloack:
                        {
                            
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    AlertViewController.showAlertWith(title: "", message: replyMsg, dismissBloack:
                        {
                            
                    })
                }
            }
        }
    }
    
    //MARK:- Updating provider list Model Array
    func updateProviderListModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ProviderListModel () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            providerListDataArr.append(model) // Adding model to array
        }
        
        if(self.providerListDataArr.count > 0)
        {
            self.noResultLabel?.isHidden = true
            self.searchTableView?.isHidden = false
            self.searchTableView?.reloadData()
        }
        else
        {
            self.noResultLabel?.isHidden = false
            self.searchTableView?.isHidden = true
        }
    }
}

//Extension -> UITextField -> UITextFieldDelegate
extension ProviderSearchVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField.text?.isEmpty)!
        {

        }
        else
        {
            providerListDataArr.removeAll()
            serviceProvidersListingApi()
        }
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool
    {
        textField.text = ""
        
        providerListDataArr.removeAll()
        
        searchTableView.reloadData()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92)
        {
            if textField.text?.count == 1
            {
                textField.text = ""
                
                providerListDataArr.removeAll()
             
                searchTableView.reloadData()
            }
        }
        
        var newString : String = ""
        
        if string.count == 0
        {
            if textField.text?.count == 0
            {
                newString = textField.text!
            }
            else
            {
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            var newStr = textField.text! as NSString
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            newString = newStr as String
        }
        
        print("newString = ", newString)
        
        if newString.count >= 3
        {
            searchString = newString
            providerListDataArr.removeAll()
            serviceProvidersListingApi()
        }
        
        return true
    }
}

//MARK: - Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension ProviderSearchVC : UITableViewDelegate, UITableViewDataSource
{
    // MARK: - tableview delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  providerListDataArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return  SCREEN_WIDTH * 0.72
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProviderCell") as! ProviderCell?
        
        cell?.imgLogo?.layer.cornerRadius  = ((0.14) * SCREEN_WIDTH)/2
        cell?.imgLogo?.layer.masksToBounds = true
        
        cell?.imgLogo?.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
        cell?.imgLogo?.layer.borderWidth = 1.0
        
        cell?.btnCall?.tag = indexPath.row
        cell?.btnCall?.addTarget(self, action: #selector(callProvider), for: .touchUpInside)
        
        cell?.unreadCountView.isHidden = true
        
        cell?.unreadCountView.layer.cornerRadius = (cell?.unreadCountView.frame.size.height)!/2
        cell?.unreadCountView.layer.borderColor = UIColor.clear.cgColor
        cell?.unreadCountView.layer.borderWidth = 1.0
        cell?.unreadCountView.layer.masksToBounds = true
        
        if((USERDEFAULT.object(forKey: "USER_DATA") != nil))
        {
            cell?.btnMail?.isHidden = false
            
            Conversation.showProviderUnReadCount(from_fcm_user_id: providerListDataArr[indexPath.row].fcm_user_id, completion: { (unreadcount) in
                    
                //print("unread count = ", unreadcount)
                    
                if unreadcount == "0"
                {
                    cell?.unreadCountLabel.text = ""
                    cell?.unreadCountView.isHidden = true
                }
                else
                {
                    cell?.unreadCountView.isHidden = false
                    cell?.unreadCountLabel.text = unreadcount
                }
            })
        }
        else
        {
            cell?.btnMail?.isHidden = true
        }
        
        cell?.btnMail?.tag = indexPath.row
        cell?.btnMail?.addTarget(self, action: #selector(mailProvider), for: .touchUpInside)
        
        if language == "en"
        {
            cell?.lblName?.text = providerListDataArr[indexPath.row].provider_name_en
        }
        else
        {
            cell?.lblName?.text = providerListDataArr[indexPath.row].provider_name_ar
        }
        
        cell?.lblName?.text = cell?.lblName?.text?.uppercased()
        
        cell?.imgService?.setShowActivityIndicator(true)
        cell?.imgService?.setIndicatorStyle(.gray)
        
        cell?.imgLogo?.setShowActivityIndicator(true)
        cell?.imgLogo?.setIndicatorStyle(.gray)
        
        if providerListDataArr[indexPath.row].cover_image.count > 0
        {
            let str1 = providerListDataArr[indexPath.row].cover_image[0]
            cell?.imgService?.sd_setImage(with: URL.init(string: str1), placeholderImage: #imageLiteral(resourceName: "group29"))
        }
        else
        {
            cell?.imgService?.image = #imageLiteral(resourceName: "group29")
        }
        
        cell?.imgLogo?.sd_setImage(with: URL.init(string: providerListDataArr[indexPath.row].provider_image), placeholderImage: #imageLiteral(resourceName: "group20"))
        
        let contact = NSLocalizedString("CONTACT_TEXT", comment: "") as NSString
        let number : NSString = String(format : "%@ %@", providerListDataArr[indexPath.row].country_code, providerListDataArr[indexPath.row].mobile) as NSString
        
        let contactAttribute : String = String(format : "\(contact): %@",number)
        
        var attributedString = NSMutableAttributedString(string: contactAttribute)
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                NSForegroundColorAttributeName: UIColor(red: 130 / 255.0, green: 130 / 255.0, blue: 130 / 255.0, alpha: 1.0)
            ], range: NSRange(location: 0, length:contact.length)
        )
        
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                NSForegroundColorAttributeName: UIColor(red: 83 / 255.0, green: 83 / 255.0, blue: 83 / 255.0, alpha: 1.0)
            ], range: NSRange(location: attributedString.length - number.length, length: number.length)
        )
        cell?.lblContact?.attributedText = attributedString
        
        let distDict = providerListDataArr[indexPath.row].dist
        let distanceNum = distDict["calculated"] as! Double
        let distanceNumInMeter = (distanceNum/1000)
        let distanceStr : String = String (format : "%.2f", distanceNumInMeter)
        let distanceStr1 = "\(distanceStr)km"
        
        attributedString = NSMutableAttributedString(string: distanceStr1)
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 15)!,
                NSForegroundColorAttributeName: UIColor(red: 83 / 255.0, green: 83 / 255.0, blue: 83 / 255.0, alpha: 1.0)
            ], range: NSRange(location: 0, length: attributedString.length-2)
        )
        
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 15)!,
                NSForegroundColorAttributeName: UIColor(red: 120 / 255.0, green: 120 / 255.0, blue: 120 / 255.0, alpha: 1.0)
            ], range: NSRange(location: attributedString.length-2, length: 2)
        )
        cell?.lblKm?.attributedText = attributedString
        
        if language == "en" {
            cell?.lblKm?.textAlignment = .right
        } else {
            cell?.lblKm?.textAlignment = .left
        }
        
        let ratings = providerListDataArr[indexPath.row].ratings
        cell?.ratingView.rating = ratings
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        
        let reviews_count = providerListDataArr[indexPath.row].review_count
        cell?.lblReview?.text = "\(reviews_count) \(NSLocalizedString("REVIEWS", comment: ""))"
        
        if((USERDEFAULT.object(forKey: "USER_DATA") != nil))
        {
            cell?.lblReview?.isUserInteractionEnabled = true
        }
        else
        {
            cell?.lblReview?.isUserInteractionEnabled = false
        }
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(reviewLabelClicked))
        cell?.lblReview?.tag = indexPath.row
        cell?.lblReview?.addGestureRecognizer(tap)
        
        cell?.newProviderLabel.text = CommonFunctions.getLocalizedString(localizedName: "New")
        
        let booking_compleated = providerListDataArr[indexPath.row].booking_compleated
        if booking_compleated > 5
        {
            cell?.ratingView.isHidden = false
            cell?.lblReview?.isHidden = false
            
            cell?.newProviderLabel.isHidden = true
        }
        else
        {
            cell?.ratingView.isHidden = true
            cell?.lblReview?.isHidden = true
            
            cell?.newProviderLabel.isHidden = false
        }
        
        return cell!;
    }
    
    func  tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let detailVC = ProviderDetailVC()
        
        detailVC.timeDict = serviceDict
        detailVC.providerData = providerListDataArr[indexPath.row]
        
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func reviewLabelClicked(sender : UITapGestureRecognizer)
    {
        let tag = sender.view?.tag
        
        let reviewlist = ReviewsListVC()
        reviewlist.reviewProviderId = providerListDataArr[tag!].provider_id
        self.navigationController?.pushViewController(reviewlist, animated: true)
    }
    
    // MARK: - Other Methods
    
    func callProvider(sender : UIButton)
    {
        let tag = sender.tag
        let contact : String = String(format : "%@%@", providerListDataArr[tag].country_code, providerListDataArr[tag].mobile)
        
        CommonFunctions.callNumber(phoneNumber: contact)
    }
    
    func mailProvider(sender : UIButton)
    {
        let tag = sender.tag
        
        var provider_name = ""
        if language == "en"
        {
            provider_name = providerListDataArr[tag].provider_name_en
        }
        else
        {
            provider_name = providerListDataArr[tag].provider_name_ar
        }
        
        if providerListDataArr[tag].fcm_user_id == ""
        {
            print("fcm user id is not available for sender")
        }
        else
        {
            let messages = MessagesVC()
            
            messages.selected_user_name = provider_name
            messages.selected_user_id = providerListDataArr[tag].fcm_user_id
            messages.device_token_string = providerListDataArr[tag].device_token
            
            self.navigationController?.pushViewController(messages, animated: true)
        }
    }
}
