//
//  ConcersationsVC.swift
//  CarWash
//
//  Created by iOS on 10/01/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit
import Firebase
import MBProgressHUD

class ConcersationsVC: UIViewController {

    @IBOutlet weak var noConversationLabel: UILabel!
    @IBOutlet weak var concersationTableview: UITableView!
    
    var conversationsArr = [Conversation]()
    var dictUser = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        concersationTableview?.register(UINib(nibName: "ConversationCell", bundle: nil), forCellReuseIdentifier: "ConversationCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
        
        noConversationLabel.isHidden = true
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        dictUser = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        
        setUpNavigationBar()
        
        self.concersationTableview.isHidden = true
        
        self.conversationsArr.removeAll()
        print("conversationsArr = \(self.conversationsArr.count)")
        fetchConversationsData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchConversationsData()
    {
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        Conversation.showConversations(completion: {[weak weakSelf = self] (conversations) in
            
            weakSelf?.conversationsArr = conversations
            
            print("weakSelf?.items = \(weakSelf?.conversationsArr.count ?? 0)")
            
            self.concersationTableview.isHidden = true
            
            DispatchQueue.main.async {
            
                if let state = weakSelf?.conversationsArr.isEmpty, state == false {
            
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    self.concersationTableview.isHidden = false
                    self.noConversationLabel.isHidden = true
            
                    self.concersationTableview?.reloadData()
                    
                    weakSelf?.concersationTableview?.scrollToRow(at: IndexPath.init(row: self.conversationsArr.count - 1, section: 0), at: .bottom, animated: false)
                }
                else
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    self.noConversationLabel.isHidden = false
                    self.concersationTableview.isHidden = true
                }
            }
        })
    }
    
    func setUpNavigationBar()
    {
        self.title = NSLocalizedString("MESSAGE_TEXT", comment: "")
        self.noConversationLabel.text = NSLocalizedString("No messages available", comment: "")
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension ConcersationsVC : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.conversationsArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60/320 * SCREEN_WIDTH
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "ConversationCell") as! ConversationCell?
        
        if cell == nil
        {
            cell = ConversationCell(style: UITableViewCellStyle.default, reuseIdentifier: "ConversationCell")
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        
        print("self.conversationsArr = \(self.conversationsArr.count)")
        
        if self.conversationsArr.count > 0 {
            cell?.nameLabel.text = self.conversationsArr[indexPath.row].user.name
            
            cell?.messageLabel.text = self.conversationsArr[indexPath.row].lastMessage.content as? String
            
            var unReadCount = 0
            let messagesArr = self.conversationsArr[indexPath.row].messagesArr
            
            for i in 0..<messagesArr.count {
                
                let message = messagesArr[i]
                
                if message.owner == .sender {
                    
                    let unRead = message.isRead
                    if unRead == false {
                        unReadCount += 1
                    }
                }
            }
            
            print("unreadcount = \(unReadCount)")
            
            if unReadCount == 0 {
                cell?.unReadLabel.isHidden = true
            } else {
                cell?.unReadLabel.isHidden = false
                cell?.unReadLabel.text = "\(unReadCount)"
            }
            
            let customer1 = CustomerService()
            
            cell?.profilePic?.setIndicatorStyle(.gray)
            cell?.profilePic?.setShowActivityIndicator(true)
            
            customer1.getConversationImageRequestWithParameters(nil, "\(self.conversationsArr[indexPath.row].user.id)" as NSString, success: { (response, data) in
                
                //print("data = \(String(describing: data))")
                
                if data?.object(forKey: "status") as! String == "success"
                {
                    let data_image = data?.object(forKey: "data") as! NSDictionary
                    let link_image = data_image.object(forKey: "avatar") as! String
                    let url_image = URL.init(string: link_image)
                    
                    DispatchQueue.main.async
                        {
                            cell?.profilePic?.sd_setImage(with: url_image, placeholderImage: #imageLiteral(resourceName: "group20"))
                    }
                }
            }) { (response, error) in
                print("error = \(String(describing: error))")
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.locale = NSLocale.current
            
            let time:Int? = self.conversationsArr[indexPath.row].lastMessage.timestamp
            
            var timeStamp = String(time!)
            
            if (timeStamp.count) > 10 {
                
                let index2 = (timeStamp.index((timeStamp.startIndex), offsetBy: 10))
                let indexStart = index2
                
                let indexEnd = (timeStamp.endIndex)
                
                timeStamp.removeSubrange(indexStart ..< indexEnd)
            }
            
            let epocTime = TimeInterval(timeStamp)
            let date = Date(timeIntervalSince1970:epocTime!)
            
            let timeString = CommonFunctions.timeStringAgoSinceDate(date, currentDate: Date(), numericDates: false)
            //print("timeString = \(timeString)")
            
            if timeString == "Today" {
                dateFormatter.dateFormat = "hh:mm a"
                cell?.timeLabel.text = dateFormatter.string(from: date)
            } else if timeString == "Yesterday" {
                cell?.timeLabel.text = "Yesterday"
            } else {
                dateFormatter.dateFormat = "dd MMM yyyy"
                cell?.timeLabel.text = dateFormatter.string(from: date)
            }
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user_id = self.conversationsArr[indexPath.row].user.id
        let user_name = self.conversationsArr[indexPath.row].user.name
        
        print("user id = \(user_id)")
        
        let messges = MessagesVC()
        
        messges.selected_user_id = user_id
        messges.selected_user_name = user_name
        
        self.navigationController?.pushViewController(messges, animated: true)
    }
}
