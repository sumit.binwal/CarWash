//
//  HistoryVC.swift
//  CarWash
//
//  Created by Neha Chaudhary on 15/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD

class HistoryVC: UIViewController
{
    @IBOutlet var tblHistory    : UITableView?
    @IBOutlet var searchView    : UIView?
    @IBOutlet var searchBar     : UISearchBar?

    @IBOutlet weak var notificationBgImage: UIImageView!
    @IBOutlet var notificationView   : UIView?
    @IBOutlet var tblNotification    : UITableView?
    @IBOutlet weak var labelNoNotification: UILabel!
    
    var bell : UIBarButtonItem?
    var filter : UIBarButtonItem?
    
    @IBOutlet var lblNoResult   : UILabel?
    var  historyAry             : NSMutableArray = []
    
    var notificatioDataAry : NSMutableArray = []
    
    @IBOutlet weak var tableHistoryTopConstraint: NSLayoutConstraint!
    
    let refreshControl = UIRefreshControl()
    
    var pageNumber = 1
    
    var isPageRefresh = false
    
    @IBOutlet var filterView: UIView!
    @IBOutlet weak var filterTableView: UITableView!
    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet var filterFooterView: UIView!
    
    var filterArr : [[String:Any]] = []
    
    var selectedFilterArr : [String] = []
    
    let formatter = DateFormatter()
    
    var date_string = ""
    
    var datePicker = UIDatePicker()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        lblNoResult?.isHidden = true
        searchView?.isHidden = true
        
        tblHistory?.register(UINib(nibName: "HistoryCell", bundle: nil), forCellReuseIdentifier: "HistoryCell")
        
        tblNotification?.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        
        filterTableView?.register(UINib(nibName: "filterProviderTableViewCell", bundle: nil), forCellReuseIdentifier: "filterProviderTableViewCell")
        
        filterTableView?.register(UINib(nibName: "SelectDateTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectDateTableViewCell")
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            tblHistory?.refreshControl = refreshControl
        } else {
            tblHistory?.addSubview(refreshControl)
        }
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshHomeData(_:)), for: .valueChanged)
        
        if language == "en" {
            notificationBgImage.image = #imageLiteral(resourceName: "popoverBg")
        } else {
            notificationBgImage.image = #imageLiteral(resourceName: "popoverBg_ar")
        }
        
        // pending=1, accept=2, reject=3, inprocess=4, complete=5, reached=6, cancel by user=7
        
        submitButton.setTitle(NSLocalizedString("DONE", comment: ""), for: .normal)
        
        var dict = [
            "image" : CommonFunctions.getLocalizedString(localizedName: "Pending"),
            "name" : "1"
        ]
        
        filterArr.append(dict)
        
        dict = [
            "image" : CommonFunctions.getLocalizedString(localizedName: "Accepted"),
            "name" : "2"
        ]
        
        filterArr.append(dict)
        
        dict = [
            "image" : CommonFunctions.getLocalizedString(localizedName: "Progress"),
            "name" : "4"
        ]
        
        filterArr.append(dict)
        
        dict = [
            "image" : CommonFunctions.getLocalizedString(localizedName: "Reached"),
            "name" : "6"
        ]
        
        filterArr.append(dict)
        
        dict = [
            "image" : CommonFunctions.getLocalizedString(localizedName: "Completed"),
            "name" : "5"
        ]
        
        filterArr.append(dict)
        
        dict = [
            "image" : CommonFunctions.getLocalizedString(localizedName: "Canceled"),
            "name" : "7"
        ]
        
        filterArr.append(dict)
        
        dict = [
            "image" : CommonFunctions.getLocalizedString(localizedName: "Rejected"),
            "name" : "3"
        ]
        
        filterArr.append(dict)
        
        datePicker.datePickerMode = .date
        formatter.dateFormat = "dd MMM yyyy"
        
        datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let cellDate = filterTableView.cellForRow(at: IndexPath(row: filterArr.count + 1, section: 0)) as! SelectDateTableViewCell
        
        formatter.dateFormat = "dd MMM yyyy"
        
        cellDate.dateTextField.text = formatter.string(from: (datePicker.date))
        
        formatter.dateFormat = "yyyy-MM-dd"
        
        date_string = formatter.string(from: (datePicker.date))
        print("date = ", date_string)
    }
    
    func refreshHomeData(_ sender: Any) {
        // Fetch Weather Data
        
        pageNumber = 1
        self.isPageRefresh = true
        self.historyAry = []
        
        if searchBar?.text == "" {
            getHistorySearchData(searchString: "")
        } else {
            getHistorySearchData(searchString: (searchBar?.text)!)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector (doneAction), name: NSNotification.Name(rawValue: "doneAction"), object: nil)
        
        self.searchBar?.text = ""
        searchView?.isHidden = true
        
        setUpNavigationBar()
        
        searchView?.layer.shadowColor = UIColor.lightGray.cgColor
        searchView?.layer.shadowOffset = CGSize(width: 3, height: 3)
        searchView?.layer.shadowOpacity = 0.2
        searchView?.layer.shadowRadius = 3.0
        searchView?.layer.masksToBounds = false
        
        searchBar?.placeholder = NSLocalizedString("SEARCH_TEXT", comment: "")
        searchBar?.setValue(NSLocalizedString("CANCEL_TEXT", comment: ""), forKey:"_cancelButtonText")
        
        let textFieldInsideUISearchBar = searchBar?.value(forKey: "searchField") as? UITextField
        
        if (LanguageManager.currentLanguageIndex() == 0)
        {
            textFieldInsideUISearchBar?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            textFieldInsideUISearchBar?.textAlignment = NSTextAlignment.left
        }
        else
        {
            textFieldInsideUISearchBar?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            textFieldInsideUISearchBar?.textAlignment = NSTextAlignment.right
        }
        
        lblNoResult?.text = NSLocalizedString("NO_HISTORY", comment: "")
        labelNoNotification?.text = NSLocalizedString("NO_NOTIFICATION", comment: "")
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        isPageRefresh = true
        pageNumber = 1
        getHistorySearchData(searchString: "")
    }

    func setUpNavigationBar()
    {
        self.title = NSLocalizedString("HISTORY_TITLE", comment: "")
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.hidesBackButton = true
        
        bell = UIBarButtonItem(image:  #imageLiteral(resourceName: "bell"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(bellButtonPressed))
        
        filter = UIBarButtonItem(image:  #imageLiteral(resourceName: "menu"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(filterButtonPressed))
        
        self.navigationItem.rightBarButtonItems = [bell!, filter!]
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func bellButtonPressed()
    {
        self.searchBar?.endEditing(true)
        searchBar?.setShowsCancelButton(false, animated: true)
        showPopOver()
    }
    
    func showPopOver()
    {
        notificationView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
        bell?.isEnabled = false
        
        self.labelNoNotification.isHidden = true
        self.tblNotification?.isHidden = true
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        notificationApi()
        
        APPDELEGATE.window?.addSubview(notificationView!)
    }
    
    func filterButtonPressed()
    {
        self.searchBar?.endEditing(true)
        searchBar?.setShowsCancelButton(false, animated: true)
        
        showFilterPopUp()
    }
    
    func showFilterPopUp()
    {
        filterView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
        
        bell?.isEnabled = false
        filter?.isEnabled = false
        
        self.filterTableView?.tableFooterView = self.filterFooterView
        
        self.filterTableView.reloadData()
        
        APPDELEGATE.window?.addSubview(filterView!)
    }
    
    // MARK: -  @IBAction Methods
    
    @IBAction func closeNotificationView()
    {
        self.tblHistory?.reloadData()
        bell?.isEnabled = true
        notificationView?.removeFromSuperview()
    }
    
    @IBAction func closeFilterView()
    {
        self.selectedFilterArr.removeAll()
        
        let cellDate = filterTableView.cellForRow(at: IndexPath(row: filterArr.count + 1, section: 0)) as! SelectDateTableViewCell
        cellDate.dateTextField.text = ""
        
        date_string = ""
        
        bell?.isEnabled = true
        filter?.isEnabled = true
        filterView?.removeFromSuperview()
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        self.historyAry = []
        pageNumber = 1
        isPageRefresh = true
        self.getHistorySearchData(searchString: "")
    }
    
    @IBAction func submitButtonAction()
    {
        bell?.isEnabled = true
        filter?.isEnabled = true
        filterView?.removeFromSuperview()
        
        if selectedFilterArr.count == 0 && date_string == "" {
            
            self.tblHistory?.reloadData()
            
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("Please select atleast one option.", comment: ""), dismissBloack: {
                
            })
        } else {
            
            MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
            self.historyAry = []
            pageNumber = 1
            isPageRefresh = true
            self.getHistorySearchData(searchString: "")
        }
    }
    
    func notificationApi()
    {
        let service = CustomerService()
        
        service.notificationApiRequestWithParameters(nil, success: { (response, data) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    self.notificatioDataAry = (data?.object(forKey: "data")as! NSArray).mutableCopy() as! NSMutableArray
                    
                    DispatchQueue.main.async
                        {
                            if(self.notificatioDataAry.count == 0)
                            {
                                self.tblNotification?.isHidden = true
                                self.labelNoNotification.isHidden = false
                            }
                            else
                            {
                                self.labelNoNotification.isHidden = true
                                self.tblNotification?.isHidden = false
                                //self.tblHistory?.isHidden = true
                                self.tblNotification?.reloadData()
                            }
                    }
                }
            }
            else
            {
                AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                    
                })
            }
        })
        { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    func getHistorySearchData(searchString : String)
    {
        var searchStr = searchString as NSString
        searchStr = searchStr.replacingOccurrences(of: " ", with: "%20") as NSString
        
        let service1 = CustomerService()
        
        var sort_string = ""
        if selectedFilterArr.count > 0 {
            sort_string = CommonFunctions.getCommaSepratedStringFromArray(completeArray: self.selectedFilterArr)
        } else {
            sort_string = ""
        }
        
        service1.getUserBookingSearchListServiceRequestWithParameters(nil, searchStr, "\(pageNumber)" as NSString, sort_string as NSString, date_string as NSString, success: { (response, data) in
                    
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            self.refreshControl.endRefreshing()
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                        
                if (str.isEqual(to: "success"))
                {
                    if (self.pageNumber == 1)
                    {
                        self.historyAry.removeAllObjects()
                    }
                            
                    if (self.historyAry.count == 0)
                    {
                        self.historyAry = (data?.object(forKey: "data")as! NSArray).mutableCopy() as! NSMutableArray
                    }
                    else
                    {
                        let dataArr : NSMutableArray = (data?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                                
                        if (dataArr.count > 0)
                        {
                            for i in 0 ..< dataArr.count
                            {
                                let dict = dataArr.object(at: i) as! NSDictionary
                                self.historyAry.add(dict)
                            }
                        }
                        else
                        {
                            self.pageNumber = 1
                            self.isPageRefresh = false
                        }
                    }
                            
                    DispatchQueue.main.async
                    {
                        self.tblHistory?.isHidden = false
                                    
                        if searchString == ""
                        {
                            if (self.historyAry.count > 0)
                            {
                                self.tblHistory?.tableHeaderView = self.searchView
                                self.searchView?.isHidden = false
                            }
                            else
                            {
                                self.tblHistory?.tableHeaderView = nil
                                self.searchView?.isHidden = true
                            }
                        }
                        else
                        {
                            self.tblHistory?.tableHeaderView = self.searchView
                            self.searchView?.isHidden = false
                        }
                                    
                        self.tblHistory?.reloadData()
                                    
                        if self.historyAry.count == 0
                        {
                            self.lblNoResult?.isHidden = false
                        }
                        else
                        {
                            self.lblNoResult?.isHidden = true
                        }
                                    
                        if self.historyAry.count == 0
                        {
                            if self.selectedFilterArr.count > 0 || self.date_string != ""
                            {
                                self.filter?.isEnabled = true
                            }
                            else
                            {
                                self.filter?.isEnabled = false
                            }
                        }
                        else
                        {
                            self.filter?.isEnabled = true
                        }
                    }
                }
            }
            else
            {
                AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                            
                    self.lblNoResult?.isHidden = false
                    self.tblHistory?.isHidden = true
                    self.searchView?.isHidden = true
                            
                    self.isPageRefresh = true
                })
            }
        })
        { (response, error) in
                
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            self.refreshControl.endRefreshing()
                
            self.lblNoResult?.isHidden = false
            self.tblHistory?.isHidden = true
            self.searchView?.isHidden = true
                
            self.isPageRefresh = true
        }
    }
    
    func doneAction()
    {
        let cellDate = filterTableView.cellForRow(at: IndexPath(row: filterArr.count + 1, section: 0)) as! SelectDateTableViewCell
        
        formatter.dateFormat = "dd MMM yyyy"
        
        cellDate.dateTextField.text = formatter.string(from: (datePicker.date))
        
        formatter.dateFormat = "yyyy-MM-dd"
        
        date_string = formatter.string(from: (datePicker.date))
        print("date = ", date_string)
    }
}

extension HistoryVC : UITableViewDelegate, UITableViewDataSource
{
    // MARK: - tableview delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblHistory {
            return  historyAry.count
        } else if tableView == filterTableView {
            return filterArr.count + 2
        } else {
            return notificatioDataAry.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == tblHistory
        {
            return UITableViewAutomaticDimension
        }
        else if tableView == filterTableView
        {
            if indexPath.row == filterArr.count + 1
            {
                return 80 * scaleFactorX
            }
            else
            {
                return SCREEN_WIDTH * (44/375)
            }
        }
        else
        {
            return SCREEN_WIDTH * (88/375)
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == tblHistory
        {
            return  SCREEN_WIDTH * 0.340
        }
        else if tableView == filterTableView
        {
            if indexPath.row == filterArr.count + 1
            {
                return 80 * scaleFactorX
            }
            else
            {
                return SCREEN_WIDTH * (44/375)
            }
        }
        else
        {
            return SCREEN_WIDTH * (88/375)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblHistory
        {
            var cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell?
            
            if cell == nil
            {
                cell = HistoryCell(style: UITableViewCellStyle.default, reuseIdentifier: "HistoryCell")
            }
            
            if historyAry.count > 0
            {
                if language == "en"
                {
                    cell?.lblKm?.textAlignment = NSTextAlignment.right
                    cell?.lblSchedule?.textAlignment = NSTextAlignment.left
                }
                else
                {
                    cell?.lblKm?.textAlignment = NSTextAlignment.left
                    cell?.lblSchedule?.textAlignment = NSTextAlignment.right
                }
                
                cell?.btnCall?.tag = indexPath.row
                cell?.btnMail?.tag = indexPath.row
                
                cell?.imgUser?.layer.cornerRadius = (0.133 * SCREEN_WIDTH)/2
                cell?.imgUser?.layer.masksToBounds = true
                
                cell?.imgUser?.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
                cell?.imgUser?.layer.borderWidth = 1.0
                
                let dict : NSDictionary = historyAry.object(at: indexPath.row) as! NSDictionary
                
                cell?.unReadCountView.isHidden = true
                
                cell?.unReadCountView.layer.cornerRadius = (cell?.unReadCountView.frame.size.height)!/2
                cell?.unReadCountView.layer.borderColor = UIColor.clear.cgColor
                cell?.unReadCountView.layer.borderWidth = 1.0
                cell?.unReadCountView.layer.masksToBounds = true
                
                let dict1 = (dict.object(forKey: "provider_id")as! NSDictionary).object(forKey: "provider_name") as? NSDictionary
                
                let dict_provider = dict.object(forKey: "provider_id") as! NSDictionary
                
                if let fcm_user_id = dict_provider.object(forKey: "fcm_user_id") as? String
                {
                    Conversation.showProviderUnReadCount(from_fcm_user_id: fcm_user_id, completion: { (unreadcount) in
                        
                        //print("unread count = ", unreadcount)
                        
                        if unreadcount == "0" {
                            cell?.unReadCountLabel.text = ""
                            cell?.unReadCountView.isHidden = true
                        } else {
                            cell?.unReadCountView.isHidden = false
                            cell?.unReadCountLabel.text = unreadcount
                        }
                    })
                }
                
                if language == "en"
                {
                    cell?.lblName?.text = dict1?.object(forKey: "en") as? String
                }
                else
                {
                    cell?.lblName?.text = dict1?.object(forKey: "ar") as? String
                }
                
                let providerDict : NSDictionary = dict.object(forKey: "provider_id") as! NSDictionary
                
                cell?.imgUser?.setShowActivityIndicator(true)
                cell?.imgUser?.setIndicatorStyle(.gray)
                
                if (providerDict.object(forKey: "cover_url") as? NSArray) != nil
                {
                    let str12 = providerDict.object(forKey: "avatar_url") as! String
                    if str12.contains("http")
                    {
                        cell?.imgUser?.sd_setImage(with: URL.init(string: str12), placeholderImage: #imageLiteral(resourceName: "group20"))
                    }
                    else
                    {
                        let avatar1 = String(format:"%@/user_images/%@", BASE_URL, str12)
                        cell?.imgUser?.sd_setImage(with: URL.init(string: avatar1), placeholderImage: #imageLiteral(resourceName: "group20"))
                    }
                }
                else
                {
                    let str12 = providerDict.object(forKey: "avatar_url") as! String
                    if str12.contains("http")
                    {
                        cell?.imgUser?.sd_setImage(with: URL.init(string: str12), placeholderImage: #imageLiteral(resourceName: "group20"))
                    }
                    else
                    {
                        let avatar1 = String(format:"%@/user_images/%@", BASE_URL, str12)
                        cell?.imgUser?.sd_setImage(with: URL.init(string: avatar1), placeholderImage: #imageLiteral(resourceName: "group20"))
                    }
                }
                
                let strCode =  (dict.object(forKey: "provider_id")as! NSDictionary).object(forKey: "country_code") as? String
                let strMobile =  (dict.object(forKey: "provider_id")as! NSDictionary).object(forKey: "mobile") as? String
                
                cell?.lblContact?.text =  NSLocalizedString("CONTACT_TEXT", comment: "") + " :  " + strCode! +  " " + strMobile!
                cell?.btnCall?.isHidden = false
                
                cell?.contactHeightConstraint.constant = 15
                cell?.contactTopConstraint.constant = 8
                
                let formatter = DateFormatter()
                
                let endTimeArr = dict.object(forKey: "endtime") as! NSArray
                var endTimeStr = endTimeArr[0] as! String
                
                if (endTimeStr.contains(" ")) {
                    formatter.dateFormat = "HH:mm a"
                } else {
                    formatter.dateFormat = "HH:mm"
                }
                
                //formatter.dateFormat = "HH:mm"
                var date = formatter.date(from: endTimeStr)
                formatter.dateFormat = "hh:mma"
                endTimeStr = formatter.string(from: date!)
                
                let startTimeArr = dict.object(forKey: "starttime") as! NSArray
                var startTimeStr = startTimeArr[0] as! String
                
                if (startTimeStr.contains(" ")) {
                    formatter.dateFormat = "HH:mm a"
                } else {
                    formatter.dateFormat = "HH:mm"
                }
                
                date = formatter.date(from: startTimeStr)
                formatter.dateFormat = "hh:mma"
                startTimeStr = formatter.string(from: date!)
                
                var str1 = dict.value(forKey: "date") as! String
                formatter.dateFormat = "yyyy-MM-dd"
                date = formatter.date(from: str1)
                formatter.dateFormat = "dd MMM"
                str1 = formatter.string(from: date!)
                
                cell?.lblScheduleText.text = String (format : "\(NSLocalizedString("SCHEDULE_TEXT", comment: "")) :")
                cell?.lblSchedule?.text = String(format: "%@, %@ - %@", str1, startTimeStr, endTimeStr)
                
                let distanceNum = dict.object(forKey: "distance") as! Double
                let distanceStr : String = String (format : "%.2f", distanceNum)
                let distanceStr1 = "\(distanceStr)km"
                
                let attributedString = NSMutableAttributedString(string: distanceStr1)
                attributedString.addAttributes(
                    [
                        NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 15)!,
                        NSForegroundColorAttributeName: UIColor(red: 83 / 255.0, green: 83 / 255.0, blue: 83 / 255.0, alpha: 1.0)
                    ], range: NSRange(location: 0, length: attributedString.length-2)
                )
                
                attributedString.addAttributes(
                    [
                        NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 15)!,
                        NSForegroundColorAttributeName: UIColor(red: 120 / 255.0, green: 120 / 255.0, blue: 120 / 255.0, alpha: 1.0)
                    ], range: NSRange(location: attributedString.length-2, length: 2)
                )
                cell?.lblKm?.attributedText = attributedString
                
                let ratings = (dict.object(forKey: "provider_id")as! NSDictionary).object(forKey: "ratings") as? Double
                cell?.ratingView.rating = ratings!
                
                let status : Int = dict.object(forKey: "request_status") as! Int
                
                if (status == 1)
                {
                    if language == "en" {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "pending")
                    } else {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "pending_ar")
                    }
                }
                else if (status == 2)
                {
                    if language == "en" {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "accepted")
                    } else {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "accepted_ar")
                    }
                }
                else if (status == 3)
                {
                    // rejected
                    if language == "en" {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "rejected")
                    } else {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "rejected_ar")
                    }
                }
                else if (status == 4)
                {
                    if language == "en" {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "progress")
                    } else {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "progress_ar")
                    }
                }
                else if (status == 5)
                {
                    if language == "en" {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "completed")
                    } else {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "completed_ar")
                    }
                }
                else if status == 6
                {
                    if language == "en" {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "reached")
                    } else {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "reached_ar")
                    }
                }
                else if status == 7
                {
                    if language == "en" {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "canceled")
                    } else {
                        cell?.imgStatus?.image = #imageLiteral(resourceName: "canceled_ar")
                    }
                }
                
                for i in 1...5
                {
                    cell?.contentView.viewWithTag(7)?.viewWithTag(i)
                }
                
                cell?.btnCall?.tag = indexPath.row
                cell?.btnCall?.addTarget(self, action: #selector(callProvider), for: .touchUpInside)
                
                cell?.btnMail?.addTarget(self, action: #selector(mailProvider), for: .touchUpInside)
                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                
                if (isPageRefresh && historyAry.count-1 == indexPath.row) {
                    pageNumber += 1
                    
                    if searchBar?.text == "" {
                        getHistorySearchData(searchString: "")
                    } else {
                        getHistorySearchData(searchString: (searchBar?.text)!)
                    }
                }
                
                cell?.newLabel?.text = CommonFunctions.getLocalizedString(localizedName: "New")
                
                if let booking_compleated = (dict.object(forKey: "provider_id") as! NSDictionary).object(forKey: "booking_compleated") as? Int
                {
                    if booking_compleated > 5
                    {
                        cell?.ratingView.isHidden = false
                        cell?.newLabel.isHidden = true
                    }
                    else
                    {
                        cell?.ratingView.isHidden = true
                        cell?.newLabel.isHidden = false
                    }
                }
                else
                {
                    cell?.ratingView.isHidden = false
                    cell?.newLabel.isHidden = true
                }
            }
            
            return cell!
        }
        else if tableView == filterTableView
        {
            if indexPath.row == filterArr.count + 1
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SelectDateTableViewCell") as! SelectDateTableViewCell?
                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                
                cell?.selectDateLabel.text = CommonFunctions.getLocalizedString(localizedName: "SELECT_DATE")
                
                cell?.dateTextField.inputView = datePicker
                cell?.dateTextField.delegate = self
                
                if(LanguageManager.currentLanguageIndex() == 0)
                {
                    cell?.dateTextField.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
                    cell?.dateTextField.textAlignment = NSTextAlignment.left
                }
                else
                {
                    cell?.dateTextField.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
                    cell?.dateTextField.textAlignment = NSTextAlignment.right
                }
                
                return cell!
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "filterProviderTableViewCell") as! filterProviderTableViewCell?
                
                cell?.selectionStyle = UITableViewCellSelectionStyle.none
                
                cell?.checkButton.isUserInteractionEnabled = false
                
                if indexPath.row == 0
                {
                    if selectedFilterArr.count > 0 || date_string != ""
                    {
                        if selectedFilterArr.count == filterArr.count
                        {
                            cell?.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                        }
                        else
                        {
                            cell?.checkButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                        }
                    }
                    else
                    {
                        cell?.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                        
                        for dict in filterArr
                        {
                            selectedFilterArr.append(dict["name"] as! String)
                        }
                    }
                    
                    cell?.sortLabel.text = CommonFunctions.getLocalizedString(localizedName: "Select All")
                }
                else
                {
                    let dict = filterArr[indexPath.row - 1]
                    
                    if selectedFilterArr.contains(dict["name"] as! String)
                    {
                        cell?.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                    }
                    else
                    {
                        cell?.checkButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                    }
                    
                    cell?.sortLabel.text = dict["image"] as? String
                }
                
                return cell!
            }
        }
        else
        {
            var cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell?
            
            if cell == nil
            {
                cell = NotificationCell(style: UITableViewCellStyle.default, reuseIdentifier: "NotificationCell")
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            let dic:NSDictionary = notificatioDataAry.object(at: indexPath.row) as! NSDictionary
            
            cell?.lblTitle?.text = dic.value(forKey: "message") as? String
            
            let datestring = dic.value(forKey: "createdAt") as? String
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let date = dateFormatter.date(from: datestring!)
            dateFormatter.dateFormat = "dd MMM yyyy, hh:mm a"
            let dateString = dateFormatter.string(from: date!)
            
            cell?.lblDate?.text = dateString
            
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == tblHistory
        {
            let booking = HistoryDetailVC()
            
            booking.booking_id = (historyAry.object(at: indexPath.row) as? NSDictionary)?.object(forKey: "_id") as! String
            
            self.navigationController?.pushViewController(booking, animated: true)
        }
        else if tableView == filterTableView
        {
            if indexPath.row == 0
            {
                if selectedFilterArr.count == 0
                {
                    for (i, dict)  in filterArr.enumerated()
                    {
                        let index = IndexPath(row: i + 1, section: 0)
                        let cell1 = tableView.cellForRow(at: index) as! filterProviderTableViewCell
                        selectedFilterArr.append(dict["name"] as! String)
                        cell1.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                    }
                    
                    let index = IndexPath(row: 0, section: 0)
                    let cell1 = tableView.cellForRow(at: index) as! filterProviderTableViewCell
                    cell1.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                }
                else
                {
                    for (i, dict)  in filterArr.enumerated()
                    {
                        for (j, str) in selectedFilterArr.enumerated()
                        {
                            if str == dict["name"] as! String
                            {
                                selectedFilterArr.remove(at: j)
                                
                                let index = IndexPath(row: i + 1, section: 0)
                                let cell1 = tableView.cellForRow(at: index) as! filterProviderTableViewCell
                                cell1.checkButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                                
                            }
                        }
                    }
                    
                    let index = IndexPath(row: 0, section: 0)
                    let cell1 = tableView.cellForRow(at: index) as! filterProviderTableViewCell
                    cell1.checkButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                }
            }
            else if indexPath.row == filterArr.count + 1
            {
                let cellDate = filterTableView.cellForRow(at: IndexPath(row: filterArr.count + 1, section: 0)) as! SelectDateTableViewCell
                
                cellDate.dateTextField.becomeFirstResponder()
            }
            else
            {
                let cell = tableView.cellForRow(at: indexPath) as! filterProviderTableViewCell
                
                let dict = filterArr[indexPath.row - 1]
                
                if selectedFilterArr.contains(dict["name"] as! String)
                {
                    cell.checkButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                    for (i, str) in selectedFilterArr.enumerated()
                    {
                        if str == dict["name"] as! String
                        {
                            selectedFilterArr.remove(at: i)
                        }
                    }
                    
                    let index = IndexPath(row: 0, section: 0)
                    let cell1 = tableView.cellForRow(at: index) as! filterProviderTableViewCell
                    cell1.checkButton.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                }
                else
                {
                    cell.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                    selectedFilterArr.append(dict["name"] as! String)
                }
                
                if selectedFilterArr.count == filterArr.count
                {
                    let index = IndexPath(row: 0, section: 0)
                    let cell1 = tableView.cellForRow(at: index) as! filterProviderTableViewCell
                    cell1.checkButton.setImage(#imageLiteral(resourceName: "agree"), for: .normal)
                }
            }
        }
    }
    
    // MARK: - Other Methods
    
    func callProvider(sender : UIButton)
    {
        let tag = sender.tag
        
        let dict : NSDictionary = historyAry.object(at: tag) as! NSDictionary
        let contact : String = String(format : "%@%@", ((dict.object(forKey: "provider_id")as! NSDictionary).object(forKey: "country_code") as? String)!, ((dict.object(forKey: "provider_id")as! NSDictionary).object(forKey: "mobile") as? String)!)
        
        CommonFunctions.callNumber(phoneNumber: contact)
    }
    
    func mailProvider(sender : UIButton)
    {
        let tag = sender.tag
        
        let dict : NSDictionary = historyAry.object(at: tag) as! NSDictionary
        let provider_dict = dict.object(forKey: "provider_id") as! NSDictionary
        let provider_name_dict = provider_dict.object(forKey: "provider_name") as! NSDictionary
        
        var provider_name = ""
        if language == "en" {
            provider_name = provider_name_dict.object(forKey: "en") as! String
        } else {
            provider_name = provider_name_dict.object(forKey: "ar") as! String
        }
        
        if let device_token = provider_dict.object(forKey: "device_token") as? String
        {
            if let fcm_user_id = provider_dict.object(forKey: "fcm_user_id") as? String
            {
                if fcm_user_id != ""
                {
                    let messages = MessagesVC()
                
                    messages.selected_user_name = provider_name
                    messages.selected_user_id = fcm_user_id
                    messages.device_token_string = device_token
                
                    self.navigationController?.pushViewController(messages, animated: true)
                }
            }
        }
    }
}

extension HistoryVC : UISearchBarDelegate
{
    //MARK: - UISearchController delegates
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        self.historyAry = []
        pageNumber = 1
        searchBar.text = ""
        getHistorySearchData(searchString: searchBar.text!)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        self.historyAry = []
        pageNumber = 1
        getHistorySearchData(searchString: searchBar.text!)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        
    }
}

extension HistoryVC : UITextFieldDelegate
{
    
}
