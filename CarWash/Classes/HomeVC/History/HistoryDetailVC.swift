//
//  BookingDetailVC.swift
//  CarWash
//
//  Created by Neha Chaudhary on 29/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD

class HistoryDetailVC: UIViewController, UITextViewDelegate, writeRatingDelagate
{
    @IBOutlet var tblBooking    : UITableView?
    @IBOutlet var cancelView    : UIView?
    @IBOutlet var complaintView : UIView?
    @IBOutlet var txtComplaint  : UITextView?
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnPopUpCancel: UIButton!
    @IBOutlet weak var lblEnterDetail: UILabel!
    @IBOutlet weak var lblEnterDetailComplain: UILabel!
    @IBOutlet weak var lblCancelBooking: UILabel!
    @IBOutlet weak var lblCancelBookingMsg: UILabel!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var lblTotalCost: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    var imageCover   : UIImage?
    var serviceDict  : NSDictionary?
    
    @IBOutlet weak var rateBtn: UIButton!
    @IBOutlet weak var totalCostLabel: UILabel!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var actionBtn: UIButton!
    @IBOutlet weak var complaintBtn: UIButton!
    
    var booking_id = ""
    var isFromHistory = false
    
    let complaintPlaceText : String = NSLocalizedString("ENTER_DETAIL_COMPLAIN", comment: "")
    
    var review = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        complaintView?.isHidden = true
        txtComplaint?.delegate = self
        
        tblBooking?.register(UINib(nibName: "ServiceCell", bundle: nil), forCellReuseIdentifier: "ServiceCell")
        tblBooking?.register(UINib(nibName: "TimeDateCell", bundle: nil), forCellReuseIdentifier: "TimeDateCell")
        tblBooking?.register(UINib(nibName: "CarServices", bundle: nil), forCellReuseIdentifier: "CarServices")
        tblBooking?.register(UINib(nibName: "MoreCell", bundle: nil), forCellReuseIdentifier: "MoreCell")
        tblBooking?.register(UINib(nibName: "ReviewCell", bundle: nil), forCellReuseIdentifier: "ReviewCell")
        
        cancelView?.viewWithTag(2)?.layer.cornerRadius = 10.0
        cancelView?.viewWithTag(2)?.layer.masksToBounds = true
        complaintView?.viewWithTag(2)?.layer.cornerRadius = 10.0
        complaintView?.viewWithTag(2)?.layer.masksToBounds = true
        
        lblTotalCost.text = String (format : "\(NSLocalizedString("TOTAL_COST", comment: "")) :")
        lblEnterDetail.text = NSLocalizedString("ENTER_DETAIL", comment: "")
        lblEnterDetailComplain.text = NSLocalizedString("ENTER_DETAIL_TITLE", comment: "")
        lblCancelBooking.text = NSLocalizedString("CANCEL_BOOKING", comment: "")
        lblCancelBookingMsg.text = NSLocalizedString("CANCEL_BOOKING_ALERT_MSG", comment: "")
        btnCancel.setTitle(NSLocalizedString("CANCEL_TEXT", comment: ""), for: UIControlState.normal)
        btnPopUpCancel.setTitle(NSLocalizedString("CANCEL_TEXT", comment: ""), for: UIControlState.normal)
        btnYes.setTitle(NSLocalizedString("YES_TEXT", comment: ""), for: UIControlState.normal)
        btnSubmit.setTitle(NSLocalizedString("SUBMIT_TEXT", comment: ""), for: UIControlState.normal)
        rateBtn.setTitle(NSLocalizedString("RATE_TEXT", comment: ""), for: UIControlState.normal)
        complaintBtn.setTitle(NSLocalizedString("COMPLAINT_TEXT", comment: ""), for: UIControlState.normal)
        actionBtn.setTitle(NSLocalizedString("CANCEL_TEXT", comment: ""), for: UIControlState.normal)
        
        txtComplaint?.text = complaintPlaceText
        
        if language == "en"
        {
            txtComplaint?.textAlignment = .left
        }
        else
        {
            txtComplaint?.textAlignment = .right
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        setUpNavBar()
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        getBookingDetailApiCall()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
    }
    
    func setUpNavBar()
    {
        self.title = NSLocalizedString("HISTORY_DETAIL", comment: "")
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex() == 0 ? "back" : "backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getBookingDetailApiCall()
    {
        let service1 = CustomerService()
        
        service1.getBookingHistoryDetailRequestWithParameters(nil, booking_id as NSString, success: { (response, data) in
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    self.serviceDict = (data?.object(forKey: "data") as! NSArray).object(at: 0) as? NSDictionary
                    
                    self.setUpData()
                }
            }
            else
            {
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                
                AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                    
                })
            }
        })
        { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    func setUpData()
    {
        tblBooking?.delegate = self
        tblBooking?.dataSource = self
        
        let status : Int = serviceDict!.object(forKey: "request_status") as! Int
        if status == 1
        {
            heightConstraint.constant = 45
            actionBtn.isHidden = false
        }
        else if status == 5
        {
            heightConstraint.constant = 45
            actionBtn.isHidden = true
            complaintBtn.isHidden = false
            rateBtn.isHidden = false
        }
        else
        {
            heightConstraint.constant = 0
            actionBtn.isHidden = true
            complaintBtn.isHidden = true
            rateBtn.isHidden = true
        }
        
        let providerDict = (self.serviceDict?.object(forKey: "provider_id") as? NSDictionary)!
        
        let countryCodeCA = providerDict.object(forKey: "iso_code")
        let localeIdCA = NSLocale.localeIdentifier(fromComponents: [ NSLocale.Key.countryCode.rawValue : countryCodeCA as! String])
        let localeCA = NSLocale(localeIdentifier: localeIdCA)
        _ = localeCA.object(forKey: NSLocale.Key.currencySymbol)
        let currencyCodeCA = localeCA.object(forKey: NSLocale.Key.currencyCode)
        
        let totalCost = serviceDict?.object(forKey: "price") as! Double
        totalCostLabel.text = String(format : "%@%.2f", currencyCodeCA as! String, totalCost)
        
        review = Int(serviceDict?.object(forKey: "is_reviewed") as! NSNumber)
        
        //if (review == 1)
        //{
        //    rateBtn.isUserInteractionEnabled = false
        //    rateBtn.alpha = 0.7
        //}
        //else
        //{
        //    rateBtn.isUserInteractionEnabled = true
        //    rateBtn.alpha = 1.0
        //}
        
        tblBooking?.reloadData()
        
        MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
    }
    
    // MARK: IBAction Methods
    
    @IBAction func btnCancelPressed()
    {
        cancelView?.removeFromSuperview()
    }
    
    @IBAction func btnYesPressed()
    {
        cancelView?.removeFromSuperview()
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        cancelBookingStatusApi()
    }
    
    @IBAction func actionClicked(_ sender: Any)
    {
        cancelView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
        APPDELEGATE.window?.addSubview(cancelView!)
    }
    
    func cancelBookingStatusApi()
    {
        let service = CustomerService()
        
        let dic : NSMutableDictionary = [
            
            "booking_id"           :    serviceDict?.object(forKey: "_id") ?? "",
            "requested_action"     :    "7"
        ]
        
        print("dic = \(dic)")
        
        service.updateBookingStatusWithParameters(dic, success: { (response, data) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            print("updateBookingStatus data = ", data ?? "")
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    DispatchQueue.main.async
                        {
                            self.navigationController?.popViewController(animated: true)
                    }
                }
            }
            else
            {
                let dict = data?.object(forKey: "error") as! NSDictionary
                let dict1 = dict.object(forKey: "booking_id") as! NSArray
                let str1 = dict1.object(at: 0) as! String
                
                AlertViewController.showAlertWith(title: "", message: str1, dismissBloack: {
                    
                })
            }
            
        })
        { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    @IBAction func rateAction(_ sender: Any) {
        
        if review == 1
        {
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("You have already Rated for this service", comment: ""), dismissBloack: {
                
            })
        }
        else
        {
            let rating = WriteRateingViewController()
        
            rating.delegate = self
            rating.serviceDict = serviceDict
        
            self.navigationController?.pushViewController(rating, animated: true)
        }
    }
    
    @IBAction func complaintAction(_ sender: Any) {
        
        complaintView?.isHidden = false
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.text.count >= 240 {
            if text == "" {
                return true
            } else {
                return false
            }
        }
        
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == complaintPlaceText {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = complaintPlaceText
        }
    }
    
    @IBAction func cancelComplaint()
    {
        complaintView?.isHidden = true
        txtComplaint?.text = complaintPlaceText
    }
    
    @IBAction func submitComplaint()
    {
        if txtComplaint?.text == complaintPlaceText {
            
            let alertController = UIAlertController(title: "", message: NSLocalizedString("COMPLAIN_ALERT", comment: ""), preferredStyle: .alert)
            
            let action2 = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: .default) { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }
            
            alertController.addAction(action2)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else if txtComplaint?.text == "" {
            
            let alertController = UIAlertController(title: "", message: NSLocalizedString("COMPLAIN_ALERT", comment: ""), preferredStyle: .alert)
            
            let action2 = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: .default) { (action:UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            }
            
            alertController.addAction(action2)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            complaintView?.isHidden = true
            MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
            postComplainApiCall()
        }
    }
    
    func postComplainApiCall()
    {
        let service = CustomerService()
        
        let dict1 = serviceDict?.object(forKey: "provider_id") as! NSDictionary
        let provider_id = dict1.object(forKey: "_id") as! String
        
        let service_id = serviceDict?.object(forKey: "service_id") as! String
        
        let dict : NSMutableDictionary = [
            "service_id" : service_id,
            "provider_id" : provider_id,
            "content" : txtComplaint!.text
        ]
        
        print("dict = ", dict)
        
        service.complaintBookedServiceRequestWithParameters(dict, success: { (response, data) in
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    DispatchQueue.main.async
                    {
                        AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack:
                        {
                            self.txtComplaint?.text = self.complaintPlaceText
                        })
                    }
                }
            }
            else
            {
                AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack:
                    {
                        
                })
            }
            
        }) { (response, error) in
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    func updateDict(dict: NSDictionary) {
        serviceDict = dict
        tblBooking?.reloadData()
    }
}

extension HistoryDetailVC : UITableViewDelegate, UITableViewDataSource
{
    // MARK: - tableview delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return  indexPath.row == 0 ? (326/375) * SCREEN_WIDTH : indexPath.row == 1 ? 0.330 * SCREEN_WIDTH : indexPath.row == 2 ? SCREEN_WIDTH * 0.319 : SCREEN_WIDTH *  0.08 //indexPath.row == 3 ? SCREEN_WIDTH *  0.08: SCREEN_WIDTH * 0.242
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell") as! ServiceCell?
            
            cell?.btnCard?.isHidden         = true
            cell?.btnCash?.isHidden         = true
            cell?.btnDicount?.isHidden      = true
            cell?.topConstraint.constant    = 14
            
            cell?.ureadCountView.isHidden = true
            
            cell?.ureadCountView.layer.cornerRadius = (cell?.ureadCountView.frame.size.height)!/2
            cell?.ureadCountView.layer.borderColor = UIColor.clear.cgColor
            cell?.ureadCountView.layer.borderWidth = 1.0
            cell?.ureadCountView.layer.masksToBounds = true
            
            cell?.btnCall?.addTarget(self, action: #selector(callProvider), for: .touchUpInside)
            cell?.btnMail?.addTarget(self, action: #selector(mailProvider), for: .touchUpInside)
            
            cell?.btnImage?.addTarget(self, action: #selector(coverImageClicked), for: .touchUpInside)
            
            cell?.imgLogo?.layer.cornerRadius  = ((0.14) * SCREEN_WIDTH)/2
            cell?.imgLogo?.layer.masksToBounds = true
            
            cell?.imgLogo?.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
            cell?.imgLogo?.layer.borderWidth = 1.0
            
            let providerDict : NSDictionary = serviceDict?.object(forKey: "provider_id") as! NSDictionary
            
            if let fcm_user_id = providerDict.object(forKey: "fcm_user_id") as? String
            {
                Conversation.showProviderUnReadCount(from_fcm_user_id: fcm_user_id, completion: { (unreadcount) in
                    
                    //print("unread count = ", unreadcount)
                    
                    if unreadcount == "0" {
                        cell?.ureadCountLabel.text = ""
                        cell?.ureadCountView.isHidden = true
                    } else {
                        cell?.ureadCountView.isHidden = false
                        cell?.ureadCountLabel.text = unreadcount
                    }
                })
            }
            
            let dict1 = providerDict.object(forKey: "provider_name") as! NSDictionary
            if language == "en" {
                cell?.lblName?.text = dict1.object(forKey: "en") as? String
            } else {
                cell?.lblName?.text = dict1.object(forKey: "ar") as? String
            }
            if language == "en" {
                cell?.lblKm?.textAlignment = .right
            } else {
                cell?.lblKm?.textAlignment = .left
            }
            
            cell?.lblName?.text = cell?.lblName?.text?.uppercased()
            
            cell?.imgService?.setShowActivityIndicator(true)
            cell?.imgService?.setIndicatorStyle(.gray)
            
            cell?.imgLogo?.setShowActivityIndicator(true)
            cell?.imgLogo?.setIndicatorStyle(.gray)
            
            if let arr1 = providerDict.object(forKey: "cover_url") as? NSArray
            {
                let str1 = arr1.object(at: 0) as! String
                
                cell?.imgService?.sd_setImage(with: URL.init(string: str1), placeholderImage: #imageLiteral(resourceName: "group29"))
                
                let str12 = providerDict.object(forKey: "avatar_url") as! String
                if str12.contains("http")
                {
                    cell?.imgLogo?.sd_setImage(with: URL.init(string: str12), placeholderImage: #imageLiteral(resourceName: "group20"))
                }
                else
                {
                    let avatar1 = String(format:"%@/user_images/%@", BASE_URL, str12)
                    cell?.imgLogo?.sd_setImage(with: URL.init(string: avatar1), placeholderImage: #imageLiteral(resourceName: "group20"))
                }
            }
            else
            {
                let cover1 = providerDict.object(forKey: "cover_url") as! CVarArg
                
                var cover : NSString = String(format:"%@", BASE_URL, cover1) as NSString
                cover = cover.replacingOccurrences(of: " ", with: "%20") as NSString
                cell?.imgService?.sd_setImage(with: URL.init(string: cover as String), placeholderImage: #imageLiteral(resourceName: "group29"))
                
                let str12 = providerDict.object(forKey: "avatar_url") as! String
                if str12.contains("http")
                {
                    cell?.imgLogo?.sd_setImage(with: URL.init(string: str12), placeholderImage: #imageLiteral(resourceName: "group20"))
                }
                else
                {
                    let avatar1 = String(format:"%@/user_images/%@", BASE_URL, str12)
                    cell?.imgLogo?.sd_setImage(with: URL.init(string: avatar1), placeholderImage: #imageLiteral(resourceName: "group20"))
                }
            }
            
            let tapService = UITapGestureRecognizer.init(target: self, action: #selector(coverImageClicked))
            cell?.imgService?.tag = indexPath.row
            cell?.imgService?.isUserInteractionEnabled = true
            cell?.imgService?.addGestureRecognizer(tapService)
            
            imageCover = cell?.imgService?.image
            
            let contactTxt = NSLocalizedString("CONTACT_TEXT", comment: "") as NSString
            let contactValue  = String(format : "%@ %@", providerDict.object(forKey: "country_code") as! CVarArg, providerDict.object(forKey: "mobile") as! CVarArg) as NSString
            
            let contact : String = String(format : "\(contactTxt): %@ %@", providerDict.object(forKey: "country_code") as! CVarArg, providerDict.object(forKey: "mobile") as! CVarArg)
            
            var attributedString = NSMutableAttributedString(string: contact)
            attributedString.addAttributes(
                [
                    NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                    NSForegroundColorAttributeName: UIColor(red: 130 / 255.0, green: 130 / 255.0, blue: 130 / 255.0, alpha: 1.0)
                ], range: NSRange(location: 0, length: contactTxt.length+1)
            )
            
            attributedString.addAttributes(
                [
                    NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                    NSForegroundColorAttributeName: UIColor(red: 83 / 255.0, green: 83 / 255.0, blue: 83 / 255.0, alpha: 1.0)
                ], range: NSRange(location: attributedString.length-contactValue.length, length: contactValue.length)
            )
            cell?.lblContact?.attributedText = attributedString
            
            let distanceNum = serviceDict?.object(forKey: "distance") as! Double
            let distanceStr : String = String (format : "%.2f", distanceNum)
            let distanceStr1 = "\(distanceStr)km"
            
            attributedString = NSMutableAttributedString(string: distanceStr1)
            attributedString.addAttributes(
                [
                    NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 15)!,
                    NSForegroundColorAttributeName: UIColor(red: 83 / 255.0, green: 83 / 255.0, blue: 83 / 255.0, alpha: 1.0)
                ], range: NSRange(location: 0, length: attributedString.length-2)
            )
            
            attributedString.addAttributes(
                [
                    NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 15)!,
                    NSForegroundColorAttributeName: UIColor(red: 120 / 255.0, green: 120 / 255.0, blue: 120 / 255.0, alpha: 1.0)
                ], range: NSRange(location: attributedString.length-2, length: 2)
            )
            cell?.lblKm?.attributedText = attributedString
            
            let status : Int = serviceDict!.object(forKey: "request_status") as! Int
            cell?.serviceStatusImage.isHidden = false
            
            if (status == 1)
            {
                if language == "en" {
                    cell?.serviceStatusImage?.image = #imageLiteral(resourceName: "pending")
                } else {
                    cell?.serviceStatusImage?.image = #imageLiteral(resourceName: "pending_ar")
                }
            }
            else if (status == 2)
            {
                if language == "en" {
                    cell?.serviceStatusImage?.image = #imageLiteral(resourceName: "accepted")
                } else {
                    cell?.serviceStatusImage?.image = #imageLiteral(resourceName: "accepted_ar")
                }
            }
            else if (status == 3)
            {
                // rejected
                if language == "en" {
                    cell?.serviceStatusImage?.image = #imageLiteral(resourceName: "rejected")
                } else {
                    cell?.serviceStatusImage?.image = #imageLiteral(resourceName: "rejected_ar")
                }
            }
            else if (status == 4)
            {
                if language == "en" {
                    cell?.serviceStatusImage?.image = #imageLiteral(resourceName: "progress")
                } else {
                    cell?.serviceStatusImage?.image = #imageLiteral(resourceName: "progress_ar")
                }
            }
            else if (status == 5)
            {
                if language == "en" {
                    cell?.serviceStatusImage?.image = #imageLiteral(resourceName: "completed")
                } else {
                    cell?.serviceStatusImage?.image = #imageLiteral(resourceName: "completed_ar")
                }
            }
            else if status == 6
            {
                if language == "en" {
                    cell?.serviceStatusImage?.image = #imageLiteral(resourceName: "reached")
                } else {
                    cell?.serviceStatusImage?.image = #imageLiteral(resourceName: "reached_ar")
                }
            }
            else if status == 7
            {
                if language == "en" {
                    cell?.serviceStatusImage?.image = #imageLiteral(resourceName: "canceled")
                } else {
                    cell?.serviceStatusImage?.image = #imageLiteral(resourceName: "canceled_ar")
                }
            }
            
            let ratings = (serviceDict?.object(forKey: "provider_id")as! NSDictionary).object(forKey: "ratings") as? Double
            cell?.ratingView.rating = ratings!
            
            let dict = serviceDict?.object(forKey: "provider_id")as! NSDictionary
            let reviews = dict.object(forKey: "review_count") as! NSNumber
            
            cell?.lblReview?.text = "\(reviews) \(NSLocalizedString("REVIEWS", comment: ""))"
            
            cell?.lblReview?.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer.init(target: self, action: #selector(reviewLabelClicked))
            cell?.lblReview?.tag = indexPath.row
            cell?.lblReview?.addGestureRecognizer(tap)
            
            cell?.newServiceProviderLabel.text = CommonFunctions.getLocalizedString(localizedName: "New")
            
            if let booking_compleated = dict.object(forKey: "booking_compleated") as? Int
            {
                if booking_compleated > 5
                {
                    cell?.ratingView.isHidden = false
                    cell?.lblReview?.isHidden = false
                
                    cell?.newServiceProviderLabel.isHidden = true
                }
                else
                {
                    cell?.ratingView.isHidden = true
                    cell?.lblReview?.isHidden = true
                
                    cell?.newServiceProviderLabel.isHidden = false
                }
            }
            else
            {
                cell?.ratingView.isHidden = false
                cell?.lblReview?.isHidden = false
                
                cell?.newServiceProviderLabel.isHidden = true
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell!;
        }
        else if(indexPath.row == 1)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarServices") as! CarServices?
            
            let serviceDict : NSDictionary = self.serviceDict!.object(forKey: "service_data") as! NSDictionary
            let service_name : NSDictionary = serviceDict.object(forKey: "service_name") as! NSDictionary
            
            let service_desc : NSDictionary = serviceDict.object(forKey: "description") as! NSDictionary
            
            cell?.lblServices?.text = String (format : "\(NSLocalizedString("SERVICES_TEXT", comment: "")):")
            
            if(language.isEqual(to: "en"))
            {
                let name : String = (service_name.object(forKey: "en") as? String)!
                cell?.lblTitle?.text = name.capitalized
                cell?.lblService?.text = service_desc.object(forKey: "en") as? String
            }
            else
            {
                cell?.lblTitle?.text = service_name.object(forKey: "ar") as? String
                cell?.lblService?.text = service_desc.object(forKey: "ar") as? String
            }
            
            let ary: NSArray = serviceDict.allKeys as NSArray
            
            if(ary.contains("duration"))
            {
                let duration : Int = (serviceDict.object(forKey: "duration") as? Int)!
                let price    : Int = (serviceDict.object(forKey: "price") as? Int)!
                
                let providerDict = (self.serviceDict?.object(forKey: "provider_id") as? NSDictionary)!
                
                let countryCodeCA = providerDict.object(forKey: "iso_code")
                let localeIdCA = NSLocale.localeIdentifier(fromComponents: [ NSLocale.Key.countryCode.rawValue : countryCodeCA as! String])
                let localeCA = NSLocale(localeIdentifier: localeIdCA)
                _ = localeCA.object(forKey: NSLocale.Key.currencySymbol)
                let currencyCodeCA = localeCA.object(forKey: NSLocale.Key.currencyCode)
                
                //print("\(currencyCodeCA!)")
                
                let durationTxt : NSString = String (format : "\(NSLocalizedString("DURATION_TEXT", comment: "")):") as NSString
                let durationValue : NSString = String (format : "%dMIN", duration) as NSString
                let priceTxt : NSString = String (format : "\(NSLocalizedString("PRICE_TEXT", comment: "")):") as NSString
                
                let priceValue : NSString = String (format : "%@%d", currencyCodeCA as! String, price) as NSString
                
                let str : String = String (format : "\(durationTxt) \(durationValue), \(priceTxt) \(priceValue)")
                
                let attributedString = NSMutableAttributedString(string: str)
                
                attributedString.addAttributes(
                    [
                        NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                        NSForegroundColorAttributeName: UIColor.uicolorFromRGB(130, 130, 130)
                    ],
                    range: NSRange(location: 0, length:durationTxt.length)
                )
                
                attributedString.addAttributes(
                    [
                        NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                        NSForegroundColorAttributeName: UIColor.uicolorFromRGB(83, 83, 83)
                    ],
                    range: NSRange(location: durationTxt.length+1, length: durationValue.length)
                )
                
                attributedString.addAttributes(
                    [
                        NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                        NSForegroundColorAttributeName: UIColor.uicolorFromRGB(130, 130, 130)
                    ],
                    range: NSRange(location: durationTxt.length+durationValue.length+2, length: priceTxt.length)
                )
                
                attributedString.addAttributes(
                    [
                        NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 13)!,
                        NSForegroundColorAttributeName: UIColor.uicolorFromRGB(83, 83, 83)
                    ],
                    range: NSRange(location: durationTxt.length+durationValue.length + priceTxt.length + 3, length: priceValue.length)
                )
                
                cell?.lblDuration?.attributedText = attributedString
            }
            else
            {
                cell?.lblDuration?.text = ""
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell!
        }
        else if(indexPath.row == 2)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimeDateCell") as! TimeDateCell?
            
            cell?.txtPoints?.layer.cornerRadius = 5
            cell?.txtPoints?.layer.masksToBounds = true
            
            cell?.lblTimeAndDate?.text = String (format : "\(NSLocalizedString("TIME_&_DATE", comment: "")):")
            cell?.lblLoyaltyPoint.text = NSLocalizedString("LOYALTY_POINTS", comment: "").uppercased()
            
            var str1 = serviceDict?.value(forKey: "date")as! String
            
            let formatter = DateFormatter()
            formatter.dateFormat = "YYYY-mm-dd"
            var date = formatter.date(from: str1)
            formatter.dateFormat = "dd MMM"
            str1 = formatter.string(from: date! )
            
            let endTimeArr = serviceDict?.object(forKey: "endtime") as! NSArray
            var endTimeStr = endTimeArr[0] as! String
            
            if (endTimeStr.contains(" "))
            {
                formatter.dateFormat = "HH:mm a"
            }
            else
            {
                formatter.dateFormat = "HH:mm"
            }
            
            //formatter.dateFormat = "HH:mm"
            date = formatter.date(from: endTimeStr)
            formatter.dateFormat = "hh:mm a"
            endTimeStr = formatter.string(from: date!)
            
            let startTimeArr = serviceDict?.object(forKey: "starttime") as! NSArray
            var startTimeStr = startTimeArr[0] as! String
            
            if (startTimeStr.contains(" "))
            {
                formatter.dateFormat = "HH:mm a"
            }
            else
            {
                formatter.dateFormat = "HH:mm"
            }
            
            //formatter.dateFormat = "HH:mm"
            date = formatter.date(from: startTimeStr)
            formatter.dateFormat = "hh:mm a"
            startTimeStr = formatter.string(from: date!)
            
            cell?.lblTime?.text = str1 + ", " + startTimeStr + " - " + endTimeStr
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell!
        }
        else if(indexPath.row == 3)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreCell") as! MoreCell?
            
            cell?.btnMore?.addTarget(self, action: #selector(showMoreReviews), for: UIControlEvents.touchUpInside)
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell!
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell") as! ReviewCell?
            
            cell?.imgUser?.layer.cornerRadius = (0.133 * SCREEN_WIDTH)/2
            cell?.imgUser?.layer.masksToBounds = true
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell!
        }
    }
    
    func coverImageClicked ()
    {
        let providerDict : NSDictionary = serviceDict?.object(forKey: "provider_id") as! NSDictionary
        
        var coverImageArr : NSMutableArray = []
        if let arr1 = providerDict.object(forKey: "cover_url") as? NSArray {
            
            coverImageArr = arr1.mutableCopy() as! NSMutableArray
        }
        else
        {
            let cover1 = providerDict.object(forKey: "cover_url") as! CVarArg
            
            var cover : NSString = String(format:"%@", BASE_URL, cover1) as NSString
            cover = cover.replacingOccurrences(of: " ", with: "%20") as NSString
            
            coverImageArr.add(cover as String)
        }
        
        let providerCoverImageVC = ProviderCoverImageVC()
        
        //providerCoverImageVC.coverImagesArr = coverImageArr
        
        self.navigationController?.pushViewController(providerCoverImageVC, animated: true)
    }
    
    func reviewLabelClicked(sender : UITapGestureRecognizer)
    {
        let reviewlist = ReviewsListVC()
        
        reviewlist.reviewProviderId = ((serviceDict?.object(forKey: "provider_id")as! NSDictionary).object(forKey: "_id") as? String)!
        
        self.navigationController?.pushViewController(reviewlist, animated: true)
    }
    
    func showMoreReviews()
    {
        let reviewVC = ReviewsListVC()
        self.navigationController?.pushViewController(reviewVC, animated: true)
    }
    
    func callProvider(sender : UIButton)
    {
        let providerDict : NSDictionary = serviceDict?.object(forKey: "provider_id") as! NSDictionary
        let contact : String = String(format : "%@%@", providerDict.object(forKey: "country_code") as! CVarArg, providerDict.object(forKey: "mobile") as! CVarArg)
        
        CommonFunctions.callNumber(phoneNumber: contact)
    }
    
    func mailProvider(sender : UIButton)
    {
        let provider_dict = serviceDict?.object(forKey: "provider_id") as! NSDictionary
        let provider_name_dict = provider_dict.object(forKey: "provider_name") as! NSDictionary
        
        var provider_name = ""
        if language == "en" {
            provider_name = provider_name_dict.object(forKey: "en") as! String
        } else {
            provider_name = provider_name_dict.object(forKey: "ar") as! String
        }
        
        if let device_token = provider_dict.object(forKey: "device_token") as? String
        {
            if let fcm_user_id = provider_dict.object(forKey: "fcm_user_id") as? String
            {
                if fcm_user_id != ""
                {
                    let messages = MessagesVC()
                
                    messages.selected_user_name = provider_name
                    messages.selected_user_id = fcm_user_id
                    messages.device_token_string = device_token
                
                    self.navigationController?.pushViewController(messages, animated: true)
                }
            }
        }
    }
}
