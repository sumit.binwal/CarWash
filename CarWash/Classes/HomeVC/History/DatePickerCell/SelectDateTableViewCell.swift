//
//  SelectDateTableViewCell.swift
//  CarWash
//
//  Created by Ratina on 4/24/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit

class SelectDateTableViewCell: UITableViewCell {

    @IBOutlet weak var selectDateLabel: UILabel!
    @IBOutlet weak var dateTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
