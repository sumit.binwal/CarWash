//
//  EditMyAccount.swift
//  CarWash
//
//  Created by Neha Chaudhary on 15/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import RSKImageCropper
import  MBProgressHUD
import AWSS3
import GooglePlaces

class EditMyAccount: UIViewController
{
    @IBOutlet weak var  imgProfile      : UIImageView?
    @IBOutlet weak var  coverImgProfile      : UIImageView?
    @IBOutlet weak var  btnCoverImg        : UIButton?
    @IBOutlet weak var  btnLayer        : UIButton!
    @IBOutlet weak var  btnSave         : UIButton!
    
    @IBOutlet weak var lblNameTitle: UILabel!
    @IBOutlet weak var lblMailTitle: UILabel!
    @IBOutlet weak var lblMobileNoTitle: UILabel!
    @IBOutlet weak var  txtName         : UITextField?
    @IBOutlet weak var  txtMail         : UITextField?
    @IBOutlet weak var  txtMobile       : UITextField?
    @IBOutlet weak var  txtCode         : UITextField?
    
    @IBOutlet weak var  scrollView      : UIScrollView!
    
    @IBOutlet weak var trailingConstantEdit: NSLayoutConstraint!
    
    @IBOutlet weak var coverPageControl: UIPageControl!
    @IBOutlet weak var coverCollectionView: UICollectionView!
    
    @IBOutlet weak var editAddressCarView: UIView!
    @IBOutlet weak var carLabel: UILabel!
    @IBOutlet weak var carTextField: UITextField!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressTextField: UITextField!
    
    @IBOutlet weak var editAddressCarViewHeightConstraint: NSLayoutConstraint!
    
    var isCoverImg : Bool = false
    var isOn : Bool = false
    var profileDict : NSDictionary = [:]
    var croppedImage : UIImage?
    
    var codeAry : NSArray?
    
    @IBOutlet weak var topConstarint: NSLayoutConstraint!
    let imagePicker = UIImagePickerController()
    var imageCropVC : RSKImageCropViewController!
    
    var langAry : NSMutableArray = ["English", "Arabic"];
    var languageStr : NSString?
    var userRoleNo = NSNumber()
    
    var updateAvatarImageName = ""
    
    var coverImageArr : [String] = []
    
    @IBOutlet weak var viewtopConstraint: NSLayoutConstraint!
    
    var isChange = 0
    var address : NSString = ""
    
    var lattitudeAddress : Float = 0.0
    var longitudeAddress : Float = 0.0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        coverCollectionView.isHidden = true
        coverPageControl.isHidden = true
        
        setNavigationBar()
        
        setUpGesture()
        
        codeAry = NSArray.init(contentsOfFile: Bundle.main.path(forResource: "Country", ofType: "plist")!)!
        
        btnLayer?.layer.cornerRadius = (0.258*SCREEN_WIDTH)/2
        btnLayer?.layer.masksToBounds = true
        btnSave?.layer.cornerRadius = 4
        btnSave?.layer.masksToBounds = true
        
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector (doneAction), name: NSNotification.Name(rawValue: "doneAction"), object: nil)
        
        lblNameTitle.text = NSLocalizedString("NAME_TEXT", comment: "")
        lblMailTitle.text = NSLocalizedString("EMAIL_ADDRESS_TITLE", comment: "")
        lblMobileNoTitle.text = NSLocalizedString("MOBILE_NUMBER", comment: "")
        carLabel.text = NSLocalizedString("NO_OF_CARS", comment: "")
        
        addressTextField.placeholder = NSLocalizedString("LOCATION_PLACEHOLDER", comment: "")
        addressLabel.text = NSLocalizedString("ADDRESS", comment: "")
        
        btnSave.setTitle(NSLocalizedString("SAVE_TEXT", comment: ""), for: UIControlState.normal)
        btnLayer.setTitle(NSLocalizedString("EDIT_PHOTO", comment: ""), for: UIControlState.normal)
        
        if(LanguageManager.currentLanguageIndex() == 0)
        {
            txtName?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            txtMail?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            txtCode?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            txtMobile?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            carTextField?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            addressTextField?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            
            txtName?.textAlignment = NSTextAlignment.left
            txtMail?.textAlignment = NSTextAlignment.left
            txtCode?.textAlignment = NSTextAlignment.left
            txtMobile?.textAlignment = NSTextAlignment.left
            carTextField?.textAlignment = NSTextAlignment.left
            addressTextField?.textAlignment = NSTextAlignment.left
        }
        else
        {
            txtName?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            txtMail?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            txtCode?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            txtMobile?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            carTextField?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            addressTextField?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            
            txtName?.textAlignment = NSTextAlignment.right
            txtMail?.textAlignment = NSTextAlignment.right
            txtCode?.textAlignment = NSTextAlignment.right
            txtMobile?.textAlignment = NSTextAlignment.right
            carTextField?.textAlignment = NSTextAlignment.right
            addressTextField?.textAlignment = NSTextAlignment.right
        }
        
        txtName?.placeholder = NSLocalizedString("NAME_PLCAEHOLDER", comment: "")
        txtMail?.placeholder = NSLocalizedString("EMAIL_PLCAEHOLDER", comment: "")
        
        coverCollectionView.register(UINib.init(nibName: "CoverImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CoverImageCollectionViewCellIdentifiler")
        
        btnCoverImg?.isHidden = true
        coverImgProfile?.isHidden = true
        
        coverCollectionView.isHidden = true
        coverPageControl.isHidden = true
        editAddressCarView.isHidden = true
        
        editAddressCarViewHeightConstraint.constant = 0
        
        callApi()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        
        userRoleNo = dict.object(forKey: "user_role") as! NSNumber
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
        
        if lattitudeAddress != 0.0
        {
            MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
            getAddressFromLatLong()
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func callApi()
    {
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute:
            {
                self.showData()
        })
    }
    
    func setNavigationBar()
    {
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        //self.title = "Select Services"
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.title = NSLocalizedString("EDIT_ACCOUNT_TITLE", comment: "")
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func backButtonPressed()
    {
        if userRoleNo == 27
        {
            if coverImageArr.count == 0
            {
                self.uplodCoverImageMethod()
            }
            else
            {
                self.navigationController?.popViewController(animated: true)
            }
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func uplodCoverImageMethod()
    {
        AlertViewController.showAlertWith(title: "", message: NSLocalizedString("Please upload atleast one cover image.", comment: ""), dismissBloack:
            {
                
        })
    }
    
    func showData()
    {
        let str : String = profileDict.object(forKey: "avatar") as! String
        let url = URL(string: str)
        
        imgProfile?.sd_setImage(with: url, placeholderImage: nil)
        
        if (userRoleNo == 27)
        {
            btnCoverImg?.isHidden = false
            coverImgProfile?.isHidden = false
            
            //viewtopConstraint.constant = 200
            
            coverCollectionView.isHidden = false
            coverPageControl.isHidden = false
            
            editAddressCarViewHeightConstraint.constant = 155
            
            imgProfile?.layer.borderWidth =  ((2/375) * SCREEN_WIDTH)
            imgProfile?.layer.borderColor = UIColor.init(red: 247/255, green: 247/255, blue: 247/255, alpha: 1).cgColor
            let dict =  profileDict.object(forKey: "provider_name") as? NSDictionary
            
            if(language == "en")
            {
                txtName?.text = dict?.object(forKey: "en") as? String
            }
            else
            {
                txtName?.text = dict?.object(forKey: "ar") as? String
            }
            
            coverImageArr = profileDict.object(forKey: "cover_image") as! [String]
            
            coverPageControl.numberOfPages = coverImageArr.count
            
            coverCollectionView.reloadData()
            
            self.carTextField.text = "\(profileDict.object(forKey: "number_of_car") as? Int ?? 0)"
            
            self.addressTextField.text = profileDict.object(forKey: "address") as? String
            
            let location1 = profileDict.object(forKey: "location") as! [Float]
            
            lattitude = location1[1]
            longtitude = location1[0]
            
            userLattitude = lattitude
            userLongtitude = longtitude
            
            lattitudeAddress = userLattitude
            longitudeAddress = userLongtitude
            
            self.address = self.addressTextField.text! as NSString
            
            editAddressCarView.isHidden = false
        }
        else
        {
            txtName?.text = profileDict.object(forKey: "name") as? String
        }
        
        imgProfile?.layer.cornerRadius = ((97/375) * SCREEN_WIDTH)/2  //97/375
        imgProfile?.layer.masksToBounds = true
        
        imgProfile?.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
        imgProfile?.layer.borderWidth = 1.0

        txtMail?.text   = profileDict.object(forKey: "email") as? String
        txtMobile?.text = profileDict.object(forKey: "mobile") as? String
        txtCode?.text   = profileDict.object(forKey: "country_code") as? String
        
        isOn = profileDict.object(forKey: "notification_setting") as! Bool
        
        var strCode : NSString
        var i: Int = 0
        
        strCode = (profileDict.object(forKey: "country_code") as? NSString)!
        strCode =  strCode.replacingOccurrences(of: "+", with: "") as NSString
        
        print("strCode = ", strCode)
        
        for countryDict in codeAry as! [NSDictionary]
        {
            var strCCode: String = countryDict["isd_code"] as? String ?? ""
            strCCode = strCCode.replacingOccurrences(of: " ", with: "")
            strCCode = (strCCode.replacingOccurrences(of: "+", with: "") as NSString) as String
            
            if (strCCode == strCode as String)
            {
                let dict = codeAry?.object(at: i) as! NSDictionary
                let str  = dict["isd_code"] as? String
                
                txtCode?.text = "+" + str!
                
                break
            }
            
            i += 1
        }
        
        MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
    }
    
    func doneAction()
    {
        
    }
    
    @IBAction func doneButtonPressed()
    {
        if(CommonFunctions .isInternetAvailable())
        {
            if(validateUserInput())
            {
                MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
                    
                let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
                let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
                let fcm_user_id = dict.object(forKey: "fcm_user_id") as! String
                    
                User.updateUser(withName: (txtName?.text)!, withEmail: (txtMail?.text)!, withId: fcm_user_id, completion: { (isUpdated) in
                    
                    if (isUpdated == true)
                    {
                        if(self.userRoleNo == 27)
                        {
                            //self.updateServiceProviderProfile()
                            //MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
                            self.isChange = 1
                            self.getAddressFromLatLong()
                        }
                        else
                        {
                            self.updateUserProfile()
                        }
                    }
                    else
                    {
                        MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                                
                        AlertViewController.showAlertWith(title: "", message: NSLocalizedString("ERROR_OTHER", comment: ""), dismissBloack:
                        {
                                        
                        })
                    }
                })
            }
        }
    }
    
    /// Used to set tap gesture on view to hide keyboard when tap on tableview
    func setUpGesture()
    {
        let tapGesture: UITapGestureRecognizer
        tapGesture =  UITapGestureRecognizer.init(target: self, action: #selector(handleTapGesture(_:)))
        tapGesture.numberOfTapsRequired = 1
        scrollView.addGestureRecognizer(tapGesture)
    }
    
    func handleTapGesture(_ sender: UITapGestureRecognizer)
    {
        self.view.endEditing(true)
    }
    
    @IBAction func showPic()
    {
        RESIGN_KEYBOARD
        self.isCoverImg = false
        let optionMenu = UIAlertController(title: nil, message: NSLocalizedString("CHOOSE_IMAGE", comment: ""), preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: NSLocalizedString("CAMERA_TEXT", comment: ""), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let galleryAction = UIAlertAction(title: NSLocalizedString("PHOTO_GALLARY", comment: ""), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
            {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("CANCEL_TEXT", comment: ""), style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func showCoverPic()
    {
        RESIGN_KEYBOARD
        self.isCoverImg = true
        
        let optionMenu = UIAlertController(title: nil, message: NSLocalizedString("CHOOSE_IMAGE", comment: ""), preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: NSLocalizedString("CAMERA_TEXT", comment: ""), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if self.isCoverImg
            {
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
                {
                    self.imagePicker.sourceType     = .camera
                    self.imagePicker.allowsEditing  = false
                    self.imagePicker.delegate       = self
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
            }
            else
            {
                self.imagePicker.sourceType     = .camera
                self.imagePicker.allowsEditing  = false
                self.imagePicker.delegate       = self
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let galleryAction = UIAlertAction(title: NSLocalizedString("PHOTO_GALLARY", comment: ""), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if self.isCoverImg
            {
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
                {
                    self.imagePicker.sourceType = .photoLibrary
                    self.imagePicker.allowsEditing =  false
                    self.imagePicker.delegate = self
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
            }
            else
            {
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
                {
                    self.imagePicker.sourceType = .photoLibrary
                    self.imagePicker.allowsEditing =  false
                    self.imagePicker.delegate = self
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
            }
        })
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("CANCEL_TEXT", comment: ""), style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    //MARK: API Call
    
    func updateProfilePic()
    {
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        let service = CustomerService()
        
        let headerDict : NSMutableDictionary = [
            "avatar"    : updateAvatarImageName
        ]
        
        service.updatePicRequestWithParameters(headerDict, success:
        { (response, data) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                        
                        self.imgProfile?.setIndicatorStyle(.gray)
                        self.imgProfile?.setShowActivityIndicator(true)
                        
                        self.imgProfile?.sd_setImage(with: URL.init(string: self.updateAvatarImageName), placeholderImage: #imageLiteral(resourceName: "group20"))
                    })
                }
            }
            else
            {
                AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                    
                })
            }
        })
        { (error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    func updateCoverPic()
    {
        let service = CustomerService()
        
        var headerDict : NSMutableDictionary = [:]
        
        if coverImageArr.count == 0 {
            headerDict = [
                "cover_image" : ""
            ]
        } else {
            headerDict = [
                "cover_image" : CommonFunctions.getCommaSepratedStringFromArrayDict(completeArray: self.coverImageArr)
            ]
        }
        
        service.updateCoverPicRequestWithParameters(headerDict, nil, success:
            { (response, data) in
                
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                
                if(response?.statusCode == 200)
                {
                    let str : NSString = data?.object(forKey: "status") as! NSString
                    
                    if (str.isEqual(to: "success"))
                    {
                        AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                            
                            self.coverCollectionView.reloadData()
                            
                            self.coverPageControl.numberOfPages = self.coverImageArr.count
                            
                            if self.coverImageArr.count == 0 {
                                self.coverCollectionView.isHidden = true
                                self.coverImgProfile?.isHidden = false
                                
                                self.trailingConstantEdit.constant = 10
                                
                            } else {
                                self.coverCollectionView.isHidden = false
                                self.coverImgProfile?.isHidden = true
                                
                                self.trailingConstantEdit.constant = 42
                            }
                        })
                    }
                }
                else
                {
                    AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                        
                    })
                }
        })
        { (error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
        }
    }
    
    func updateUserProfile()
    {
        let service = CustomerService()
        
        let dic : NSMutableDictionary =
            [
            "name"                  : txtName?.text ?? "",
            "mobile"                : txtMobile?.text ?? "",
            "country_code"          : profileDict.object(forKey: "country_code") as! String,
            "language"              : language,
            "notification_setting"  : isOn ?? false
        ]
        
        service.updateProfileRequestWithParameters(dic, success:
        { (response, data) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    let dict = data?.object(forKey: "data") as! NSDictionary
                    print("dict = \(dict)")
                    
                    let name = dict.object(forKey: "name") as! String
                    let fcm_user_id = dict.object(forKey: "fcm_user_id") as! String
                    
                    User.updateUserInformation(withName: name, withEmail: (self.txtMail?.text!)!, withId: fcm_user_id, device_token: device_token as String, completion: { (success) in
                        
                        if (success)
                        {
                            USERDEFAULT.set(NSKeyedArchiver.archivedData(withRootObject: data?.object(forKey: "data") as! NSDictionary), forKey: "USER_DATA")
                            USERDEFAULT.synchronize()
                            
                            AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                                
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                        else
                        {
                            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("ERROR_OTHER", comment: ""), dismissBloack: {
                                
                            })
                        }
                    })
                }
                else
                {
                    AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                        
                    })
                }
            }
            else
            {
                AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                    
                })
            }
        })
        { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    func getAddressFromLatLong()
    {
        let location = CLLocation(latitude: CLLocationDegrees(userLattitude), longitude: CLLocationDegrees(userLongtitude))
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            
            if error != nil
            {
                return
            }
            
            if ((placemarks?.count)! > 0 && userLattitude != 0)
            {
                self.address = ""
                
                let pm = placemarks?[0]
                
                var subLocality : NSString            = ""
                var locality : NSString               = ""
                var administrativeArea : NSString     = ""
                var thoroughfare : NSString           = ""
                
                
                if( pm?.thoroughfare != nil)
                {
                    thoroughfare = (pm?.thoroughfare!)! as NSString
                    self.address = thoroughfare.appending(", ") as NSString
                }
                
                if(pm?.subLocality != nil)
                {
                    subLocality = (pm?.subLocality!)! as NSString
                    self.address = self.address.appending(subLocality as String) as NSString
                    self.address = self.address.appending(", ") as NSString
                }
                
                if(pm?.locality != nil)
                {
                    locality = (pm?.locality!)! as NSString
                    self.address = self.address.appending(locality as String) as NSString
                    self.address = self.address.appending(", ") as NSString
                }
                
                if(pm?.administrativeArea != nil )
                {
                    administrativeArea = (pm?.administrativeArea!)! as NSString
                    self.address = self.address.appending(administrativeArea as String) as NSString
                }
                
                if self.isChange == 0 {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    self.addressTextField.text = self.address as String
                } else {
                    //calling api to switch user
                    self.updateServiceProviderProfile()
                }
            }
            else
            {
                print("Problem with the data received from geocoder")
                self.addressTextField.placeholder = NSLocalizedString("LOCATION_PLACEHOLDER", comment: "")
            }
        })
    }
    
    func updateServiceProviderProfile()
    {
        let service = CustomerService()
        
        let noOfCars = carTextField.text
        
        let dict =  profileDict.object(forKey: "provider_name") as? NSDictionary
        
        var dic = NSMutableDictionary()
        
        if (language == "en")
        {
            dic = [
                "provider_name_en"      : txtName?.text ?? "",
                "provider_name_ar"      : dict?.object(forKey: "ar") as! String,
                "number_of_car"         : noOfCars ?? "0",
                "mobile"                : txtMobile?.text ?? "",
                "country_code"          : profileDict.object(forKey: "country_code") as! String,
                "language"              : language,
                "notification_setting"  : isOn ?? "",
                "lat"                   : userLattitude,
                "lng"                   : userLongtitude,
                "address"               : self.addressTextField.text ?? ""
            ]
        }
        else
        {
            dic = [
                "provider_name_en"      : dict?.object(forKey: "en") as! String,
                "provider_name_ar"      : txtName?.text ?? "",
                "number_of_car"         : noOfCars ?? "0",
                "mobile"                : txtMobile?.text ?? "",
                "country_code"          : profileDict.object(forKey: "country_code") as! String,
                "language"              : language,
                "notification_setting"  : isOn ?? "",
                "lat"                   : userLattitude,
                "lng"                   : userLongtitude,
                "address"               : self.addressTextField.text ?? ""
            ]
        }
        
        service.updateServiceProviderProfileRequestWithParameters(dic, success:
            { (response, data) in
                
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                
                if(response?.statusCode == 200)
                {
                    let str : NSString = data?.object(forKey: "status") as! NSString
                    
                    if (str.isEqual(to: "success"))
                    {
                        let dict = data?.object(forKey: "data") as! NSDictionary
                        print("dict = \(dict)")
                        
                        let name = dict.object(forKey: "name") as! String
                        let fcm_user_id = dict.object(forKey: "fcm_user_id") as! String
                        
                        User.updateUserInformation(withName: name, withEmail: (self.txtMail?.text!)!, withId: fcm_user_id, device_token: device_token as String, completion: { (success) in
                            
                            if (success)
                            {
                                USERDEFAULT.set(NSKeyedArchiver.archivedData(withRootObject: data?.object(forKey: "data") as! NSDictionary), forKey: "USER_DATA")
                                USERDEFAULT.synchronize()
                                
                                AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                                    
                                    self.navigationController?.popViewController(animated: true)
                                })
                            }
                            else
                            {
                                AlertViewController.showAlertWith(title: "", message: NSLocalizedString("ERROR_OTHER", comment: ""), dismissBloack: {
                                    
                                })
                            }
                        })
                    }
                    else
                    {
                        AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                            
                        })
                    }
                }
                else
                {
                    AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                        
                    })
                }
        })
        { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    //MARK: Validate Method
    
    func validateUserInput() -> Bool
    {
        var isError : Bool = true
        
        if(txtName?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_NAME", comment: ""), dismissBloack:
                {
                    self.txtName?.becomeFirstResponder()
            })
        }
        else if(txtMobile?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_PHONE", comment: ""), dismissBloack:
                {
                    self.txtMobile?.becomeFirstResponder()
            })
        }
        else if userRoleNo == 27
        {
            if coverImageArr.count == 0
            {
                isError = false
                self.uplodCoverImageMethod()
            }
            else if(carTextField?.text?.isEmpty)!
            {
                isError = false
                AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_NUMBER", comment: ""), dismissBloack:
                    {
                        self.carTextField?.becomeFirstResponder()
                })
            }
        }
        
        return isError
    }
}

extension EditMyAccount : UITextFieldDelegate
{
    // MARK: UITextField Delegates
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == addressTextField
        {
            let locationVC = SelectLocationViewController()
            self.navigationController?.pushViewController(locationVC, animated: true)
            
            return false
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if(textField == txtMobile)
        {
            resetView()
        }
    }
    
    func resetView()
    {
        scrollView.isScrollEnabled = false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField.textInputMode?.primaryLanguage == "emoji" || !((textField.textInputMode?.primaryLanguage) != nil)
        {
            return false
        }
        
        //if textField  == txtMobile
        //{
        //    if range.location == 12
        //    {
        //        return false
        //    }
        //
        //    let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        //
        //    return checkEnglishPhoneNumberFormat(string: string, str: str, textField: textField)
        //}
        
        if textField == carTextField
        {
            let str = textField.text! as NSString
            
            if str.hasPrefix("0") {
                return false
            }
            
            if str.length >= 2 {
                if string == "" {
                    return true
                } else {
                    return false
                }
            }
        }
        
        return true
    }
    
    func checkEnglishPhoneNumberFormat(string: String?, str: String?, textField: UITextField) -> Bool{
        
        if string == ""
        {
            //BackSpace
            return true
        }
        else if str!.count < 3
        {
            
        }
        else if str!.count == 4
        {
            textField.text = textField.text! + "-"
        }
        else if str!.count == 8
        {
            textField.text = textField.text! + "-"
        }
        else if str!.count > 14
        {
            return false
        }
        
        return true
    }
}

extension EditMyAccount : RSKImageCropViewControllerDelegate, RSKImageCropViewControllerDataSource
{
    // RSKImageCropViewController Delegates
    
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imageCropViewControllerCustomMaskRect(_ controller: RSKImageCropViewController) -> CGRect
    {
        let maskSize = CGSize(width: SCREEN_WIDTH, height: SCREEN_WIDTH * (200/375))
        
        let rect = CGRect(x: (SCREEN_WIDTH - maskSize.width) * 0.5, y: (SCREEN_HEIGTH - maskSize.height) * 0.5, width: maskSize.width, height: maskSize.height)
        
        return rect
    }
    
    func imageCropViewControllerCustomMaskPath(_ controller: RSKImageCropViewController) -> UIBezierPath
    {
        let rect: CGRect = controller.maskRect
        
        if controller.cropMode == RSKImageCropMode.custom
        {
            let point1       = CGPoint(x: rect.minX, y: rect.maxY)
            let point2       = CGPoint(x: rect.maxX, y: rect.maxY)
            let point3       = CGPoint(x: rect.maxX, y: rect.minY)
            let point4       = CGPoint(x: rect.minX, y: rect.minY)
            
            let triangle     = UIBezierPath()
            
            triangle.move(to: point1)
            triangle.addLine(to: point2)
            triangle.addLine(to: point3)
            triangle.addLine(to: point4)
            
            triangle.close()
            
            return triangle
        }
        else
        {
            let point1       = CGPoint(x: rect.minX, y: rect.maxY)
            let point2       = CGPoint(x: rect.maxX, y: rect.maxY)
            let point3       = CGPoint(x: rect.midX, y: rect.minY)
            
            let triangle     = UIBezierPath()
            
            triangle.move(to: point1)
            triangle.addLine(to: point2)
            triangle.addLine(to: point3)
            
            triangle.close()
            
            return triangle
        }
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage _croppedImage: UIImage, usingCropRect cropRect: CGRect)
    {
        croppedImage = _croppedImage
        
        if (!self.isCoverImg)
        {
            imgProfile?.layer.cornerRadius = ((97/375) * SCREEN_WIDTH)/2  //97/375
            imgProfile?.layer.masksToBounds = true
            
            //imgProfile?.image = croppedImage
            
            imgProfile?.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
            imgProfile?.layer.borderWidth = 1.0
            
            _ = CommonFunctions.addFileToFolder("saved", fileName: "save.jpg", fileData: UIImageJPEGRepresentation(croppedImage!, 0.6)!)
            
            if let fileURL = CommonFunctions.getFileURLFromFolder("saved", fileName: "save.jpg") {
                self.uploadAvatarImage(usingImage: fileURL as NSURL)
            }
        }
        else
        {
            //coverImgProfile?.image = croppedImage
            
            _ = CommonFunctions.addFileToFolder("saved", fileName: "save.jpg", fileData: UIImageJPEGRepresentation(croppedImage!, 0.6)!)
            
            if let fileURL = CommonFunctions.getFileURLFromFolder("saved", fileName: "save.jpg") {
                self.uploadCoverImage(usingImage: fileURL as NSURL)
            }
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func uploadAvatarImage(usingImage : NSURL) {
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        let ext = "jpg"
        
        let keyName = "avatar" + CommonFunctions.getCurrentTimeStamp() + ".jpg"
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.body = usingImage as URL
        uploadRequest?.key = keyName
        uploadRequest?.bucket = AmazonS3BucketNameAvatar + CommonFunctions.getUserIdData()!
        uploadRequest?.contentType = "image/" + ext
        uploadRequest?.acl = .publicReadWrite
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith { (task) -> AnyObject! in
            
            if let error = task.error {
                print("Upload failed  (\(error))")
                
                DispatchQueue.main.async(execute: {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    return
                })
            }
            
            if task.result != nil
            {
                print("Uploaded to:fksjflksdajf askldjf")
                
                DispatchQueue.main.async(execute: {
                    
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    self.updateAvatarImageName = "https://s3-us-west-2.amazonaws.com/car-wash/" + "avatar/" + CommonFunctions.getUserIdData()! + "/" + keyName
                    
                    print("name = ", self.updateAvatarImageName)
                    
                    self.updateProfilePic()
                    
                    return
                })
            }
            else
            {
                print("Unexpected empty result.")
                DispatchQueue.main.async(execute: {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    return
                })
            }
            
            return nil
        }
    }
    
    func uploadCoverImage(usingImage : NSURL) {
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        let ext = "jpg"
        
        let keyName = "cover" + CommonFunctions.getCurrentTimeStamp() + ".jpg"
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.body = usingImage as URL
        uploadRequest?.key = keyName
        uploadRequest?.bucket = AmazonS3BucketNameCover + CommonFunctions.getUserIdData()!
        uploadRequest?.contentType = "image/" + ext
        uploadRequest?.acl = .publicReadWrite
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith { (task) -> AnyObject! in
            
            if let error = task.error {
                print("Upload failed  (\(error))")
                
                DispatchQueue.main.async(execute: {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    return
                })
            }
            
            if task.result != nil
            {
                print("Uploaded to:fksjflksdajf askldjf")
                
                DispatchQueue.main.async(execute: {
                    
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    let name = "https://s3-us-west-2.amazonaws.com/car-wash/" + "cover/" + CommonFunctions.getUserIdData()! + "/" + keyName
                    
                    print("name = ", name)
                    
                    self.coverImageArr.append(name)
                    
                    self.coverPageControl.numberOfPages = self.coverImageArr.count
                    
                    self.updateCoverPic()
                    
                    return
                })
            }
            else
            {
                print("Unexpected empty result.")
                DispatchQueue.main.async(execute: {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    return
                })
            }
            
            return nil
        }
    }
}

extension EditMyAccount : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let editedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            picker.dismiss(animated: false, completion: { () -> Void in
                
                if(!self.isCoverImg)
                {
                    self.imageCropVC = nil
                    
                    self.imageCropVC = RSKImageCropViewController(image: editedImage, cropMode: RSKImageCropMode.circle)
                    
                    self.imageCropVC.delegate = self
                    self.imageCropVC.dataSource = self
                
                    self.present(self.imageCropVC, animated: true, completion: nil)
                }
                else
                {
                    self.imageCropVC = nil
                    
                    self.imageCropVC = RSKImageCropViewController(image: editedImage, cropMode: RSKImageCropMode.custom)
                
                    self.imageCropVC.delegate = self
                    self.imageCropVC.dataSource = self
                    
                    self.present(self.imageCropVC, animated: true, completion: nil)
                }
            })
        }
    }
}

//MARK: - Extension -> UICollectionView -> UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension EditMyAccount : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return coverImageArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CoverImageCollectionViewCellIdentifiler", for: indexPath) as! CoverImageCollectionViewCell
        
        print("coverImageArr[indexPath.row] = ", coverImageArr[indexPath.row])
        
        cell.coverImageView.setIndicatorStyle(.gray)
        cell.coverImageView.setShowActivityIndicator(true)
        
        cell.coverImageView.sd_setImage(with: URL.init(string: coverImageArr[indexPath.row]), placeholderImage: #imageLiteral(resourceName: "group29"))
        
        cell.deleteButton.isHidden = false
        
        cell.deleteButton.tag = indexPath.row
        
        cell.deleteButton.addTarget(self, action: #selector(deleteCoverImagesButtonAction), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // your code here
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if coverCollectionView.isHidden == false
        {
            let indexPath = coverCollectionView.indexPathsForVisibleItems.first
            coverPageControl.currentPage = (indexPath?.row)!
        }
    }
    
    func deleteCoverImagesButtonAction(sender : UIButton)
    {
        let tag = sender.tag
        print("tag = \(tag)")
        
        AlertViewController.showAlertWith(title: "", message: NSLocalizedString("Are you sure to delete this cover image?", comment: ""), buttonsArray: [NSLocalizedString("YES_TEXT", comment: ""), NSLocalizedString("NO_TEXT", comment: "")]) { (buttonIndex) in
        
            if buttonIndex == 0
            {
                self.coverImageArr.remove(at: tag)
                self.updateCoverPic()
            }
        }
    }
}
