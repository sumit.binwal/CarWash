//
//  ContactUs.swift
//  CarWash
//
//  Created by Neha Chaudhary on 25/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD
import MessageUI
import SafariServices

class ContactUs: UIViewController
{
    @IBOutlet var txtTo         : UITextField?
    @IBOutlet var txtSubject    : UITextField?
    @IBOutlet var txtMessage    : UITextField?
    @IBOutlet var btnSend       : UIButton?
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblNavigationTitle: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var webLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var twitterLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var navigationView: UIView!
    var isParesent : String = "0"

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if isParesent == "1"
        {
            navigationView.isHidden = false
            topConstraint.constant = 70
            
            //add shadow on navigation bar
            navigationView.layer.masksToBounds = false
            navigationView.layer.shadowColor = UIColor.darkGray.cgColor
            navigationView.layer.shadowOpacity = 0.2
            navigationView.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        }
        else
        {
            navigationView.isHidden = true
            topConstraint.constant = 0
        }
        
        setNavigationBar()
        
        lblTo.text = NSLocalizedString("TO_TEXT", comment: "")
        lblSubject.text = NSLocalizedString("SUBJECT_TEXT", comment: "")
        lblMessage.text = NSLocalizedString("MESSAGE_TEXT", comment: "")
        btnSend?.setTitle(NSLocalizedString("SEND_TEXT", comment: ""), for: UIControlState.normal)
        
        webLabel.text = NSLocalizedString("Web", comment: "")
        emailLabel.text = NSLocalizedString("EMAIL_PLCAEHOLDER", comment: "")
        twitterLabel.text = NSLocalizedString("Twitter", comment: "")
        phoneLabel.text = NSLocalizedString("Phone No.", comment: "")
        
        if (LanguageManager.currentLanguageIndex() == 0)
        {
            txtTo?.contentHorizontalAlignment         = UIControlContentHorizontalAlignment.right
            txtSubject?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            txtMessage?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
           
            txtTo?.textAlignment      = NSTextAlignment.left
            txtSubject?.textAlignment = NSTextAlignment.left
            txtMessage?.textAlignment = NSTextAlignment.left
        }
        else
        {
            txtTo?.contentHorizontalAlignment         = UIControlContentHorizontalAlignment.left
            txtSubject?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            txtMessage?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
        
            txtTo?.textAlignment      = NSTextAlignment.right
            txtSubject?.textAlignment = NSTextAlignment.right
            txtMessage?.textAlignment = NSTextAlignment.right
        }
    }

    func setNavigationBar()
    {
        txtTo?.text = "admin@admin.com"
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        //self.title = "Select Services"
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.title = NSLocalizedString("CONTACT_US", comment: "")
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        
        btnSend?.layer.cornerRadius = 5.0
        btnSend?.layer.masksToBounds = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
    }
    
    @IBAction func send ()
    {
        if (CommonFunctions.isInternetAvailable())
        {
            if(validateUserInput())
            {
                self.view.endEditing(true)
                
                MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
                contactUsApi()
            }
        }
    }
    
    @IBAction func phoneButtonAction(_ sender: Any) {
        
        let contact : String = "+966538937331"
        CommonFunctions.callNumber(phoneNumber: contact)
    }
    
    @IBAction func twitterButtonAction(_ sender: Any) {
        
        let openUrl = URL(string: "https://twitter.com/@sayarapp")
        
        let safariController : SFSafariViewController = SFSafariViewController(url: openUrl!)
        safariController.delegate = self as? SFSafariViewControllerDelegate
        self.present(safariController, animated: true, completion: nil)
    }
    
    @IBAction func webButtonAction(_ sender: Any) {
        
        let openUrl = URL(string: "http://sayarapp.com")
        
        let safariController : SFSafariViewController = SFSafariViewController(url: openUrl!)
        safariController.delegate = self as? SFSafariViewControllerDelegate
        self.present(safariController, animated: true, completion: nil)
    }
    
    @IBAction func emailButtonAction(_ sender: Any)
    {
        if MFMailComposeViewController.canSendMail()
        {
            let mail = MFMailComposeViewController()
            
            mail.mailComposeDelegate = self
            mail.setToRecipients(["info@sayarapp.com"])
            
            self.present(mail, animated: true, completion: nil)
        }
        else
        {
            print("Cannot send mail")
            // give feedback to the user
        }
    }
    
    //MARK: Validate Method
    
    func validateUserInput() -> Bool
    {
        var isError : Bool = true
        
        if(txtSubject?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_SUBJECT", comment: ""), dismissBloack:
                {
                    self.txtSubject?.becomeFirstResponder()
            })
        }
        else if(txtMessage?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_MESSAGE", comment: ""), dismissBloack:
                {
                    self.txtMessage?.becomeFirstResponder()
            })
        }
        
        return isError
    }

    // MARK: API Call
    
    func contactUsApi()
    {
        let service = CustomerService()
        
        let dic : NSMutableDictionary =
        [
            "to"            : txtTo?.text ?? "",
            "subject"       : txtSubject?.text ?? "",
            "messages"      : txtMessage?.text ?? ""
        ]
        
        service.contactUsRequestWithParameters(dic, success:
            { (response, data) in
                
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                
                if(response?.statusCode == 200)
                {
                  //  let str : NSString = data?.object(forKey: "status") as! NSString
                    
                    //print("data = \(String(describing: data))")
                    let msg = data?.object(forKey: "msg") as! String
                    //print("msg = \(msg)")
                    
                    if self.isParesent == "1" {
                        
                        var alertController: UIAlertController
                        
                        alertController = UIAlertController.init(title: "", message: msg, preferredStyle: UIAlertControllerStyle.alert)
                        
                        var alertAction: UIAlertAction
                        
                        alertAction = UIAlertAction.init(title: NSLocalizedString("OK_TEXT", comment: ""), style: UIAlertActionStyle.default, handler: { (alertAction) in
                            
                            self.dismiss(animated: true, completion: nil)
                            
                        })
                        
                        alertController.addAction(alertAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                    } else {
                        
                        AlertViewController.showAlertWith(title: "", message: msg, dismissBloack:
                            {
                                self.navigationController?.popViewController(animated: true)
                        })
                    }
                }
                else
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack:
                        {
                            
                    })
                }
        })
        { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ContactUs : MFMailComposeViewControllerDelegate
{
    // MARK: - MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result.rawValue
        {
        case MFMailComposeResult.cancelled.rawValue:
            print("Cancelled")
            
        case MFMailComposeResult.saved.rawValue:
            print("Saved")
            
        case MFMailComposeResult.failed.rawValue:
            print("Failed")
            
        case MFMailComposeResult.sent.rawValue:
            print("Sent")
            
        default:
            break
        }
        
        controller.dismiss(animated: true, completion: nil)
    }
}
