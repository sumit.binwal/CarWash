//
//  ChangePasswordVC.swift
//  CarWash
//
//  Created by Neha Chaudhary on 20/09/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD

class SetPasswordVC: UIViewController
{
    @IBOutlet weak var  txtNewPwd         : UITextField?
    @IBOutlet weak var  txtConfirm        : UITextField?
    @IBOutlet weak var  btnSave           : UIButton?
    @IBOutlet var pwdChangedPopUp         : UIView?
    
    @IBOutlet weak var  imgTick           : UIImageView?
    @IBOutlet weak var lblNewPassword: UILabel!
    @IBOutlet weak var lblConfirmPassword: UILabel!
    @IBOutlet weak var lblPasswordChanged: UILabel!
    @IBOutlet weak var lblPasswordUpdate: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    
    var isFromConfirmBooking = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        setNavigationBar()
        
        imgTick?.isHidden = true
        btnSave?.layer.cornerRadius = 5.0
        btnSave?.layer.masksToBounds = true
        
        pwdChangedPopUp?.viewWithTag(1)?.layer.cornerRadius = 5.0
        pwdChangedPopUp?.viewWithTag(1)?.layer.masksToBounds = true
        
        lblNewPassword.text = NSLocalizedString("NEW_PASSWORD", comment: "")
        lblConfirmPassword.text = NSLocalizedString("CONFIRM_PASSWORD", comment: "")
        lblPasswordChanged.text = NSLocalizedString("PASSWORD_CHANGED", comment: "")
        lblPasswordUpdate.text = NSLocalizedString("PASSWORD_UPDATE_MESSAGE", comment: "")
        btnSave?.setTitle(NSLocalizedString("SET_PASSWORD", comment: ""), for: UIControlState.normal)
        btnOk.setTitle(NSLocalizedString("OK_TEXT", comment: ""), for: UIControlState.normal)
        
        if LanguageManager.currentLanguageIndex() == 0 {
            
            txtNewPwd?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            txtConfirm?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            
            txtNewPwd?.textAlignment = NSTextAlignment.left
            txtConfirm?.textAlignment = NSTextAlignment.left
            
        } else {
            
            txtNewPwd?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            txtConfirm?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            
            txtNewPwd?.textAlignment = NSTextAlignment.right
            txtConfirm?.textAlignment = NSTextAlignment.right
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
    }
    
    func setNavigationBar()
    {
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.title =  NSLocalizedString("SET_NEW_PASSWORD", comment: "")
        
        self.navigationItem.hidesBackButton = true
        
        //self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex() == 0 ? "back" : "backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - IBAction Methods
    
    @IBAction func pwdChangedOk()
    {
        pwdChangedPopUp?.removeFromSuperview()
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: SignInVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @IBAction func closePwdChangedView()
    {
        pwdChangedPopUp?.removeFromSuperview()
        for controller in self.navigationController!.viewControllers as Array
        {
            if controller.isKind(of: SignInVC.self)
            {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @IBAction func save()
    {
        if validateUserInput()
        {
            if(CommonFunctions.isInternetAvailable())
            {
                MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
                resetPwdApi()
            }
        }
    }
    
    //MARK: Validate Method
    
    func validateUserInput() -> Bool
    {
        var isError : Bool = true
        
        if(txtNewPwd?.text?.isEmpty)!
        {
            isError = false
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("EMPTY_NEW_PASSWORD", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
            })
        }
        else if((txtNewPwd?.text?.count)! < 6 || (txtNewPwd?.text?.count)! > 30)
        {
            isError = false
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("PASSWORD_LENGTH", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
            })
        }
        else if(txtConfirm?.text?.isEmpty)!
        {
            isError = false
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("EMPTY_CONFIRMPASSWORD", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
            })
        }
        else if(txtNewPwd?.text != txtConfirm?.text)
        {
            isError = false
            CommonFunctions.showAlertWithTitle(title: "", message: NSLocalizedString("MATCH_CONFIRMPASSWORD", comment: ""), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
            })
        }
    
        return isError
    }
 
    // MARK: API Call
    
    func resetPwdApi()
    {
        let service = CustomerService()
        let token : NSString = USERDEFAULT.object(forKey: "temp_token") as! NSString
        
        let dic : NSMutableDictionary = [
            
            "temp_token"      : token,
            "password"        : txtNewPwd?.text ?? ""
        ]
        
        service.resetPwdAPIRequestWithParameters(dic, success:
            { (response, data) in
                
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                
                if(response?.statusCode == 200)
                {
                    let str : NSString = data?.object(forKey: "status") as! NSString
                    
                    if (str.isEqual(to: "success"))
                    {
                       self.pwdChangedPopUp?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
                        APPDELEGATE.window?.addSubview(self.pwdChangedPopUp!)
                    }
                }
                else
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    CommonFunctions.showAlertWithTitle(title: "", message: data?.object(forKey: "msg") as? String, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                    })
                }
        })
        { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
}

extension SetPasswordVC : UITextFieldDelegate
{
    // MARK: TextField Delegates
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        //if !string.canBeConverted(to: String.Encoding.ascii){
        //    return false
        //}
        
        if textField.textInputMode?.primaryLanguage == "emoji" || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        if(textField == txtConfirm)
        {
            var updatedTextString : NSString = textField.text! as NSString
            updatedTextString = updatedTextString.replacingCharacters(in: range, with: string) as NSString
            
            if (updatedTextString .isEqual(to: (txtNewPwd?.text)!))
            {
                imgTick?.isHidden = false
            }
            else
            {
                imgTick?.isHidden = true
            }
        }
        
        return true
    }
}
