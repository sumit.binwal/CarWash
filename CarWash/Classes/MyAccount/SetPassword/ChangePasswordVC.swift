//
//  ChangePasswordVC.swift
//  CarWash
//
//  Created by iOS on 04/11/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import  MBProgressHUD

class ChangePasswordVC: UIViewController
{
    @IBOutlet weak var  txtOldPwd         : UITextField?
    @IBOutlet weak var  txtNewPwd         : UITextField?
    @IBOutlet weak var  txtConfirm        : UITextField?
    @IBOutlet weak var  btnSave           : UIButton!
    @IBOutlet var pwdChangedPopUp         : UIView?
    @IBOutlet weak var lblPasswordChanged: UILabel!
    @IBOutlet weak var lblPasswordUpdate: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    
    @IBOutlet weak var lblOldPassword: UILabel!
    @IBOutlet weak var lblNewPassword: UILabel!
    @IBOutlet weak var lblConfirmPassword: UILabel!
    @IBOutlet weak var  imgTick           : UIImageView?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        setNavigationBar()
        imgTick?.isHidden = true
        btnSave?.layer.cornerRadius = 5.0
        btnSave?.layer.masksToBounds = true
        
        pwdChangedPopUp?.viewWithTag(1)?.layer.cornerRadius = 3.0
        pwdChangedPopUp?.viewWithTag(1)?.layer.masksToBounds = true
        
        lblNewPassword.text = NSLocalizedString("NEW_PASSWORD", comment: "")
        lblOldPassword.text = NSLocalizedString("OLD_PASSWORD", comment: "")
        lblConfirmPassword.text = NSLocalizedString("CONFIRM_PASSWORD", comment: "")
        lblPasswordChanged.text = NSLocalizedString("PASSWORD_CHANGED", comment: "")
        lblPasswordUpdate.text = NSLocalizedString("PASSWORD_UPDATE_MESSAGE", comment: "")
        btnSave.setTitle(NSLocalizedString("SAVE_PASSWORD", comment: ""), for: UIControlState.normal)
        btnOk.setTitle(NSLocalizedString("OK_TEXT", comment: ""), for: UIControlState.normal)

        
        if(LanguageManager.currentLanguageIndex()==0){
            txtOldPwd?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            txtNewPwd?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            txtConfirm?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            txtOldPwd?.textAlignment = NSTextAlignment.left
            txtNewPwd?.textAlignment = NSTextAlignment.left
            txtConfirm?.textAlignment = NSTextAlignment.left
            
        }else{
            txtOldPwd?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            txtNewPwd?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            txtConfirm?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            txtOldPwd?.textAlignment = NSTextAlignment.right
            txtNewPwd?.textAlignment = NSTextAlignment.right
            txtConfirm?.textAlignment = NSTextAlignment.right
        }
    }

    func setNavigationBar()
    {
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        //self.title = "Select Services"
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.title = NSLocalizedString("CHANGE_PASSWORD", comment: "")
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
    }
    
    // MARK: - IBAction Methods
    
    @IBAction func pwdChangedOk()
    {
        pwdChangedPopUp?.removeFromSuperview()
        self.userLogoutApi()
        //self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closePwdChangedView()
    {
        pwdChangedPopUp?.removeFromSuperview()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func save()
    {
        self.view.endEditing(true)
        
        if validateUserInput()
        {
            if(CommonFunctions.isInternetAvailable())
            {
                MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
                
                changePasswordApi()
            }
        }
    }
    
    //MARK: Validate Method
    
    func validateUserInput() -> Bool
    {
        var isError : Bool = true
        
        if(txtOldPwd?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_OLD_PASSWORD", comment: ""), dismissBloack:
                {
                    self.txtOldPwd?.becomeFirstResponder()
            })
        }
        else if(txtNewPwd?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_NEW_PASSWORD", comment: ""), dismissBloack:
                {
                    self.txtNewPwd?.becomeFirstResponder()
            })
        }
        else if((txtNewPwd?.text?.count)! < 6 || (txtNewPwd?.text?.count)! > 30)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("PASSWORD_LENGTH", comment: ""), dismissBloack:
                {
                    self.txtNewPwd?.becomeFirstResponder()
            })
        }
        else if(txtConfirm?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_CONFIRMPASSWORD", comment: ""), dismissBloack:
                {
                     self.txtConfirm?.becomeFirstResponder()
            })
        }
        else if(txtNewPwd?.text != txtConfirm?.text)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("MATCH_CONFIRMPASSWORD", comment: ""), dismissBloack:
                {
                    self.txtConfirm?.becomeFirstResponder()
            })
        }
        
        return isError
    }
    
    //MARK: API Call
    
    func changePasswordApi()
    {
        let service = CustomerService()
        
        let dic : NSMutableDictionary = [
            
            "current_password"      : txtOldPwd?.text ?? "" as NSString,
            "password"              : txtNewPwd?.text ?? "" as NSString
        ]
        
        service.changePwdAPIRequestWithParameters(dic, success:
            { (response, data) in
                
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                
                if(response?.statusCode == 200)
                {
                    let str : NSString = data?.object(forKey: "status") as! NSString
                    
                    if (str.isEqual(to: "success"))
                    {
                        self.pwdChangedPopUp?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
                        APPDELEGATE.window?.addSubview(self.pwdChangedPopUp!)
                    }
                    else
                    {
                        AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack:
                            {
                        })
                    }
                }
                else
                {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack:
                        {
                            
                    })
                }
        })
        { (response, error) in
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    func userLogoutApi()
    {
        let service = CustomerService()
        
        service.logoutUserRequestWithParameters(nil, success: { (response, data) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            //print("about us data = ", data ?? "")
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    //AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                        
                        USERDEFAULT.removeObject(forKey: "USER_DATA")
                        USERDEFAULT.synchronize()
                        
                        let vc = WelComeVC() //change this to your class name
                        
                        let navController = UINavigationController(rootViewController: vc)
                        
                        APPDELEGATE.window?.rootViewController = navController
                    //})
                }
            }
            else
            {
                AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                    
                })
            }
            
        }) { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    //MARK: TextField Delegates
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        //if !string.canBeConverted(to: String.Encoding.ascii){
        //    return false
        //}
        
        if textField.textInputMode?.primaryLanguage == "emoji" || !((textField.textInputMode?.primaryLanguage) != nil) {
            return false
        }
        
        if(textField == txtConfirm)
        {
            var updatedTextString : NSString = textField.text! as NSString
            updatedTextString = updatedTextString.replacingCharacters(in: range, with: string) as NSString
            
            if (updatedTextString .isEqual(to: (txtNewPwd?.text)!))
            {
                imgTick?.isHidden = false
            }
            else
            {
                imgTick?.isHidden = true
            }
        }
        
        return true
    }

}
