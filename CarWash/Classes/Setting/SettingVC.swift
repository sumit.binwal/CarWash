//
//  SettingVC.swift
//  CarWash
//
//  Created by Devendra on 12/12/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD

class SettingVC: UIViewController, Document, MyProtocol
{
    @IBOutlet var tblSetting : UITableView?
    @IBOutlet var aboutUsView        : UIView?
    
    @IBOutlet weak var aboutView: UIView!
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var aboutWebView: UIWebView!
    
    @IBOutlet weak var versionLabel: UILabel!
    
    let ary : NSMutableArray = []
    var hoursAry : NSMutableArray = []
    var isDocument  : Bool?
    
    var userDict : NSDictionary = [:]
    var isOn  : Bool = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        isDocument  = false
        
        aboutView.layer.cornerRadius = 10.0
        aboutView.layer.borderColor = UIColor.clear.cgColor
        aboutView.layer.borderWidth = 1.0
        aboutView.layer.masksToBounds = true
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        
        //print("dict = ", dict)
        
        let str = dict.object(forKey: "user_role") as! NSNumber
        
        if(str == 27)
        {
            ary.add(NSLocalizedString("MY_ACCOUNT_TITLE", comment: ""))
            ary.add(NSLocalizedString("HISTORY_TITLE", comment: ""))
            ary.add(NSLocalizedString("My Reviews", comment: ""))
            ary.add(NSLocalizedString("NOTIFICATIONS_TEXT", comment: ""))
            ary.add(NSLocalizedString("AVAILABILITY_TEXT", comment: ""))
            ary.add(NSLocalizedString("UPLOAD_EDIT_DOCUMENT", comment: ""))
            ary.add(NSLocalizedString("MANAGE_SHIFT_HOUR", comment: ""))
            ary.add(NSLocalizedString("RATE_TEXT", comment: ""))
            ary.add(NSLocalizedString("SHARE_TEXT", comment: ""))
            ary.add(NSLocalizedString("Tips", comment: ""))
            ary.add(NSLocalizedString("ABOUT_US", comment: ""))
            ary.add(NSLocalizedString("CONTACT_US", comment: ""))
            ary.add(NSLocalizedString("LOG_OUT", comment: ""))
            
            if let tempNames: NSArray = dict.object(forKey: "operating_hours") as? NSArray
            {
                hoursAry = tempNames.mutableCopy() as! NSMutableArray
            }
            else
            {
                hoursAry = (dict.object(forKey: "operating_hours") as? NSMutableArray)!
            }
        }
        else
        {
            ary.add(NSLocalizedString("MY_ACCOUNT_TITLE", comment: ""))
            ary.add(NSLocalizedString("NOTIFICATIONS_TEXT", comment: ""))
            ary.add(NSLocalizedString("RATE_TEXT", comment: ""))
            ary.add(NSLocalizedString("SHARE_TEXT", comment: ""))
            ary.add(NSLocalizedString("Tips", comment: ""))
            ary.add(NSLocalizedString("ABOUT_US", comment: ""))
            ary.add(NSLocalizedString("CONTACT_US", comment: ""))
            ary.add(NSLocalizedString("LOG_OUT", comment: ""))
        }
        
        self.versionLabel.text = NSLocalizedString("VERSION", comment : "")
        
        setUpNavigationBar()
        
        tblSetting?.register(UINib(nibName: "SettingCell", bundle: nil), forCellReuseIdentifier: "SettingCellID")
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        userProfile()
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        
        let str = dict.object(forKey: "user_role") as! NSNumber
        if(str == 27)
        {
            if let tempNames: NSArray = dict.object(forKey: "operating_hours") as? NSArray
            {
                hoursAry = tempNames.mutableCopy() as! NSMutableArray
            }
            else
            {
                hoursAry = (dict.object(forKey: "operating_hours") as? NSMutableArray)!
            }
        }
    }
    
    func setCountOfDocument(valueSent: Bool)
    {
        isDocument = valueSent
    }
    
    func setResultOfBusinessLogic(valueSent: NSMutableArray)
    {
        hoursAry = valueSent
    }
    
    func setUpNavigationBar()
    {
        self.title = NSLocalizedString("SETTING", comment: "")
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
    }
    
    //MARK:  API Call
    
    func userProfile()
    {
        let service = CustomerService()
        
        service.profileRequestWithParameters(nil, success:
            { (response, data) in
                
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                
                if(response?.statusCode == 200)
                {
                    let str : NSString = data?.object(forKey: "status") as! NSString
                    
                    if (str.isEqual(to: "success"))
                    {
                        self.userDict = (data?.object(forKey: "data") as? NSDictionary)!
                        self.showData()
                    }
                }
        })
        { (response, error) in
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    func showData()
    {
        self.tblSetting?.reloadData()
    }
    
    func userLogoutApi()
    {
        let service = CustomerService()
        
        service.logoutUserRequestWithParameters(nil, success: { (response, data) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            //print("about us data = ", data ?? "")
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                        
                        USERDEFAULT.removeObject(forKey: "USER_DATA")
                        USERDEFAULT.synchronize()
                        
                        let vc = SignInVC() //change this to your class name
                        
                        let navController = UINavigationController(rootViewController: vc)
                       
                        APPDELEGATE.window?.rootViewController = navController
                    })
                }
            }
            else
            {
                AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                    
                })
            }
            
        }) { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    @IBAction func closeAboutUs()
    {
        //aboutUsView?.removeFromSuperview()
        
        let animation = CATransition()
        animation.delegate = self as? CAAnimationDelegate
        animation.type = kCATransitionPush
        animation.subtype = kCATransitionFromBottom
        animation.duration = 0.40
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        self.aboutUsView?.layer.add(animation, forKey: kCATransition)
        self.aboutUsView?.frame = CGRect(x: 0, y: SCREEN_HEIGTH, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getAboutUsApi(tap_str : String)
    {
        let service = CustomerService()
        
        var stringName = ""
        if tap_str == "about"
        {
            stringName = "about-us"
        }
        else
        {
            stringName = "hint-and-tips"
        }
        
        service.getAboutUsDataRequestWithParameters(nil, stringName as NSString, success: { (response, data) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            //print("about us data = ", data ?? "")
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    let sectionNames : NSDictionary = data?.object(forKey: "data") as! NSDictionary
                    
                    var stringDescription : NSString? = nil
                    var titleNameStr : String? = nil
                    
                    if language == "en"
                    {
                        stringDescription = sectionNames.object(forKey: "description") as? NSString
                        titleNameStr = sectionNames.object(forKey: "title") as? String
                    }
                    else
                    {
                        stringDescription = sectionNames.object(forKey: "description_ar") as? NSString
                        titleNameStr = sectionNames.object(forKey: "title_ar") as? String
                    }
                    
                    DispatchQueue.main.async
                        {
                            self.aboutWebView.scrollView.isScrollEnabled = true
                            self.aboutWebView.isUserInteractionEnabled = true
                            self.aboutWebView.scrollView.showsVerticalScrollIndicator = false;
                            self.aboutWebView.scrollView.showsHorizontalScrollIndicator = false
                            
                            self.aboutWebView.delegate = self
                            
                            self.aboutLabel.text = titleNameStr
                            self.aboutWebView.loadHTMLString(stringDescription! as String, baseURL: nil)
                    }
                }
            }
            else
            {
                AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                    
                })
            }
            
        }) { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
}

extension SettingVC : UITableViewDataSource, UITableViewDelegate
{
    // MARK: - tableview delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  ary.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return (50.0/375.0) * SCREEN_WIDTH
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCellID") as! SettingCell?
        
        cell?.lblTitle?.text = ary[indexPath.row] as? String
        
        if (LanguageManager.currentLanguageIndex() == 0) {
            cell?.imgView?.image = UIImage(named:"tableArrow")
        } else {
            cell?.imgView?.image = UIImage(named:"tableArrowRight")
        }
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        
        let str = dict.object(forKey: "user_role") as! NSNumber
        
        var num = 0
        if str == 27 {
            num = 3
        } else {
            num = 1
        }
        
        if str == 27 || indexPath.row == num
        {
            cell?.notificationSwitch.isHidden = true
            
            if(userDict.allKeys.count > 0)
            {
                cell?.notificationSwitch?.tintColor = UIColor.clear
                cell?.notificationSwitch?.backgroundColor = UIColor.uicolorFromRGB(168, 168, 168)
                cell?.notificationSwitch?.layer.cornerRadius = 16
                
                if indexPath.row == num{
             cell?.notificationSwitch.isHidden = false
                    cell?.notificationSwitch?.isOn =  (userDict.object(forKey: "notification_setting") as? Bool)!
                    cell?.notificationSwitch?.addTarget(self, action: #selector(changeNotificationValue), for: UIControlEvents.touchUpInside)

                }
                else if (indexPath.row == 4 && str==27)
                
                {
                    cell?.notificationSwitch.isHidden = false

                    cell?.notificationSwitch?.isOn =  (userDict.object(forKey: "is_available") as? Bool)!
                    cell?.notificationSwitch?.addTarget(self, action: #selector(activityOnOffApi), for: UIControlEvents.touchUpInside)
                }
//                cell?.notificationSwitch?.isOn = isOn
                cell?.notificationSwitch?.isUserInteractionEnabled = true
            }

        }
        else
        {
            cell?.notificationSwitch.isHidden = true
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell!
    }
    
    func  tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        
        let str = dict.object(forKey: "user_role") as! NSNumber
        
        switch indexPath.row {
        case 0:
            
            if(str == 27)
            {
                // my account
                let myAccountVC = MyAccountVC()
                self.navigationController?.pushViewController(myAccountVC, animated: true)
            }
            else
            {
                // my account
                let myAccountVC = MyAccountVC()
                self.navigationController?.pushViewController(myAccountVC, animated: true)
            }
            
        case 1:
            
            if(str == 27)
            {
                // provider history
                let providerHistoryVC = ProviderHistoryVC()
                self.navigationController?.pushViewController(providerHistoryVC, animated: true)
            }
            
        case 2:
            if(str == 27)
            {
                print("my reviews")
                
                let reviewlist = RatingViewController()
                self.navigationController?.pushViewController(reviewlist, animated: true)
            }
            else
            {
                print("Rate ")
                APPDELEGATE.requestReview()
            }
            
        case 3:
            if(str != 27)
            {
                print("Share ")
                
                let activityItems = [NSLocalizedString("SHARE_MSG", comment: "")]
                
                let activityViewController = UIActivityViewController.init(activityItems: activityItems, applicationActivities: nil)
                
                activityViewController.excludedActivityTypes = [UIActivityType.copyToPasteboard, UIActivityType.assignToContact, UIActivityType.postToWeibo, UIActivityType.saveToCameraRoll, UIActivityType.postToFacebook, UIActivityType.mail]
                
                self.present(activityViewController, animated: true, completion: nil)
            }
        case 4:
            if (str != 27)
            {
                print("Tips")
                
                self.aboutUsView?.removeFromSuperview()
                APPDELEGATE.window?.addSubview(self.aboutUsView!)
                self.aboutUsView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
                
                MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
                self.getAboutUsApi(tap_str: "tips")
                
                let animation = CATransition()
                
                let scrollableSize = CGSize(width: aboutWebView.frame.size.width, height: aboutWebView.scrollView.contentSize.height)
                self.aboutWebView?.scrollView.contentSize = scrollableSize
                
                animation.delegate = self as? CAAnimationDelegate
                animation.type = kCATransitionPush
                animation.subtype = kCATransitionFromTop
                animation.duration = 0.40
                animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                
                self.aboutUsView?.layer.add(animation, forKey: kCATransition)
            }
            
        case 5:
            if(str == 27)
            {
                print("upload document ")
                
                let uploadVC = UploadVC()
                uploadVC.delegate = self
                uploadVC.isEditMode = true
                self.navigationController?.pushViewController(uploadVC, animated: true)
            }
            else
            {
                print("About Us ")
                
                self.aboutUsView?.removeFromSuperview()
                APPDELEGATE.window?.addSubview(self.aboutUsView!)
                self.aboutUsView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
                
                MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
                self.getAboutUsApi(tap_str: "about")
                
                let animation = CATransition()
                
                animation.delegate = self as? CAAnimationDelegate
                animation.type = kCATransitionPush
                animation.subtype = kCATransitionFromTop
                animation.duration = 0.40
                animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                
                self.aboutUsView?.layer.add(animation, forKey: kCATransition)
            }
            
        case 6:
            if(str == 27)
            {
                print("Manage Shift ")
                
                let operating       =  OperatingHoursVC()
                
                operating.delegate  = self
                
                if(hoursAry.count > 0)
                {
                    operating.hoursDict = hoursAry.mutableCopy() as? NSMutableArray
                }
                
                if (userDict.object(forKey: "is_available") as? Bool)!
                {
                self.navigationController?.pushViewController(operating, animated: true)
                }
                
            }
            else
            {
                print("Contact Us ")
                let contactVC = ContactUs()
                self.navigationController?.pushViewController(contactVC, animated: true)
            }
            
            
        case 7:
            if(str == 27)
            {
                print("Rate ")
                
                APPDELEGATE.requestReview()
            }
            else
            {
                print("Logout ")
                
                DispatchQueue.main.async
                    {
                        let alertController = UIAlertController(title: "", message: NSLocalizedString("LOGOUT_ALERT", comment: ""), preferredStyle: .alert)
                        
                        let action1 = UIAlertAction(title: NSLocalizedString("YES_TEXT", comment: ""), style: .default) { (action:UIAlertAction) in
                            
                            self.deleteInformationData()
                        }
                        
                        let action2 = UIAlertAction(title: NSLocalizedString("NO_TEXT", comment: ""), style: .default) { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        
                        
                        alertController.addAction(action2)
                        alertController.addAction(action1)
                        
                        self.present(alertController, animated: true, completion: nil)
                }
            }
            
        case 8:
            if(str == 27)
            {
                print("Share ")
                
                let activityItems = [NSLocalizedString("SHARE_MSG", comment: "")]
                
                let activityViewController = UIActivityViewController.init(activityItems: activityItems, applicationActivities: nil)
                
                activityViewController.excludedActivityTypes = [UIActivityType.copyToPasteboard, UIActivityType.assignToContact, UIActivityType.postToWeibo, UIActivityType.saveToCameraRoll, UIActivityType.postToFacebook, UIActivityType.mail]
                
                self.present(activityViewController, animated: true, completion: nil)
            }
            
            
            
        case 9:
            if(str == 27)
            {
                print("tips")
                
                self.aboutUsView?.removeFromSuperview()
                APPDELEGATE.window?.addSubview(self.aboutUsView!)
                self.aboutUsView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
                
                MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
                self.getAboutUsApi(tap_str: "tips")
                
                let scrollableSize = CGSize(width: aboutWebView.frame.size.width, height: aboutWebView.scrollView.contentSize.height)
                self.aboutWebView?.scrollView.contentSize = scrollableSize
                
                let animation = CATransition()
                
                animation.delegate = self as? CAAnimationDelegate
                animation.type = kCATransitionPush
                animation.subtype = kCATransitionFromTop
                animation.duration = 0.40
                animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                
                self.aboutUsView?.layer.add(animation, forKey: kCATransition)
            }
            
        case 10:
            if(str == 27)
            {
                print("About Us ")
                
                self.aboutUsView?.removeFromSuperview()
                APPDELEGATE.window?.addSubview(self.aboutUsView!)
                self.aboutUsView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
                
                MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
                self.getAboutUsApi(tap_str: "about")
                
                let animation = CATransition()
                
                animation.delegate = self as? CAAnimationDelegate
                animation.type = kCATransitionPush
                animation.subtype = kCATransitionFromTop
                animation.duration = 0.40
                animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                
                self.aboutUsView?.layer.add(animation, forKey: kCATransition)
            }
            
        case 11:
            if(str == 27)
            {
                print("Contact Us ")
                
                let contactVC = ContactUs()
                self.navigationController?.pushViewController(contactVC, animated: true)
            }
            
        case 12:
            if str == 27
            {
                print("Logout ")
                DispatchQueue.main.async
                    {
                        let alertController = UIAlertController(title: "", message: NSLocalizedString("LOGOUT_ALERT", comment: ""), preferredStyle: .alert)
                        
                        let action1 = UIAlertAction(title: NSLocalizedString("YES_TEXT", comment: ""), style: .default) { (action:UIAlertAction) in
                            
                            self.deleteInformationData()
                        }
                        
                        let action2 = UIAlertAction(title: NSLocalizedString("NO_TEXT", comment: ""), style: .default) { (action:UIAlertAction) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        
                        
                        alertController.addAction(action2)
                        alertController.addAction(action1)
                        
                        self.present(alertController, animated: true, completion: nil)
                }
            }
            
        default:
            return
        }
    }
    
    func deleteInformationData()
    {
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        
        let name = dict.object(forKey: "name") as! String
        let email = dict.object(forKey: "email") as! String
        let fcm_user_id = dict.object(forKey: "fcm_user_id") as! String
        
        User.updateUserInformation(withName: name, withEmail: email, withId: fcm_user_id, device_token: "", completion: { (success) in
            
            if (success)
            {
                MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
                self.userLogoutApi()
            }
            else
            {
                self.getLogoutAlert()
            }
        })
    }
    
    func getLogoutAlert()
    {
        let alertController = UIAlertController(title: "", message:NSLocalizedString("ERROR_OTHER", comment: ""), preferredStyle: .alert)
        
        let action2 = UIAlertAction(title: NSLocalizedString("OK_TEXT", comment: ""), style: .default) { (action:UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(action2)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func changeNotificationValue()
    {
        if isOn
        {
            isOn = false
            self.notificationOnOffApi()
        }
        else
        {
            isOn = true
            self.notificationOnOffApi()
        }
    }
    
    
    
    func activityOnOffApi()
    {
        let service = CustomerService()
        
        
        let activityState:Bool
        
        
        
        let dic : NSMutableDictionary =
            [
                "is_available" : !(userDict.object(forKey: "is_available") as? Bool)!
        ]
        
        service.activityOnOffApiRequestWithParameters(dic, success: { (response, data) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    DispatchQueue.main.async
                        {
                            self.userProfile()
                    }
                }
                else
                {
                    AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                        
                    })
                }
            }
            else
            {
                let msg = data?.object(forKey: "msg") as! NSString
                
                AlertViewController.showAlertWith(title: "", message: msg as String, dismissBloack: {
                    
                })
            }
            
        }) { (response, error) in
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    // Notification ON OFF api
    
    func notificationOnOffApi()
    {
        let service = CustomerService()
        
        let dic : NSMutableDictionary =
            [
                "notification_setting" : isOn
        ]
        
        service.notificationOnOffApiRequestWithParameters(dic, success: { (response, data) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    DispatchQueue.main.async
                        {
                            self.userProfile()
                    }
                }
                else
                {
                    AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                        
                    })
                }
            }
            else
            {
                let msg = data?.object(forKey: "msg") as! NSString
                
                AlertViewController.showAlertWith(title: "", message: msg as String, dismissBloack: {
                    
                })
            }
            
        }) { (response, error) in
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
}

extension SettingVC : UIWebViewDelegate
{
    func webViewDidFinishLoad(_ webView: UIWebView) {
        //let scrollableSize = CGSize(width: aboutWebView.frame.size.width, height: webView.scrollView.contentSize.height)
        //self.aboutWebView?.scrollView.contentSize = scrollableSize
    }
}
