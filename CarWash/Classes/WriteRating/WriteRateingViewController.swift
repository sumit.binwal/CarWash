//
//  WriteRateingViewController.swift
//  CarWash
//
//  Created by Suman Payal on 28/12/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol writeRatingDelagate {
    func updateDict(dict : NSDictionary)
}

class WriteRateingViewController: UIViewController, FloatRatingViewDelegate, UITextViewDelegate {

    var delegate : writeRatingDelagate?
    
    @IBOutlet weak var priceRatingView: FloatRatingView!
    @IBOutlet weak var quantityRatingView: FloatRatingView!
    @IBOutlet weak var timeRatingView: FloatRatingView!
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblWriteYourReview: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblQuality: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblProvideRating: UILabel!
    var priceRating = 0.0
    var quantityRating = 0.0
    var timeRating = 0.0
    
    @IBOutlet weak var reviewTextField: UITextView!
    
    var serviceDict  : NSDictionary?
    let reviewPlaceText : String = NSLocalizedString("WRITE_YOUR_REVIEW", comment: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("service dict = \(String(describing: serviceDict))")
        
        // Do any additional setup after loading the view.
        
        // Reset float rating view's background color
        priceRatingView.backgroundColor = UIColor.clear
        
        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        priceRatingView.delegate = self
        priceRatingView.type = .halfRatings
        priceRatingView.rating = 0.0
        
        // Reset float rating view's background color
        quantityRatingView.backgroundColor = UIColor.clear
        
        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        quantityRatingView.delegate = self
        quantityRatingView.type = .halfRatings
        quantityRatingView.rating = 0.0
        
        // Reset float rating view's background color
        timeRatingView.backgroundColor = UIColor.clear
        
        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        timeRatingView.delegate = self
        timeRatingView.type = .halfRatings
        timeRatingView.rating = 0.0
        
        //reviewTextField.layer.borderColor = UIColor.lightGray.cgColor
        //reviewTextField.layer.borderWidth = 1.0
        //reviewTextField.layer.cornerRadius = 5.0
        //reviewTextField.layer.masksToBounds = true
        
        reviewTextField.text = reviewPlaceText
        
        lblProvideRating.text = NSLocalizedString("PLZ_PROVIDE_YOUR_RATING", comment: "")
        lblTime.text = NSLocalizedString("TIME_TEXT", comment: "")
        lblQuality.text = NSLocalizedString("QUALITY_TEXT", comment: "").capitalized
        lblPrice.text = NSLocalizedString("PRICE_TEXT", comment: "").capitalized
        btnSubmit.setTitle(NSLocalizedString("SUBMIT_TEXT", comment: ""), for: UIControlState.normal)
        lblWriteYourReview.text = "\(NSLocalizedString("WRITE_YOUR_REVIEW", comment: "")) :"
        
        if(LanguageManager.currentLanguageIndex() == 0)
        {
            reviewTextField.textAlignment = NSTextAlignment.left
        }
        else
        {
            reviewTextField.textAlignment = NSTextAlignment.right
        }
        
        if language == "en"
        {
            
        }
        else
        {
            priceRatingView.transform = priceRatingView.transform.rotated(by: CGFloat(Double.pi))
            quantityRatingView.transform = quantityRatingView.transform.rotated(by: CGFloat(Double.pi))
            timeRatingView.transform = timeRatingView.transform.rotated(by: CGFloat(Double.pi))
        }
        
        priceRatingView.fullImage = language == "en" ? UIImage(named:"full_star") : UIImage(named:"full_star_ar")
        
        priceRatingView.emptyImage = language == "en" ? UIImage(named:"empty_star") : UIImage(named:"empty_star_ar")
        
        quantityRatingView.fullImage = language == "en" ? UIImage(named:"full_star") : UIImage(named:"full_star_ar")
        
        quantityRatingView.emptyImage = language == "en" ? UIImage(named:"empty_star") : UIImage(named:"empty_star_ar")
        
        timeRatingView.fullImage = language == "en" ? UIImage(named:"full_star") : UIImage(named:"full_star_ar")
        
        timeRatingView.emptyImage = language == "en" ? UIImage(named:"empty_star") : UIImage(named:"empty_star_ar")
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        setUpNavBar()
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
    }
    
    func setUpNavBar()
    {
        self.title = NSLocalizedString("REVIEW&RATING", comment: "")
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: FloatRatingViewDelegate
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        //print("isUpdating rating = \(priceRatingView.rating)")
        if ratingView == priceRatingView {
            priceRating = priceRatingView.rating
        } else if ratingView == quantityRatingView {
            quantityRating = quantityRatingView.rating
        } else {
            timeRating = timeRatingView.rating
        }
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        //print("didUpdate rating = \(priceRatingView.rating)")
        if ratingView == priceRatingView {
            priceRating = priceRatingView.rating
        } else if ratingView == quantityRatingView {
            quantityRating = quantityRatingView.rating
        } else {
            timeRating = timeRatingView.rating
        }
    }
    
    // MARK: Textviewdelegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.text.count >= 240 {
            if text == "" {
                return true
            } else {
                return false
            }
        }
        
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == reviewPlaceText {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = reviewPlaceText
        }
    }
    
    func validateCondition() -> Bool
    {
        var isError : Bool = true
        
        if priceRating == 0.0
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("PLZ_PROVIDE_PRICE_RATING", comment: ""), dismissBloack:
                {
                    
            })
        }
        else if quantityRating == 0.0
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("PLZ_PROVIDE_QUANTITY_RATING", comment: ""), dismissBloack:
                {
                    
            })
        }
        else if timeRating == 0.0
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("PLZ_PROVIDE_TIME_RATING", comment: ""), dismissBloack:
                {
                    
            })
        }
        //else if reviewTextField.text == reviewPlaceText
        //{
        //    isError = false
        //    AlertViewController.showAlertWith(title: "", message: NSLocalizedString("PLZ_WRITE_YOUR_REVIEW", comment: ""), dismissBloack:
        //        {
        //
        //    })
        //}
        
        return isError
    }
    
    @IBAction func submitClicked(_ sender: Any) {
        if validateCondition() {
            writeReviewApiCall()
        }
    }
    
    func writeReviewApiCall()
    {
        let service = CustomerService()
        
        let dict1 = serviceDict?.object(forKey: "provider_id") as! NSDictionary
        let provider_id = dict1.object(forKey: "_id") as! String
        
        let service_id = serviceDict?.object(forKey: "service_id") as! String
        
        let main_id = serviceDict?.object(forKey: "_id") as! String
        
        var ratingText = ""
        if reviewTextField.text == reviewPlaceText {
            ratingText = ""
        } else {
            ratingText = reviewTextField.text
        }
        
        let dict : NSMutableDictionary = [
            "service_id" : service_id,
            "booking_id" : main_id,
            "provider_id" : provider_id,
            "review" : ratingText,
            "time_rating" : timeRating,
            "quality_rating" : quantityRating,
            "price_rating" : priceRating
        ]
        
        service.writeReviewServiceRequestWithParameters(dict, success: { (response, data) in
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    DispatchQueue.main.async {
                        
                        AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                           
                            self.delegate?.updateDict(dict: data?.object(forKey: "booking") as! NSDictionary)
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                } else {
                    
                }
            }
            else
            {
                AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack:
                    {
                        
                })
            }
            
        }) { (response, error) in
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
}
