//
//  SelectDocument.swift
//  CarWash
//
//  Created by iOS on 17/11/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import  MBProgressHUD
import AWSS3

class SelectDocument: UIViewController, UITextFieldDelegate
{
    @IBOutlet var txtName   : UITextField?
    @IBOutlet var imgDoc    : UIImageView?
    @IBOutlet weak var lblName: UILabel!
    
    let imagePicker         = UIImagePickerController()
    var isPic               : Bool?
    
    var document_url = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        isPic = false
        imagePicker.delegate = self
        
        setUpNavigationBar()
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 20))
        txtName?.leftView = paddingView
        txtName?.leftViewMode = .always
        lblName?.text = NSLocalizedString("NAME_TEXT", comment: "")
        txtName?.placeholder = NSLocalizedString("ENTER_DOCUMENTS_NAME", comment: "")
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
    
        if(LanguageManager.currentLanguageIndex() == 0)
        {
            txtName?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            txtName?.textAlignment = NSTextAlignment.left
        }
        else
        {
            txtName?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            txtName?.textAlignment = NSTextAlignment.right
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpNavigationBar()
    {
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.title = NSLocalizedString("SELECT_DOCUMENT", comment: "")
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("DONE", comment: ""), style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func doneButtonPressed()
    {
        if(CommonFunctions.isInternetAvailable())
        {
            if(validateUserInput())
            {
                MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
                uploadDocument()
            }
        }
    }
    
    //MARK: IBAction Methods
    
    @IBAction func showPic()
    {
        RESIGN_KEYBOARD
        
        let optionMenu = UIAlertController(title: nil, message: NSLocalizedString("CHOOSE_IMAGE", comment: ""), preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: NSLocalizedString("CAMERA_TEXT", comment: ""), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let galleryAction = UIAlertAction(title: NSLocalizedString("PHOTO_GALLARY", comment: ""), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
            {
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("CANCEL_TEXT", comment: ""), style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    // MARK: API Call
    
    func uploadDocument()
    {
        let service1     =  ServiceProvider()
        
        let dic : NSMutableDictionary =
        [
            "name"      : txtName?.text ?? "",
            "document"  : self.document_url
        ]
    
        service1.uploadDocsWithParameters(dic, nil, success:
            { (response, data) in
                
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                
                if(response?.statusCode == 200)
                {
                    let str : NSString = data?.object(forKey: "status") as! NSString
                    
                    if (str.isEqual(to: "success"))
                    {
                        AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack:
                            {
                                self.txtName?.text = ""
                                self.navigationController?.popViewController(animated: true)
                        })
                    }
                }
        })
        { (error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    //MARK: Validate Method
    
    func validateUserInput() -> Bool
    {
        var isError : Bool = true
        
        if(txtName?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_NAME", comment: ""), dismissBloack:
                {
                    self.txtName?.becomeFirstResponder()
            })
        }
        else if(!isPic!)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_DOC", comment: ""), dismissBloack:
                {
                    
            })
        }
        
        return isError
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let string1 = textField.text! as NSString
        if string1.length > 50 {
            if string == "" {
                return true
            } else {
                return  false
            }
        }
        
        return true
    }
}

extension SelectDocument : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    // UIImagePicker Delegates
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if (info[UIImagePickerControllerOriginalImage] as? UIImage) != nil
        {
            picker.dismiss(animated: false, completion: { () -> Void in
                
                _ = CommonFunctions.addFileToFolder("saved", fileName: "save.jpg", fileData: UIImageJPEGRepresentation((info[UIImagePickerControllerOriginalImage] as? UIImage)!, 0.6)!)
                
                if let fileURL = CommonFunctions.getFileURLFromFolder("saved", fileName: "save.jpg") {
                    self.uploadImage(usingImage: fileURL as NSURL)
                }
            })
        }
    }
    
    func uploadImage(usingImage : NSURL) {
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        let ext = "jpg"
        
        let keyName = "document" + CommonFunctions.getCurrentTimeStamp() + ".jpg"
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.body = usingImage as URL
        uploadRequest?.key = keyName
        uploadRequest?.bucket = AmazonS3BucketNameDocuments + CommonFunctions.getUserIdData()!
        uploadRequest?.contentType = "image/" + ext
        uploadRequest?.acl = .publicReadWrite
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith { (task) -> AnyObject! in
            
            if let error = task.error {
                print("Upload failed  (\(error))")
                
                DispatchQueue.main.async(execute: {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    self.isPic = false
                    self.imgDoc?.isHidden = true
                    
                    return
                })
            }
            
            if task.result != nil
            {
                print("Uploaded to:fksjflksdajf askldjf")
                
                DispatchQueue.main.async(execute: {
                    
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    self.isPic = true
                    self.imgDoc?.isHidden = false
                    
                    self.document_url = "https://s3-us-west-2.amazonaws.com/car-wash/documents/" + CommonFunctions.getUserIdData()! + "/" + keyName
                    print(self.document_url)
                    
                    self.imgDoc?.setIndicatorStyle(.gray)
                    self.imgDoc?.setShowActivityIndicator(true)
                    
                    self.imgDoc?.sd_setImage(with: URL.init(string: self.document_url), placeholderImage: nil)
                    
                    return
                })
            }
            else
            {
                print("Unexpected empty result.")
                DispatchQueue.main.async(execute: {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    self.isPic = false
                    self.imgDoc?.isHidden = true
                    
                    return
                })
            }
            
            return nil
        }
    }
}
