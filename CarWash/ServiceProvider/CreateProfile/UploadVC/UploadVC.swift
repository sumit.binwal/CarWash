//
//  UploadVC.swift
//  CarWash
//
//  Created by iOS on 05/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MobileCoreServices
import  MBProgressHUD


protocol Document
{
    func setCountOfDocument(valueSent: Bool)
}


class UploadVC: UIViewController, UIDocumentPickerDelegate, UIDocumentMenuDelegate
{
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
    }
    
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var termsWebView: UIWebView!
    
    @IBOutlet  var  profileView       : UIView!
    @IBOutlet  var  tblUploads        : UITableView!
    @IBOutlet  var  termsView         : UIView!
    
    @IBOutlet weak var aboutView: UIView!
    
    var isEditMode = false
    
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var lblProfileUpdate: UILabel!
    @IBOutlet weak var lblUpdateMsg: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    var docAry      : NSMutableArray = []
    var docPicker   : UIDocumentMenuViewController? = nil
    var isAgree     : Bool?
    
    var delegate    :  Document?
    
    var deletedDocumentName : String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setUpNavigationBar ()
        
        aboutView.layer.cornerRadius = 10.0
        aboutView.layer.borderColor = UIColor.clear.cgColor
        aboutView.layer.borderWidth = 1.0
        aboutView.layer.masksToBounds = true
        
        isAgree = false
        tblUploads?.register(UINib(nibName: "DocumentCell", bundle: nil), forCellReuseIdentifier: "DocumentCell")
        
        tblUploads?.register(UINib(nibName: "UploadCell", bundle : nil), forHeaderFooterViewReuseIdentifier: "UploadCell")
        
        termsView.viewWithTag(1)?.layer.cornerRadius = 4.0
        termsView.viewWithTag(1)?.layer.masksToBounds = true
        
        btnSave.setTitle(NSLocalizedString("SAVE_TEXT", comment: ""), for: UIControlState.normal)
        btnOk.setTitle(NSLocalizedString("OK_TEXT", comment: ""), for: UIControlState.normal)
        
        ///////////////////////////////
        
        setFooter ()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        getDocumentList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpNavigationBar()
    {
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.title = NSLocalizedString("UPLOAD_DOC", comment: "")
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func setFooter()
    {
        let headerView = UIView()
        
        let headerCell: UploadCell = tblUploads!.dequeueReusableHeaderFooterView(withIdentifier: "UploadCell") as! UploadCell
        headerCell.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_WIDTH * 0.58)
        headerCell.tag = 6
        headerView.addSubview(headerCell)
        tblUploads?.tableFooterView = headerView
        
        var attributedString = NSMutableAttributedString(string: NSLocalizedString("SUPPORT_DOCUMENT", comment: ""))
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 15)!,
                NSForegroundColorAttributeName: UIColor.uicolorFromRGB(97, 97, 97)
            ], range: NSRange(location: 1, length: attributedString.length-1)
        )
        
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 15)!,
                NSForegroundColorAttributeName: UIColor.uicolorFromRGB(2, 166, 242)
            ], range: NSRange(location: 0, length: 1)
        )
        
        headerCell.lblText1?.attributedText = attributedString
        
        attributedString = NSMutableAttributedString(string: NSLocalizedString("TYPE_OF_DOCUMENT", comment: ""))
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 15)!,
                NSForegroundColorAttributeName: UIColor.uicolorFromRGB(97, 97, 97)
            ], range: NSRange(location: 2, length: attributedString.length-2)
        )
        
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 15)!,
                NSForegroundColorAttributeName: UIColor.uicolorFromRGB(2, 166, 242)
            ], range: NSRange(location: 0, length: 2)
        )
        
        headerCell.lblText2?.attributedText = attributedString
        
        let str1 = NSLocalizedString("I_AGREE_ALL", comment: "") as NSString
        let str2 = NSLocalizedString("TERMS_&_CONDITIONS", comment: "") as NSString
        
        let str3 = String(format: "\(str1) \(str2)") as NSString
        
        attributedString = NSMutableAttributedString(string: str3 as String)
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 15)!,
                NSForegroundColorAttributeName: UIColor.uicolorFromRGB(97, 97, 97)
            ], range: NSRange(location: 0 , length: str1.length)
        )
        
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_REGULAR, size: 15)!,
                NSForegroundColorAttributeName: UIColor.uicolorFromRGB(2, 166, 242)
            ], range: NSRange(location: str3.length - str2.length, length: str2.length)
        )
        
        headerCell.btnTerms?.setAttributedTitle(attributedString, for: .normal)
        
        
        if (LanguageManager.currentLanguageIndex() == 0) {
            headerCell.btnTerms?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        } else {
            headerCell.btnTerms?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
        }
        
        headerCell.btnAgree?.addTarget(self, action: #selector(agree), for: .touchUpInside)
        headerCell.btnTerms?.addTarget(self, action: #selector(termsCond), for: .touchUpInside)
        tblUploads?.tableFooterView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_WIDTH * 0.58)
        
        if isEditMode {
            isAgree = true
            headerCell.btnAgree?.setImage(UIImage(named:"agree"), for: .normal)
        }
    }
    
    // MARK: - IBAction Methods
    
    @IBAction func save()
    {
        if docAry.count == 0 {
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("PLZ_UPLOAD_ANY_ONE_DOCUMENT", comment: ""), dismissBloack: {
                
            })
        } else {
            if isAgree == false {
                AlertViewController.showAlertWith(title: "", message: NSLocalizedString("PLZ_AGREE_TERMS_CONDITIONS", comment: ""), dismissBloack: {
                    
                })
            } else {
                delegate?.setCountOfDocument(valueSent: docAry.count > 0 ? true  : false)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func oK()
    {
        profileView.removeFromSuperview()
    }
    
    @IBAction func closePopUp()
    {
        profileView.removeFromSuperview()
    }
    
    func backButtonPressed()
    {
        delegate?.setCountOfDocument(valueSent: docAry.count > 0 ? true  : false)
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: Other Methods
    
    func addNewDoc()
    {
        let docObj = SelectDocument()
        self.navigationController?.pushViewController(docObj, animated: true)
    }
    
    func agree()
    {
        let headerView : UIView = tblUploads.tableFooterView!
        let cell : UploadCell = headerView.viewWithTag(6) as! UploadCell
        
        if(isAgree)!
        {
            isAgree = false
            cell.btnAgree?.setImage(UIImage(named:"uncheck"), for: .normal)
        }
        else
        {
            isAgree = true
            cell.btnAgree?.setImage(UIImage(named:"agree"), for: .normal)
        }
    }
    
    func termsCond()
    {
        RESIGN_KEYBOARD
        
        termsView.removeFromSuperview()
        APPDELEGATE.window?.addSubview(termsView)
        termsView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        getAboutUsApi()
        
        let animation = CATransition()
        animation.delegate = self as? CAAnimationDelegate
        animation.type = kCATransitionPush
        animation.subtype = kCATransitionFromTop
        animation.duration = 0.40
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        termsView.layer.add(animation, forKey: kCATransition)
    }
    
    func getAboutUsApi()
    {
        let service = CustomerService()
        
        let stringName = "terms-and-conditions"
        
        service.getAboutUsDataRequestWithParameters(nil, stringName as NSString, success: { (response, data) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            print("about us data = ", data ?? "")
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    let sectionNames : NSDictionary = data?.object(forKey: "data") as! NSDictionary
                    
                    var stringDescription : NSString? = nil
                    var titleNameStr : String? = nil
                    
                    if language == "en"
                    {
                        stringDescription = sectionNames.object(forKey: "description") as? NSString
                        titleNameStr = sectionNames.object(forKey: "title") as? String
                    }
                    else
                    {
                        stringDescription = sectionNames.object(forKey: "description_ar") as? NSString
                        titleNameStr = sectionNames.object(forKey: "title_ar") as? String
                    }
                    
                    DispatchQueue.main.async
                        {
                            self.termsWebView.scrollView.isScrollEnabled = true
                            self.termsWebView.isUserInteractionEnabled = true
                            self.termsWebView.scrollView.showsVerticalScrollIndicator = false;
                            self.termsWebView.scrollView.showsHorizontalScrollIndicator = false
                            
                            self.termsLabel.text = titleNameStr
                            self.termsWebView.loadHTMLString(stringDescription! as String, baseURL: nil)
                    }
                }
            }
            else
            {
                AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                    
                })
            }
            
        }) { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    // MARK: IBAction Methods
    
    @IBAction func closeTermsView()
    {
        let animation = CATransition()
        animation.delegate = self as? CAAnimationDelegate
        animation.type = kCATransitionPush
        animation.subtype = kCATransitionFromBottom
        animation.duration = 0.40
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        termsView.layer.add(animation, forKey: kCATransition)
        termsView.frame = CGRect(x: 0, y: SCREEN_HEIGTH, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
    }
    
    // MARK: API Call
    
    func getDocumentList()
    {
        let service = CustomerService()
        
        service.profileRequestWithParameters(nil, success:
            { (response, data) in
                
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                
                if(response?.statusCode == 200)
                {
                    let str : NSString = data?.object(forKey: "status") as! NSString
                    
                    if (str.isEqual(to: "success"))
                    {
                        let dict : NSDictionary = (data?.object(forKey: "data") as? NSDictionary)!
                        let ary : NSArray = dict.object(forKey: "uploaded_docs") as! NSArray
                        self.docAry =  ary.mutableCopy() as! NSMutableArray
                        self.tblUploads.reloadData()
                    }
                }
        })
        { (response, error) in
            
        }
    }
}

extension UploadVC : UITableViewDelegate, UITableViewDataSource
{
    // MARK: - tableview delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return docAry.count+1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return  indexPath.row == docAry.count+2 ? SCREEN_WIDTH * 0.58 : indexPath.row == docAry.count+1 ? SCREEN_WIDTH * 0.16 : SCREEN_WIDTH * 0.208
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == docAry.count)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DocumentCell") as! DocumentCell?
            
            cell?.lblAddDocument.text = NSLocalizedString("ADD_DOCUMENT", comment: "")
            cell?.addView?.isHidden = false
            cell?.btnAdd?.addTarget(self, action: #selector(addNewDoc), for: UIControlEvents.touchUpInside)
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            cell?.backgroundColor = UIColor.white
            
            return cell!
        }
        
        // else
        let cell = tableView.dequeueReusableCell(withIdentifier: "DocumentCell") as! DocumentCell?
        
        if(indexPath.row < docAry.count)
        {
            let _dict = docAry[indexPath.row] as! NSDictionary
            cell?.lblDoc?.text = _dict.object(forKey: "name") as? String
            
            let str = _dict.object(forKey: "document") as? String
            let url : URL =   URL(string: str!)!
            
            cell?.icon?.setIndicatorStyle(.gray)
            cell?.icon?.setShowActivityIndicator(true)
            
            cell?.icon?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "group29"))
        }
        
        cell?.deleteBtn.tag = indexPath.row
        cell?.deleteBtn?.addTarget(self, action: #selector(deleteUploadedDocument), for: UIControlEvents.touchUpInside)
        
        cell?.addView?.isHidden = true
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.backgroundColor = UIColor.white
        
        return cell!
    }
    
    func deleteUploadedDocument(sender : UIButton)
    {
        let tag = sender.tag
        let _dict = docAry[tag] as! NSDictionary
        let stringName = (_dict.object(forKey: "document") as? String)!
        let name1 = stringName.components(separatedBy: "/") as NSArray
        deletedDocumentName = name1.lastObject as! String
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        deleteDocumentApi()
    }
    
    // delete document api
    
    func deleteDocumentApi()
    {
        let service = CustomerService()
        
        let dic : NSMutableDictionary =
            [
                "document" : deletedDocumentName
        ]
        
        service.deleteUploadedDocumentRequestWithParameters(dic, success: { (response, data) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    DispatchQueue.main.async
                        {
                            AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack: {
                                
                                MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
                                self.getDocumentList()
                            })
                    }
                }
            }
            else
            {
                let data1 = data?.object(forKey: "document") as! NSArray
                let msg = data1.object(at: 0) as! String
                
                AlertViewController.showAlertWith(title: "", message: msg, dismissBloack: {
                    
                })
            }
            
        }) { (response, error) in
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
}
