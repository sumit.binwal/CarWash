//
//  OperatingHoursVC.swift
//  CarWash
//
//  Created by iOS on 06/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import IQDropDownTextField
import IQKeyboardManagerSwift
import MBProgressHUD

protocol MyProtocol
{
    func setResultOfBusinessLogic(valueSent: NSMutableArray)
}

class OperatingHoursVC: UIViewController, IQDropDownTextFieldDelegate
{
    @IBOutlet var tblHours    : UITableView?
    @IBOutlet var datePicker  : UIDatePicker?
    var txtCurrent  : UITextField?
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var daysAry : NSMutableArray = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    
    var delegate : MyProtocol?
    
    var hoursDict : NSMutableArray?
    var hoursUpdateDict : NSMutableArray = []
    
    let formatter = DateFormatter()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setUpNavigationBar ()
        
        print("hoursDict didload = \(String(describing: hoursDict))")
        
        if(hoursDict?.count == 0)
        {
            setUpData()
        }
        
        tblHours?.register(UINib(nibName: "OperatingHourCell", bundle: nil), forCellReuseIdentifier: "OperatingHourCell")
        tblHours?.register(UINib(nibName: "Header", bundle: nil), forCellReuseIdentifier: "Header")
        tblHours?.tableHeaderView?.backgroundColor = UIColor.clear
        
        datePicker?.datePickerMode = UIDatePickerMode.time
        
        formatter.dateFormat = "hh:mm a"
        
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector (doneAction), name: NSNotification.Name(rawValue: "doneAction"), object: nil)
        
        lblTitle.text = NSLocalizedString("OPERATING_HOUR_MSG", comment: "")
        btnSubmit.setTitle(NSLocalizedString("SUBMIT_TEXT", comment: ""), for: UIControlState.normal)
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
        
        IQKeyboardManager.sharedManager().previousNextDisplayMode = IQPreviousNextDisplayMode.alwaysHide
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        IQKeyboardManager.sharedManager().previousNextDisplayMode = IQPreviousNextDisplayMode.alwaysShow
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpNavigationBar()
    {
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.title = NSLocalizedString("OPERATING_HOURS", comment: "")
        
        navigationController?.navigationBar.titleTextAttributes =
        [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        if(LanguageManager.currentLanguageIndex()==0){
            //cell?.imgView?.image = UIImage(named:"tableArrow")
        }else{
            //cell?.imgView?.image = UIImage(named:"tableArrowRight")
        }
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func setUpData()
    {
        for(i, str) in daysAry.enumerated()
        {
            print(i)
            
            let shift_dict1 : NSMutableDictionary = ["open" : "",
                                                    "close" : ""]
            
            let shift_dict2 : NSMutableDictionary = ["open" : "",
                                                    "close" : ""]
            
            let dict : NSMutableDictionary = [
                "day"       : str,
                "is_close"  : 0,
                "shift_1"   : shift_dict1,
                "shift_2"   : shift_dict2
            ]
            
            hoursDict?.add(dict)
        }
        
        print("hoursDict = ", hoursDict ?? "")
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func doneAction()
    {
        let strTime : NSString?
        let tag : Int = (txtCurrent?.tag)!
        
        let origin: CGPoint = txtCurrent!.frame.origin
        let point: CGPoint = txtCurrent!.superview!.convert(origin, to: tblHours)

        var indexPath: IndexPath? = tblHours?.indexPathForRow(at: point)

        formatter.dateFormat = "HH:mm"
        strTime = formatter.string(from: (datePicker?.date)!) as NSString
        
        var mainDict = NSMutableDictionary()
        if let dict1: NSDictionary = hoursDict!.object(at: (indexPath?.row)!) as? NSDictionary {
            mainDict = dict1.mutableCopy() as! NSMutableDictionary
        }else{
            mainDict = hoursDict!.object(at: (indexPath?.row)!) as! NSMutableDictionary
        }
        //let mainDict : NSMutableDictionary = hoursDict!.object(at: (indexPath?.row)!) as! NSMutableDictionary
        
        if(tag <= 2)
        {
            //let dict: NSMutableDictionary = mainDict.object(forKey: "shift_1") as! NSMutableDictionary
            var dict = NSMutableDictionary()
            if let dict1: NSDictionary = mainDict.object(forKey: "shift_1") as? NSDictionary {
                dict = dict1.mutableCopy() as! NSMutableDictionary
            }else{
                dict  = mainDict.object(forKey: "shift_2") as! NSMutableDictionary
            }
            
            tag  == 1 ?  dict.setObject(strTime ?? "", forKey: "open" as NSCopying) : dict.setObject(strTime ?? "", forKey: "close" as NSCopying)

            mainDict.setObject(dict, forKey: "shift_1" as NSCopying)
            print("mainDict= ", mainDict)
        }
        else
        {
            var dict = NSMutableDictionary()
            if let dict1: NSDictionary = mainDict.object(forKey: "shift_2") as? NSDictionary {
                 dict = dict1.mutableCopy() as! NSMutableDictionary
            }else{
                dict  = mainDict.object(forKey: "shift_2") as! NSMutableDictionary
            }
            //let dict: NSMutableDictionary = mainDict.object(forKey: "shift_2") as! NSMutableDictionary

            tag  == 3 ?  dict.setObject(strTime ?? "", forKey: "open" as NSCopying) : dict.setObject(strTime ?? "", forKey: "close" as NSCopying)

            mainDict.setObject(dict, forKey: "shift_2" as NSCopying)
        }

        hoursDict?.replaceObject(at: (indexPath?.row)!, with: mainDict)
        
        tblHours?.beginUpdates()
        tblHours?.reloadRows(at: [indexPath!], with: UITableViewRowAnimation.none)
        tblHours?.endUpdates()
    }
    
    @IBAction func submit()
    {
        print("submit = ", hoursDict ?? "")
        
        hoursUpdateDict = hoursDict!
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        let userRoleNo: NSNumber = dict.object(forKey: "user_role") as! NSNumber
        
        if(validate())
        {
            if (userRoleNo == 27)
            {
                updateOperatingHoursApi()
            }
            else
            {
                delegate?.setResultOfBusinessLogic(valueSent: hoursDict!)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    //MARK: Validation
    
    func validate() -> Bool
    {
        var isError : Bool = true
        
        for(i, dict) in (hoursDict?.enumerated())!
        {
            print(i)
            
            let _dict : NSDictionary = dict as! NSDictionary
            let str   : NSString     = _dict.object(forKey: "day") as! NSString
            let isclose = (dict as AnyObject).object(forKey: "is_close") as! NSNumber
            
            if(isclose == 0)
            {
                let _shiftDict : NSDictionary = _dict.object(forKey: "shift_1") as! NSDictionary
                
                if((_shiftDict.object(forKey: "open")as! String).count == 0 || (_shiftDict.object(forKey: "close")as! String).count == 0)
                {
                    let message : NSString   = String(format:"\(NSLocalizedString("PLEASE_SELECT", comment: "")) %@ \(NSLocalizedString("SHIFT_TIMING", comment: ""))", str) as NSString
                    
                     isError = false
                    AlertViewController.showAlertWith(title: "", message: message as String, dismissBloack:
                    {
                    })
                    
                    break
                }
                else if ((_shiftDict.object(forKey: "open")as! String).count >  0 || (_shiftDict.object(forKey: "close")as! String).count > 0)
                {
                    formatter.dateFormat = "HH:mm"
                    
                    let open = _shiftDict.object(forKey: "open") as? String
                    let close = _shiftDict.object(forKey: "close") as? String
                    
                    let openDate : Date = formatter.date(from: open!)! as Date
                    let closeDate : Date = formatter.date(from: close!)! as Date
                    
                    if (closeDate < openDate)
                    {
                        isError = false
                        AlertViewController.showAlertWith(title: "", message: NSLocalizedString("CLOSING_TIME_ALERT", comment: "") as String, dismissBloack:
                            {
                        })
                        
                        break
                    }
                }
                
                
                if ( isError)//
                {
                    if _dict.object(forKey: "shift_2") != nil
                    {
                        let _shiftDict2 : NSDictionary = _dict.object(forKey: "shift_2") as! NSDictionary
                        
                        let message : NSString   = String(format:"\(NSLocalizedString("PLEASE_SELECT", comment: "")) %@ \(NSLocalizedString("2ND_SHIFT_TIMING", comment: ""))", str) as NSString
                        
                        if((_shiftDict2.object(forKey: "open")as! String).count == 0 || (_shiftDict2.object(forKey: "close")as! String).count == 0)
                        {
                            isError = false
                            AlertViewController.showAlertWith(title: "", message: message as String, dismissBloack:
                                {
                            })
                            
                            break
                        }
                        else if ((_shiftDict.object(forKey: "open")as! String).count >  0 || (_shiftDict.object(forKey: "close")as! String).count > 0)
                        {
                            formatter.dateFormat = "HH:mm"
                            
                            let open = _shiftDict2.object(forKey: "open") as? String
                            let close = _shiftDict2.object(forKey: "close") as? String
                            
                            let openDate : Date = formatter.date(from: open!)! as Date
                            let closeDate : Date = formatter.date(from: close!)! as Date
                            
                            if (closeDate < openDate)
                            {
                                isError = false
                                AlertViewController.showAlertWith(title: "", message: NSLocalizedString("CLOSING_TIME_ALERT", comment: "") as String, dismissBloack:
                                    {
                                })
                                
                                break
                            }
                        }
                    }
                }
            }
        }
        return isError
    }

//MARK: API Call

func updateOperatingHoursApi()
{
    let service = CustomerService()
    var JSONString : NSString?
   
    do
    {
        let jsonData = try JSONSerialization.data(withJSONObject: hoursDict! , options: JSONSerialization.WritingOptions.prettyPrinted)
        
        JSONString = String(data: jsonData, encoding: String.Encoding.utf8)! as NSString
        
        print(JSONString ?? "")
    }
    catch
    {
        print(error)
    }
    
    let dic : NSMutableDictionary =
        [
            "operating_hours"       : JSONString ?? "" ,
        ]
    
   //let arr: NSMutableArray = []
    
    service.updateOperatingHoursRequestWithParameters(dic , success:
        { (response, data) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            //print("response = ", response)
            
            if(response?.statusCode == 200)
            {
                let str : NSString = data?.object(forKey: "status") as! NSString
                
                if (str.isEqual(to: "success"))
                {
                    USERDEFAULT.set(NSKeyedArchiver.archivedData(withRootObject: data?.object(forKey: "data") as! NSDictionary), forKey: "USER_DATA")
                    USERDEFAULT.synchronize()
                    
                   self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack:
                        {
                            
                    })
                }
            }
            else
            {
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            }
    })
    { (response, error) in
        
        MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
    }
  }
    
}

extension OperatingHoursVC : UITableViewDelegate, UITableViewDataSource
{
    // MARK: - tableview delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return daysAry.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let dict = (hoursDict?.object(at: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
        
        if(dict.object(forKey: "shift_2") == nil)
        {
            return 110 * scaleFactorX
        }
        else
        {
            return 170 * scaleFactorX
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OperatingHourCell") as! OperatingHourCell?
        
        cell?.lblOpeningTime1.text = NSLocalizedString("OPENING_TIME", comment: "")
        cell?.lblClosingTime1.text = NSLocalizedString("CLOSING_TIME", comment: "")
        cell?.lblOpeningTime2.text = NSLocalizedString("OPENING_TIME", comment: "")
        cell?.lblClosingTime2.text = NSLocalizedString("CLOSING_TIME", comment: "")
        
        if(LanguageManager.currentLanguageIndex() == 0)
        {
            cell?.txtOpening1?.contentHorizontalAlignment  = UIControlContentHorizontalAlignment.right
            cell?.txtOpening2?.contentHorizontalAlignment  = UIControlContentHorizontalAlignment.right
            cell?.txtClosing1?.contentHorizontalAlignment  = UIControlContentHorizontalAlignment.right
            cell?.txtClosing2?.contentHorizontalAlignment  = UIControlContentHorizontalAlignment.right
         
            cell?.txtOpening1?.textAlignment = NSTextAlignment.left
            cell?.txtOpening2?.textAlignment = NSTextAlignment.left
            cell?.txtClosing1?.textAlignment = NSTextAlignment.left
            cell?.txtClosing2?.textAlignment = NSTextAlignment.left
        }
        else
        {
            cell?.txtOpening1?.contentHorizontalAlignment  = UIControlContentHorizontalAlignment.left
            cell?.txtOpening2?.contentHorizontalAlignment  = UIControlContentHorizontalAlignment.left
            cell?.txtClosing1?.contentHorizontalAlignment  = UIControlContentHorizontalAlignment.left
            cell?.txtClosing2?.contentHorizontalAlignment  = UIControlContentHorizontalAlignment.left
        
            cell?.txtOpening1?.textAlignment = NSTextAlignment.right
            cell?.txtOpening2?.textAlignment = NSTextAlignment.right
            cell?.txtClosing1?.textAlignment = NSTextAlignment.right
            cell?.txtClosing2?.textAlignment = NSTextAlignment.right
        }
        
        cell?.btnAdd?.setTitle(NSLocalizedString("ADD_SHIFT", comment: ""), for: UIControlState.normal)
        
        cell?.btnAdd?.addTarget(self, action: #selector(addShift), for: .touchUpInside)
        cell?.delSwitch?.addTarget(self, action: #selector(deleteDay), for: .valueChanged)
        cell?.btnCross?.addTarget(self, action: #selector(deleteShift), for: .touchUpInside)
        
        cell?.txtOpening1?.delegate  = self
        cell?.txtOpening2?.delegate  = self
        cell?.txtClosing1?.delegate  = self
        cell?.txtClosing2?.delegate  = self
        
        cell?.txtOpening1?.inputView = datePicker
        cell?.txtOpening2?.inputView = datePicker
        cell?.txtClosing1?.inputView = datePicker
        cell?.txtClosing2?.inputView = datePicker
        
        cell?.btnAdd?.isHidden = false
        cell?.delSwitch?.setOn(true, animated: false)
        
        cell?.txtOpening1?.text = ""
        cell?.txtOpening2?.text = ""
        cell?.txtClosing1?.text = ""
        cell?.txtClosing2?.text = ""
        
        //let dict : NSMutableDictionary = hoursDict!.object(at: indexPath.row) as! NSMutableDictionary
        
        let dict = (hoursDict?.object(at: indexPath.row) as! NSDictionary).mutableCopy() as! NSMutableDictionary
        
        if(((dict.object(forKey: "shift_1")as! NSDictionary).object(forKey: "open") as! NSString).length > 1)
        {
            formatter.dateFormat = "HH:mm"
            
            let str = ((dict.object(forKey: "shift_1") as! NSDictionary).mutableCopy() as! NSMutableDictionary).object(forKey: "open") as? String
            
            let date : NSDate = formatter.date(from: str!)! as NSDate
            formatter.dateFormat = "hh:mm a"
            
            cell?.txtOpening1?.text = formatter.string(from: date as Date)
        }
        
        if(((dict.object(forKey: "shift_1")as! NSDictionary).object(forKey: "close") as! NSString).length > 1)
        {
            formatter.dateFormat = "HH:mm"
            
            let str = ((dict.object(forKey: "shift_1") as! NSDictionary).mutableCopy() as! NSMutableDictionary).object(forKey: "close") as? String
            
            let date : NSDate = formatter.date(from: str!)! as NSDate
            
            formatter.dateFormat = "hh:mm a"
            cell?.txtClosing1?.text = formatter.string(from: date as Date)
        }
        
        if(dict.object(forKey: "shift_2") != nil)
        {
            cell?.btnAdd?.isHidden = true
            
            if(((dict.object(forKey: "shift_2")as! NSDictionary).object(forKey: "open")as! NSString).length > 1)
            {
                formatter.dateFormat = "HH:mm"
                
                var str = ((dict.object(forKey: "shift_2") as! NSDictionary).mutableCopy() as! NSMutableDictionary).object(forKey: "open") as? String
                
                let date : NSDate = formatter.date(from: str!)! as NSDate
                formatter.dateFormat = "hh:mm a"
                
                str = formatter.string(from: date as Date)
                cell?.txtOpening2?.text = str
            }
            
            if(((dict.object(forKey: "shift_2")as! NSDictionary).object(forKey: "close")as! NSString).length > 1)
            {
                formatter.dateFormat = "HH:mm"
                
                let str = ((dict.object(forKey: "shift_2") as! NSDictionary).mutableCopy() as! NSMutableDictionary).object(forKey: "close") as? String
                
                let date : NSDate = formatter.date(from: str!)! as NSDate
                formatter.dateFormat = "hh:mm a"
                cell?.txtClosing2?.text = formatter.string(from: date as Date)
            }
        }
        
        var str = 0
        
        if let quantity = dict.object(forKey: "is_close") as? NSNumber {
            str = Int(quantity)
        }
        else if let quantity = dict.object(forKey: "is_close") as? NSNumber {
            str = Int(quantity)
        }
        
        if(str == 0)
        {
            cell?.lblDay?.textColor = UIColor.uicolorFromRGB(2, 166, 242)
            cell?.txtOpening1?.isEnabled = true
            cell?.txtOpening2?.isEnabled = true
            cell?.txtClosing1?.isEnabled = true
            cell?.txtClosing2?.isEnabled = true
            cell?.delSwitch?.isOn = true
            cell?.btnCross?.isHidden = false
        }
        else
        {
            cell?.btnCross?.isHidden = true
            cell?.lblDay?.textColor = UIColor.uicolorFromRGB(110, 110, 110)
            cell?.txtOpening1?.isEnabled = false
            cell?.txtOpening2?.isEnabled = false
            cell?.txtClosing1?.isEnabled = false
            cell?.txtClosing2?.isEnabled = false
            cell?.delSwitch?.isOn = false
            cell?.delSwitch?.tintColor = UIColor.clear
            cell?.delSwitch?.backgroundColor = UIColor.uicolorFromRGB(168, 168, 168)
            cell?.delSwitch?.layer.cornerRadius = 16
        }
        
        cell?.lblDay?.text = dict.object(forKey: "day") as? String
        cell?.shift2?.isHidden = true
        
        if(dict.object(forKey: "shift_2") != nil)
        {
            cell?.shiftHeight.constant = 60 * scaleFactorX
            cell?.heightConstraint.constant = 165 * scaleFactorX
            cell?.shift2?.isHidden = false
        }
        else
        {
            cell?.shiftHeight.constant = 0
            cell?.heightConstraint.constant = 104 * scaleFactorX
        }
        
        cell?.btnCross?.tag = indexPath.row
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.view.endEditing(true)
    }
    
    // MARK: Other Methods
    
    func addShift (_ sender:AnyObject)
    {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: tblHours)
        let indexPath = tblHours?.indexPathForRow(at: buttonPosition)
        
        //let dict : NSMutableDictionary = hoursDict!.object(at: (indexPath?.row)!) as! NSMutableDictionary
        var dict = NSMutableDictionary()
        if let dict1: NSDictionary = hoursDict!.object(at: (indexPath?.row)!) as? NSDictionary {
            dict = dict1.mutableCopy() as! NSMutableDictionary
        }else{
            dict = hoursDict!.object(at: (indexPath?.row)!) as! NSMutableDictionary
        }
        
        let str = dict.object(forKey: "is_close") as! NSNumber
        if(str == 0)
        {
            let shift_dict : NSMutableDictionary = ["open" : "",
                                                    "close" : ""]
            dict.setObject(shift_dict, forKey: "shift_2" as NSCopying)
            
            hoursDict?.replaceObject(at: (indexPath?.row)!, with: dict)
            
            if(dict.object(forKey: "shift_2") != nil)
            {
                let cell : OperatingHourCell = tblHours?.cellForRow(at: indexPath!) as! OperatingHourCell
                cell.btnAdd?.isHidden = true
            }
            
            tblHours?.beginUpdates()
            tblHours?.reloadRows(at: [indexPath!], with: UITableViewRowAnimation.none)
            tblHours?.endUpdates()
        }
    }
    
    func deleteDay (_ sender:AnyObject)
    {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: tblHours)
        let indexPath = tblHours?.indexPathForRow(at: buttonPosition)
        
        //let dict : NSMutableDictionary = hoursDict!.object(at: (indexPath?.row)!) as! NSMutableDictionary
        var dict = NSMutableDictionary()
        if let dict1: NSDictionary = hoursDict!.object(at: (indexPath?.row)!) as? NSDictionary {
            dict = dict1.mutableCopy() as! NSMutableDictionary
        }else{
            dict = hoursDict!.object(at: (indexPath?.row)!) as! NSMutableDictionary
        }
        
        //let str = dict.object(forKey: "is_close") as! String
        
        var str = 0
        
        if let quantity = dict.object(forKey: "is_close") as? NSNumber {
            str = Int(quantity)
        }
        else if let quantity = dict.object(forKey: "is_close") as? NSNumber {
            str = Int(quantity)
        }
        
        if(str == 0)
        {
            dict.setObject(1, forKey: "is_close" as NSCopying)
        }
        else
        {
            dict.setObject(0, forKey: "is_close" as NSCopying)
        }
        
        hoursDict?.replaceObject(at: (indexPath?.row)!, with: dict)
        
        let deadlineTime = DispatchTime.now() + .seconds(0)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime)
        {
            self.tblHours?.beginUpdates()
            self.tblHours?.reloadRows(at: [indexPath!], with: UITableViewRowAnimation.none)
            self.tblHours?.endUpdates()
        }
    }
    
    func deleteShift(_ sender:AnyObject)
    {
        let tag = sender.tag
        print("tag = \(String(describing: tag))")
        
        //let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: tblHours)
        //let indexPath = tblHours?.indexPathForRow(at: buttonPosition)
        
        let indexPath = IndexPath(row: tag!, section: 0)
        
        print("index = \(String(describing: indexPath.row))")
        
        //let dict : NSMutableDictionary = hoursDict!.object(at: (indexPath?.row)!) as! NSMutableDictionary
        var dict = NSMutableDictionary()
        if let dict1: NSDictionary = hoursDict!.object(at: (indexPath.row)) as? NSDictionary {
            dict = dict1.mutableCopy() as! NSMutableDictionary
        }else{
            dict = hoursDict!.object(at: (indexPath.row)) as! NSMutableDictionary
        }
        
        let str = dict.object(forKey: "is_close") as! NSNumber
        if(str == 0)
        {
            dict.removeObject(forKey: "shift_2")
            hoursDict?.replaceObject(at: (indexPath.row), with: dict)
            
            if(dict.object(forKey: "shift_2") == nil)
            {
                let cell : OperatingHourCell = tblHours?.cellForRow(at: indexPath) as! OperatingHourCell
                cell.btnAdd?.isHidden = false
            }
            
            tblHours?.beginUpdates()
            tblHours?.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
            tblHours?.endUpdates()
        }
    }
}

extension OperatingHoursVC : UITextFieldDelegate
{
    // MARK: UITextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        txtCurrent = nil
        
        formatter.dateFormat = "hh:mm a"
        txtCurrent = textField
        
        let origin: CGPoint = textField.frame.origin
        let point: CGPoint = textField.superview!.convert(origin, to: tblHours)
        var indexPath: IndexPath? = tblHours?.indexPathForRow(at: point)
        
        // let mainDict : NSMutableDictionary = hoursDict!.object(at: (indexPath?.row)!) as! NSMutableDictionary
        var mainDict = NSMutableDictionary()
        if let dict1: NSDictionary = hoursDict!.object(at: (indexPath?.row)!) as? NSDictionary
        {
            mainDict = dict1.mutableCopy() as! NSMutableDictionary
        }
        else
        {
            mainDict = hoursDict!.object(at: (indexPath?.row)!) as! NSMutableDictionary
        }
        
        var dict: NSMutableDictionary?
        
        formatter.dateFormat = "HH:mm"
        if(textField.tag <= 2)
        {
            dict = (mainDict.object(forKey: "shift_1") as! NSDictionary).mutableCopy() as? NSMutableDictionary
            
            if(textField.tag == 1)
            {
                let str = dict?.object(forKey: "open") as! NSString
                
                if(str.length > 1)
                {
                    let date : NSDate = formatter.date(from: str as String)! as NSDate
                    datePicker?.setDate(date as Date, animated: true)
                }
            }
            else
            {
                let str = dict?.object(forKey: "close") as! NSString
                if(str.length > 1)
                {
                    let date : NSDate = formatter.date(from: str as String)! as NSDate
                    datePicker?.setDate(date as Date, animated: true)
                }
            }
        }
        else
        {
            dict = (mainDict.object(forKey: "shift_2") as! NSDictionary).mutableCopy() as? NSMutableDictionary
            
            if(textField.tag == 3)
            {
                let str = dict?.object(forKey: "open") as! NSString
                if(str.length > 1)
                {
                    let date : NSDate = formatter.date(from: str as String)! as NSDate
                    datePicker?.setDate(date as Date, animated: true)
                }
            }
            else
            {
                let str = dict?.object(forKey: "close") as! NSString
                guard str != nil else
                {
                    return
                }
                
                if(str.length > 1)
                {
                    let date : NSDate = formatter.date(from: dict?.object(forKey: "close") as! String)! as NSDate
                    datePicker?.setDate(date as Date, animated: true)
                }
            }
        }
    }
}
