//
//  SelectLocationViewController.swift
//  CarWash
//
//  Created by iOS on 30/01/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit
import MapKit
import GooglePlaces
import GooglePlacePicker

class SelectLocationViewController: UIViewController, UISearchBarDelegate, GMSAutocompleteViewControllerDelegate, MKMapViewDelegate, UIGestureRecognizerDelegate
{
    @IBOutlet var mapView : MKMapView?
    @IBOutlet var searchBar : UISearchBar?
    
    var address : NSString = ""
    
    var selectedLat : CLLocationDegrees?
    var selectedLon : CLLocationDegrees?
    
    var annotation = PinAnnotation()
    
    var isAddressChnage: Bool = true
    
    var selectedNewLat = lattitude
    var selectedNewLang = longtitude
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        mapView?.delegate = self
        mapView?.showsUserLocation = true
        
        configureSearchController()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
        mapView?.addGestureRecognizer(tapGesture)
        
        searchBar?.setValue(NSLocalizedString("CANCEL_TEXT", comment: ""), forKey:"_cancelButtonText")
        searchBar?.placeholder = NSLocalizedString("SEARCH_TEXT", comment: "")
        let textFieldInsideUISearchBar = searchBar?.value(forKey: "searchField") as? UITextField
        
        if(LanguageManager.currentLanguageIndex() == 0)
        {
            textFieldInsideUISearchBar?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.right
            textFieldInsideUISearchBar?.textAlignment = NSTextAlignment.left
        }
        else
        {
            textFieldInsideUISearchBar?.contentHorizontalAlignment    = UIControlContentHorizontalAlignment.left
            textFieldInsideUISearchBar?.textAlignment = NSTextAlignment.right
        }
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        if isAddressChnage
        {
            self.searchBar?.text = NSLocalizedString("SELECT_LOCATION", comment: "")
            
            let center = CLLocationCoordinate2D(latitude: CLLocationDegrees(selectedNewLat), longitude: CLLocationDegrees(selectedNewLang))
            
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            
            mapView?.setRegion(region, animated: true)
            
            getAddressFromCoordinates(center: center)
        }
        
        setUpNavigationBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureSearchController()
    {
        searchBar?.delegate = self
    }
    
    func setUpNavigationBar()
    {
        self.title = NSLocalizedString("LOCATION_PLACEHOLDER", comment: "")
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("DONE", comment: ""), style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func doneButtonPressed()
    {
        userLattitude = selectedNewLat
        userLongtitude = selectedNewLang
        
        self.navigationController?.popViewController(animated: true)
    }
    
    private func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?
    {
        if annotation.isKind(of: MKUserLocation.self.self)
        {
            return nil
        }
        
        var annotationIdentifier = ""
        var annotationView: MKAnnotationView?
        
        if(annotation.isKind(of: PinAnnotation.self))
        {
            annotationIdentifier = "Pin"
            
            if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
                annotationView = dequeuedAnnotationView
                annotationView?.annotation = annotation
            }
            else
            {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            }
            
            if let annotationView = annotationView
            {
                annotationView.canShowCallout = false
                annotationView.image = UIImage(named: "location")
            }
            
            annotationView?.annotation = annotation
            return annotationView
        }
        
        return nil
    }
    
    func getAddress(handler: @escaping (String) -> Void)
    {
        var address: String = ""
        let geoCoder = CLGeocoder()
        let location = CLLocation (latitude: selectedLat!, longitude: selectedLon!)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark?
            placeMark = placemarks?[0]
            
            // Location name
            if let locationName = placeMark?.addressDictionary?["Name"] as? String
            {
                address += locationName + ", "
            }
            // Street address
            else if let street = placeMark?.addressDictionary?["Thoroughfare"] as? String
            {
                address += street + ", "
            }
            // City
            else if let city = placeMark?.addressDictionary?["City"] as? String
            {
                address += city + ", "
            }
            // Zip code
            else if let zip = placeMark?.addressDictionary?["ZIP"] as? String
            {
                address += zip + ", "
            }
            // Country
            else if let country = placeMark?.addressDictionary?["Country"] as? String
            {
                address += country
            }
            
            // Passing address back
            handler(address)
        })
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool
    {
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)
        
        return false
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar)
    {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace)
    {
        searchBar?.text = place.formattedAddress
        address = place.formattedAddress! as NSString
        
        self.dismiss(animated: true, completion: nil)
        
        selectedNewLat = Float(CLLocationDegrees(place.coordinate.latitude))
        selectedNewLang = Float(CLLocationDegrees(place.coordinate.longitude))
        
        isAddressChnage = true
        
        if let annotations = self.mapView?.annotations {
            for annotation in annotations {
                self.mapView?.removeAnnotation(annotation)
            }
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error)
    {
        print( "error", error)
        self.dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: UIGestureRecognizer Delegates
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool
    {
        return true
    }
    
    func handleLongPress(_ gestureRecognizer: UIGestureRecognizer)
    {
        let touchPoint = gestureRecognizer.location(in: mapView)
        let touchMapCoordinate = mapView?.convert(touchPoint, toCoordinateFrom: mapView)
        
        getAddressFromCoordinates(center: touchMapCoordinate!)
    }
    
    func getAddressFromCoordinates(center : CLLocationCoordinate2D)
    {
        let location = CLLocation(latitude: CLLocationDegrees(center.latitude), longitude: CLLocationDegrees(center.longitude))
        
        selectedNewLang = Float(center.longitude)
        selectedNewLat = Float(center.latitude)
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            
            if error != nil {
                
                return
            }
            
            if ((placemarks?.count)! > 0 && center.latitude != 0)
            {
                self.address = ""
                
                let pm = placemarks?[0]
                
                var subLocality : NSString            = ""
                var locality : NSString               = ""
                var administrativeArea : NSString     = ""
                var thoroughfare : NSString           = ""
                
                
                if( pm?.thoroughfare != nil)
                {
                    thoroughfare = (pm?.thoroughfare!)! as NSString
                    self.address = thoroughfare.appending(", ") as NSString
                }
                
                if(pm?.subLocality != nil)
                {
                    subLocality = (pm?.subLocality!)! as NSString
                    self.address = self.address.appending(subLocality as String) as (String) as NSString
                    self.address = self.address.appending(", ") as NSString
                }
                
                if(pm?.locality != nil)
                {
                    locality = (pm?.locality!)! as NSString
                    self.address = self.address.appending(locality as String) as NSString
                    self.address = self.address.appending(", ") as NSString
                }
                
                if(pm?.administrativeArea != nil )
                {
                    administrativeArea = (pm?.administrativeArea!)! as NSString
                    self.address = self.address.appending(administrativeArea as String) as NSString
                }
                
                addressUser = self.address as String
                
                self.searchBar?.text = self.address as String
                
                /////////////
                self.mapView?.removeAnnotation(self.annotation)
                self.annotation = PinAnnotation()
                self.annotation.coordinate = center
                self.annotation.userName = self.address as String
                self.mapView?.addAnnotation(self.annotation)
            }
            else
            {
                print("Problem with the data received from geocoder")
                
                self.searchBar?.text = NSLocalizedString("SELECT_YOUR_LOCATION", comment: "")
            }
        })
    }
}
