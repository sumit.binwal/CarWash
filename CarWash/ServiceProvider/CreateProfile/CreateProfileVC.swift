//
//  CreateProfileVC.swift
//  CarWash
//
//  Created by iOS on 05/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import RSKImageCropper
import MBProgressHUD
import GooglePlaces
import AWSS3

class CreateProfileVC: UIViewController, MyProtocol, Document
{
    @IBOutlet var imgProfile       : UIImageView?
    @IBOutlet var imgLogo          : UIImageView?
    
    @IBOutlet var txtTitle_en      : UITextField?
    @IBOutlet var txtTitle_ar      : UITextField?
    @IBOutlet var txtNumber        : UITextField?
    @IBOutlet weak var locationTextField: UITextField!
    
    @IBOutlet var profileView      : UIView?
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnOkay: UIButton!
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var manageHoursBtn: UIButton!
    
    @IBOutlet weak var lblProfileUpdate: UILabel!
    @IBOutlet weak var lblNoOfCars: UILabel!
    @IBOutlet weak var lblUpdateMsg: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    var address : NSString?
    
    var isLogo      : Bool?
    var isCircle    : Bool?
    var isDocument  : Bool?
    
    var libraryEnabled: Bool = true
    var croppingEnabled: Bool = true
    var allowResizing: Bool = false
    var allowMoving: Bool = false
    
    let imagePicker = UIImagePickerController()
    var imageCropVC : RSKImageCropViewController!
    
    var hoursAry : NSMutableArray = []
    
    var minimumSize: CGSize = CGSize(width: 60, height: 60)
    
    var isChange = 0
    
    var coverImageArr : [String] = []
    
    var logoImageString = ""
    
    @IBOutlet var coverImageCollectionView: UICollectionView!
    @IBOutlet var pageControlCoverImage: UIPageControl!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        isDocument  = false
        isCircle    = false
        isLogo      = false
        
        locationTextField.delegate = self
        
        setUpNavigationBar()
        
        profileView?.viewWithTag(1)?.layer.cornerRadius  = 10.0
        profileView?.viewWithTag(1)?.layer.masksToBounds = true

        lblNoOfCars.text = NSLocalizedString("NO_OF_CARS", comment: "")
        lblProfileUpdate.text = NSLocalizedString("PROFILE_UPDATED", comment: "")
        lblUpdateMsg.text = NSLocalizedString("PROFILE_UPDATED_ALERT", comment: "")
        locationTextField.placeholder = NSLocalizedString("LOCATION_PLACEHOLDER", comment: "")
        lblAddress.text = NSLocalizedString("ADDRESS", comment: "")
        
        coverImageCollectionView.register(UINib.init(nibName: "CoverImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CoverImageCollectionViewCellIdentifiler")
        
        uploadBtn.setTitle(NSLocalizedString("UPLOAD_DOC", comment: ""), for: UIControlState.normal)
        
        manageHoursBtn.setTitle(NSLocalizedString("MANAGE_OPE_HOURS", comment: ""), for: UIControlState.normal)
        
        btnSubmit.setTitle(NSLocalizedString("SUBMIT_TEXT", comment: ""), for: UIControlState.normal)
        
        btnOkay.setTitle(NSLocalizedString("OKEY", comment: ""), for: UIControlState.normal)
        
        coverImageCollectionView.isHidden = true
        pageControlCoverImage.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        print("lat = \(userLattitude)")
        print("long = \(userLongtitude)")
        
        getAddressFromLatLong()
        
        if(LanguageManager.currentLanguageIndex() == 0)
        {
            txtNumber?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
            txtNumber?.textAlignment = NSTextAlignment.left
            lblNoOfCars.textAlignment = NSTextAlignment.left
            
            manageHoursBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            uploadBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            
            manageHoursBtn.titleEdgeInsets.left = 70*SCREEN_WIDTH/375
            uploadBtn.titleEdgeInsets.left = 70*SCREEN_WIDTH/375
            
            locationTextField?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
            locationTextField?.textAlignment = NSTextAlignment.left
        }
        else
        {
            txtNumber?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            txtNumber?.textAlignment = NSTextAlignment.right
            lblNoOfCars.textAlignment = NSTextAlignment.right
            
            manageHoursBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
            uploadBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
            
            locationTextField?.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            locationTextField?.textAlignment = NSTextAlignment.right
            
            manageHoursBtn.titleEdgeInsets.right = 70*SCREEN_WIDTH/375
            uploadBtn.titleEdgeInsets.right = 70*SCREEN_WIDTH/375
        }
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
    }                      

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setUpNavigationBar()
    {
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.title = NSLocalizedString("CREATE_PROFILE", comment: "")
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func setResultOfBusinessLogic(valueSent: NSMutableArray)
    {
        hoursAry = valueSent
    }

    func setCountOfDocument(valueSent: Bool)
    {
        isDocument = valueSent
    }
    
    // MARK: IBAction Methods
    
    @IBAction func uploadDocument()
    {
        let uploadVC = UploadVC()
        uploadVC.delegate = self
        self.navigationController?.pushViewController(uploadVC, animated: true)
    }
    
    @IBAction func manageHours()
    {
        let operating = OperatingHoursVC()
        
        operating.delegate = self
        
        if hoursAry != nil
        {
            operating.hoursDict = hoursAry
        }
        
        self.navigationController?.pushViewController(operating, animated: true)
    }
    
    @IBAction func save()
    {
        if(CommonFunctions.isInternetAvailable())
        {
            if(validateUserInput())
            {
                MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
                isChange = 1
                getAddressFromLatLong()
            }
        }
    }

    @IBAction func openCamera()
    {
        RESIGN_KEYBOARD
        isCircle = false
        showPic()
    }
    
    @IBAction func selectLogo()
    {
        RESIGN_KEYBOARD
        isCircle = true
        isLogo = true
        showPic()
    }
    
    @IBAction func okay()
    {
        profileView?.removeFromSuperview()
        APPDELEGATE.addTabBarForServiceProvider()
    }
    
    func showPic()
    {
        let optionMenu = UIAlertController(title: nil, message: NSLocalizedString("CHOOSE_IMAGE", comment: ""), preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: NSLocalizedString("CAMERA_TEXT", comment: ""), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if(self.isCircle)!
            {
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
                {
                    self.imagePicker.sourceType     = .camera
                    self.imagePicker.allowsEditing  = false
                    self.imagePicker.delegate       = self
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
            }
            else
            {
                self.imagePicker.sourceType     = .camera
                
                self.imagePicker.allowsEditing  = false
                self.imagePicker.delegate       = self
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let galleryAction = UIAlertAction(title: NSLocalizedString("PHOTO_GALLARY", comment: ""), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if(self.isCircle)!
            {
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
                {
                    self.imagePicker.sourceType = .photoLibrary
                    self.imagePicker.allowsEditing =  false
                    self.imagePicker.delegate = self
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
            }
            else
            {
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
                {
                    self.imagePicker.sourceType = .photoLibrary
                    self.imagePicker.allowsEditing =  false
                    self.imagePicker.delegate = self
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
            }
        })
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("CANCEL_TEXT", comment: ""), style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func switchUser()
    {
        let service = ServiceProvider()
        
        var JSONString = ""
        
        do
        {
            let jsonData = try JSONSerialization.data(withJSONObject: hoursAry , options: JSONSerialization.WritingOptions.prettyPrinted)
            JSONString = String(data: jsonData, encoding: String.Encoding.utf8)! as String
        }
        catch
        {
            print(error)
        }
        
        let dic : NSMutableDictionary =
        [
            "provider_name_en"      : txtTitle_en?.text ?? "",
            "provider_name_ar"      : txtTitle_ar?.text ?? "",
            "operating_hours"       : JSONString,
            "number_of_car"         : txtNumber?.text ?? "",
            "address"               : self.address ?? "",
            "latitude"              : userLattitude.description,
            "longitude"             : userLongtitude.description,
            "cover_image"           : CommonFunctions.getCommaSepratedStringFromArrayDict(completeArray: coverImageArr),
            "avatar"                : self.logoImageString
        ]
        
        service.switchUserRequestWithParameters(dic, nil, success:
            { (response, data) in
                
                MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                
                if(response?.statusCode == 200)
                {
                    let str : NSString = data?.object(forKey: "status") as! NSString
                    
                    if (str.isEqual(to: "success"))
                    {
                        USERDEFAULT.set(NSKeyedArchiver.archivedData(withRootObject: data?.object(forKey: "data") as! NSDictionary), forKey: "USER_DATA")
                        USERDEFAULT.synchronize()
                        
                        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
                        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
                        let fcm_user_id = dict.object(forKey: "fcm_user_id") as! String
                        let email = dict.object(forKey: "email") as! String
                        let provider_name = dict.object(forKey: "provider_name") as! NSDictionary
                        
                        var name = ""
                        if language == "en" {
                            name = provider_name.object(forKey: "en") as! String
                        } else {
                            name = provider_name.object(forKey: "ar") as! String
                        }
                        
                        User.updateUser(withName: name, withEmail: email, withId: fcm_user_id, completion: { (isUpdated) in
                            
                            if (isUpdated == true)
                            {
                                self.profileView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGTH)
                                APPDELEGATE.window?.addSubview(self.profileView!)
                            }
                            else
                            {
                                AlertViewController.showAlertWith(title: "", message: NSLocalizedString("ERROR_OTHER", comment: ""), dismissBloack:
                                    {
                                        
                                })
                            }
                        })
                    }
                    else
                    {
                        AlertViewController.showAlertWith(title: "", message: data?.object(forKey: "msg") as! String, dismissBloack:
                            {
                      
                        })
                    }
                }
        })
        { (response, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
        }
    }
    
    func getAddressFromLatLong()
    {
        let location = CLLocation(latitude: CLLocationDegrees(userLattitude), longitude: CLLocationDegrees(userLongtitude))

        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in

            if error != nil
            {
                return
            }

            if ((placemarks?.count)! > 0 && userLattitude != 0)
            {
                self.address = ""

                let pm = placemarks?[0]

                var subLocality : NSString            = ""
                var locality : NSString               = ""
                var administrativeArea : NSString     = ""
                var thoroughfare : NSString           = ""


                if( pm?.thoroughfare != nil)
                {
                    thoroughfare = (pm?.thoroughfare!)! as NSString
                    self.address = thoroughfare.appending(", ") as NSString
                }

                if(pm?.subLocality != nil)
                {
                    subLocality = (pm?.subLocality!)! as NSString
                    self.address = self.address?.appending(subLocality as String) as NSString?
                    self.address = self.address?.appending(", ") as NSString?
                }

                if(pm?.locality != nil)
                {
                    locality = (pm?.locality!)! as NSString
                    self.address = self.address?.appending(locality as String) as NSString?
                    self.address = self.address?.appending(", ") as NSString?
                }

                if(pm?.administrativeArea != nil )
                {
                    administrativeArea = (pm?.administrativeArea!)! as NSString
                    self.address = self.address?.appending(administrativeArea as String) as NSString?
                }

                if self.isChange == 0 {
                    self.locationTextField.text = self.address! as String
                } else {
                    //calling api to switch user
                    self.switchUser()
                }
            }
            else
            {
                print("Problem with the data received from geocoder")
                self.locationTextField.placeholder = NSLocalizedString("LOCATION_PLACEHOLDER", comment: "")
            }
        })
    }
    
    //MARK: Validate Method
    
    func validateUserInput() -> Bool
    {
        var isError : Bool = true
        
        if(!isLogo!)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_LOGO", comment: ""), dismissBloack:
                {
            })
        }
        else if coverImageArr.count == 0
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_COVER", comment: ""), dismissBloack:
                {
            })
        }
        else if(txtTitle_en?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_TITLE_EN", comment: ""), dismissBloack:
                {
                    self.txtTitle_en?.becomeFirstResponder()
            })
        }
        else if(txtTitle_ar?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_TITLE_AR", comment: ""), dismissBloack:
                {
                    self.txtTitle_ar?.becomeFirstResponder()
            })
        }
        else if(txtNumber?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_NUMBER", comment: ""), dismissBloack:
                {
                    self.txtNumber?.becomeFirstResponder()
            })
        }
        else if(hoursAry.count == 0)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_HOURS", comment: ""), dismissBloack:
                {
            })
        }
        else if(isDocument == false)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_DOC", comment: ""), dismissBloack:
                {
            })
        }
        
        return isError
    }
}

//MARK: - Extension -> UICollectionView -> UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension CreateProfileVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return coverImageArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CoverImageCollectionViewCellIdentifiler", for: indexPath) as! CoverImageCollectionViewCell
        
        print("coverImageArr[indexPath.row] = ", coverImageArr[indexPath.row])
        
        cell.coverImageView.setIndicatorStyle(.gray)
        cell.coverImageView.setShowActivityIndicator(true)
        
        cell.coverImageView.sd_setImage(with: URL.init(string: coverImageArr[indexPath.row]), placeholderImage: #imageLiteral(resourceName: "group29"))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // your code here
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let indexPath = coverImageCollectionView.indexPathsForVisibleItems.first
        pageControlCoverImage.currentPage = (indexPath?.row)!
    }
}

extension CreateProfileVC : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtNumber
        {
            let str = textField.text! as NSString
            
            if str.hasPrefix("0") {
                return false
            }
            
            if str.length >= 2 {
                if string == "" {
                    return true
                } else {
                    return false
                }
            }
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == locationTextField {
            
            let locationVC = SelectLocationViewController()
            self.navigationController?.pushViewController(locationVC, animated: true)
            
            return false
        }
        
        return true
    }
}

extension CreateProfileVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    // UIImagePicker Delegates
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let editedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            picker.dismiss(animated: false, completion: { () -> Void in
                
                if(self.isCircle)!
                {
                    self.imageCropVC = nil
                    
                    self.imageCropVC = RSKImageCropViewController(image: editedImage, cropMode: RSKImageCropMode.circle)
                    
                    self.imageCropVC.delegate = self
                    
                    self.present(self.imageCropVC, animated: true, completion: nil)
                }
                else
                {
                    self.imageCropVC = nil
                    
                    self.imageCropVC = RSKImageCropViewController(image: editedImage, cropMode: RSKImageCropMode.custom)
                    
                    self.imageCropVC.delegate = self
                    self.imageCropVC.dataSource = self
                    
                    self.present(self.imageCropVC, animated: true, completion: nil)
                }
            })
        }
    }
}

extension CreateProfileVC : RSKImageCropViewControllerDelegate, RSKImageCropViewControllerDataSource
{
    //MARK: RSKImageCropViewController Delegates
    
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect)
    {
        if(isCircle!)
        {
            _ = CommonFunctions.addFileToFolder("saved", fileName: "save.jpg", fileData: UIImageJPEGRepresentation(croppedImage, 0.6)!)
            
            if let fileURL = CommonFunctions.getFileURLFromFolder("saved", fileName: "save.jpg") {
                self.uploadAvatarImageToAWS(usingImage: fileURL as NSURL)
            }
        }
        else
        {
            //imgProfile?.image = croppedImage
            
            _ = CommonFunctions.addFileToFolder("saved", fileName: "save.jpg", fileData: UIImageJPEGRepresentation(croppedImage, 0.6)!)
            
            if let fileURL = CommonFunctions.getFileURLFromFolder("saved", fileName: "save.jpg") {
                self.uploadCoverImageToAWS(usingImage: fileURL as NSURL)
            }
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: RSKImageCropViewController Datasource
    
    func imageCropViewControllerCustomMaskRect(_ controller: RSKImageCropViewController) -> CGRect
    {
        let maskSize = CGSize(width: SCREEN_WIDTH, height: SCREEN_WIDTH * (200/375))
        
        let rect = CGRect(x: (SCREEN_WIDTH - maskSize.width) * 0.5, y: (SCREEN_HEIGTH - maskSize.height) * 0.5, width: maskSize.width, height: maskSize.height)
        
        return rect
    }
    
    func imageCropViewControllerCustomMaskPath(_ controller: RSKImageCropViewController) -> UIBezierPath
    {
        let rect: CGRect = controller.maskRect
        
        if controller.cropMode == RSKImageCropMode.custom {
            
            let point1       = CGPoint(x: rect.minX, y: rect.maxY)
            let point2       = CGPoint(x: rect.maxX, y: rect.maxY)
            let point3       = CGPoint(x: rect.maxX, y: rect.minY)
            let point4       = CGPoint(x: rect.minX, y: rect.minY)
            
            let triangle     = UIBezierPath()
            
            triangle.move(to: point1)
            triangle.addLine(to: point2)
            triangle.addLine(to: point3)
            triangle.addLine(to: point4)
            
            triangle.close()
            
            return triangle
            
        } else {
            let point1       = CGPoint(x: rect.minX, y: rect.maxY)
            let point2       = CGPoint(x: rect.maxX, y: rect.maxY)
            let point3       = CGPoint(x: rect.midX, y: rect.minY)
            
            let triangle     = UIBezierPath()
            
            triangle.move(to: point1)
            triangle.addLine(to: point2)
            triangle.addLine(to: point3)
            
            triangle.close()
            
            return triangle
        }
    }
    
    func uploadAvatarImageToAWS(usingImage : NSURL) {
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        let ext = "jpg"
        
        let keyName = "avatar" + CommonFunctions.getCurrentTimeStamp() + ".jpg"
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.body = usingImage as URL
        uploadRequest?.key = keyName
        uploadRequest?.bucket = AmazonS3BucketNameAvatar + CommonFunctions.getUserIdData()!
        uploadRequest?.contentType = "image/" + ext
        uploadRequest?.acl = .publicReadWrite
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith { (task) -> AnyObject! in
            
            if let error = task.error {
                print("Upload failed  (\(error))")
                
                DispatchQueue.main.async(execute: {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    return
                })
            }
            
            if task.result != nil
            {
                print("Uploaded to:fksjflksdajf askldjf")
                
                DispatchQueue.main.async(execute: {
                    
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    self.logoImageString = "https://s3-us-west-2.amazonaws.com/car-wash/" + "avatar/" + CommonFunctions.getUserIdData()! + "/" + keyName
                    print(self.logoImageString)
                    
                    self.imgLogo?.setIndicatorStyle(.gray)
                    self.imgLogo?.setShowActivityIndicator(true)
                    
                    self.imgLogo?.sd_setImage(with: URL.init(string: self.logoImageString), placeholderImage: nil)
                    
                    self.imgLogo?.layer.cornerRadius = (self.imgLogo?.frame.size.width)!/2
                    self.imgLogo?.layer.masksToBounds = true
                    self.imgLogo?.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
                    self.imgLogo?.layer.borderWidth = 1.0
                    
                    return
                })
            }
            else
            {
                print("Unexpected empty result.")
                DispatchQueue.main.async(execute: {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    return
                })
            }
            
            return nil
        }
    }
    
    func uploadCoverImageToAWS(usingImage : NSURL) {
        
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        let ext = "jpg"
        
        let keyName = "cover_image" + CommonFunctions.getCurrentTimeStamp() + ".jpg"
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.body = usingImage as URL
        uploadRequest?.key = keyName
        uploadRequest?.bucket = AmazonS3BucketNameCover + CommonFunctions.getUserIdData()!
        uploadRequest?.contentType = "image/" + ext
        uploadRequest?.acl = .publicReadWrite
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith { (task) -> AnyObject! in
            
            if let error = task.error {
                print("Upload failed  (\(error))")
                
                DispatchQueue.main.async(execute: {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    return
                })
            }
            
            if task.result != nil
            {
                print("Uploaded to:fksjflksdajf askldjf")
                
                DispatchQueue.main.async(execute: {
                    
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    let nameCoverImage = "https://s3-us-west-2.amazonaws.com/car-wash/" + "cover/" + CommonFunctions.getUserIdData()! + "/" + keyName
                    
                    self.coverImageArr.append(nameCoverImage)
                    
                    self.pageControlCoverImage.numberOfPages = self.coverImageArr.count
                    self.coverImageCollectionView.reloadData()
                    
                    self.coverImageCollectionView.isHidden = false
                    self.pageControlCoverImage.isHidden = false
                    
                    self.imgProfile?.isHidden = true
                    
                    return
                })
            }
            else
            {
                print("Unexpected empty result.")
                DispatchQueue.main.async(execute: {
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    return
                })
            }
            
            return nil
        }
    }
}
