//
//  PRatingTableViewCell.swift
//  CarWash
//
//  Created by Ratina on 5/1/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit

class PRatingTableViewCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var requestNoLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var ratingView: FloatRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        ratingView.backgroundColor = UIColor.clear
        
        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        ratingView.type = .halfRatings
        ratingView.rating = 0.0
        ratingView.isUserInteractionEnabled = false
        
        if language == "en"
        {
            
        }
        else
        {
            ratingView.transform = ratingView.transform.rotated(by: CGFloat(Double.pi))
        }
        
        ratingView.fullImage = language == "en" ? UIImage(named:"full_star") : UIImage(named:"full_star_ar")
        
        ratingView.emptyImage = language == "en" ? UIImage(named:"empty_star") : UIImage(named:"empty_star_ar")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
