//
//  MapPathVC.swift
//  CarWash
//
//  Created by Jitendra Singh on 03/01/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit
import GoogleMaps

enum JSONError: String, Error
{
    case NoData = "ERROR: no data"
    case ConversionFailed = "ERROR: conversion from JSON failed"
}

class MapPathVC: UIViewController,GMSMapViewDelegate
{
    @IBOutlet weak var mapView: GMSMapView!
    
    var destLattitude = Float()
    var destLongitude = Float()
    
    var currentLattitude = Float()
    var currentLongitude = Float()
    
    var destAddress : NSString?
    var currentAddress : NSString?
    
    var routeArray : NSArray?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setUpNavigationBar()
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        
        let locationArr = dict.object(forKey: "location") as! NSArray
        
        currentLongitude = locationArr.object(at: 0) as! Float
        currentLattitude = locationArr.object(at: 1) as! Float
        
        currentAddress = dict.object(forKey: "address") as! String as NSString
        print("current address -> ",currentAddress ?? "")
        
        print("current lattitude -> ",currentLattitude)
        print("current longtitude -> ",currentLongitude)
        
        print("user lattitude -> ", destLattitude )
        print("user lattitude -> ", destLongitude )
        
        print("user address -> ", destAddress ?? "")
        
        let cameraPositionCoordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(currentLattitude), longitude: CLLocationDegrees(currentLongitude))
        let cameraPosition = GMSCameraPosition.camera(withTarget: cameraPositionCoordinates, zoom: 18)
        
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: cameraPosition)
        mapView.isMyLocationEnabled = true
        
        self.view = mapView
        // Do any additional setup after loading the view.
        
       // let a_coordinate_string = "26.9124,75.7873"
        let a_coordinate_string = "\(currentLattitude),\(currentLongitude)"
        let b_coordinate_string = "\(destLongitude),\(destLattitude)" as NSString
        
        let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(a_coordinate_string)&destination=\(b_coordinate_string)&key=AIzaSyAh87Bhv7nvo-A-z44iuTelhBMGlJefjMk"
        
        print("urlstring",urlString)
        
        guard let url = URL(string: urlString) else {
            print("Error: cannot create URL")
            return
        }
        
        let urlRequest = URLRequest(url: url)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            
            do {
                guard let data = data else {
                    throw JSONError.NoData
                }
                
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
                    throw JSONError.ConversionFailed
                }
                
                print(json)
                
                if let array = json["routes"] as? NSArray
                {
                    if (array.count > 0)
                    {
                        if let routes = array[0] as? NSDictionary
                        {
                            if let overview_polyline = routes["overview_polyline"] as? NSDictionary
                            {
                                if let points = overview_polyline["points"] as? String
                                {
                                    print("points123456qwe--",points)
                                    // Use DispatchQueue.main for main thread for handling UI
                                
                                    DispatchQueue.main.async
                                    {
                                        // show polyline
                                        let path = GMSPath(fromEncodedPath:points)
                                        let polyline = GMSPolyline()
                                        polyline.path = path
                                        polyline.strokeWidth = 4
                                        polyline.strokeColor = UIColor.init(hue: 210, saturation: 88, brightness: 84, alpha: 1)
                                        polyline.map = mapView
                                    
                                        let bounds : GMSCoordinateBounds = GMSCoordinateBounds(path: path!)
                                        let update = GMSCameraUpdate .fit(bounds, withPadding: 100)
                                    
                                        let marker = GMSMarker()
                                        marker.position = CLLocationCoordinate2DMake(CLLocationDegrees(self.currentLattitude), CLLocationDegrees(self.currentLongitude))
                                        marker.title = self.currentAddress! as String
                                        marker.snippet = self.currentAddress! as String
                                        marker.groundAnchor = CGPoint.init(x: 0.5, y: 0.5)
                                        marker.icon = UIImage(named: "location")
                                        marker.map = mapView
                                    
                                        // Creates a marker in the center of the map.
                                        let marker1 = GMSMarker()
                                        marker1.position = CLLocationCoordinate2DMake(CLLocationDegrees(self.destLongitude), CLLocationDegrees(self.destLattitude))
                                        marker1.icon = UIImage(named: "location")
                                        marker1.groundAnchor = CGPoint.init(x: 0.5, y: 0.5)
                                        marker1.title = self.destAddress! as String
                                        marker1.snippet = self.destAddress! as String
                                        marker1.map = mapView
                                    
                                        mapView.moveCamera(update)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch let error as JSONError
            {
                print(error.rawValue)
            }
            catch let error as NSError
            {
                print(error.debugDescription)
            }
        })
        
        task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpNavigationBar()
    {
        self.title = NSLocalizedString("PATH_DISTANCE", comment: "")
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        //self.title = "Select Services"
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex() == 0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
