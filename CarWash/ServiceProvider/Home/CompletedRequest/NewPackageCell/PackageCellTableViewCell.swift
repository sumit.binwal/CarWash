//
//  PackageCellTableViewCell.swift
//  CarWash
//
//  Created by Ratina on 6/12/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import UIKit

class PackageCellTableViewCell: UITableViewCell {

    @IBOutlet weak var package_name_label: UILabel!
    
    @IBOutlet weak var time_label1: UILabel!
    @IBOutlet weak var time_label: UILabel!
    
    @IBOutlet weak var cost_label1: UILabel!
    @IBOutlet weak var cost_label: UILabel!
    
    @IBOutlet weak var required_label1: UILabel!
    @IBOutlet weak var required_label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
