//  MIT License

//  Copyright (c) 2017 Haik Aslanyan

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.


import UIKit
import Photos
import Firebase
import CoreLocation
import MBProgressHUD

class ChatVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,  UINavigationControllerDelegate, UIImagePickerControllerDelegate, CLLocationManagerDelegate {
    
    //MARK: Properties
    @IBOutlet var inputBar: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var inputTextField: UITextField!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var chatTableView: UITableView?
    
    override var inputAccessoryView: UIView? {
        get {
            //self.inputBar.frame.size.height = self.barHeight
            //self.inputBar.clipsToBounds = true
            return nil
        }
    }
    
    override var canBecomeFirstResponder: Bool{
        return true
    }
    
    let locationManager = CLLocationManager()
    var messagesArr = [Message]()
    let imagePicker = UIImagePickerController()
    let barHeight: CGFloat = 50
    var currentUser: User?
    var canSendLocation = true
    var isNotify = Int();
    let dateFormatter = DateFormatter()
    var dictUser = NSDictionary()

    let refreshControl = UIRefreshControl()
    
    //MARK: Methods
    func customization() {
        
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "hh:mm a"
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        dictUser = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.applicationWillEnterBackGround(notification:)), name: NSNotification.Name.UIApplicationDidEnterBackground, object:nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.applicationWillBecomeActive(notification:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object:nil)
        
        //self.imagePicker.delegate = self
        
        //
        //chatTableView?.contentInset.bottom = barHeight
        //chatTableView?.scrollIndicatorInsets.bottom = barHeight
        
        self.locationManager.delegate = self
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            chatTableView?.refreshControl = refreshControl
        } else {
            chatTableView?.addSubview(refreshControl)
        }
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshHomeData(_:)), for: .valueChanged)
    }
    
    func refreshHomeData(_ sender: Any) {
        // Fetch Weather Data
        
        chatTableView?.reloadData()
    }
    
    func applicationWillEnterBackGround(notification: NSNotification) {
        
        inputTextField.resignFirstResponder()
        User.setUserStatusOffline (completion:{ (_) in
        })
    }
    
    func applicationWillBecomeActive(notification: NSNotification) {
        User.setUserStatusOnline(completion: { (_) in
        })
    }
    
    //Downloads messages   self.currentUser!.id
    func fetchData() {
        
        let uid = dictUser["fcm_user_id"] as! String
        
        print("uid = \(uid)")
        
        //CommonFunctions.showActivityIndicator(withText: "")
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        Message.downloadAllMessages(forUserID:uid , completion: {[weak weakSelf = self] (message) in
            
            //print("message = \(message)")
            
            weakSelf?.messagesArr.append(message)
            weakSelf?.messagesArr.sort{ $0.timestamp < $1.timestamp }
            
            print("weakSelf?.items = \(weakSelf?.messagesArr.count ?? 0)")
            
            DispatchQueue.main.async {
                
                if let state = weakSelf?.messagesArr.isEmpty, state == false {
                    
                    //CommonFunctions.removeActivityIndicator()
                    MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
                    
                    self.chatTableView?.reloadData()
                    weakSelf?.chatTableView?.scrollToRow(at: IndexPath.init(row: self.messagesArr.count - 1, section: 0), at: .bottom, animated: false)
                }
            }
        })
        
        //Message.markMessagesRead(forUserID: uid)
    }
    
    //Hides current viewcontroller
    func dismissSelf() {
        
        if let navController = self.navigationController {
            
            UserDefaults.standard.set("0", forKey: "isChatScreen")
            UserDefaults.standard.synchronize()
            
            //if(isNotify == 0){
                navController.popViewController(animated: true)
            //}
            //else{
            //    self.dismiss(animated: true, completion: nil)
            //}
        }
    }
    
    func composeMessage(type: MessageType, content: Any)  {
        
        let message = Message.init(type: type, content: content, owner: .sender, timestamp: Int(Date().timeIntervalSince1970), isRead: false)
        
        let uid = dictUser["fire_user_id"] as! String
        let fcmID = dictUser["device_id"] as! String
        let senderName = dictUser["name"] as! String
        
        let dictUserInfo = UserDefaults.standard.object(forKey:"UserInfo") as! NSDictionary
        
        let senderInfoDict = ["fcm_user_id" : dictUserInfo["fcm_user_id"]!, "device_id" : dictUserInfo["device_id"]!, "name":dictUserInfo["name"]!] as NSDictionary
        
        let myDID = dictUserInfo["device_id"] as! String
        
        Message.send(message: message, toID: uid, completion: {(state) in
            
            //Send Push Notification if user is Offline
            if state == true{
                User.checkUserStatus(firebaseID: uid, completion: { (status) in
                    if status == "offline"{
                        
                        print("FCM ID : ",fcmID)
                        print("MY ID : ",myDID)
                        
                        if (fcmID != myDID)
                        {
                            let pushDict = ["title" : senderName, "message" : message.content, "userData" : senderInfoDict] as NSDictionary
                            
                            APPDELEGATE.sendPush(toDevice: fcmID, withNotifyData: pushDict as! [AnyHashable : Any], withTitle: dictUserInfo["name"] as! String)
                        }
                    }
                })
            }
        })
    }
    
    func checkLocationPermission() -> Bool {
        
        var state = false
        
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            state = true
        case .authorizedAlways:
            state = true
        default: break
        }
        
        return state
    }
    
    func animateExtraButtons(toHide: Bool)  {
        
        switch toHide {
        case true:
            self.bottomConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.inputBar.layoutIfNeeded()
            }
        default:
            self.bottomConstraint.constant = -50
            UIView.animate(withDuration: 0.3) {
                self.inputBar.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func showMessage(_ sender: Any) {
       self.animateExtraButtons(toHide: true)
    }
    
    @IBAction func selectGallery(_ sender: Any) {
        
        self.animateExtraButtons(toHide: true)
        let status = PHPhotoLibrary.authorizationStatus()
        
        if (status == .authorized || status == .notDetermined) {
            self.imagePicker.sourceType = .savedPhotosAlbum;
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func selectCamera(_ sender: Any) {
        
        self.animateExtraButtons(toHide: true)
        let status = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        
        if (status == .authorized || status == .notDetermined) {
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func selectLocation(_ sender: Any) {
        
        self.canSendLocation = true
        self.animateExtraButtons(toHide: true)
        
        if self.checkLocationPermission() {
            self.locationManager.startUpdatingLocation()
        } else {
            self.locationManager.requestWhenInUseAuthorization()
        }
    }
    
    @IBAction func showOptions(_ sender: Any) {
        self.animateExtraButtons(toHide: false)
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        
        if let text = self.inputTextField.text {
            if text.count > 0 {
                self.composeMessage(type: .text, content: self.inputTextField.text!)
                self.inputTextField.text = ""
            }
        }
    }
    
    //MARK: NotificationCenter handlers
    func showKeyboard(notification: Notification) {
        
        if let frame = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            self.chatTableView?.contentInset.bottom = height
            self.chatTableView?.scrollIndicatorInsets.bottom = height
            
            if self.messagesArr.count > 0 {
                let indexPath = IndexPath(row: self.messagesArr.count-1, section: 0)
                self.chatTableView?.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }

    //MARK: Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("messagesArr count = \(self.messagesArr.count)")
        return self.messagesArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("self.messagesArr[indexPath.row].owner = \(self.messagesArr[indexPath.row].owner)")
        
        switch self.messagesArr[indexPath.row].owner {
        case .receiver:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Receiver", for: indexPath) as! ReceiverCell
            
            cell.clearCellData()
            
            switch self.messagesArr[indexPath.row].type {
                
            case .text:
                cell.message.text = self.messagesArr[indexPath.row].content as? String
                
                if let img = UIImage(named: "") {
                    let resizable = img.resizableImage(withCapInsets: UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12), resizingMode: .stretch)
                    cell.messageBackground.image = resizable
                }

                let time:Int? = self.messagesArr[indexPath.row].timestamp
                let epocTime = TimeInterval(time!)
                let date = Date(timeIntervalSince1970:epocTime)
                cell.lblTime.text = dateFormatter.string(from: date)
                
            case .photo:
                if let image = self.messagesArr[indexPath.row].image {
                    cell.messageBackground.image = image
                    cell.message.isHidden = true
                } else {
                    cell.messageBackground.image = UIImage.init(named: "")
                    self.messagesArr[indexPath.row].downloadImage(indexpathRow: indexPath.row, completion: { (state, index) in
                        if state == true {
                            DispatchQueue.main.async {
                                self.chatTableView?.reloadData()
                            }
                        }
                    })
                }
                
            case .location:
                cell.messageBackground.image = UIImage.init(named: "")
                cell.message.isHidden = true
            }
            
            return cell
            
        case .sender:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Sender", for: indexPath) as! SenderCell
            cell.clearCellData()
            //cell.profilePic.image = UIImage(named:"profile")//self.currentUser?.profilePic
            
            switch self.messagesArr[indexPath.row].type {
                
            case .text:
                cell.message.text = self.messagesArr[indexPath.row].content as? String
                
                if let img = UIImage(named: "") {
                    let resizable = img.resizableImage(withCapInsets: UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12), resizingMode: .stretch)
                    cell.messageBackground.image = resizable
                }
                
                let time:Int? = self.messagesArr[indexPath.row].timestamp
                let epocTime = TimeInterval(time!)
                let date = Date(timeIntervalSince1970:epocTime)
                cell.lblTime.text = dateFormatter.string(from: date)
                
            case .photo:
                if let image = self.messagesArr[indexPath.row].image {
                    cell.messageBackground.image = image
                    cell.message.isHidden = true
                } else {
                    cell.messageBackground.image = UIImage.init(named: "")
                    self.messagesArr[indexPath.row].downloadImage(indexpathRow: indexPath.row, completion: { (state, index) in
                        if state == true {
                            DispatchQueue.main.async {
                                self.chatTableView?.reloadData()
                            }
                        }
                    })
                }
                
            case .location:
                cell.messageBackground.image = UIImage.init(named: "")
                cell.message.isHidden = true
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.inputTextField.resignFirstResponder()
        
        switch self.messagesArr[indexPath.row].type {
            
        case .photo:
            if let photo = self.messagesArr[indexPath.row].image {
                let info = ["viewType" : ShowExtraView.preview, "pic": photo] as [String : Any]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showExtraView"), object: nil, userInfo: info)
                self.inputAccessoryView?.isHidden = true
            }
            
        case .location:
            let coordinates = (self.messagesArr[indexPath.row].content as! String).components(separatedBy: ":")
            let location = CLLocationCoordinate2D.init(latitude: CLLocationDegrees(coordinates[0])!, longitude: CLLocationDegrees(coordinates[1])!)
            let info = ["viewType" : ShowExtraView.map, "location": location] as [String : Any]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showExtraView"), object: nil, userInfo: info)
            self.inputAccessoryView?.isHidden = true
            
        default: break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.composeMessage(type: .photo, content: pickedImage)
        } else {
            let pickedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            self.composeMessage(type: .photo, content: pickedImage)
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationManager.stopUpdatingLocation()
    }

    //MARK: ViewController lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "Chat Screen"
        
        chatTableView?.delegate = self
        chatTableView?.dataSource = self
        
        chatTableView?.estimatedRowHeight = barHeight
        chatTableView?.rowHeight = UITableViewAutomaticDimension

        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        //self.title = "Select Services"
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(dismissSelf))
        
        User.setUserStatusOnline(completion: { (_) in
        })
        
        UserDefaults.standard.set("1", forKey: "isChatScreen")
        UserDefaults.standard.synchronize()
        
        //self.inputBar.backgroundColor = UIColor.clear
        
        self.view.layoutIfNeeded()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVC.showKeyboard(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
        self.messagesArr.removeAll()
        
        self.fetchData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        User.setUserStatusOffline (completion:{ (_) in
        })
        
        UserDefaults.standard.set("0", forKey: "isChatScreen")
        UserDefaults.standard.synchronize()
        
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.customization()
    }
}
