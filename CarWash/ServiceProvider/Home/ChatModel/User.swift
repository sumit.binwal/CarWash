//
//  User.swift
//  TeenJobs
//
//  Created by Pankaj Asudani on 14/07/17.
//  Copyright © 2017 Pankaj Asudani. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

@objc class User: NSObject {
    
    //MARK: Properties
    let name: String
    let email: String
    let id: String
    var profilePic: UIImage
    
    //MARK: Methods
    class func registerUser(withName: String, email: String, password: String, device_token : String, completion: @escaping (Bool, String, String) -> Swift.Void) {
        
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
            
            print("user = \(String(describing: user))")
            print("error = \(String(describing: error))")
            
            if error == nil {
                
                let values = ["name": withName, "email": email, "device_token" : device_token]
            Database.database().reference().child("users").child((user?.uid)!).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
                    
                    if errr == nil {
                        completion(true,(user?.uid)!,"")
                    }
                    else {
                        
                        var error_code = ""
                        if let error = errr as NSError? {
                            if let errorCode = AuthErrorCode.init(rawValue: error.code) {
                                switch errorCode {
                                case .emailAlreadyInUse:
                                    error_code = "emailAlreadyInUse"
                                    
                                default :
                                    error_code = "default"
                                }
                            }
                        }
                        
                        completion(false, error_code, error!.localizedDescription)
                    }
                })
            }
            else {
                print("error1")
                
                var error_code = ""
                if let error = error as NSError? {
                    if let errorCode = AuthErrorCode.init(rawValue: error.code) {
                        switch errorCode {
                        case .emailAlreadyInUse:
                            error_code = "emailAlreadyInUse"
                            
                        default :
                            error_code = "default"
                        }
                    }
                }
                
                completion(false, error_code, error!.localizedDescription)
            }
        })
    }
    
    class func loginUser(withEmail: String, password: String, completion: @escaping (Bool, String) -> Swift.Void) {
        
        Auth.auth().signIn(withEmail: withEmail, password: password, completion: { (user, error) in
            
            print("user = \(String(describing: user))")
            print("error = \(String(describing: error))")
            
            if error == nil {
                completion(true, "")
            } else {
                
                var error_code = ""
                if let error = error as NSError? {
                    if let errorCode = AuthErrorCode.init(rawValue: error.code) {
                        switch errorCode {
                        case .userNotFound:
                            error_code = "userNotFound"
                            
                        default :
                            error_code = "default"
                        }
                    }
                }
                
                completion(false, error_code)
            }
        })
    }
    
    class func setUserStatusOnline(completion: @escaping (Bool) -> Swift.Void) {
        
        let values = ["status": "online"]
        
        if let currentUserID = Auth.auth().currentUser?.uid {
        Database.database().reference().child("users").child(currentUserID).child("userStatus").updateChildValues(values, withCompletionBlock: { (errr, _) in
                if errr == nil {
                    completion(true)
                }
                else {
                    completion(false)
                }
            })
        }
    }
    
    class func setUserStatusOffline(completion: @escaping (Bool) -> Swift.Void) {
        
        let values = ["status": "offline"]
        
        let dbRef = Database.database().reference()
        
        if let currentUserID = Auth.auth().currentUser?.uid {
            
            dbRef.child("users").child(currentUserID).child("userStatus").updateChildValues(values, withCompletionBlock: { (errr, _) in
                
                if errr == nil {
                    completion(true)
                }
                else {
                    completion(false)
                }
            })
        }
    }
    
    class func checkUserStatus(firebaseID: String, completion: @escaping (String) -> Swift.Void) {
        
        Database.database().reference().child("users").child(firebaseID).child("userStatus").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                let data = snapshot.value as! [String: String]
                let userStatus = data["status"]!
                completion(userStatus)
            }
            else
            {
                completion("offline")
            }
        })
    }
    
    class func logOutUser(completion: @escaping (Bool) -> Swift.Void) {
        do {
            
            //try FIRAuth.auth()?.signOut()
            try Auth.auth().signOut()
            UserDefaults.standard.removeObject(forKey: "userInformation")
            completion(true)
        } catch _ {
            completion(false)
        }
    }
    
    class func info(forUserID: String, completion: @escaping (User) -> Swift.Void) {
    Database.database().reference().child("users").child(forUserID).child("credentials").observeSingleEvent(of: .value, with: { (snapshot) in
            if let data = snapshot.value as? [String: String] {
                let name = data["name"]!
                let email = data["email"]!
                
                //let link = URL.init(string: data["profilePicLink"]!)
                //URLSession.shared.dataTask(with: link!, completionHandler: { (data, response, error) in
                //    if error == nil {
                //        let profilePic = UIImage.init(data: data!)
                        //let user = User.init(name: name, email: email, id: forUserID, profilePic: profilePic!)
                
                let profilePic = UIImage.init(named: "group20")
                let user = User.init(name: name, email: email, id: forUserID, profilePic: profilePic!)
                        completion(user)
                //    }
                //}).resume()
            }
        })
    }
    
    class func downloadAllUsers(exceptID: String, completion: @escaping (User) -> Swift.Void) {
        
        Database.database().reference().child("users").observe(.childAdded, with: { (snapshot) in
            
            let id = snapshot.key
            let data = snapshot.value as! [String: Any]
            let credentials = data["credentials"] as! [String: String]
            
            if id != exceptID {
                let name = credentials["name"]!
                let email = credentials["email"]!
                
                let link = URL.init(string: credentials["profilePicLink"]!)
                URLSession.shared.dataTask(with: link!, completionHandler: { (data, response, error) in
                    if error == nil {
                        let profilePic = UIImage.init(data: data!)
                        let user = User.init(name: name, email: email, id: id, profilePic: profilePic!)
                        completion(user)
                    }
                }).resume()
            }
        })
    }
    
    class func checkUserVerification(completion: @escaping (Bool) -> Swift.Void) {
        
        Auth.auth().currentUser?.reload(completion: { (_) in
            let status = (Auth.auth().currentUser?.isEmailVerified)!
            completion(status)
        })
    }
    
    //MARK: Inits
    init(name: String, email: String, id: String, profilePic: UIImage) {
        self.name = name
        self.email = email
        self.id = id
        self.profilePic = profilePic
    }
    
    class func updateUser(withName: String, withEmail : String, withId : String, completion: @escaping (Bool) -> Swift.Void) {
        
        let values = ["name": withName, "email" : withEmail]
    Database.database().reference().child("users").child(withId).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
            
            if errr == nil {
                completion(true)
            } else {
                print("update name error = \(String(describing: errr))")
                completion(false)
            }
        })
    }
    
    class func updateUserPassword(withPassword: String, withId : String, completion: @escaping (Bool) -> Swift.Void) {
        
        let url = Auth.auth().currentUser?.photoURL
        print("url = \(String(describing: url))")
        
        let values = ["password": withPassword, "email" : "provider@firebase.com"]
        Database.database().reference().child("users").child(withId).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
            
            if errr == nil {
                completion(true)
            } else {
                print("update name error = \(String(describing: errr))")
                completion(false)
            }
        })
    }
    
    class func updateUserInformation(withName : String, withEmail : String, withId : String, device_token : String,  completion: @escaping (Bool) -> Swift.Void) {

        var withname1 = withName
        if withname1.contains(".") {
            withname1 = withname1.replacingOccurrences(of: ".", with: "")
        }
        
        let values = ["name": withname1, "email": withEmail, "device_token" : device_token]
    Database.database().reference().child("users").child(withId).child("credentials").updateChildValues(values, withCompletionBlock: { (errr, _) in
            
            if errr == nil {
                completion(true)
            } else {
                print("error = \(String(describing: errr))")
                completion(false)
            }
        })
    }
    
    class func getDeviceToken (forUserID: String, completion: @escaping (String) -> Swift.Void) {
    Database.database().reference().child("users").child(forUserID).child("credentials").observeSingleEvent(of: .value, with: { (snapshot) in
            if let data = snapshot.value as? [String: String] {
                let device_token = data["device_token"]!
                completion(device_token)
            }
        })
    }
}
