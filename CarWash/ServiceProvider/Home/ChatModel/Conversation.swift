//
//  Conversation.swift
//  TeenJobs
//
//  Created by Pankaj Asudani on 14/07/17.
//  Copyright © 2017 Pankaj Asudani. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

@objc class Conversation: NSObject {
    
    //MARK: Properties
    let user: User
    var lastMessage: Message
    var messagesArr : [Message]
    
    //MARK: Methods
    class func showConversations(completion: @escaping ([Conversation]) -> Swift.Void) {
        
        if let currentUserID = Auth.auth().currentUser?.uid {
            
            var existValue = 0
       
            var conversations = [Conversation]()
        Database.database().reference().child("users").child(currentUserID).child("conversations").observe(.childAdded, with: { (snapshot) in
                
                existValue = 1
                print("snapshot = \(snapshot)")
                            
                if snapshot.exists() {
                                
                    print("exist")
                            
                    let fromID = snapshot.key
                    let values = snapshot.value as! [String: String]
                    let location = values["location"]!
                                
                    User.info(forUserID: fromID, completion: { (user) in
                        
                        let emptyMessage = Message.init(type: .text, content: "loading", owner: .sender, timestamp: 0, isRead: false)
                        
                        let messageArrEmpty = [Message]()
                        
                        let conversation = Conversation.init(user: user, lastMessage: emptyMessage, messagesArr: messageArrEmpty)
                        
                        conversations.append(conversation)
                   
                        Message.downloadAllMessages(forUserID: fromID, completion: { (messages) in
                            conversation.messagesArr.append(messages)
                        })
                    conversation.lastMessage.downloadLastMessage(forLocation: location, completion: { (_) in
                        
                        conversations.sort(by: { (conversion1, conversion2) -> Bool in
                            conversion1.lastMessage.timestamp > conversion2.lastMessage.timestamp
                        })
                            completion(conversations)
                        })
                    })
                }
                else
                {
                    let conversations = [Conversation]()
                    completion(conversations)
                }
            })
            
            let when = DispatchTime.now() + 10 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when)
            {
                if existValue == 0 {
                    let conversations = [Conversation]()
                    completion(conversations)
                }
            }
        }
        else
        {
            let conversations = [Conversation]()
            completion(conversations)
        }
    }
    
    //MARK: Methods
    class func showProviderUnReadCount(from_fcm_user_id from_user_id : String, completion: @escaping (String) -> Swift.Void) {
        
        if let currentUserID = Auth.auth().currentUser?.uid {
            
            var existValue = 0
            
            var conversations = [Conversation]()
            Database.database().reference().child("users").child(currentUserID).child("conversations").observe(.childAdded, with: { (snapshot) in
                
                existValue = 1
                //print("snapshot = \(snapshot)")
                
                if snapshot.exists() {
                    
                    //print("exist")
                    
                    let fromID = snapshot.key
                    //let values = snapshot.value as! [String: String]
                    //_ = values["location"]!
                    
                    if from_user_id == fromID
                    {
                        User.info(forUserID: fromID, completion: { (user) in
                            
                            let emptyMessage = Message.init(type: .text, content: "loading", owner: .sender, timestamp: 0, isRead: false)
                            
                            let messageArrEmpty = [Message]()
                            
                            let conversation = Conversation.init(user: user, lastMessage: emptyMessage, messagesArr: messageArrEmpty)
                            
                            conversations.append(conversation)
                            
                            Message.downloadAllMessages(forUserID: fromID, completion: { (messages) in
                                conversation.messagesArr.append(messages)
                                
                                var unReadCount = 0
                                let messagesArr = conversation.messagesArr
                                
                                for i in 0..<messagesArr.count {
                                    
                                    let message = messagesArr[i]
                                    
                                    if message.owner == .sender {
                                        
                                        let unRead = message.isRead
                                        if unRead == false {
                                            unReadCount += 1
                                        }
                                    }
                                }
                                
                                //print("unreadcount = \(unReadCount)")
                                
                                if unReadCount == 0 {
                                    completion("0")
                                } else {
                                    completion("\(unReadCount)")
                                }
                            })
                        })
                    }
                }
                else
                {
                    completion("0")
                }
            })
            
            let when = DispatchTime.now() + 10 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when)
            {
                if existValue == 0 {
                    completion("0")
                }
            }
        }
        else
        {
            completion("0")
        }
    }
    
    //MARK: Inits
    init(user: User, lastMessage: Message, messagesArr : [Message]) {
        self.user = user
        self.lastMessage = lastMessage
        self.messagesArr = messagesArr
    }
}
