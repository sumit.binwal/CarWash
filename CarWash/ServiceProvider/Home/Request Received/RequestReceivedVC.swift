//
//  RequestReceivedVC.swift
//  CarWash
//
//  Created by iOS on 05/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class RequestReceivedVC: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var tblRequest : UITableView?
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var serviceDict : NSDictionary?

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setUpNavigationBar()
        
        print("service dict = \(String(describing: serviceDict))")
        
        tblRequest?.register(UINib(nibName: "RequestCell", bundle: nil), forCellReuseIdentifier: "RequestCell")
        
        btnAccept.setTitle(NSLocalizedString("ACCEPT_TEXT", comment: ""), for: UIControlState.normal)
        btnCancel.setTitle(NSLocalizedString("CANCEL_TEXT", comment: ""), for: UIControlState.normal)
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
    }
    
    func setUpNavigationBar()
    {
        //UIApplication.shared.isStatusBarHidden = false
        
        self.title = NSLocalizedString("REQUEST_RECEIVED", comment: "")
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        //self.title = "Select Services"
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - IBAction Methods
    
    @IBAction func accept()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cancel()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - UITableView Delegate Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  1 //notificationAry.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return  SCREEN_WIDTH * 0.21
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestCell") as! RequestCell?
        
        cell?.btnCall?.addTarget(self, action: #selector(callUser), for: .touchUpInside)
        cell?.btnMail?.addTarget(self, action: #selector(mailUser), for: .touchUpInside)
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        return cell!;
    }
    
    // MARK: - Other Methods
    
    func callUser(sender : UIButton)
    {
        let point = tblRequest?.convert(CGPoint.zero, from: sender)
        let indexPath = tblRequest?.indexPathForRow(at: point!)
        
        print("indexPath", indexPath ?? "")
        CommonFunctions.callNumber(phoneNumber: "7178881234")
    }
    
    func mailUser(sender : UIButton)
    {
        
    }
}
