//
//  AddSubServices.swift
//  CarWash
//
//  Created by iOS on 30/11/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit

class AddSubServices: UIViewController, UITextFieldDelegate, UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource
{
    var subService  : NSMutableDictionary = [:]
    
    var serviceDic : NSDictionary? = nil
    
    var minAry  : NSMutableArray = []
    var hourAry  : NSMutableArray = []
    
    @IBOutlet var txtTitle_en    : UITextField?
    @IBOutlet var txtTitle_ar    : UITextField?
    @IBOutlet var txtCurrent     : UITextField?
    
    @IBOutlet var txtDuration_en : UITextField?
    @IBOutlet var txtDuration_ar : UITextField?
    @IBOutlet var txtPrice_en    : UITextField?
    @IBOutlet var txtPrice_ar    : UITextField?
    
    @IBOutlet var txvDesc_en     : UITextView?
    @IBOutlet var txvDesc_ar     : UITextView?
    
    @IBOutlet var btnSave        : UIButton?
    
    @IBOutlet var durationPicker         : UIPickerView?
    
    var strDesc_en : String = "Sub - Service Description"
    var strDesc_ar : String = "الفرعية - وصف الخدمة"
    
    var serviceTypeTitle : String = ""
    
    var currency_code : String = ""
    
    var savePressed: ((_ dict : NSMutableDictionary) -> Void)? = nil
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        //print("service dict = \(String(describing: serviceDic))")
        
        let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
        let dict = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
        //print("dict = \(dict)")
        
        let countryCodeCA = dict.object(forKey: "iso_code")
        let localeIdCA = NSLocale.localeIdentifier(fromComponents: [ NSLocale.Key.countryCode.rawValue : countryCodeCA as! String])
        let localeCA = NSLocale(localeIdentifier: localeIdCA)
        currency_code = localeCA.object(forKey: NSLocale.Key.currencyCode) as! String
        
        if serviceDic != nil
        {
            txtTitle_en?.text = serviceDic?.object(forKey: "title_en") as? String
            txtTitle_ar?.text = serviceDic?.object(forKey: "title_ar") as? String
            
            txvDesc_ar?.text = serviceDic?.object(forKey: "desc_ar") as! String
            txvDesc_en?.text = serviceDic?.object(forKey: "desc_en") as! String
            
            txtPrice_ar?.text = serviceDic?.object(forKey: "price_ar") as? String
            txtPrice_en?.text = serviceDic?.object(forKey: "price_en") as? String
            
            txtDuration_en?.text = serviceDic?.object(forKey: "duration_en") as? String
            txtDuration_ar?.text = serviceDic?.object(forKey: "duration_ar") as? String
            
            txvDesc_ar?.textColor = UIColor.uicolorFromRGB(88, 88, 88)
            txvDesc_en?.textColor = UIColor.uicolorFromRGB(88, 88, 88)
        }
        else
        {
            txvDesc_ar?.textColor = UIColor.uicolorFromRGB(199, 199, 205)
            txvDesc_en?.textColor = UIColor.uicolorFromRGB(199, 199, 205)
        }
        
        setUpNavigationBar()
        
        btnSave?.layer.cornerRadius = 4
        btnSave?.layer.masksToBounds = true
        
        btnSave?.setTitle(NSLocalizedString("SAVE_TEXT", comment: ""), for: UIControlState.normal)
        
        txtDuration_en?.inputView = durationPicker
        txtDuration_ar?.inputView = durationPicker
        
        var min : String?
        for i in sequence(first: 00, next: { $0 + 5 <= 55 ? $0 + 5 : nil })
        {
            if i == 0 || i == 5 {
                if i == 0 {
                    min = "00"
                } else {
                    min = "05"
                }
            } else {
                min = String(format: "%d", i)
            }
            
            minAry.add(min ?? "")
        }
    
        for i in 0 ... 11
        {
            min = String(format: "%d", i)
            hourAry.add(min ?? "")
        }
        
        //////////////////////////////////////////////////
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector (doneAction), name: NSNotification.Name(rawValue: "doneAction"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setUpNavigationBar()
    {
        if serviceDic != nil
        {
            if serviceTypeTitle == "Sub" {
                self.title = NSLocalizedString("EDIT_SUB_SERVICES", comment: "")
            } else {
                self.title = NSLocalizedString("EDIT_ADDITIONAL_SERVICES", comment: "")
            }
        }
        else
        {
            if serviceTypeTitle == "Sub" {
                self.title = NSLocalizedString("ADD_SUB_SERVICES", comment: "")
            } else {
                self.title = NSLocalizedString("ADD_ADDITIONAL_SERVICES", comment: "")
            }
        }
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    //MARK: Other Methods
    
    @IBAction func save()
    {
        if (validateUserInput())
        {
            subService.setObject(txtTitle_en?.text ?? "", forKey: "title_en" as NSCopying)
            subService.setObject(txtTitle_ar?.text ?? "", forKey: "title_ar" as NSCopying)
            subService.setObject(txvDesc_en?.text ?? "", forKey: "desc_en" as NSCopying)
            subService.setObject(txvDesc_ar?.text  ?? "", forKey: "desc_ar" as NSCopying)
            subService.setObject(txtDuration_en?.text  ?? "", forKey: "duration_en" as NSCopying)
            subService.setObject(txtDuration_ar?.text ?? "", forKey: "duration_ar" as NSCopying)
            subService.setObject(txtPrice_en?.text ?? "", forKey: "price_en" as NSCopying)
            subService.setObject(txtPrice_ar?.text ?? "", forKey: "price_ar" as NSCopying)
            
            if (savePressed != nil)
            {
                savePressed!(subService)
            }
            
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK: UITextField Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        txtCurrent = textField
        
        if textField == txtPrice_ar
        {
            if #available(iOS 10.0, *)
            {
                textField.keyboardType = UIKeyboardType.asciiCapableNumberPad
            }
            else
            {
                textField.keyboardType = UIKeyboardType.numberPad
            }
        }
        else if textField == txtPrice_en
        {
            if #available(iOS 10.0, *)
            {
                textField.keyboardType = UIKeyboardType.asciiCapableNumberPad
            }
            else
            {
                textField.keyboardType = UIKeyboardType.numberPad
            }
        }
        
        if (textField == txtDuration_en!)
        {
            if (!(textField.text?.isEmpty)!)
            {
                let ary = txtDuration_en?.text?.components(separatedBy: ":")
                var str1 : String = ary![0]
                
                durationPicker?.selectRow(hourAry.index(of: str1), inComponent: 0, animated: false)
                
                str1 = ary![1]
                durationPicker?.selectRow(minAry.index(of: str1), inComponent: 1, animated: false)
            }
            else
            {
                durationPicker?.selectRow(0, inComponent: 0, animated: false)
                durationPicker?.selectRow(0, inComponent: 1, animated: false)
            }
        }
        else if(textField == txtDuration_ar)
        {
            if (!(textField.text?.isEmpty)!)
            {
                let ary = txtDuration_ar?.text?.components(separatedBy: ":")
                var str1 : String = ary![0]
                
                durationPicker?.selectRow(hourAry.index(of: str1), inComponent: 0, animated: false)
                
                str1 = ary![1]
                durationPicker?.selectRow(minAry.index(of: str1), inComponent: 1, animated: false)
            }
            else
            {
                durationPicker?.selectRow(0, inComponent: 0, animated: false)
                durationPicker?.selectRow(0, inComponent: 1, animated: false)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        //print("string = \(string)")
        //print("textfield text = \(textField.text ?? "")")
        
        if (textField == txtPrice_en || textField == txtPrice_ar)
        {
            //if !string.canBeConverted(to: String.Encoding.ascii)
            //{
            //    return false
            //}
            
            if textField.textInputMode?.primaryLanguage == "emoji" || !((textField.textInputMode?.primaryLanguage) != nil) {
                return false
            }
            
            if (textField.text?.hasPrefix(currency_code))! {
                var text = textField.text! as NSString
                text = text.replacingOccurrences(of: currency_code, with: "") as NSString
                
                if (string.count > 0) && (text.length + string.count) > 3
                {
                    return false
                }
            } else {
                if (string.count > 0) && ((textField.text?.count)! + string.count) > 3
                {
                    return false
                }
            }

            if !((textField.text?.hasPrefix(currency_code))!)
            {
                //print("text = \(textField.text ?? "")")
                
                if string.count > 0 {
                    let str1 = textField.text
                    if str1?.count == currency_code.count {
                        let mutableString: String = "\(currency_code)\((textField.text)!)"
                        
                        textField.text = mutableString
                    } else {
                        let mutableString: String = "\(currency_code)"
                        
                        textField.text = mutableString
                    }
                }
            } else {
                if string.count == 0 {
                    if textField.text?.count == currency_code.count {
                        textField.text = ""
                    }
                }
            }
        }
        
        if (textField == txtTitle_en || textField == txtTitle_ar)
        {
            if (string.count > 0) && ((textField.text?.count)! + string.count) > 40
            {
                return false
            }
        }

        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if (textField == txtTitle_en)
        {
            subService.setObject(txtTitle_en?.text ?? "", forKey: "title_en" as NSCopying)
        }
        else if (textField == txtTitle_ar)
        {
            subService.setObject(txtTitle_ar?.text ?? "", forKey: "title_ar" as NSCopying)
        }
        else if (textField == txtDuration_en)
        {
            subService.setObject(txtDuration_en?.text ?? "", forKey: "duration_en" as NSCopying)
        }
        else if (textField == txtDuration_ar)
        {
            subService.setObject(txtDuration_ar?.text ?? "", forKey: "duration_ar" as NSCopying)
        }
        else if (textField == txtPrice_en)
        {
            if textField.text == currency_code {
                txtPrice_en?.text = ""
                txtPrice_ar?.text = ""
            } else {
                txtPrice_en?.text = textField.text
                txtPrice_ar?.text = txtPrice_en?.text
                
                subService.setObject(txtPrice_en?.text ?? "", forKey: "price_en" as NSCopying)
                subService.setObject(txtPrice_ar?.text ?? "", forKey: "price_ar" as NSCopying)
            }
        }
        else if (textField == txtPrice_ar)
        {
            if textField.text == currency_code {
                txtPrice_en?.text = ""
                txtPrice_ar?.text = ""
            } else {
                txtPrice_ar?.text = textField.text
                txtPrice_en?.text = txtPrice_ar?.text
                
                subService.setObject(txtPrice_en?.text ?? "", forKey: "price_en" as NSCopying)
                subService.setObject(txtPrice_ar?.text ?? "", forKey: "price_ar" as NSCopying)
            }
        }
    }
    
    // MARK: uitextview delegates
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        textView.textColor = UIColor.uicolorFromRGB(88, 88, 88)
        
        if (textView == txvDesc_en && textView.text == strDesc_en)
        {
            textView.text = ""
        }
        else if (textView.text == strDesc_ar && textView == txvDesc_ar)
        {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if (textView == txvDesc_en)
        {
            if (textView.text == strDesc_en || textView.text.isEmpty)
            {
                txvDesc_en?.textColor = UIColor.uicolorFromRGB(199, 199, 205)
                textView.text = strDesc_en
            }
            else
            {
                subService.setObject(txvDesc_en?.text ?? "", forKey: "desc_en" as NSCopying)
            }
        }
        else if (textView == txvDesc_ar)
        {
            if (textView.text == strDesc_ar || textView.text.isEmpty)
            {
                txvDesc_ar?.textColor = UIColor.uicolorFromRGB(199, 199, 205)
                textView.text = strDesc_ar
            }
            else
            {
                subService.setObject(txvDesc_ar?.text ?? "", forKey: "desc_ar" as NSCopying)
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if (textView == txvDesc_en || textView == txvDesc_ar)
        {
            if (textView.text.count > 150)
            {
                //return false
                
                if text == "" {
                    return true
                } else {
                    return false
                }
            }
        }
        
        return true
    }
    
    //MARK: KeyboardManager done action
    
    func doneAction()
    {
        if (txtCurrent == txtDuration_ar || txtCurrent == txtDuration_en)
        {
            if (txtCurrent?.text?.isEmpty)!
            {
                var strMin  : String = ""
                var strHour : String = ""
                
                strHour = (hourAry[0] as? String)!
                strMin  = (minAry[0] as? String)!
                
                let str : String = String(format: "%@:%@", strHour, strMin)
                txtCurrent?.text = str
                
                txtDuration_ar?.text = txtCurrent?.text
                txtDuration_en?.text = txtCurrent?.text
            }
            else
            {
                txtDuration_ar?.text = txtCurrent?.text
                txtDuration_en?.text = txtCurrent?.text
            }
        }
    }
    
    //MARK: Validate Method
    
    func validateUserInput() -> Bool
    {
        var isError : Bool = true
        
        if(txtTitle_en?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_TITLE_EN", comment: ""), dismissBloack:
                {
                    self.txtTitle_en?.becomeFirstResponder()
            })
        }
        else if((txvDesc_en?.text?.isEmpty)! || txvDesc_en?.text == strDesc_en)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_DESC_EN", comment: ""), dismissBloack:
                {
                    self.txvDesc_en?.becomeFirstResponder()
            })
        }
        else if (txtDuration_en?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_DURATION", comment: ""), dismissBloack:
                {
                    self.txtDuration_en?.becomeFirstResponder()
            })
        }
        else if(txtPrice_en?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_PRICE", comment: ""), dismissBloack:
                {
                    self.txtPrice_en?.becomeFirstResponder()
            })
        }
        else if(txtTitle_ar?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_TITLE_AR", comment: ""), dismissBloack:
                {
                    self.txtTitle_ar?.becomeFirstResponder()
            })
        }
        else if((txvDesc_ar?.text?.isEmpty)! || txvDesc_ar?.text == strDesc_ar)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_DESC_AR", comment: ""), dismissBloack:
                {
                    self.txvDesc_ar?.becomeFirstResponder()
            })
        }
        else if (txtDuration_ar?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_DURATION", comment: ""), dismissBloack:
                {
                    self.txtDuration_ar?.becomeFirstResponder()
            })
        }
        else if(txtPrice_ar?.text?.isEmpty)!
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("EMPTY_PRICE", comment: ""), dismissBloack:
                {
                    self.txtPrice_ar?.becomeFirstResponder()
            })
        }
        else if (txtDuration_ar?.text != txtDuration_en?.text)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("DIFFERENT_DURATION", comment: "") , dismissBloack:
                {
                    
            })
        }
        else if (txtPrice_en?.text != txtPrice_ar?.text)
        {
            isError = false
            AlertViewController.showAlertWith(title: "", message: NSLocalizedString("DIFFERENT_PRICE", comment: ""), dismissBloack:
                {
                    
            })
        }
        
        return isError
    }
    
    // MARK: -  PickerView Delegates
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 2
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return component == 0 ? hourAry.count : minAry.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return  component == 0  ? hourAry[row] as? String :  minAry[row] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        var strMin  : String = ""
        var strHour : String = ""
        
        strHour = (hourAry[pickerView.selectedRow(inComponent: 0)] as? String)!
        strMin  = (minAry[pickerView.selectedRow(inComponent: 1)] as? String)!
        
        let str : String = String(format: "%@:%@", strHour, strMin)
        txtCurrent?.text = str
    }
}
