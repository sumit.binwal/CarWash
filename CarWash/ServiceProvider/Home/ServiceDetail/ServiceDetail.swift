//
//  ServiceDetail.swift
//  CarWash
//
//  Created by iOS on 23/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import RSKImageCropper
import QuartzCore

class ServiceDetail: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, RSKImageCropViewControllerDelegate
{
    @IBOutlet var tblDetail : UITableView?
    
    let imagePicker = UIImagePickerController()
    var imageCropVC : RSKImageCropViewController!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        imagePicker.delegate = self
        setUpNavigationBar()
        
        tblDetail?.register(UINib(nibName: "ProfileDetailCell", bundle: nil), forCellReuseIdentifier: "ProfileDetailCell")
        tblDetail?.register(UINib(nibName: "SubServiceList", bundle: nil), forCellReuseIdentifier: "SubServiceList")
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = true
        APPDELEGATE.customView?.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
    }
    
    func setUpNavigationBar()
    {
        self.title = NSLocalizedString("SERVICE_DETAIL", comment: "")
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        //self.title = "Select Services"
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: LanguageManager.currentLanguageIndex()==0 ? "back":"backRight"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image:  UIImage(named: "edit"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(editService))
    }
    
    func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func editService()
    {
        
    }
    
    // MARK: - tableview delegates
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
       return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  4 //notificationAry.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return  indexPath.row == 0 ? SCREEN_WIDTH * 1.44 : SCREEN_WIDTH * 1.17066
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileDetailCell") as! ProfileDetailCell?
            
            cell?.btnType?.setTitle(NSLocalizedString("CAR_TYPE", comment: ""), for: UIControlState.normal)
            
            cell?.btnType?.addTarget(self, action: #selector(selectCarType), for: UIControlEvents.touchUpInside)
            cell?.btnCamera?.addTarget(self, action: #selector(changePic), for: UIControlEvents.touchUpInside)
            
            cell?.btnType?.layer.cornerRadius = 5.0
            cell?.btnType?.layer.masksToBounds = true
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            return cell!;
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SubServiceList") as! SubServiceList?
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            return cell!;
        }
    }
    
    // UIImagePicker Delegates
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let editedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            picker.dismiss(animated: false, completion: { () -> Void in
                
                self.imageCropVC = nil
                
                self.imageCropVC = RSKImageCropViewController(image: editedImage, cropMode: RSKImageCropMode.circle)
                
                self.imageCropVC.delegate = self
                
                self.navigationController?.pushViewController(self.imageCropVC, animated: true)
                
            })
        }
    }
    
    // MARK: Other Methods
    
    func selectCarType()
    {
        let typeObj = TypeVC()
        
        typeObj.okPressed =
            { (str : NSString) -> Void in
                
                let cell : ProfileDetailCell = self.tblDetail?.cellForRow(at: IndexPath(row: 0, section: 0)) as! ProfileDetailCell
                cell.btnType?.setTitle(str as String, for: .normal)
        }
        self.navigationController?.pushViewController(typeObj, animated: true)
    }
    
    func changePic()
    {
        RESIGN_KEYBOARD
        
        let optionMenu = UIAlertController(title: nil, message: NSLocalizedString("CHOOSE_IMAGE", comment: ""), preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: NSLocalizedString("CAMERA_TEXT", comment: ""), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let galleryAction = UIAlertAction(title: NSLocalizedString("PHOTO_GALLARY", comment: ""), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
            {
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("CANCEL_TEXT", comment: ""), style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    // RSKImageCropViewController Delegates
    
    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect)
    {
        let indexpath : NSIndexPath = NSIndexPath(row: 1, section: 0) as NSIndexPath
        let cell : ProfileDetailCell = tblDetail?.cellForRow(at: indexpath as IndexPath) as! ProfileDetailCell
        
        cell.imgService?.layer.cornerRadius = (0.2933 * SCREEN_WIDTH)/2  //97/375
        cell.imgService?.layer.masksToBounds = true
        cell.imgService?.image = croppedImage
        
        cell.imgService?.layer.borderColor = CommonFunctions.imageBorderColor().cgColor
        cell.imgService?.layer.borderWidth = 1.0
        
        self.navigationController?.popViewController(animated: true)
    }
}
