//
//  ServicesModel.swift
//  CarWash
//
//  Created by Ratina on 6/11/18.
//  Copyright © 2018 Neha Choudhary. All rights reserved.
//

import Foundation

class ServicesModel
{
    var id : String = ""
    var inc_id = ""
    
    var service_name_ar : String = ""
    var service_name_en : String = ""
    
    var car_type = ""
    
    var description_ar = ""
    var description_en = ""
    
    var sort_description_en = ""
    var sort_description_ar = ""
    
    var duration = ""
    var price = ""
    
    var requirements : [String] = []
    var additional_services : [[String : Any]] = []
    
    init()
    {
        id = ""
        inc_id = ""
        
        service_name_ar = ""
        service_name_en = ""
        
        car_type = ""
        
        description_ar = ""
        description_en = ""
        
        sort_description_ar = ""
        sort_description_en = ""
        
        duration = ""
        price = ""
        
        requirements = []
        additional_services = []
    }
    
    deinit
    {
        print("Services Model deinit")
    }
    
    func updateModel(usingDictionary dictionary:[String:Any]) -> Void
    {
        if let cateID = dictionary["id"] as? String
        {
            id = cateID
        }
        
        if let cateID = dictionary["inc_id"] as? String
        {
            inc_id = cateID
        }
        
        if let service_name_arr = dictionary["service_name"] as? [String : Any]
        {
            service_name_ar = service_name_arr["ar"] as! String
        }
        
        if let service_name_arr = dictionary["service_name"] as? [String : Any]
        {
            service_name_en = service_name_arr["en"] as! String
        }
        
        if let description_arr = dictionary["description"] as? [String : Any]
        {
            description_en = description_arr["en"] as! String
        }
        
        if let description_arr = dictionary["description"] as? [String : Any]
        {
            description_ar = description_arr["ar"] as! String
        }
        
        if let sort_description_arr = dictionary["sort_description"] as? [String : Any]
        {
            sort_description_en = sort_description_arr["en"] as! String
        }
        
        if let sort_description_arr = dictionary["sort_description"] as? [String : Any]
        {
            sort_description_ar = sort_description_arr["ar"] as! String
        }
        
        if let additional_services1 = dictionary["additional_services"] as? [[String : Any]]
        {
            additional_services = additional_services1
        }
        
        if let car_type1 = dictionary["car_type"] as? String
        {
            if (car_type1.isEqual("small"))
            {
                car_type = NSLocalizedString("SMALL_CARS", comment: "")
            }
            else if (car_type1.isEqual("medium"))
            {
                car_type = NSLocalizedString("MEDIUM_CARS", comment: "")
            }
            else if (car_type1.isEqual("big"))
            {
                car_type = NSLocalizedString("BIG_CARS", comment: "")
            }
        }
        
        if let duration1 = dictionary["duration"] as? Int
        {
            let duration_hr = duration1 / 60
            let duration_min = duration1 % 60
            
            if duration_min == 0
            {
                duration = "\(duration_hr):00"
            }
            else if duration_min == 5
            {
                duration = "\(duration_hr):05"
            }
            else
            {
                duration = "\(duration_hr):\(duration_min)"
            }
        }
        
        if let price1 = dictionary["price"] as? Int
        {
            price = String(price1)
        }
        
        if let requirements1 = dictionary["requirements"] as? [String]
        {
            requirements = requirements1
        }
    }
}

//(
//    {
//        //"additional_services" =             (
//            {
//                "desc_ar" = "Gdgk;ldfh";
//                "desc_en" = "Lsjflk skflsdg";
//                price = 5;
//                "title_ar" = "additional ar";
//                "title_en" = additional;
//            }
//        );
//        //"car_type" = medium;
//        createdAt = "2018-06-11T12:51:44.551Z";
//        //description =             {
//            ar = Fgjfj;
//            en = Hfhgf;
//        };
//        //duration = 10;
//        //id = 5b1e70605dbeb2be56dbec63;
//        //"inc_id" = 1003;
//        isDeleted = 0;
//        isactive = 1;
//        notes =             (
//        ""
//        );
//        //price = 10;
//        requirements =             (
//        water,
//        electricity
//        );
//        //"service_name" =             {
//            ar = hkjhsf;
//            en = "package name eng";
//        };
//        //"sort_description" =             {
//            ar = Fdgdfh;
//            en = "Hksjhfkjds gdshgksj\thfkdhgs";
//        };
//        updatedAt = "2018-06-11T12:51:44.551Z";
//        "user_id" = 5ad9d255757011592a5f4788;
//    }
//);

