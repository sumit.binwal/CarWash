//
//  SelectServicesVC.swift
//  CarWash
//
//  Created by iOS on 05/10/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

import UIKit
import MBProgressHUD

class SelectServicesVC: UIViewController
{
    @IBOutlet var tblServices : UITableView?
    
    @IBOutlet weak var noServicesLabelEnglish: UILabel!
    @IBOutlet weak var servicesAddBtn: UIButton!
    @IBOutlet weak var noServicesLabelArabic: UILabel!
    
    @IBOutlet weak var heightConstarint: NSLayoutConstraint!
    let kHeaderSectionTag: Int = 6900;
    
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: NSMutableArray = []
    var sectionNames: NSMutableArray = []
    
    var servicesData = [ServicesModel]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //setUpNavigationBar()
        
        tblServices?.register(UINib(nibName: "SubServiceCell", bundle: nil), forCellReuseIdentifier: "SubServiceCell")
        tblServices?.register(UINib(nibName: "ServiceHeader", bundle: nil), forCellReuseIdentifier: "ServiceHeader")
        
        noServicesLabelEnglish.isHidden = true
        noServicesLabelArabic.isHidden = true
        servicesAddBtn.isHidden = true
        tblServices?.isHidden = true
        
        setUpLabels()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        setUpNavigationBar()
        
        APPDELEGATE.tabBarController?.tabBar.isHidden = false
        APPDELEGATE.customView?.isHidden = false
        
        servicesData.removeAll()
        getListApi()
    }
    
    func setUpLabels()
    {
        let attributedString = NSMutableAttributedString(string: "You don't have any services added in your account. Would you like to add a package.")
        
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 15)!,
                NSForegroundColorAttributeName: UIColor.uicolorFromRGB(88, 88, 88)
            ], range: NSRange(location: 0, length: attributedString.length-14)
        )
        
        attributedString.addAttributes(
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 15)!,
                NSForegroundColorAttributeName: UIColor.uicolorFromRGB(2, 166, 242)
            ], range: NSRange(location: attributedString.length-14, length: 14)
        )
        
        noServicesLabelEnglish?.attributedText = attributedString
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpNavigationBar()
    {
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.title = NSLocalizedString("SELECT_SERVICES", comment: "")
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSFontAttributeName: UIFont(name: FONT_SEMIBOLD, size: 17.5)!,
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.uicolorFromRGB(88.0, 88.0, 88.0)]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        //add shadow on navigation bar
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.darkGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.2
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
    }
    
    // MARK: @IBAction Methods
    
    @IBAction func addNew()
    {
        let services = AddServiceViewController()
        self.navigationController?.pushViewController(services, animated: true)
    }
    
    //MARK: API Call
    
    func getListApi()
    {
        MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
        
        let url = BASE_URL + "service/servicelist"
        
        AppWebHandler.sharedInstance().fetchData(fromURL: URL.init(string: url), httpMethod: .get, parameters: nil) { [weak self] (data, dictionary, statusCode, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            guard self != nil else {return}
            
            if error != nil
            {
                AlertViewController.showAlertWith(title: "", message: (error?.localizedDescription)!, dismissBloack:
                    {
                        
                })
                
                return
            }
            
            if dictionary == nil
            {
                return
            }
            
            if statusCode == 200
            {
                let str = dictionary!["status"] as? String
                
                if str == "success"
                {
                    let servicesDtaArr1 = dictionary!["data"] as? [[String : Any]]
                
                    guard servicesDtaArr1 != nil else
                    {
                        return
                    }
                
                    self?.updateServicesModelArray(usingArray: servicesDtaArr1!)
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    AlertViewController.showAlertWith(title: "", message: replyMsg, dismissBloack:
                        {
                            
                    })
                }
            }
        }
    }
    
    //MARK:- Updating review Model Array
    func updateServicesModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ServicesModel () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            servicesData.append(model) // Adding model to array
        }
        
        //reviewTableView.reloadData()
        
        if self.servicesData.count > 0
        {
            self.tblServices?.reloadData()
            self.tblServices?.isHidden = false
    
            self.noServicesLabelArabic.isHidden = true
            self.noServicesLabelEnglish.isHidden = true
            self.servicesAddBtn.isHidden = true
        }
        else
        {
            self.tblServices?.isHidden = true
            
            self.noServicesLabelArabic.isHidden = false
            self.noServicesLabelEnglish.isHidden = false
            self.servicesAddBtn.isHidden = false
        }
    }
    
    @IBAction func servicAddBtnPressed(_ sender: Any)
    {
        let services = AddServiceViewController() //AddServiceVC()
        self.navigationController?.pushViewController(services, animated: true)
    }
}

extension SelectServicesVC : UITableViewDelegate, UITableViewDataSource
{
    // MARK: - tableview delegates
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if servicesData.count > 0
        {
            return servicesData.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (self.expandedSectionHeaderNumber == section)
        {
            if servicesData[section].additional_services.count > 0
            {
                return servicesData[section].additional_services.count
            }
            
            return 0
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 75 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 81 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        //recast your view as a UITableViewHeaderFooterView
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        
        header.textLabel?.textColor = UIColor.white
        
        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section)
        {
            viewWithTag.removeFromSuperview()
        }
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "ServiceHeader") as! ServiceHeader?
        
        if cell == nil
        {
            cell = ServiceHeader(style: UITableViewCellStyle.default, reuseIdentifier: "ServiceHeader")
        }
        
        cell?.contentView.backgroundColor = UIColor.white
        cell?.contentView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: (cell?.contentView.frame.size.height)! * scaleFactorX)
        
        if language == "en"
        {
            cell?.lblTitle?.text = servicesData[section].service_name_en
        }
        else
        {
            cell?.lblTitle?.text = servicesData[section].service_name_ar
        }
        
        header.addSubview((cell?.contentView)!)
        
        if (self.expandedSectionHeaderNumber == section)
        {
            let headerFrame = self.view.frame.size
            var theImageView = UIImageView()
            
            if(LanguageManager.currentLanguageIndex() == 0)
            {
                theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 30, y: ((IS_IPHONE_6P ? SCREEN_WIDTH * 0.19 : SCREEN_WIDTH * 0.208)/2)-3.5, width: 11, height: 7));
            }
            else
            {
                theImageView = UIImageView(frame: CGRect(x:15, y: ((IS_IPHONE_6P ? SCREEN_WIDTH * 0.19 : SCREEN_WIDTH * 0.208)/2)-3.5, width: 11, height: 7));
            }
            
            theImageView.image = UIImage(named: "closeDown")
            theImageView.tag = kHeaderSectionTag + section
            
            header.addSubview(theImageView)
        }
        else
        {
            let headerFrame = self.view.frame.size
            var theImageView = UIImageView()
            
            if(LanguageManager.currentLanguageIndex() == 0)
            {
                theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 30, y: ((IS_IPHONE_6P ? SCREEN_WIDTH * 0.19 : SCREEN_WIDTH * 0.208)/2)-5, width: 6, height: 10));
                theImageView.image = UIImage(named: "tableArrow")
            }
            else
            {
                theImageView = UIImageView(frame: CGRect(x: 15, y: ((IS_IPHONE_6P ? SCREEN_WIDTH * 0.19 : SCREEN_WIDTH * 0.208)/2)-5, width: 6, height: 10));
                theImageView.image = UIImage(named: "tableArrowRight")
            }
            
            theImageView.tag = kHeaderSectionTag + section
            header.addSubview(theImageView)
        }
        
        // make headers touchable
        header.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(self.sectionHeaderWasTouched(_:)))
        header.addGestureRecognizer(headerTapGesture)
        
        cell?.btnEdit?.tag = section;
        cell?.btnEdit?.addTarget(self, action: #selector(editBtnClicked(sender:)), for: .touchUpInside)
        
        cell?.btnDelete?.tag = section;
        cell?.btnDelete?.addTarget(self, action: #selector(deleteBtnClicked(sender:)), for: .touchUpInside)
    }
    
    func deleteBtnClicked(sender : UIButton)
    {
        AlertViewController.showAlertWith(title: "", message: CommonFunctions.getLocalizedString(localizedName: "Are you sure to delete this Servive?"), buttonsArray: [CommonFunctions.getLocalizedString(localizedName: "YES_TEXT"), CommonFunctions.getLocalizedString(localizedName: "NO_TEXT")]) { (buttonIndex) in
            
            //print("button = ", buttonIndex)
            
            if buttonIndex == 0
            {
                MBProgressHUD.showAdded(to: APPDELEGATE.window!, animated: true)
                self.deleteServiceApi(service_id: self.servicesData[sender.tag].id)
            }
        }
    }
    
    func editBtnClicked(sender : UIButton)
    {
        let services = AddServiceViewController()
        
        services.editServiceData = servicesData[sender.tag]
        services.isEdit = true
        
        self.navigationController?.pushViewController(services, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubServiceCell") as! SubServiceCell?
        
        let dic = servicesData[indexPath.section].additional_services[indexPath.row]
        
        if (language == "en")
        {
            cell?.lblTitle?.text = dic["title_en"] as? String
            cell?.lblSubTitle?.text = dic["desc_en"] as? String
        }
        else
        {
            cell?.lblTitle?.text = dic["title_ar"] as? String
            cell?.lblSubTitle?.text = dic["desc_ar"] as? String
        }
        
        return cell!
    }
    
    // MARK: - Expand / Collapse Methods
    
    func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer)
    {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView
        
        if (self.expandedSectionHeaderNumber == -1)
        {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section, imageView: eImageView!)
        }
        else
        {
            if (self.expandedSectionHeaderNumber == section)
            {
                tableViewCollapeSection(section, imageView: eImageView!)
            }
            else
            {
                let cImageView = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as? UIImageView
                tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!)
                tableViewExpandSection(section, imageView: eImageView!)
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView)
    {
        let sectionData = self.servicesData[section].additional_services
        
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0)
        {
            return;
        }
        else
        {
            imageView.frame =  CGRect(x: imageView.frame.origin.x, y: ((IS_IPHONE_6P ? SCREEN_WIDTH * 0.19 : SCREEN_WIDTH * 0.208)/2)-5, width: 6, height: 10)
            
            if(LanguageManager.currentLanguageIndex()==0)
            {
                imageView.image = UIImage(named: "tableArrow")
            }
            else
            {
                imageView.image = UIImage(named: "tableArrowRight")
            }
            
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count
            {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            
            tblServices!.beginUpdates()
            tblServices!.deleteRows(at: indexesPath, with: UITableViewRowAnimation.top)
            tblServices!.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView)
    {
        let sectionData = self.servicesData[section].additional_services
        
        if (sectionData.count == 0)
        {
            self.expandedSectionHeaderNumber = -1;
            return;
        }
        else
        {
            imageView.frame =  CGRect(x: imageView.frame.origin.x, y: ((IS_IPHONE_6P ? SCREEN_WIDTH * 0.19 : SCREEN_WIDTH * 0.208)/2)-3.5, width: 11, height: 7)
            imageView.image = UIImage(named: "closeDown")
            
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count
            {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            
            self.expandedSectionHeaderNumber = section
            
            tblServices!.beginUpdates()
            tblServices!.insertRows(at: indexesPath, with: UITableViewRowAnimation.top)
            tblServices!.endUpdates()
        }
    }
    
    // delete service api call
    
    func deleteServiceApi(service_id : String)
    {
        let dic : [String : Any] = [
            "service_id" : service_id
        ]
        
        print("dict = ", dic)
        
        let url = BASE_URL + "service/deleteservice"
        
        AppWebHandler.sharedInstance().fetchData(fromURL: URL.init(string: url), httpMethod: .post, parameters: dic) { [weak self] (data, dictionary, statusCode, error) in
            
            MBProgressHUD.hide(for: APPDELEGATE.window!, animated: true)
            
            guard self != nil else {return}
            
            if error != nil
            {
                AlertViewController.showAlertWith(title: "", message: (error?.localizedDescription)!, dismissBloack:
                    {
                        
                })
                
                return
            }
            
            if dictionary == nil
            {
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    AlertViewController.showAlertWith(title: "", message: (responseDictionary["msg"] as? String)!, dismissBloack:
                        {
                            self?.servicesData.removeAll()
                            self?.getListApi()
                    })
                }
                else
                {
                    AlertViewController.showAlertWith(title: "", message: (responseDictionary["msg"] as? String)!, dismissBloack:
                        {
                            
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    AlertViewController.showAlertWith(title: "", message: replyMsg, dismissBloack:
                        {
                            
                    })
                }
            }
        }
    }
}

//self.sectionItems.removeAllObjects()
//
//self.sectionNames = ((data?.object(forKey: "data")as! NSArray).mutableCopy() as! NSMutableArray)
//
//var dict : NSMutableDictionary = [:]
//var ary  : NSMutableArray = []
//var aryAdditional  : NSMutableArray = []
//
//for i in 0 ..< self.sectionNames.count
//{
//    dict = (self.sectionNames.object(at: i ) as! NSDictionary).mutableCopy() as! NSMutableDictionary
//print("dict = \(dict)")
//
//    let decoded  = USERDEFAULT.object(forKey: "USER_DATA") as! Data
//    let dict1 = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! NSDictionary
//    //print("dict = \(dict)")
//
//    let countryCodeCA = dict1.object(forKey: "iso_code")
//    let localeIdCA = NSLocale.localeIdentifier(fromComponents: [ NSLocale.Key.countryCode.rawValue : countryCodeCA as! String])
//    let localeCA = NSLocale(localeIdentifier: localeIdCA)
//    let currency_code = localeCA.object(forKey: NSLocale.Key.currencyCode) as! String
//
//    ary = ((dict.object(forKey: "subservices")) as! NSArray).mutableCopy() as! NSMutableArray
//
//    let arr1 = dict.allKeys as NSArray
//    if arr1.contains("additional_services")
//    {
//        aryAdditional = ((dict.object(forKey: "additional_services")) as! NSArray).mutableCopy() as! NSMutableArray
//
//        for i in 0..<aryAdditional.count
//        {
//            let dict1 : NSMutableDictionary = (aryAdditional.object(at: i) as! NSDictionary).mutableCopy() as! NSMutableDictionary
//
//            let strPrice = dict1.object(forKey: "price_ar") as AnyObject
//
//            let mutableString: String = "\(currency_code)\(strPrice)"
//
//            dict1.setValue(mutableString, forKey: "price_ar")
//
//            let price_en = dict1.object(forKey: "price_en") as AnyObject
//
//            let mutableString1: String = "\(currency_code)\(price_en)"
//
//            dict1.setValue(mutableString1, forKey: "price_en")
//
//            aryAdditional.replaceObject(at: i, with: dict1)
//        }
//
//        dict.setValue(aryAdditional, forKey: "additional_services")
//    }
//
//    for i in 0..<ary.count
//    {
//        let dict1 : NSMutableDictionary = (ary.object(at: i) as! NSDictionary).mutableCopy() as! NSMutableDictionary
//
//        let strPrice = dict1.object(forKey: "price_ar") as AnyObject
//
//        let mutableString: String = "\(currency_code)\(strPrice)"
//
//        dict1.setValue(mutableString, forKey: "price_ar")
//
//        let price_en = dict1.object(forKey: "price_en") as AnyObject
//
//        let mutableString1: String = "\(currency_code)\(price_en)"
//
//        dict1.setValue(mutableString1, forKey: "price_en")
//
//        ary.replaceObject(at: i, with: dict1)
//    }
//
//    self.sectionItems.add(ary)
//
//    dict.setValue(ary, forKey: "subservices")
//
//    self.sectionNames.replaceObject(at: i, with: dict)
//}
//
//DispatchQueue.main.async
//    {
//        if self.sectionNames.count > 0 {
//
//            self.tblServices?.reloadData()
//            self.tblServices?.isHidden = false
//
//            self.noServicesLabelArabic.isHidden = true
//            self.noServicesLabelEnglish.isHidden = true
//            self.servicesAddBtn.isHidden = true
//
//        } else {
//
//            self.tblServices?.isHidden = true
//
//            self.noServicesLabelArabic.isHidden = false
//            self.noServicesLabelEnglish.isHidden = false
//            self.servicesAddBtn.isHidden = false
//        }
//}
