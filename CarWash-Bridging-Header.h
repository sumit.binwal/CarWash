//
//  CarWash-Bridging-Header.h
//  CarWash
//
//  Created by iOS on 03/11/17.
//  Copyright © 2017 Neha Choudhary. All rights reserved.
//

#ifndef CarWash_Bridging_Header_h
#define CarWash_Bridging_Header_h

#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>

#import "UIImage-Extension.h"

#import "LanguageManager.h"
#import "NSBundle+Language.h"

#endif /* CarWash_Bridging_Header_h */
